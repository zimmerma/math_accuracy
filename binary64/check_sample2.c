/* Search worst cases of a bivariate function, using a recursive algorithm.

   This program is open-source software distributed under the terms 
   of the GNU General Public License <http://www.fsf.org/copyleft/gpl.html>.

   Compile with:

   gcc -DFOO=atan2 -DUSE_xxx -O3 check_sample2.c -lmpfr -lgmp -lm -fopenmp
   icc -DFOO=atan2 -Qoption,cpp,--extended_float_types -no-ftz -DUSE_xxx -O3 check_sample2.c -lmpfr -lgmp -fopenmp

   where xxx is FLOAT, DOUBLE, LDOUBLE, or FLOAT128.

   For NEWLIB: add -DNEWLIB (to avoid compilation error with __errno).

   You can add -DWORST to use some precomputed values to guide the search.

   You can add -DGLIBC to print the GNU libc release (with -v).

   Command-line options:
   -threshold nnn : set the effort threshold to nnn, the runtime is roughly
                    proportional to nnn
   -seed nnn   sets the random seed to nnn
   -mode k     sets the mode of the heuristic search to k (0 <= k <= 2).
               k=0: considers the maximal error in the range
               k=1: considers the average error in the range
               k=2: considers the estimated maximal error
   -v          verbose
   -rndn       rounding to nearest (default)
   -rndz       rounding towards zero
   -rndu       rounding towards +Inf
   -rndd       rounding towards -Inf
   -nthreads n uses n threads
   -worst xxx,yyy uses xxx,yyy (hexadecimal values) as worst values
   -xmin  xxx  use xxx a minimal value for x
   -xmax  xxx  use xxx a maximal value for x
   -ymin  yyy  use yyy a minimal value for y
   -ymax  yyy  use yyy a maximal value for y

   References and credit:
   * https://www.vinc17.net/research/testlibm/: worst-cases computed by
     Vincent Lefèvre.
   * the idea to sample several intervals instead of only one is due to
     Eric Schneider
*/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE /* to define f128 functions */
#endif

#define RANK /* print the maximal list-rank of best values */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <limits.h>
#ifndef NO_FLOAT128
#define MPFR_WANT_FLOAT128
#endif
/* icx does not have _Float128 */
#ifdef __INTEL_CLANG_COMPILER
#define _Float128 __float128
#endif
#include <mpfr.h>
#ifndef ICC
#include <math.h>
#else
#include <mathimf.h>
#endif
#include <assert.h>
#include <sys/types.h>
#include <unistd.h>
#ifndef NO_OPENMP
#include <omp.h>
#endif
#include <float.h> /* for DBL_MAX */
#include <fenv.h>
#ifdef CRLIBM
#include "crlibm.h"
#endif
#ifdef _MSC_VER
#include <BaseTsd.h>
typedef SSIZE_T ssize_t;
#endif

/* define GLIBC to print the GNU libc version */
#ifdef GLIBC
#include <gnu/libc-version.h>
#endif

#if defined(__aarch64__) && defined(__ARM_FEATURE_RNG)
  #define ARM_USE_HARDWARE_RNG
  #include <arm_acle.h>
#endif

#define MAX_THREADS 192

#ifndef atan2pif
extern float atan2pif (float, float);
#endif
#ifndef atan2pi
extern double atan2pi (double, double);
#endif
#ifndef atan2pil
extern long double atan2pil (long double, long double);
#endif

#ifndef NO_FLOAT128
#ifndef atan2pif128
extern _Float128 atan2pif128 (_Float128, _Float128);
#endif
#endif

/* rounding modes */
int rnd1[] = { FE_TONEAREST, FE_TOWARDZERO, FE_UPWARD, FE_DOWNWARD };
mpfr_rnd_t rnd2[] = { MPFR_RNDN, MPFR_RNDZ, MPFR_RNDU, MPFR_RNDD };
mpfr_rnd_t rnd = MPFR_RNDN; /* default rounding mode */
int verbose = 0;

/* mode (0,1,2), if -1 set according to omp_get_thread_num() */
int use_mode = -1;

FILE *extra_file = NULL;

#ifdef NEWLIB
/* without this, newlib says: undefined reference to `__errno' */
int errno;
int* __errno () { return &errno; }
#endif

#define CAT1(X,Y) X ## Y
#define CAT2(X,Y) CAT1(X,Y)
#ifndef MPFR_FOO
#define MPFR_FOO CAT2(mpfr_,FOO)
#endif
#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)
#define NAME TOSTRING(FOO)

#ifdef USE_FLOAT
#if defined(USE_DOUBLE) || defined(USE_LDOUBLE) || defined(USE_FLOAT128)
#error "only one of USE_FLOAT, USE_DOUBLE, USE_LDOUBLE or USE_FLOAT128 can be defined"
#endif
#define TYPE float
#define UTYPE uint32_t
#define FOO2 CAT2(FOO,f)
#define EMAX 128
#define EMIN -149
#define PREC 24
#define mpfr_set_type mpfr_set_flt
#define mpfr_get_type mpfr_get_flt
#define TYPE_MAX FLT_MAX
#endif

#ifdef USE_DOUBLE
#if defined(USE_FLOAT) || defined(USE_LDOUBLE) || defined(USE_FLOAT128)
#error "only one of USE_FLOAT, USE_DOUBLE, USE_LDOUBLE or USE_FLOAT128 can be defined"
#endif
#define TYPE double
#define UTYPE uint64_t
#define FOO2 FOO
#define EMAX 1024
#define EMIN -1074
#define PREC 53
#define mpfr_set_type mpfr_set_d
#define mpfr_get_type mpfr_get_d
#define TYPE_MAX DBL_MAX
#endif

#ifdef USE_LDOUBLE
#if defined(USE_FLOAT) || defined(USE_DOUBLE) || defined(USE_FLOAT128)
#error "only one of USE_FLOAT, USE_DOUBLE, USE_LDOUBLE or USE_FLOAT128 can be defined"
#endif
#define FOO2 CAT2(FOO,l)
#define TYPE long double
#define UTYPE __uint128_t
#define EMAX 16384
#define EMIN -16445
#define PREC 64
#define mpfr_set_type mpfr_set_ld
#define mpfr_get_type mpfr_get_ld
#define TYPE_MAX LDBL_MAX
#endif

#ifdef USE_FLOAT128
#if defined(USE_FLOAT) || defined(USE_DOUBLE)
#error "only one of USE_FLOAT, USE_DOUBLE or USE_FLOAT128 can be defined"
#endif
#if defined(__INTEL_COMPILER) || defined(__INTEL_CLANG_COMPILER)
#define TYPE _Quad
#define FOO3 CAT2(FOO,q)
#define FOO2 CAT2(__,FOO3)
extern _Quad FOO2 (_Quad, _Quad);
#define Q(x) (x ## q)
#else
#define TYPE _Float128
#define FOO2 CAT2(FOO,f128)
#define Q(x) (x ## f128)
#endif
#define TYPE_MAX Q(0xf.fffffffffffffffffffffffffff8p+16380)
#define UTYPE __uint128_t
#define EMAX 16384
#define EMIN -16494
#define PREC 113
#define mpfr_set_type mpfr_set_float128
#define mpfr_get_type mpfr_get_float128
#endif

static void
print_type_hex (TYPE x)
{
#ifdef USE_FLOAT
  printf ("%a", x);
#endif
#ifdef USE_DOUBLE
  printf ("%a", x);
#endif
#ifdef USE_LDOUBLE
  mpfr_t y;
  mpfr_init2 (y, PREC);
  mpfr_set_type (y, x, MPFR_RNDN);
  mpfr_printf ("%Ral", y);
  mpfr_clear (y);
#endif
#ifdef USE_FLOAT128
  mpfr_t y;
  mpfr_init2 (y, PREC);
  mpfr_set_type (y, x, MPFR_RNDN);
  mpfr_printf ("%Ra", y);
  mpfr_clear (y);
#endif
}

typedef union { UTYPE n; TYPE x; } union_t;

TYPE
get_type (UTYPE n)
{
  union_t u;
  u.n = n;
  return u.x;
}

UTYPE
get_utype (TYPE x)
{
  union_t u;
  u.x = x;
  return u.n;
}

#ifdef USE_LDOUBLE
static int
is_valid (TYPE x)
{
  UTYPE n = get_utype (x);
  int e = (n >> 64) & 0x7fff; /* exponent */
  uint64_t s = (uint64_t) n;  /* significand */
  if (e == 0) return (s >> 63) == 0;
  else return (n >> 63) & 1;
}
#endif

#ifdef LIBMVEC
/* the libmvec results were obtained with GNU libc revision
   49e2bf58d57758df244eb621d63cedd2ab6d1971 */
/* LIBMVEC should be 128 (sse4.2, default), or 256 (avx2) or 512 (avx512f) */
#if !(LIBMVEC == 128 || LIBMVEC == 256 || LIBMVEC == 512)
#error "LIBMVEC should be 128 (sse4.2) or 256 (avx2) or 512 (avx512f)"
#else
#define	LIBMVEC_N (LIBMVEC/sizeof(TYPE)/CHAR_BIT)
#endif
#endif

/* return FOO2(x,y) */
static TYPE
WRAPPER (TYPE x, TYPE y)
{
#ifdef LIBMVEC
  TYPE xx[LIBMVEC_N] = {x,}, yy[LIBMVEC_N] = {y,}, zz[LIBMVEC_N];
  for (int i = 0; i < LIBMVEC_N; i++)
    zz[i] = FOO2 (xx[i], yy[i]);
  return zz[0];
#else
  return FOO2 (x, y);
#endif
}

/* return the distance between FOO2(x,y) and the value computed by MPFR
   with precision 3*PREC, or if CHECK_CR is defined, with the value computed
   by MPFR with precision PREC */
static double
distance (TYPE x, TYPE y)
{
  mpfr_t xx, yy, zz;
  TYPE z;
  double ret = 0;
  int underflow = 0;
  mpfr_exp_t expz;

  if (isnan (x) || isnan (y))
    return 0;

#ifdef USE_LDOUBLE
  if (!is_valid (x) || !is_valid (y))
    return 0;
#endif

  mpfr_exp_t emax = mpfr_get_emax ();
  mpfr_set_emax (EMAX);
  z = WRAPPER (x, y);
  mpfr_init2 (xx, PREC);
  mpfr_init2 (yy, PREC+1);
#ifndef CHECK_CR
  mpfr_init2 (zz, 3*PREC);
#else
  /* To check correct rounding, we need to compute zz with the same precision,
     to avoid double rounding issues. */
  mpfr_init2 (zz, PREC);
#endif
  mpfr_set_type (xx, x, MPFR_RNDN);
  mpfr_set_type (yy, y, MPFR_RNDN);
  MPFR_FOO (zz, xx, yy, rnd);
  if (isnan (z) && mpfr_nan_p (zz))
    goto end;
  if (isnan (z) || mpfr_nan_p (zz))
  {
    ret = 0x1p1023 + 0x1p1023;
    goto end;
  }
  mpfr_set_emax (mpfr_get_emax_max ());
  if (isinf (z) && mpfr_inf_p (zz) == 0)
    {
      /* libm returns Inf, MPFR not, we assume the libm rounded to the next
         number after the largest finite number in the format */
      mpfr_set_ui_2exp (yy, 1, EMAX, MPFR_RNDN);
      if (z < 0)
        mpfr_neg (yy, yy, MPFR_RNDN);
    }
  else if (!isinf (z) && mpfr_inf_p (zz))
  {
    /* MPFR yields Inf, but libm no: we recompute the MPFR result with
       unbounded exponent */
    MPFR_FOO (zz, xx, yy, rnd);
    expz = mpfr_get_exp (zz);
    underflow = expz <= EMIN;
    mpfr_set_type (yy, z, MPFR_RNDN);
  }
  else
    mpfr_set_type (yy, z, MPFR_RNDN);

  if (mpfr_number_p (zz))
  {
    if (mpfr_zero_p (zz))
      underflow = 1;
    else
    {
      expz = mpfr_get_exp (zz);
      underflow = expz <= EMIN;
    }
  }
  mpfr_sub (zz, zz, yy, MPFR_RNDN);
  mpfr_abs (zz, zz, MPFR_RNDN);
  /* we divide by the ulp of the correctly rounded value (zz)
     which is 2^(expz-PREC) if zz is normalized, and 2^EMIN otherwise */
  if (mpfr_number_p (zz) && !underflow && expz - PREC >= EMIN)
    mpfr_mul_2si (zz, zz, PREC - expz, MPFR_RNDN);
  else
    mpfr_mul_2si (zz, zz, -EMIN, MPFR_RNDN);
  ret = mpfr_get_d (zz, MPFR_RNDU);
 end:
  mpfr_clear (xx);
  mpfr_clear (yy);
  mpfr_clear (zz);
  mpfr_set_emax (emax);
  return ret;
}

uint64_t threshold = 1000000;
double Threshold;

static unsigned int Seed[128];

uint32_t my_rand_wrapper (int tid)
{
  uint32_t val = 0;

#ifdef ARM_USE_HARDWARE_RNG
  /* Utilize hardware RNG instructions on the ARM processors that support them.
   *
   * During profiling, it was discovered that almost all of the processing time
   * was spent servicing the rand calls due to continual exhaustion of the
   * available system entropy. Using hardware RNG resolved this issue.
   *
   * TODO: The rndr instruction returns a 64 bit unsigned integer, of which we
   *       throw out 32 bits in order to be compatible with the existing code
   *       as it was written with rand() in mind, which returns a 32 bit
   *       integer. Ideally, we would refactor the code so that we don't throw
   *       out the bits just to request another 32 bits to build a full 64 bit
   *       value when USE_DOUBLE is defined. */
  uint64_t temp = 0;
  int res = __rndr(&temp);
  assert(res == 0);
  val = (uint32_t)temp;
#else
  val = rand_r (Seed + tid);
#endif

  return val;
}

#if RAND_MAX == 2147483647
#ifdef USE_FLOAT
static uint32_t
my_random (uint32_t n, int tid)
{
  uint32_t ret = my_rand_wrapper (tid);
  if (n > RAND_MAX)
    ret = (ret << 31) | my_rand_wrapper (tid);
  return ret % n;
}
#else /* USE_FLOAT */
#ifdef USE_DOUBLE
static uint64_t
my_random (uint64_t n, int tid)
{
  uint64_t ret = my_rand_wrapper (tid);
  if (n > RAND_MAX)
    {
      ret = (ret << 31) | my_rand_wrapper (tid);
      /* now ret <= 2^62-1 */
      if (n >> 62)
        ret = (ret << 31) | my_rand_wrapper (tid);
    }
  return ret % n;
}
#else /* USE_LDOUBLE or USE_FLOAT128 */
static __uint128_t
my_random (__uint128_t n, int tid)
{
  __uint128_t ret = my_rand_wrapper (tid);
  if (n > RAND_MAX)
    {
      ret = (ret << 31) | my_rand_wrapper (tid);
      /* now ret <= 2^62-1 */
      if (n >> 62)
        {
          ret = (ret << 31) | my_rand_wrapper (tid);
          /* now ret <= 2^93-1 */
          if (n >> 93)
            {
              ret = (ret << 31) | my_rand_wrapper (tid);
              /* now ret <= 2^93-1 */
              if (n >> 124)
                ret = (ret << 31) | my_rand_wrapper (tid);
            }
        }
    }
  return ret % n;
}
#endif /* USE_DOUBLE */
#endif /* USE_FLOAT */

#elif RAND_MAX == 0x7fff /* Microsoft Visual C/C++ */

#ifdef USE_FLOAT
static uint32_t
my_random (uint32_t n, int tid)
{
  uint32_t ret = my_rand_wrapper (tid);
  if (n > RAND_MAX)
    {
      ret = (ret << 15) | my_rand_wrapper (tid);
      /* now ret <= 2^30-1 */
      if (n >> 30)
        ret = (ret << 15) | my_rand_wrapper (tid);
    }
  return ret % n;
}
#endif
#ifdef USE_DOUBLE
static uint64_t
my_random (uint64_t n, int tid)
{
  uint64_t ret = my_rand_wrapper (tid);
  if (n > RAND_MAX)
    {
      ret = (ret << 15) | my_rand_wrapper (tid);
      /* now ret <= 2^30-1 */
      if (n >> 30)
        {
          ret = (ret << 15) | my_rand_wrapper (tid);
          /* now ret <= 2^45-1 */
          if (n >> 45)
            {
              ret = (ret << 15) | my_rand_wrapper (tid);
              /* now ret <= 2^60-1 */
              if (n >> 60)
                ret = (ret << 15) | my_rand_wrapper (tid);
            }
        }
    }
  return ret % n;
}
#endif
#if defined(USE_LDOUBLE) || defined(USE_FLOAT128)
#error "my_random() not yet implemented for 128-bit type"
#endif

#else
#error "Unexpected value of RAND_MAX"
#endif

TYPE Xbest, Ybest; // best values found so far
TYPE Xmin, Xmax, Ymin, Ymax;
double Dbest = 0.0;
int Rbest = -1;
int mode_best = 0;

/* return the maximal error on [nxmin,nxmax] x [nymin,nymax],
   and update nxbest,nybest if it improves distance(x,y) */
static double
max_heuristic1 (UTYPE nxmin, UTYPE nxmax, UTYPE nymin, UTYPE nymax,
                UTYPE *nxbest, UTYPE *nybest, int tid)
{
  TYPE x, y;
  UTYPE i, nx, ny;
  double dbest, dmax, d;
  x = get_type (*nxbest);
  y = get_type (*nybest);
  dbest = distance (x, y);
  dmax = 0;
  for (i = 0; i < threshold; i++)
    {
      nx = nxmin + my_random (nxmax - nxmin, tid);
      ny = nymin + my_random (nymax - nymin, tid);
#ifdef USE_LDOUBLE
      /* for long double, bit 63 must be set */
      nx |= (UTYPE) 1 << 63;
      ny |= (UTYPE) 1 << 63;
#endif
      x = get_type (nx);
      y = get_type (ny);
      d = distance (x, y);
      if (d > dmax)
        {
          dmax = d;
          if (d > dbest)
            {
              dbest = d;
              *nxbest = nx;
              *nybest = ny;
            }
        }
    }
  return dmax;
}

/* return the average error on [nxmin,nxmax] x [nymin,nymax],
   given (nxbest,nybest) */
static double
max_heuristic2 (UTYPE nxmin, UTYPE nxmax, UTYPE nymin, UTYPE nymax,
               UTYPE *nxbest, UTYPE *nybest, int tid)
{
  TYPE x, y;
  UTYPE i, nx, ny;
  double dbest, d, s = 0, n = 0;
  x = get_type (*nxbest);
  y = get_type (*nybest);
  dbest = distance (x, y);
  for (i = 0; i < threshold; i++)
    {
      nx = nxmin + my_random (nxmax - nxmin, tid);
      ny = nymin + my_random (nymax - nymin, tid);
#ifdef USE_LDOUBLE
      /* for long double, bit 63 must be set */
      nx |= (UTYPE) 1 << 63;
      ny |= (UTYPE) 1 << 63;
#endif
      x = get_type (nx);
      y = get_type (ny);
      d = distance (x, y);
      if (d != 0)
        {
          s += d;
          n ++;
        }
      if (d > dbest)
        {
          dbest = d;
          *nxbest = nx;
          *nybest = ny;
        }
    }
  if (n != 0)
    s = s / n;
  return s;
}

/* some libraries do not have log(), for example llvm */
static double mylog (double n)
{
  mpfr_t x;
  mpfr_init2 (x, 53);
  mpfr_set_d (x, n, MPFR_RNDN);
  mpfr_log (x, x, MPFR_RNDN);
  double ret = mpfr_get_d (x, MPFR_RNDN);
  mpfr_clear (x);
  return ret;
}

/* return the estimated maximal error on [nxmin,nxmax] x [nymin,nymax],
   taking into account mean and standard deviation,
   and update (nxbest,nybest) */
static double
max_heuristic3 (UTYPE nxmin, UTYPE nxmax, UTYPE nymin, UTYPE nymax,
               UTYPE *nxbest, UTYPE *nybest, int tid)
{
  TYPE x, y;
  UTYPE i, nx, ny;
  double dbest, d, s = 0, v = 0, n = 0;
  x = get_type (*nxbest);
  y = get_type (*nybest);
  dbest = distance (x, y);
  for (i = 0; i < threshold; i++)
    {
      nx = nxmin + my_random (nxmax - nxmin, tid);
      ny = nymin + my_random (nymax - nymin, tid);
#ifdef USE_LDOUBLE
      /* for long double, bit 63 must be set */
      nx |= (UTYPE) 1 << 63;
      ny |= (UTYPE) 1 << 63;
#endif
      x = get_type (nx);
      y = get_type (ny);
      d = distance (x, y);
      if (d != 0)
        {
          s += d;
          v += d * d;
          n ++;
        }
      if (d > dbest)
        {
          dbest = d;
          *nxbest = nx;
          *nybest = ny;
        }
    }
  /* compute mean and standard deviation */
  if (n != 0)
    {
      s = s / n;
      v = v / n - s * s;
      if (v < 0)
        v = 0;
      double sigma = sqrt (v);
      /* we got n non-zero values out of threshold, thus we should get
         n/threshold*(nxmax-nxmin)*(nymin-nymax) */
      n = n * (double) (nxmax - nxmin) * (double) (nymax - nymin)
        / (double) threshold;
      double logn = mylog (n);
      double t = sqrt (2.0 * logn);
      /* Reference: A note on the first moment of extreme order statistics
         from the normal distribution, Max Petzold,
         https://gupea.ub.gu.se/handle/2077/3092 */
      s = s + sigma * (t - (mylog (logn) + 1.3766) / (2.0 * t));
    }
  return s;
}

#ifdef NO_OPENMP
static int omp_get_thread_num (void) { return 0; }
static int omp_get_num_threads (void) { return 1; }
static void omp_set_num_threads (int i) { }
#endif

static int
mode (void)
{
  if (use_mode != -1)
    return use_mode;
  int i = omp_get_thread_num ();
  /* Modes 1,2 seem to be less efficient, thus we use only 1 thread on each. */
  if (i == 1 || i == 2)
    return i;
  return 0;
}

#ifdef STAT
unsigned long eval_heuristic = 0;
unsigned long eval_exhaustive = 0;
#endif

static double
max_heuristic (UTYPE nxmin, UTYPE nxmax, UTYPE nymin, UTYPE nymax,
               UTYPE *nxbest, UTYPE *nybest, int tid)
{
  if (nxmin == nxmax || nymin == nymax)
    return 0;

  int k = mode ();

#ifdef STAT
#pragma omp atomic update
  eval_heuristic += threshold;
#endif

  if (k == 0)
    return max_heuristic1 (nxmin, nxmax, nymin, nymax, nxbest, nybest, tid);
  else if (k == 1)
    return max_heuristic2 (nxmin, nxmax, nymin, nymax, nxbest, nybest, tid);
  else
    return max_heuristic3 (nxmin, nxmax, nymin, nymax, nxbest, nybest, tid);
}

typedef struct {
  UTYPE nxmin, nxmax;
  UTYPE nymin, nymax;
  double d;
#ifdef RANK
  int rank; /* worst rank */
#endif
} chunk_t;

static double
chunk_size (chunk_t c)
{
  return (double) (c.nxmax - c.nxmin) * (double) (c.nymax - c.nymin);
}

static void
chunk_swap (chunk_t *a, chunk_t *b)
{
  UTYPE tmp;
  tmp = a->nxmin; a->nxmin = b->nxmin; b->nxmin = tmp;
  tmp = a->nxmax; a->nxmax = b->nxmax; b->nxmax = tmp;
  tmp = a->nymin; a->nymin = b->nymin; b->nymin = tmp;
  tmp = a->nymax; a->nymax = b->nymax; b->nymax = tmp;
  double ump;
  ump = a->d; a->d = b->d; b->d = ump;
#ifdef RANK
  int rmp;
  rmp = a->rank; a->rank = b->rank; b->rank = rmp;
#endif
}

typedef struct {
  chunk_t *l;
  int size;
} List_struct;
typedef List_struct List_t[1];

/* Idea from Eric Schneider: instead of sampling only one interval at each
   level of the binary splitting tree, we sample up to LIST_ALLOC intervals,
   and keep the most promising ones. We use the default value suggested by
   Eric Schneider (20). */
#define LIST_ALLOC 20

static void
List_init (List_t L)
{
  L->l = (chunk_t*) malloc (LIST_ALLOC * sizeof (chunk_t));
  L->size = 0;
}

static void
List_init2 (List_t L, UTYPE nxmin, UTYPE nxmax, UTYPE nymin, UTYPE nymax)
{
  L->l = (chunk_t*) malloc (LIST_ALLOC * sizeof (chunk_t));
  L->l[0].nxmin = nxmin;
  L->l[0].nxmax = nxmax;
  L->l[0].nymin = nymin;
  L->l[0].nymax = nymax;
  L->size = 1;
}

#if 0
static void
List_print (List_t L)
{
  for (int i = 0; i < L->size; i++)
    printf ("%.3g ", L->l[i].d);
  printf ("\n");
}

static void
List_check (List_t L)
{
  for (int i = 1; i < L->size; i++)
    if (L->l[i-1].d < L->l[i].d)
      {
        fprintf (stderr, "Error, list not sorted:\n");
        List_print (L);
        exit (1);
      }
}
#endif

static void
List_insert (List_t L, UTYPE nxmin, UTYPE nxmax, UTYPE nymin, UTYPE nymax,
             double d)
{
  if (nxmin == nxmax || nymin == nymax)
    return;
  int i = L->size;
  if (i < LIST_ALLOC || d > L->l[LIST_ALLOC-1].d)
    {
      /* if list is not full, we insert at position i,
         otherwise we insert at position LIST_ALLOC-1 */
      if (i == LIST_ALLOC)
        i--;
      else
        L->size++;
      L->l[i].nxmin = nxmin;
      L->l[i].nxmax = nxmax;
      L->l[i].nymin = nymin;
      L->l[i].nymax = nymax;
      L->l[i].d = d;
      /* now insertion sort */
      while (i > 0 && d > L->l[i-1].d)
        {
          chunk_swap (L->l + (i-1), L->l + i);
#ifdef RANK
          if (L->l[i].rank < i)
            L->l[i].rank = i; /* updates maximal rank */
#endif
          i--;
        }
#ifdef RANK
      L->l[i].rank = i; /* set initial rank */
#endif
    }
  // List_check (L);
}

static void
List_swap (List_t L1, List_t L2)
{
  chunk_t *tmp;
  tmp = L1->l; L1->l = L2->l; L2->l = tmp;
  int ump;
  ump = L1->size; L1->size = L2->size; L2->size = ump;
}

static void
List_clear (List_t L)
{
  free (L->l);
}

static void
exhaustive_search (chunk_t *c, UTYPE *nxbest, UTYPE *nybest, double *dbest,
                   int *rbest)
{
  for (UTYPE nx = c->nxmin; nx < c->nxmax; nx ++)
    {
      TYPE x = get_type (nx);
      for (UTYPE ny = c->nymin; ny < c->nymax; ny ++)
        {
          TYPE y = get_type (ny);
          double d = distance (x, y);
          if (d > *dbest)
            {
              *dbest = d;
              *nxbest = nx;
              *nybest = ny;
#ifdef RANK
              *rbest = c->rank;
#endif
            }
        }
    }
#ifdef STAT
#pragma omp atomic update
  eval_exhaustive += (c->nxmax - c->nxmin) * (c->nymax - c->nymin);
#endif
}

static void
search_aux (UTYPE nxmin, UTYPE nxmax, UTYPE nymin, UTYPE nymax,
            UTYPE *nxbest, UTYPE *nybest, int tid, List_t L)
{
  double d;
  d = max_heuristic (nxmin, nxmax, nymin, nymax, nxbest, nybest, tid);
  List_insert (L, nxmin, nxmax, nymin, nymax, d);
}

/* search for nxmin <= nx < nxmax and nymin <= ny < nymax,
   where the worst found so far is (nxbest,nybest,dbest) */
static void
search (UTYPE nxmin, UTYPE nxmax, UTYPE nymin, UTYPE nymax,
        UTYPE *nxbest, UTYPE *nybest, double *dbest, int *rbest)
{
  int tid = omp_get_thread_num ();
  List_t L;

  List_init2 (L, nxmin, nxmax, nymin, nymax);
  while (1)
    {
      assert (1 <= L->size && L->size <= LIST_ALLOC);
      double width = chunk_size (L->l[0]);
      if (width <= Threshold) /* exhaustive search */
        {
          for (int i = 0; i < L->size; i++)
            exhaustive_search (L->l + i, nxbest, nybest, dbest, rbest);
          break;
        }
      else /* split each chunk in two */
        {
          List_t NewL;
          List_init (NewL);
          for (int i = 0; i < L->size; i++)
            {
              UTYPE nxmin = L->l[i].nxmin;
              UTYPE nxmax = L->l[i].nxmax;
              UTYPE width_x = nxmax - nxmin;
              UTYPE nymin = L->l[i].nymin;
              UTYPE nymax = L->l[i].nymax;
              UTYPE width_y = nymax - nymin;
              if (width_x >= width_y) // cut the x-side in two
              {
                UTYPE nxmid = nxmin + (nxmax - nxmin) / 2;
                search_aux (nxmin, nxmid, nymin, nymax, nxbest, nybest, tid, NewL);
                search_aux (nxmid, nxmax, nymin, nymax, nxbest, nybest, tid, NewL);
              }
              else // cut the y-side in two
              {
                UTYPE nymid = nymin + (nymax - nymin) / 2;
                search_aux (nxmin, nxmax, nymin, nymid, nxbest, nybest, tid, NewL);
                search_aux (nxmin, nxmax, nymid, nymax, nxbest, nybest, tid, NewL);
              }
            }
          List_swap (L, NewL);
#ifdef STAT
          printf ("L: "); List_print (L);
#endif
          List_clear (NewL);
        }
    }
  List_clear (L);
}

#if defined(USE_FLOAT) || defined(USE_DOUBLE) || defined(USE_LDOUBLE)
#define NLIBS 12
#else
#define	NLIBS 2 /* only glibc and icc do provide binary128 */
#endif
#define SIZE (5*NLIBS) /* 4 functions (atan2, hypot, pow, atan2pi, compound) */

#ifdef WORST
/* since CRLIBM is correctly rounded, we should not find any error larger
   than 0.5 ulp for rounding to nearest, or 1 ulp for directed rounding */
#if defined(GLIBC) || defined(CRLIBM)
#define NUMBER 0
#endif
#ifdef ICC
#define NUMBER 1
#endif
#ifdef AMD
#define NUMBER 2
#endif
#ifdef NEWLIB
#define NUMBER 3
#endif
#ifdef OPENLIBM
#define NUMBER 4
#endif
#ifdef MUSL
#define NUMBER 5
#endif
#ifdef APPLE
#define NUMBER 6
#endif
#ifdef LLVM
#define NUMBER 7
#endif
#ifdef LIBMVEC
#define NUMBER 8
#endif
#ifdef MSVC
#define NUMBER 9
#endif
#ifdef FREEBSD
#define NUMBER 10
#endif
#ifdef ARM
#define NUMBER 11
#endif
#ifndef NUMBER
#error "NUMBER undefined"
#endif
#endif

static void
setround (int rnd)
{
#ifndef CRLIBM
  fesetround (rnd);
#endif
}

static void
doit (unsigned int seed)
{
  UTYPE nxbest = 0, nybest = 0;
  double dbest = 0;
  int rbest = -1;

  /* set the random seed of the current thread */
  srand (seed);

  /* set the rounding mode of the current thread */
  setround (rnd1[rnd]);

  /* for thread 0, get the "worst" value so far as initial point */
  if (omp_get_thread_num () == 0)
    {
      dbest = Dbest;
      nxbest = get_utype (Xbest);
      nybest = get_utype (Ybest);
    }

  search (get_utype (Xmin), get_utype (Xmax) + 1,
          get_utype (Ymin), get_utype (Ymax) + 1, &nxbest, &nybest, &dbest, &rbest);
#pragma omp critical
  if (dbest > Dbest)
    {
      Dbest = dbest;
#ifdef RANK
      Rbest = rbest;
#endif
      mode_best = mode ();
      Xbest = get_type (nxbest);
      Ybest = get_type (nybest);
    }
  mpfr_free_cache (); /* free cache of current thread */
}

#ifdef WORST

#define SIZE_EXTRA 50

#ifdef USE_FLOAT
  TYPE worst[SIZE][2] = {
    /* atan2(x,y) */
    {0x1.960176p-63,0x1.f88dp+80},      /* GNU libc       0.5 */
    {-0x1.58a7ecp-118,0x1.58a7bep-123}, /* icx            0.549965 */
    {0x1.fffe24p+59,0x1.000adcp+73},    /* AMD LibM       0.583286 */
    {-0x1.f9cf48p+49,0x1.f60598p+51},   /* Newlib         1.51236 */
    {0x1.a10104p+123,0x1.99f182p+125},  /* OpenLibm       1.54065 */
    {0x1.a10104p+123,0x1.99f182p+125},  /* Musl           1.54065 */
    {-0x1.ce62cep-116,0x1.cbf9bp-113},  /* Darwin 20.4.0  0.721704 */
    {0x1.960176p-63,0x1.f88dp+80},      /* llvm 0.5 */
#if LIBMVEC==128 /* sse4.2 */
    {0x1.b9895cp+92,0x1.c6fe64p+92},    /* libmvec 3.84566 */
#elif LIBMVEC==256 /* avx2 */
    {-0x1.bddbe6p+68,0x1.a76414p+69},   /* libmvec 3.45800 */
#else /* avx512 */
    {0x1.001998p-125,0x1.ffe0e4p-123},  /* libmvec 3.86596 */
#endif
    {0x1.fffe24p+59,0x1.000adcp+73},    /* msvc */
    {0x1.a10104p+123,0x1.99f182p+125},  /* freebsd 1.54065 */
    {-0x1.9be82ap-101,0x1.92cfb2p-101}, /* ArmPL 2.92727 */

    /* hypot(x,y) */
    {-0x1.003222p-20,-0x1.6a2d58p-32},  /* GNU libc       0.500000001392678 */
    {-0x1.527182p+127,-0x1.803006p+127},/* icx            Inf */
    {-0x1.003222p-20,-0x1.6a2d58p-32},  /* AMD LibM       0.500000001392678 */
    {-0x1.6b05c4p-127,0x1.6b3146p-126}, /* Newlib         1.20805 */
    {-0x1.6b05c4p-127,0x1.6b3146p-126}, /* OpenLibm       1.20806 */
    {0x1.26b188p-127,-0x1.a4f2fp-128},  /* Musl           0.926707 */
    {-0x1.003222p-20,-0x1.6a2d58p-32},  /* Darwin 20.4.0  0.500001 */
    {-0x1.65e2cp+124,0x1.ac1p+112},     /* llvm           */
    /* https://reviews.llvm.org/D114726 */
#if LIBMVEC==128 /* sse4.2 */
    {-0x1.6af304p-2,-0x1.01d9d4p-2},    /* libmvec 1.4287 */
#elif LIBMVEC==256 /* avx2 */
    {-0x1.6a11aap+20,-0x1.a9216ep+12},  /* libmvec 1.20676 */
#else /* avx512 */
    {0x1.6be8b4p-62,-0x1.b8703cp-66},   /* libmvec 1.29410 */
#endif
    {-0x1.003222p-20,-0x1.6a2d58p-32},  /* msvc */
    {-0x1.6b05c4p-127,0x1.6b3146p-126}, /* freebsd 1.20806 */
    {-0x1.003222p-20,-0x1.6a2d58p-32},  /* ArmPL */

    /* pow(x,y) */
    {0x1.025736p+0,0x1.309f94p+13},     /* GNU libc       0.816737 */
    {0x1.fe7782p-1,-0x1.c361cap+14},    /* icc            0.514374 */
    {0x1.10fff4p+0,0x1.58fd76p+10},     /* AMD LibM       1.55284 */
    {0x1.d6411cp-102,0x1.793482p+0},    /* Newlib         1.00000 */
    /* the following is with -D__OBSOLETE_MATH_DEFAULT=1 */
    // {0x1.025736p+0,0x1.309f94p+13},     /* Newlib         0.816736 */
    {0x1.343e4ep+0,0x1.af3c4p+8},       /* OpenLibm       0.969482 */
    {0x1.025736p+0,0x1.309f94p+13},     /* Musl           0.816736 */
    {0x1.95e42cp-27,0x1.369972p-14},    /* Darwin         0.513872 */
    {-0x1.8p-49,0x1.8p+1},              /* llvm 0.5 but not CR! */
#if LIBMVEC==128 /* sse4.2 */
    {0x1.b85816p+89,-0x1.90194ap-32},   /* libmvec 0.62007 */
#elif LIBMVEC==256 /* avx2 */
    {0x1.b85816p+89,-0x1.90194ap-32},   /* libmvec 0.62007 */
#else /* avx512 */
    {0x1.ff7f02p-1,0x1.55026cp+16},     /* libmvec 0.961324 */
#endif
    {0x1.107fe4p+0,0x1.631e6cp+10},     /* msvc 0.567212 */
    {0x1.343e4ep+0,0x1.af3c4p+8},       /* freebsd 0.969482 */
    {0x1.025736p+0,0x1.309f94p+13},     /* ArmPL */

    /* atan2pi(x,y) */
    {-0x1.baca2ap-96,-0x1.a6c0c4p-82},  /* GNU libc 1.60206 */
    {0x1.45c8a4p+34,-0x1.749dbcp+33},   /* icx            0.840436 */
    {0,0},                              /* AMD LibM */
    {0,0},                              /* Newlib */
    {0,0},                              /* OpenLibm */
    {0,0},                              /* Musl */
    {0,0},                              /* Darwin */
    {0,0},                              /* llvm */
    {0,0},                              /* libmvec */
    {0,0},                              /* msvc */
    {0,0},                              /* freebsd */
    {0,0},                              /* ArmPL */

    /* compound(x,y) */
    {0,0},                              /* GNU libc */
    {0x1.46674cp+50,0x1.d5738ap-30},    /* icx 0.500001 */
    {0,0},                              /* AMD LibM */
    {0,0},                              /* Newlib */
    {0,0},                              /* OpenLibm */
    {0,0},                              /* Musl */
    {0,0},                              /* Darwin */
    {0,0},                              /* llvm */
    {0,0},                              /* libmvec */
    {0,0},                              /* msvc */
    {0,0},                              /* freebsd */
    {0,0},                              /* ArmPL */
  };
TYPE extra[SIZE_EXTRA][2] = {
  {0x1.a10104p+123,0x1.99f182p+125},
  /* the following are hard-to-round cases of hypot with many identical bits after
     the round bit */
  {0x1.900004p+34,0x1.400002p+23}, /* 45 identical bits */
  {0x1.05555p+34,0x1.bffffep+23},  /* 44 identical bits */
  {0x1.e5fffap+34,0x1.affffep+23}, /* 45 identical bits */
  {0x1.260002p+34,0x1.500002p+23}, /* 45 identical bits */
  {0x1.fffffap+34,0x1.fffffep+23}, /* 45 identical bits */
  {0x1.8ffffap+34,0x1.3ffffep+23}, /* 45 identical bits */
  {0x1.87fffcp+35,0x1.bffffep+23}, /* 47 identical bits */
  /* the following exhibit errors in a previous llvm-libc implementation,
     see https://reviews.llvm.org/D118157 */
  {0x1.faf49ep+25,0x1.480002p+23},
  {0x1.014bf2p+26,0x1.780012p+23},
  {0x1.097d9ep+27,0x1.ac0024p+23},
  {0x1.0a776ap+27,0x1.d80024p+23},
  {0x1.055176p+28,0x1.400048p+23},
  {0x1.216c96p+29,0x1.7c009p+23},
  {0x1.0bec92p+30,0x1.b4012p+23},
  {0x1.463d9ap+31,0x1.ec024p+23},
  {0x1.0d6d6ap+32,0x1.7c048p+23},
  {0x1.2ebd96p+33,0x1.d809p+23},
  {0x1.ffffecp-1,-0x1.000002p+27}, /* bug in Openlibm 0.7.5 for powf */
  {-0x1.8p-49,0x1.8p+1},           /* bug in LLVM 18.1.8 powf RNDN */
  {0x1.9a1p-20,0x1.ep+0},          /* bug in LLVM 18.1.8 powf RNDZ */
  {0x1.8p-48,0x1.8p+1},            /* bug in LLVM 18.1.8 powf RNDU */
  {0x1.9a1p-20,0x1.ep+0},          /* bug in LLVM 18.1.8 powf RNDD */
  {0x1.d6411cp-102,0x1.793482p+0}, /* Newlib powf 1.0 (with patch from Bruce Evans) */
  };
#endif

#ifdef USE_DOUBLE
  TYPE worst[SIZE][2] = {
    /* atan2(x,y) */
    {0x1.ed6060626eecfp-429,0x1.f42ebb62994dcp-426},   /* glibc 0.523171 */
    {0x1.b77ade79a36d5p-326,0x1.ff6a37b72b52bp-319},   /* icx 0.547508 */
    {-0x1.ec2003fe89e36p-433,0x1.ec173ed5fd8f5p-427},  /* AMD 1.50098 */
    {-0x1.358bb5eb25bdcp+813,0x1.2f86b82481a0ap+815},  /* Newlib 1.54908 */
    {-0x1.358bb5eb25bdcp+813,0x1.2f86b82481a0ap+815},  /* OpenLibm 1.54908 */
    {-0x1.358bb5eb25bdcp+813,0x1.2f86b82481a0ap+815},  /* Musl 1.54908 */
    {-0x1.6a539153430d8p-416,0x1.d2b5b9dc716d8p-415},  /* Apple 0.746154 */
    {0,0},                                             /* llvm */
#if LIBMVEC==128 /* sse4.2 */
    /* see https://sourceware.org/bugzilla/show_bug.cgi?id=28765:
       error was larger than 4 ulps */
    {0x1.abe93a1719613p-948,0x1.aab7bbb5ca811p-948},   /* libmvec 2.47316 */
#elif LIBMVEC==256 /* avx2 */
    {0x1.a83f842ef3f73p-633,0x1.a799d8a6677ep-633},    /* libmvec 3.46942 */
#else /* avx512 */
    {0x1.499c920038ab4p+559,0x1.4939bd8e01601p+559},   /* libmvec 3.41468 */
#endif
    {-0x1.f1037a6756bfep-881,0x1.959f99be632e6p+142},  /* msvc 0.75 */
    {-0x1.358bb5eb25bdcp+813,0x1.2f86b82481a0ap+815},  /* freebsd 1.54909 */
    {0x1.d5de7a294d493p-935,0x1.d030d7b608be1p-935},   /* ArmPL 2.22671 */

    /* hypot(x,y) */
    {0x0.603e52daf0bfdp-1022,-0x0.a622d0a9a433bp-1022},  /* glibc 0.791665 */
    {0x0.19deaac345ffap-1022,0x0.92c8727c389b6p-1022},   /* icc 0.7500000255880764 */
    {0x1.ea74e4b1da32bp+1002,-0x1.0a9efc37c242cp+1003},  /* AMD 1.02908 */
    {0x1.6a0a41410b1abp-1004,-0x0.a24afe71b539fp-1022},  /* Newlib 1.20710 */
    {0x1.6a0a41410b1abp-1004,-0x0.a24afe71b539fp-1022},  /* OpenLibm 1.20710 */
    {0x1.00014d4b1c6b9p-1015,-0x1.000105ba9bf4p-1015},   /* Musl 1.03031 */
    {0x1.6a0a41410b1abp-1004,-0x1.4495fce36a73ep-1023},  /* Apple 1.20710 */
    /* llvm 14.0.6: tested up to target=1000000000 */
    {0x1.a308e1455f447p+0,0x1.9d931a83ef879p+0},         /* llvm */
#if LIBMVEC==128 /* sse4.2 */
    {0x1.205e5d5fee071p-9,0x1.a71193d4eb838p-9},         /* libmvec 2.66580 */
#elif LIBMVEC==256 /* avx2 */
    {0x1.6d080c1f5339ep+25,0x1.149ee0ad66632p+13},       /* libmvec 1.38804 */
#else /* avx512 */
    {-0x1.72b48b14296a7p-510,-0x1.3dcd53d99b107p-518},   /* libmvec 1.50331 */
#endif
    {-0x1.6a5a0ce661358p+890,-0x1.0151c108425b1p+890},   /* msvc 1.21768 */
    {0x1.6a0a41410b1abp-1004,-0x1.4495fce36a73ep-1023},  /* freebsd 1.2071 */
    {-0x0.5a22c27a3893p-1022,0x0.9cfea180c00dap-1022},   /* ArmPL 0.948812 */

    /* pow(x,y) */
    {0x1.010e2e7ee71aep+0,0x1.44bf0047427f6p+17},    /* glibc 0.522909 */
    {0x1.fffff9c61ce4p-1,0x1.c4e304ed4c734p+31},     /* icc 1.72811 */
    {0x1.3bbe002a257a7p-771,0x1.64bf51fe77808p+0},   /* AMD 0.999324 */
    {0x1.a26d96e778dc8p+163,0x1.90d5de9f964f6p-9},   /* Newlib 0.894018 */
    {0x1.000002c5e2e99p+0,0x1.c9eee35374af6p+31},    /* OpenLibm 635.33 */
    {0x1.010e2e7ec0c83p+0,0x1.44bf00479249dp+17},    /* Musl 0.524293 */
    {0x1.111616f835fb1p-72,0x1.c6cfa07925d49p+3},    /* Apple 0.756470 */
    {0,0},                                           /* llvm */
#if LIBMVEC==128 /* sse4.2 */
    {0x1.e174ee53813b7p+859,-0x1.d929d0607bf52p-12}, /* libmvec 1.00093 */
#elif LIBMVEC==256 /* avx2 */
    {0x1.bb393b102aa6p+246,-0x1.9c23caed44f1fp-10},  /* libmvec 0.999950 */
#else /* avx512 */
    {0x1.65f5c9d0c7bc9p-828,0x1.eba10d43b8f54p-12},  /* libmvec 0.999925 */
#endif
    {0x1.fffff9c61ce40p-1,0x1.c4e304ed4c734p+31},    /* msvc 91.2719 */
    {0x1.000002c5e2e99p+0,0x1.c9eee35374af6p+31},    /* freebsd 635.33 */
    {0x1.010e2e7ee71aep+0,0x1.44bf0047427f6p+17},    /* ArmPL 0.522909 */

    /* atan2pi(x,y) */
    {-0x1.fe856e7997f8p+381,0x1.90ece816f9a7cp+343},   /* GNU libc 1.48772 */
    {-0x1.026462f302171p-391,0x1.39b157b1210a4p-390},  /* icx 1.01209 */
    {0,0},                              /* AMD LibM */
    {0,0},                              /* Newlib */
    {0,0},                              /* OpenLibm */
    {0,0},                              /* Musl */
    {0,0},                              /* Darwin */
    {0,0},                              /* llvm */
    {0,0},                              /* libmvec */
    {0,0},                              /* msvc */
    {0,0},                              /* freebsd */
    {0,0},                              /* ArmPL */

    /* compound(x,y) */
    {0,0},                              /* GNU libc */
    {-0x1.0000000000001p-1,0x1p+10},    /* icx 1.1259e+15 */
    {0,0},                              /* AMD LibM */
    {0,0},                              /* Newlib */
    {0,0},                              /* OpenLibm */
    {0,0},                              /* Musl */
    {0,0},                              /* Darwin */
    {0,0},                              /* llvm */
    {0,0},                              /* libmvec */
    {0,0},                              /* msvc */
    {0,0},                              /* freebsd */
    {0,0},                              /* ArmPL */
  };
TYPE extra[SIZE_EXTRA][2] = {
  {0x1.9c6d80093ebf8p+446,0x1.add8969f3a07ep+421},    /* as_hypot 0.5000000000000002 */
  {-0x0.5a22c27a3893p-1022,0x0.9cfea180c00dap-1022},  /* llvm with patch */
  /* the following is for glibc hypot (patch series 5463 without fma) */
  {0x0.603e52daf0bfdp-1022,-0x0.a622d0a9a433bp-1022}, /* 0.791665 */
  /* the following is for glibc hypot (patch series 5463 with fma) */
  {-0x0.5a22c27a3893p-1022,0x0.9cfea180c00dap-1022}, /* 0.948812 */
  {0x1.171436ab898ccp-796,-0x1.db3136be02ca6p-63},   /* bug crlibm pow_rn */
  /* the following is a hard-to-round case from Carlos Borges's article
     "Algorithm 1014: An Improved Algorithm for hypot(x,y)" */
  {0x1.a308e1455f447p+0,0x1.9d931a83ef879p+0},
  {0x0.89c127b351f2bp-1022,0x0.4fd9b86bb4fe8p-1022},
  /* the following gave a huge error for pow in Newlib:
     https://sourceware.org/pipermail/newlib/2023/020164.html */
  {-0x1.647ff80007ff8p-576,-0x1.3d018267f12fp+48},
  };
#endif

#ifdef USE_LDOUBLE
  TYPE worst[SIZE][2] = {
/* atan2(x,y) */
{-0x7.9301460b8463cbp+15368l,0xf.25cd5eb1280b4d1p+15372l},/* glibc 0.750106 */
{-0x5.c0c9cc5a59632f88p+16340l,0x5.db7810fba1ce4908p+16348l},/* icc 0.500209 */
{0,0},                                                    /* AMD Libm */
{0,0},                                                    /* Newlib */
{0x3.d34c9d81dcd29354p+5568l,0xf.3afc4f6c9f5c4a2p+5568l}, /* OpenLibm 1.68651 */
{-0x7.9301460b8463cbp+15368l,0xf.25cd5eb1280b4d1p+15372l},/* Musl 0.750106 */
    {0,0},                                                /* Apple */
    {0,0},                                                /* llvm */
    {0,0},                                                /* libmvec */
    {0,0},                                                /* msvc */
{0x3.d34c9d81dcd29354p+5568l,0xf.3afc4f6c9f5c4a2p+5568l}, /* freebsd 1.68651 */
    {0,0},                                                /* ArmPL */

    /* hypot(x,y) */
{-0x2.97b86706043d619p+7240l,0x1.8256bdd12d2e163ep+7240l},/* glibc 0.583306 */
{-0x3.00bad8a56d87a0cp-16384l,-0xe.6d794db04791398p-16388l}, /* icc 0.75001 */
{0,0},                                                    /* AMD Libm */
{0x6.0d9f0aa73c9f1438p+11112l,0x1.82fdacf00c626b3ap+11116l}, /* Newlib inf */
{0x1.73f339f61eda21dp-16384l,0x2.e45f9f9500877e2p-16384l},/* OpenLibm 0.980552 */
{0x2.00007da75fd5903cp-8960l,0x2.d42207352184bff4p-8960l},/* Musl 1.07731 */
    {0,0},                                                /* Apple */
    {0,0},                                                /* llvm */
    {0,0},                                                /* libmvec */
    {0,0},                                                /* msvc */
{0x1.73f339f61eda21dp-16384l,0x2.e45f9f9500877e2p-16384l},/* freebsd 0.980552 */
    {0,0},                                                /* ArmPL */

    /* pow(x,y) */
{0x2.21dda4bcec55b158p-3616l,0x7.ef1ef5fbe3df50dp-16l},   /* glibc 0.913900 */
{0xc.b80572af668bb57p+152l,-0x6.8a6d3d7b442f3c18p+4l},    /* icc 0.500654 */
{0,0},                                                    /* AMD 0.913476 */
{0,0},                                                    /* Newlib */
{0xf.a795000b7dae5b4p-4l,-0x7.e4a42355b11a8098p+16l}, /* OpenLibm 37677.9 */
{0xf.a795000b7dae5b4p-4l,-0x7.e4a42355b11a8098p+16l}, /* Musl 37677.9 */
    {0,0},                                                /* Apple */
    {0,0},                                                /* llvm */
    {0,0},                                                /* libmvec */
    {0,0},                                                /* msvc */
/* the following FreeBSD value is with OMP_NUM_THREADS=1,
   with several threads FreeBSD's powl returns nonsense like OpenLibm:
   https://github.com/JuliaMath/openlibm/issues/222 */
{0xf.a795000b7dae5b4p-4l,-0x7.e4a42355b11a8098p+16l},  /* freebsd 37677.9 */
    {0,0},                                                /* ArmPL */

    /* atan2pi(x,y) */
    {0x4.8010e21a5d13ad38p+212l,0x5.bb4879ec6325337p+10940l}, /* GNU libc 1.59019 */
    {0,0},                              /* icx */
    {0,0},                              /* AMD LibM */
    {0,0},                              /* Newlib */
    {0,0},                              /* OpenLibm */
    {0,0},                              /* Musl */
    {0,0},                              /* Darwin */
    {0,0},                              /* llvm */
    {0,0},                              /* libmvec */
    {0,0},                              /* msvc */
    {0,0},                              /* freebsd */
    {0,0},                              /* ArmPL */

    /* compound(x,y) */
    {0,0},                              /* GNU libc */
    {0xa.613dc3f1daa2566p+5432l,-0x4.fcf34863ee4e7a38p-20l}, /* icx 0.501073 */
    {0,0},                              /* AMD LibM */
    {0,0},                              /* Newlib */
    {0,0},                              /* OpenLibm */
    {0,0},                              /* Musl */
    {0,0},                              /* Darwin */
    {0,0},                              /* llvm */
    {0,0},                              /* libmvec */
    {0,0},                              /* msvc */
    {0,0},                              /* freebsd */
    {0,0},                              /* ArmPL */
  };
TYPE extra[SIZE_EXTRA][2] = {
  {-0x4p-16320l,0x4.0000000000000008p-16384l},
  {0x4p-16320l,0x4.0000000000000008p-16384l},
  {0x4p-16384l,0x4.0000000000000008p-16320l},
  {0x4p-16384l,-0x4.0000000000000008p-16320l},
  {0x4p-16320l,-0x4.0000000000000008p-16384l},
  {-0x4p-16384l,0x4.0000000000000008p-16320l},
  {-0x4p-16384l,-0x4.0000000000000008p-16320l},
  {-0x4p-16320l,-0x4.0000000000000008p-16384l},
  {-0x5.e7da736ff042237p-16320l,-0x5.1c6dd26cc70b3cc8p-16384l},
  {0x5.480b3781eecccadp-16384l,0x4.0687a05a152d8b18p-16320l},
  {0x6.c4e6069efd8b27b8p-16320l,-0x5.21e077ba3d981c7p-16384l},
  {-0x6.2d56393a2d535118p-16384l,-0x5.974a475e5cf1823p-16320l},
  /* the following is for musl hypot (non deterministic) */
  {0x1.868c729944440d8p-16388l,0x5.a9300a1cf661ee58p-16384l}, /* 1.20301 */
  {0x1.3c563460396af68p-16388l,0x5.ba04a504ccdc064p-16384l}, /* 1.19331 */
  {-0x9.4027deaa9f77748p-16388l,-0x5.a936e8226632238p-16384l}, /* 1.19401 */
  {-0x1.f8b424b1875c998p-16388l,-0x5.b1d67270d564d3p-16384l}, /* 1.19713 */
  /* the following is for glibc hypot (patch series 5463 with/without fma) */
  {-0x2.97b86706043d619p+7240l,0x1.8256bdd12d2e163ep+7240l}, /* 0.58330 */
  };
#endif

#ifdef USE_FLOAT128
  TYPE worst[SIZE][2] = {
    /* atan2(x,y) */
    {Q(0x1.41df5aa214612c7e019fa6ade88p-13316),
     Q(0x5.e53b26a270a29eb9f77ef8ef7af8p-13316)},  /* glibc 1.88155 */
    {Q(-0x1.fb41ff205f5ade930a9fcbba8ea8p-16384),
     Q(0x2.23f098fd6b8799dbeb03219bfa08p-10520)},  /* icc 0.500500 */
    /* hypot(x,y) */
    {Q(-0x1.80e7403e1b344c4a78edeced92e4p-16384),
     Q(-0x2.986c750d01c32e4c807c12ad685p-16384)},  /* glibc 0.791623 */
    {Q(0x8.79ec30b61f9b839fe507bbdf414p-11908),
     Q(0xb.94f6832f64d0729ebd68035ed7a8p-11908)},  /* icc 0.500324 */
    /* pow(x,y) */
    {Q(0x1.364dcbbad0512d7bacaae2a8d56bp+0),
     Q(-0xe.68759219434c37725fdf30d17d2p+12)},     /* glibc 30.2577 */
    {Q(0x4p-16496),
     Q(0x3.ffffff39c102f0aa11bb2c8a91dp-128)},     /* icc 1.395601741238769 */
    /* atan2pi(x,y) */
    {Q(-0x2.c6994d7f40fae88117550b428404p+16040),
     Q(0xe.181a2c7696ba5cf23a6eab680b2p+16040)},   /* glibc 2.80451 */
    {Q(0x0p0),Q(0x0p0)},                           /* icx */
    /* compound(x,y) */
    {Q(0x0p0),Q(0x0p0)},                           /* glibc */
    {Q(0x2.660c17a54ded3960195d40db8ee4p-11652),
     Q(0xd.45349a19be26b49582eb8c9bb588p+11660)},  /* icx 0.500215 */
  };
TYPE extra[SIZE_EXTRA][2] = {
  /* the following is for glibc hypot (patch series 5463 without fma) */
  {Q(0x2.2d5faf4036d6e68566f01054612p-8192),
   Q(0x3.5738e8e2505f5d1fc2973716f05p-8192)},      /* 0.748943 */
  };
#endif
#endif

static void
print_error (double d)
{
  mpfr_t x;
  mpfr_init2 (x, 53);
  mpfr_set_d (x, d, MPFR_RNDU);
  if (d < 0.999)
    mpfr_printf ("%.3RUf", x);
  else if (d < 9.99)
    mpfr_printf ("%.2RUf", x);
  else if (d < 99.9)
    mpfr_printf ("%.1RUf", x);
  else
    mpfr_printf ("%.2RUe", x);
  mpfr_clear (x);
}

static void
init_Threshold (void)
{
  UTYPE boundx = get_utype (Xmax) - get_utype (Xmin);
  UTYPE boundy = get_utype (Ymax) - get_utype (Ymin);
  double w = (double) boundx * (double) boundy;
  double s = 0;              /* number of evaluations so far */
  while (s < w)
    {
      w = w / 2.0; // cut in 2
      s += 2 * (double) threshold; // use threshold evaluations in each half
    }
  Threshold = w;
}

int
main (int argc, char *argv[])
{
  unsigned int seed = 0;
  int nthreads = 0;
  double worst_input_x = 0, worst_input_y = 0;

#ifdef GLIBC
  if (verbose)
    {
      printf("GNU libc version: %s\n", gnu_get_libc_version ());
      printf("GNU libc release: %s\n", gnu_get_libc_release ());
    }
#endif
#ifdef _MSC_VER
  printf ("Using Microsoft math library %d\n", _MSC_VER);
#endif
#ifdef CRLIBM
  /* We should call the crlibm_init() function, which sets the rounding
     precision to double. */
  crlibm_init ();
#endif

  Xmin = 0;
  Xmax = -TYPE_MAX;
  Ymin = 0;
  Ymax = -TYPE_MAX;

  while (argc >= 2 && argv[1][0] == '-')
    {
      if (strcmp (argv[1], "-v") == 0)
        {
          verbose ++;
          argc --;
          argv ++;
        }
      else if (argc >= 3 && strcmp (argv[1], "-threshold") == 0)
        {
          threshold = strtoul (argv[2], NULL, 10);
          argv += 2;
          argc -= 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-seed") == 0)
        {
#ifdef ARM_USE_HARDWARE_RNG
  printf("Warning: Unable to set the seed when hardware RNG is utilized.\n");
#endif
          seed = strtoul (argv[2], NULL, 10);
          argv += 2;
          argc -= 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-mode") == 0)
        {
          use_mode = strtoul (argv[2], NULL, 10);
          assert (0 <= use_mode && use_mode <= 2);
          argv += 2;
          argc -= 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-extra") == 0)
        {
          extra_file = fopen (argv[2], "r");
          argv += 2;
          argc -= 2;
        }
      else if (strcmp (argv[1], "-rndn") == 0)
        {
          rnd = (mpfr_rnd_t) 0;
          argc --;
          argv ++;
        }
      else if (strcmp (argv[1], "-rndz") == 0)
        {
          rnd = (mpfr_rnd_t) 1;
          argc --;
          argv ++;
        }
      else if (strcmp (argv[1], "-rndu") == 0)
        {
          rnd = (mpfr_rnd_t) 2;
          argc --;
          argv ++;
        }
      else if (strcmp (argv[1], "-rndd") == 0)
        {
          rnd = (mpfr_rnd_t) 3;
          argc --;
          argv ++;
        }
      else if (argc >= 3 && strcmp (argv[1], "-nthreads") == 0)
        {
          nthreads = atoi (argv[2]);
          argc -= 2;
          argv += 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-worst") == 0)
        {
          sscanf (argv[2], "%la,%la", &worst_input_x, &worst_input_y);
          argc -= 2;
          argv += 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-xmin") == 0)
        {
          long double t;
          sscanf (argv[2], "%La", &t);
          Xmin = t;
          argc -= 2;
          argv += 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-xmax") == 0)
        {
          long double t;
          sscanf (argv[2], "%La", &t);
          Xmax = t;
          argc -= 2;
          argv += 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-ymin") == 0)
        {
          long double t;
          sscanf (argv[2], "%La", &t);
          Ymin = t;
          argc -= 2;
          argv += 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-ymax") == 0)
        {
          long double t;
          sscanf (argv[2], "%La", &t);
          Ymax = t;
          argc -= 2;
          argv += 2;
        }
      else
        {
          fprintf (stderr, "Unknown option %s\n", argv[1]);
          exit (1);
        }
    }
  assert (threshold > 0);
  /* divide threshold by LIST_ALLOC so that the total number of evaluations
     does not vary with LIST_ALLOC */
  threshold = 1 + (threshold - 1) / LIST_ALLOC;
  init_Threshold ();

  if (seed == 0)
    seed = getpid ();
  if (verbose)
    printf ("Using seed %lu\n", (unsigned long) seed);
  if (verbose)
  {
    printf ("Using xmin=");
    print_type_hex (Xmin);
    printf (" xmax=");
    print_type_hex (Xmax);
    printf (" ymin=");
    print_type_hex (Ymin);
    printf (" ymax=");
    print_type_hex (Ymax);
    printf ("\n");
  }

#ifdef WORST
  TYPE x, y;
  double d, Dbest0;
  setround (rnd1[rnd]);
  /* first check the given -worst value, if any */
  if (worst_input_x != 0 || worst_input_y != 0)
  {
    x = worst_input_x;
    y = worst_input_y;
    d = distance (x, y);
      if (d > Dbest)
        {
          Dbest = d;
          Xbest = x;
          Ybest = y;
        }
  }
  /* first check the 'worst' values for the given library, with the given
     rounding mode */
  for (int i = NUMBER; i < SIZE; i+=NLIBS)
    {
      x = worst[i][0];
      y = worst[i][1];
      d = distance (x, y);
      if (d > Dbest)
        {
          Dbest = d;
          Xbest = x;
          Ybest = y;
        }
    }
  Dbest0 = Dbest;

  /* then check the 'worst' values for the other libraries */
  for (int i = 0; i < SIZE; i++)
    {
      if ((i % NLIBS) == NUMBER)
        continue;
      x = worst[i][0];
      y = worst[i][1];
      d = distance (x, y);
      if (d > Dbest)
        {
          Dbest = d;
          Xbest = x;
          Ybest = y;
        }
    }

  /* then check the 'extra' values if any */
  for (int i = 0; i < SIZE_EXTRA; i++)
    {
      x = extra[i][0];
      y = extra[i][1];
      if (x == 0 && y == 0)
        break;
      d = distance (x, y);
      if (d > Dbest)
        {
          Dbest = d;
          Xbest = x;
          Ybest = y;
        }
    }

  /* then read extra file if given */
  if (extra_file != NULL)
  {
    char *buf = NULL;
    size_t buflength = 0;
    ssize_t nn;
    while ((nn = getline(&buf, &buflength, extra_file)) >= 0) {
      if (nn > 0 && buf[0] == '#')
        continue;
#ifdef USE_FLOAT
      if (sscanf (buf, "%a,%a", &x, &y) == 2)
#elif defined(USE_DOUBLE)
      if (sscanf (buf, "%la,%la", &x, &y) == 2)
#else
      fprintf (stderr, "extra_file not implemented for this type\n");
      exit (1);
#endif
      {
        d = distance (x, y);
        if (d > Dbest)
        {
          Dbest = d;
          Xbest = x;
          Ybest = y;
        }
      }
    }
  }
#endif

  /* Apparently Visual Studio does not properly set the number of threads. */
#pragma omp parallel
  if (nthreads <= 0)
    nthreads = omp_get_num_threads ();
  if (verbose)
    printf ("Using %d threads\n", nthreads);
  omp_set_num_threads (nthreads);

  assert (nthreads <= MAX_THREADS);

  int n;
#pragma omp parallel for
  for (n = 0; n < nthreads; n++)
    {
      Seed[n] = seed + n;
      doit (Seed[n]);
    }
#ifdef WORST
  if (Dbest > Dbest0)
    printf ("NEW ");
#endif
  printf ("%s %d %d ", NAME, mode_best, Rbest);
  print_type_hex (Xbest);
  printf (",");
  print_type_hex (Ybest);
  printf (" [");
  print_error (Dbest);
  setround (FE_UPWARD);
  printf ("] %.6g %.16g\n", Dbest, Dbest);
  /* reset to the current rounding mode */
  setround (rnd1[rnd]);
#ifdef CRLIBM /* force verification for CRLIBM */
  if (1)
#else
  if (verbose)
#endif
    {
      mpfr_t xx, yy;
      TYPE y, z;
      z = WRAPPER (Xbest, Ybest);
      printf ("libm gives ");
      print_type_hex (z);
      printf ("\n");
      mpfr_set_emin (EMIN + 1);
      mpfr_set_emax (EMAX);
      mpfr_init2 (xx, PREC);
      mpfr_init2 (yy, PREC);
      mpfr_set_type (xx, Xbest, MPFR_RNDN);
      mpfr_set_type (yy, Ybest, MPFR_RNDN);
      int ret = MPFR_FOO (yy, xx, yy, rnd2[rnd]);
      mpfr_subnormalize (yy, ret, rnd2[rnd]);
      y = mpfr_get_type (yy, MPFR_RNDN);
      printf ("mpfr gives ");
      print_type_hex (y);
      printf ("\n");
#ifdef CRLIBM
      if (y != z)
        printf ("BUG in MPFR or CRLIBM\n");
#endif
      fflush (stdout);
      mpfr_clear (xx);
      mpfr_clear (yy);
    }
  fflush (stdout);
  mpfr_free_cache ();
#ifdef STAT
  printf ("eval_heuristic=%lu eval_exhaustive=%lu\n",
          eval_heuristic, eval_exhaustive);
#endif

  if (extra_file != NULL)
    fclose (extra_file);

  return 0;
}
