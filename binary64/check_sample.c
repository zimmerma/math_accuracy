/* Search worst cases of a univariate function, using a recursive algorithm.

   This program is open-source software distributed under the terms 
   of the GNU General Public License <http://www.fsf.org/copyleft/gpl.html>.

   Compile with:

   gcc -DFOO=acos -DUSE_xxx -O3 check_sample.c -lmpfr -lgmp -lm -fopenmp
   icc -DFOO=acos -Qoption,cpp,--extended_float_types -no-ftz -DUSE_xxx -O3 check_sample.c -lmpfr -lgmp -fopenmp

   where xxx is FLOAT, DOUBLE, LDOUBLE, or FLOAT128.

   For NEWLIB: add -DNEWLIB (to avoid compilation error with __errno).

   You can add -DWORST to use some precomputed values to guide the search.

   You can add -DGLIBC to print the GNU libc release (with -v).

   Command-line options:
   -threshold nnn : set the effort threshold to nnn, the runtime is roughly
                    proportional to nnn
   -seed nnn   sets the random seed to nnn
   -mode k     sets the mode of the heuristic search to k (0 <= k <= 2).
               k=0: considers the maximal error in the range
               k=1: considers the average error in the range
               k=2: considers the estimated maximal error
   -v          verbose
   -rndn       rounding to nearest (default)
   -rndz       rounding towards zero
   -rndu       rounding towards +Inf
   -rndd       rounding towards -Inf
   -nthreads n uses n threads
   -worst xxx  uses xxx (hexadecimal value) as worst value

   References and credit:
   * https://www.vinc17.net/research/testlibm/: worst-cases computed by
     Vincent Lefèvre.
   * the idea to sample several intervals instead of only one is due to
     Eric Schneider
*/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE /* to define ...f128 functions */
#endif

#define RANK /* print the maximal list-rank of best values */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#ifndef NO_FLOAT128
#define MPFR_WANT_FLOAT128
#endif
/* icx does not have _Float128, and we should use mathimf.h instead of
   math.h if we want access to the new C23 functions */
#ifdef __INTEL_CLANG_COMPILER
#define _Float128 __float128
#include <mathimf.h>
#else
#include <math.h>
#endif
#ifdef _MSC_VER
/* The Microsoft library does not define j0 but _j0,
   you should compile with -DFOO=_j0 */
#define mpfr__j0 mpfr_j0
#define mpfr__j1 mpfr_j1
#define mpfr__y0 mpfr_y0
#define mpfr__y1 mpfr_y1
#endif
#include <mpfr.h>
#include <assert.h>
#include <sys/types.h>
#include <unistd.h>
#ifndef NO_OPENMP
#include <omp.h>
#endif
#include <float.h> /* for DBL_MAX */
#include <fenv.h>
#ifdef APPLE
#include <sys/sysctl.h>
#endif
#ifdef CRLIBM
#include "crlibm.h"
#endif

/* define GLIBC to print the GNU libc version */
#ifdef GLIBC
#include <gnu/libc-version.h>
#endif

#if defined(__aarch64__) && defined(__ARM_FEATURE_RNG)
  #define ARM_USE_HARDWARE_RNG
  #include <arm_acle.h>
#endif

#define MAX_THREADS 192

/* rounding modes */
int rnd1[] = { FE_TONEAREST, FE_TOWARDZERO, FE_UPWARD, FE_DOWNWARD };
mpfr_rnd_t rnd2[] = { MPFR_RNDN, MPFR_RNDZ, MPFR_RNDU, MPFR_RNDD };
mpfr_rnd_t rnd = MPFR_RNDN; /* default rounding mode */

/* mode (0,1,2), if -1 set according to omp_get_thread_num() */
int use_mode = -1;

FILE *extra_file = NULL;

#ifndef acospi
extern double acospi (double);
#endif
#ifndef acospil
extern long double acospil (long double);
#endif
#ifndef asinpi
extern double asinpi (double);
#endif
#ifndef asinpil
extern long double asinpil (long double);
#endif
#ifndef atanpi
extern double atanpi (double);
#endif
#ifndef atanpil
extern long double atanpil (long double);
#endif
#ifndef cospi
extern double cospi (double);
#endif
#ifndef cospil
extern long double cospil (long double);
#endif
#ifndef exp10m1
extern double exp10m1 (double);
#endif
#ifndef exp10m1l
extern long double exp10m1l (long double);
#endif
#ifndef exp2m1
extern double exp2m1 (double);
#endif
#ifndef exp2m1l
extern long double exp2m1l (long double);
#endif
#ifndef log10p1
extern double log10p1 (double);
#endif
#ifndef log10p1l
extern long double log10p1l (long double);
#endif
#ifndef log2p1
extern double log2p1 (double);
#endif
#ifndef log2p1l
extern long double log2p1l (long double);
#endif
#ifndef rsqrt
extern double rsqrt (double);
#endif
#ifndef sinpi
extern double sinpi (double);
#endif
#ifndef sinpil
extern long double sinpil (long double);
#endif
#ifndef tanpi
extern double tanpi (double);
#endif
#ifndef tanpil
extern long double tanpil (long double);
#endif

#ifndef NO_FLOAT128
#ifndef acospif128
extern _Float128 acospif128 (_Float128);
#endif
#ifndef asinpif128
extern _Float128 asinpif128 (_Float128);
#endif
#ifndef atanpif128
extern _Float128 atanpif128 (_Float128);
#endif
#ifndef cospif128
extern _Float128 cospif128 (_Float128);
#endif
#ifndef exp10m1f128
extern _Float128 exp10m1f128 (_Float128);
#endif
#ifndef exp2m1f128
extern _Float128 exp2m1f128 (_Float128);
#endif
#ifndef log10p1f128
extern _Float128 log10p1f128 (_Float128);
#endif
#ifndef log2p1f128
extern _Float128 log2p1f128 (_Float128);
#endif
#ifndef sinpif128
extern _Float128 sinpif128 (_Float128);
#endif
#ifndef tanpif128
extern _Float128 tanpif128 (_Float128);
#endif
#endif

#ifdef NEWLIB
/* without this, newlib says: undefined reference to `__errno' */
int errno;
int* __errno () { return &errno; }

/* cf https://sourceware.org/pipermail/newlib/2020/018027.html */
float
mylgammaf (float x)
{
  int s;
  return lgammaf_r (x, &s);
}

double
mylgamma (double x)
{
  int s;
  return lgamma_r (x, &s);
}
#else
#ifndef LLVM /* LLVM 14.0.6 doesn't have lgamma */
double
mylgamma (double x)
{
  return lgamma (x);
}

long double
mylgammal (long double x)
{
  return lgammal (x);
}
#endif
#endif

#define mpfr_tgamma mpfr_gamma

static int
mpfr_rsqrt (mpfr_t y, mpfr_t x, mpfr_rnd_t rnd)
{
  /* MPFR differs by IEEE 754-2019 for x=-0, where MPFR returns +Inf
     whereas IEEE 754-2019 gives -Inf */
  if (mpfr_zero_p (x) && mpfr_signbit (x))
  {
    mpfr_set_inf (y, -1);
    return 0;
  }
  return mpfr_rec_sqrt (y, x, rnd);
}

#ifdef ICC /* rsqrt is called invsqrt in the Intel math library */
double rsqrt (double x)
{
  return invsqrt (x);
}
long double rsqrtl (long double x)
{
  return invsqrtl (x);
}
#if defined(USE_FLOAT128) && 0
_Quad rsqrtq (_Quad x)
{
  return invsqrtq (x);
}
#endif
#endif

#define CAT1(X,Y) X ## Y
#define CAT2(X,Y) CAT1(X,Y)
#ifndef MPFR_FOO
#define MPFR_FOO CAT2(mpfr_,FOO)
#endif
#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)
#define NAME TOSTRING(FOO)

#ifdef USE_FLOAT
#if defined(USE_DOUBLE) || defined(USE_LDOUBLE) || defined(USE_FLOAT128)
#error "only one of USE_FLOAT, USE_DOUBLE, USE_LDOUBLE or USE_FLOAT128 can be defined"
#endif
#define FOO2 CAT2(FOO,f)
#define TYPE float
#define UTYPE uint32_t
#define EMAX 128
#define EMIN -149
#define PREC 24
#define mpfr_set_type mpfr_set_flt
#define mpfr_get_type mpfr_get_flt
#define TYPE_MAX FLT_MAX
#endif

#ifdef USE_DOUBLE
#if defined(USE_FLOAT) || defined(USE_LDOUBLE) || defined(USE_FLOAT128)
#error "only one of USE_FLOAT, USE_DOUBLE, USE_LDOUBLE or USE_FLOAT128 can be defined"
#endif
#define FOO2 FOO
#define TYPE double
#define UTYPE uint64_t
#define EMAX 1024
#define EMIN -1074
#define PREC 53
#define mpfr_set_type mpfr_set_d
#define mpfr_get_type mpfr_get_d
#define TYPE_MAX DBL_MAX
#endif

#ifdef USE_LDOUBLE
#if defined(USE_FLOAT) || defined(USE_DOUBLE) || defined(USE_FLOAT128)
#error "only one of USE_FLOAT, USE_DOUBLE, USE_LDOUBLE or USE_FLOAT128 can be defined"
#endif
#define FOO2 CAT2(FOO,l)
#define TYPE long double
#define UTYPE __uint128_t
#define EMAX 16384
#define EMIN -16445
#define PREC 64
#define mpfr_set_type mpfr_set_ld
#define mpfr_get_type mpfr_get_ld
#define TYPE_MAX LDBL_MAX
#endif

#ifdef USE_FLOAT128
#if defined(USE_FLOAT) || defined(USE_DOUBLE) || defined(USE_LDOUBLE)
#error "only one of USE_FLOAT, USE_DOUBLE, USE_LDOUBLE or USE_FLOAT128 can be defined"
#endif
#if defined(__INTEL_COMPILER) || defined(__INTEL_CLANG_COMPILER)
#define TYPE _Quad
#define FOO3 CAT2(__,FOO)
#define FOO2 CAT2(FOO3,q)
extern _Quad FOO2 (_Quad);
#define Q(x) (x ## q)
#else
#define FOO2 CAT2(FOO,f128)
#define TYPE _Float128
#define Q(x) (x ## f128)
#endif
#define TYPE_MAX Q(0xf.fffffffffffffffffffffffffff8p+16380)
#define UTYPE __uint128_t
#define EMAX 16384
#define EMIN -16494
#define PREC 113
#define mpfr_set_type mpfr_set_float128
#define mpfr_get_type mpfr_get_float128
#endif

#ifdef USE_FLOAT128
#if defined(__INTEL_COMPILER) || defined(__INTEL_CLANG_COMPILER)
#define MYLGAMMAF128 __mylgammaq
#define	LGAMMAF128 __lgammaq
extern _Quad __lgammaq (_Quad);
#else /* glibc */
#define MYLGAMMAF128 mylgammaf128
#define	LGAMMAF128 lgammaf128
#endif
TYPE
MYLGAMMAF128 (TYPE x)
{
  return LGAMMAF128 (x);
}
#endif

/* define "mylgamma" since the MPFR function mpfr_lgamma has an extra
   parameter s */
int
mpfr_mylgamma (mpfr_t y, mpfr_t x, mpfr_rnd_t r)
{
  int s;
  return mpfr_lgamma (y, &s, x, r);
}

static void
print_type_hex (TYPE x)
{
#ifdef USE_FLOAT
  printf ("%a", x);
#endif
#ifdef USE_DOUBLE
  printf ("%a", x);
#endif
#ifdef USE_LDOUBLE
  mpfr_t y;
  mpfr_init2 (y, PREC);
  mpfr_set_type (y, x, MPFR_RNDN);
  mpfr_printf ("%Ral", y);
  mpfr_clear (y);
#endif
#ifdef USE_FLOAT128
  mpfr_t y;
  mpfr_init2 (y, PREC);
  mpfr_set_type (y, x, MPFR_RNDN);
  mpfr_printf ("%Ra", y);
  mpfr_clear (y);
#endif
}

typedef union { UTYPE n; TYPE x; } union_t;

TYPE
get_type (UTYPE n)
{
  union_t u;
  u.n = n;
  return u.x;
}

UTYPE
get_utype (TYPE x)
{
  union_t u;
  u.x = x;
  return u.n;
}

#ifdef USE_LDOUBLE
/* cf https://en.wikipedia.org/wiki/Extended_precision */
static int
is_valid (TYPE x)
{
  UTYPE n = get_utype (x);
  int e = (n >> 64) & 0x7fff; /* exponent */
  uint64_t s = (uint64_t) n;  /* significand */
  if (e == 0) return (s >> 63) == 0;
  else return (n >> 63) & 1;
}
#endif

#ifdef LIBMVEC
/* the libmvec results were obtained with GNU libc revision
   49e2bf58d57758df244eb621d63cedd2ab6d1971 */
/* LIBMVEC should be 128 (sse4.2, default), or 256 (avx2) or 512 (avx512f) */
#if !(LIBMVEC == 128 || LIBMVEC == 256 || LIBMVEC == 512)
#error "LIBMVEC should be 128 (sse4.2) or 256 (avx2) or 512 (avx512f)"
#else
#define	LIBMVEC_N (LIBMVEC/sizeof(TYPE)/CHAR_BIT)
#endif
#endif

/* return FOO2(x) */
static TYPE
WRAPPER (TYPE x)
{
#ifdef LIBMVEC
  TYPE xx[LIBMVEC_N] = {x,}, yy[LIBMVEC_N];
  for (int i = 0; i < LIBMVEC_N; i++)
    yy[i] = FOO2 (xx[i]);
  return yy[0];
#else
  return FOO2 (x);
#endif
}

/* return the (floating-point) distance in ulps between FOO(x) and the
   infinite precision value (estimated with MPFR and double precision) */
static double
distance (TYPE x)
{
  mpfr_t xx, yy, zz;
  TYPE z;
  double ret = 0;
  int underflow = 0;
  mpfr_exp_t expz;

  if (isnan (x))
    return 0;

#ifdef USE_LDOUBLE
  if (!is_valid (x))
    return 0;
#endif

  // ignore negative integers for Newlib tgamma
#if defined(USE_DOUBLE) && defined(EXCLUDE_NEG_INT)
  if (x < 0 && round (x) == x)
    return 0;
#endif

  mpfr_exp_t emax = mpfr_get_emax ();
  mpfr_set_emax (EMAX);
  /* the rounding mode of the current thread is set in doit() */
  z = WRAPPER (x);
  mpfr_init2 (xx, PREC);
  mpfr_init2 (yy, PREC+1);
  mpfr_init2 (zz, 3*PREC);
  mpfr_set_type (xx, x, MPFR_RNDN);
  MPFR_FOO (zz, xx, rnd);
  if (isnan (z) && mpfr_nan_p (zz))
    goto end;
  if (isnan (z) && !mpfr_nan_p (zz))
  {
    ret = +INFINITY;
    goto end;
  }
  if (!isnan (z) && mpfr_nan_p (zz))
  {
    ret = +INFINITY;
    goto end;
  }
  mpfr_set_emax (mpfr_get_emax_max ());
  if (isinf (z) && mpfr_inf_p (zz) == 0)
    {
      /* libm returns Inf, MPFR not, we assume the libm rounded to the next
         number after the largest finite number in the format */
      mpfr_set_ui_2exp (yy, 1, EMAX, MPFR_RNDN);
      if (z < 0)
        mpfr_neg (yy, yy, MPFR_RNDN);
    }
  else if (!isinf (z) && mpfr_inf_p (zz))
  {
    /* MPFR yields Inf, but libm no: we recompute the MPFR result with
       unbounded exponent */
    MPFR_FOO (zz, xx, rnd);
    expz = mpfr_get_exp (zz);
    underflow = expz <= EMIN;
    mpfr_set_type (yy, z, MPFR_RNDN);
  }
  else
    mpfr_set_type (yy, z, MPFR_RNDN);

  if (mpfr_number_p (zz))
  {
    if (mpfr_zero_p (zz))
      underflow = 1;
    else
    {
      expz = mpfr_get_exp (zz);
      underflow = expz <= EMIN;
    }
  }
  mpfr_sub (zz, zz, yy, MPFR_RNDN);
  mpfr_abs (zz, zz, MPFR_RNDN);
  /* we divide by the ulp of the correctly rounded value (zz)
     which is 2^(expz-PREC) if zz is normalized, and 2^EMIN otherwise */
  if (mpfr_number_p (zz) && !underflow && expz - PREC >= EMIN)
    mpfr_mul_2si (zz, zz, PREC - expz, MPFR_RNDN);
  else
    mpfr_mul_2si (zz, zz, -EMIN, MPFR_RNDN);
  ret = mpfr_get_d (zz, MPFR_RNDU);

 end:
  mpfr_clear (xx);
  mpfr_clear (yy);
  mpfr_clear (zz);
  mpfr_set_emax (emax);
  return ret;
}

/* return the (integer) distance in ulps between FOO(x) and the value computed
   by MPFR */
static double
ulps (TYPE x)
{
  mpfr_t xx, yy, zz;
  TYPE z;
  double ret;
  int underflow = 0;
  mpfr_exp_t expz;

#ifdef USE_LDOUBLE
  if (!is_valid (x))
    return 0;
#endif

  mpfr_set_emax (EMAX);
  /* the rounding mode of the current thread is set in doit() */
  z = WRAPPER (x);
  mpfr_init2 (xx, PREC);
  mpfr_init2 (yy, PREC+1);
  mpfr_init2 (zz, PREC);
  mpfr_set_type (xx, x, MPFR_RNDN);
  MPFR_FOO (zz, xx, rnd);
  expz = mpfr_get_exp (zz);
  underflow = expz <= EMIN;
  if (isinf (z) && mpfr_inf_p (zz) == 0)
    {
      /* set yy to (1-2^-(PREC+1))*2^EMAX */
      mpfr_set_ui_2exp (yy, 1, EMAX, MPFR_RNDN);
      mpfr_nextbelow (yy);
    }
  else
    mpfr_set_type (yy, z, MPFR_RNDN);
  mpfr_sub (zz, zz, yy, MPFR_RNDN);
  mpfr_abs (zz, zz, MPFR_RNDN);
  /* we divide by the ulp of the correctly rounded value (zz)
     which is 2^(ez-PREC) if zz is normalized, and 2^EMIN otherwise */
  if (PREC - expz <= -EMIN)
    mpfr_mul_2si (zz, zz, PREC - expz, MPFR_RNDN);
  else
    mpfr_mul_2si (zz, zz, -EMIN, MPFR_RNDN);
  ret = (underflow) ? (double) 0.0 : mpfr_get_d (zz, MPFR_RNDU);
  mpfr_clear (xx);
  mpfr_clear (yy);
  mpfr_clear (zz);
  return ret;
}

uint64_t threshold = 1000;
double Threshold;

static unsigned int Seed[MAX_THREADS];

uint32_t my_rand_wrapper (int tid)
{
  uint32_t val = 0;

#ifdef ARM_USE_HARDWARE_RNG
  /* Utilize hardware RNG instructions on the ARM processors that support them.
   *
   * During profiling, it was discovered that almost all of the processing time
   * was spent servicing the rand calls due to continual exhaustion of the
   * available system entropy. Using hardware RNG resolved this issue.
   *
   * TODO: The rndr instruction returns a 64 bit unsigned integer, of which we
   *       throw out 32 bits in order to be compatible with the existing code
   *       as it was written with rand() in mind, which returns a 32 bit
   *       integer. Ideally, we would refactor the code so that we don't throw
   *       out the bits just to request another 32 bits to build a full 64 bit
   *       value when USE_DOUBLE is defined. */
  uint64_t temp = 0;
  int res = __rndr(&temp);
  assert(res == 0);
  val = (uint32_t)temp;
#else
  val = rand_r (Seed + tid);
#endif

  return val;
}

#if RAND_MAX == 2147483647

#ifdef USE_FLOAT
static uint32_t
my_random (uint32_t n, int tid)
{
  uint32_t ret = my_rand_wrapper (tid);
  if (n > RAND_MAX)
    ret = (ret << 31) | my_rand_wrapper (tid);
  return ret % n;
}
#endif
#ifdef USE_DOUBLE
static uint64_t
my_random (uint64_t n, int tid)
{
  uint64_t ret = my_rand_wrapper (tid);
  if (n > RAND_MAX)
    {
      ret = (ret << 31) | my_rand_wrapper (tid);
      /* now ret <= 2^62-1 */
      if (n > 0x3fffffffffffffffLU)
        ret = (ret << 31) | my_rand_wrapper (tid);
    }
  return ret % n;
}
#endif
#if defined(USE_LDOUBLE) || defined(USE_FLOAT128)
static __uint128_t
my_random (__uint128_t n, int tid)
{
  __uint128_t ret = my_rand_wrapper (tid);
  if (n > RAND_MAX)
    {
      ret = (ret << 31) | my_rand_wrapper (tid);
      /* now ret <= 2^62-1 */
      if (n >> 62)
        {
          ret = (ret << 31) | my_rand_wrapper (tid);
          /* now ret <= 2^93-1 */
          if (n >> 93)
            {
              ret = (ret << 31) | my_rand_wrapper (tid);
              /* now ret <= 2^93-1 */
              if (n >> 124)
                ret = (ret << 31) | my_rand_wrapper (tid);
            }
        }
    }
  return ret % n;
}
#endif

#elif RAND_MAX == 0x7fff /* Microsoft Visual C/C++ */

#ifdef USE_FLOAT
static uint32_t
my_random (uint32_t n, int tid)
{
  uint32_t ret = my_rand_wrapper (tid);
  if (n > RAND_MAX)
    {
      ret = (ret << 15) | my_rand_wrapper (tid);
      /* now ret <= 2^30-1 */
      if (n > 0x3fffffffLU)
        ret = (ret << 15) | my_rand_wrapper (tid);
    }
  return ret % n;
}
#endif
#ifdef USE_DOUBLE
static uint64_t
my_random (uint64_t n, int tid)
{
  uint64_t ret = my_rand_wrapper (tid);
  if (n > RAND_MAX)
    {
      ret = (ret << 15) | my_rand_wrapper (tid);
      /* now ret <= 2^30-1 */
      if (n >> 30)
        {
          ret = (ret << 15) | my_rand_wrapper (tid);
          /* now ret <= 2^45-1 */
          if (n >> 45)
            {
              ret = (ret << 15) | my_rand_wrapper (tid);
              /* now ret <= 2^60-1 */
              if (n >> 60)
                ret = (ret << 15) | my_rand_wrapper (tid);
            }
        }
    }
  return ret % n;
}
#endif
#if defined(USE_LDOUBLE) || defined(USE_FLOAT128)
#error "my_random() not yet implemented for 128-bit type"
#endif

#else
#error "Unexpected value of RAND_MAX"
#endif

TYPE Xmax;
double Dbest = 0.0;
int Rbest = -1;
int mode_best = 0;

static void
print_error (double d)
{
  mpfr_t x;
  mpfr_init2 (x, 53);
  mpfr_set_d (x, d, MPFR_RNDU);
  if (d < 0.999)
    mpfr_printf ("%.3RUf", x);
  else if (d < 9.99)
    mpfr_printf ("%.2RUf", x);
  else if (d < 99.9)
    mpfr_printf ("%.1RUf", x);
  else
    mpfr_printf ("%.2RUe", x);
  mpfr_clear (x);
}

/* return the maximal error on [nxmin,nxmax],
   and update nxbest if it improves distance(get_type(*nxbest)) */
static double
max_heuristic1 (UTYPE nxmin, UTYPE nxmax, UTYPE *nxbest, int tid)
{
  TYPE x;
  UTYPE nx, i;
  double dbest, dmax = 0, d;
  x = get_type (*nxbest);
  dbest = distance (x);
  /* if the best value so far is in [nxmin,nxmax], use it */
  dmax = (nxmin <= *nxbest && *nxbest < nxmax) ? dbest : 0;
  for (i = 0; i < threshold; i++)
    {
      nx = nxmin + my_random (nxmax - nxmin, tid);
      x = get_type (nx);
      d = distance (x);
      if (d > dmax)
        {
          dmax = d;
          if (d > dbest)
            {
              dbest = d;
              *nxbest = nx;
            }
        }
    }
  return dmax;
}

/* return the average error on [nxmin,nxmax],
   and update nxbest if it improves distance(get_type(*nxbest)) */
static double
max_heuristic2 (UTYPE nxmin, UTYPE nxmax, UTYPE *nxbest, int tid)
{
  TYPE x;
  double dbest, d, s = 0, n = 0;
  UTYPE i, nx;
  x = get_type (*nxbest);
  dbest = distance (x);
  for (i = 0; i < threshold; i++)
    {
      nx = nxmin + my_random (nxmax - nxmin, tid);
      x = get_type (nx);
      d = distance (x);
      if (d != 0)
        {
          s += d;
          n ++;
        }
      if (d > dbest)
        {
          dbest = d;
          *nxbest = nx;
        }
    }
  if (n != 0)
    s = s / n;
  return s;
}

/* some libraries do not have log(), for example llvm */
static double mylog (double n)
{
  mpfr_t x;
  mpfr_init2 (x, 53);
  mpfr_set_d (x, n, MPFR_RNDN);
  mpfr_log (x, x, MPFR_RNDN);
  double ret = mpfr_get_d (x, MPFR_RNDN);
  mpfr_clear (x);
  return ret;
}

// define our own sqrt to avoid adding -lm
static double mysqrt (double n)
{
  mpfr_t x;
  mpfr_init2 (x, 53);
  mpfr_set_d (x, n, MPFR_RNDN);
  mpfr_sqrt (x, x, MPFR_RNDN);
  double ret = mpfr_get_d (x, MPFR_RNDN);
  mpfr_clear (x);
  return ret;
}

/* return the estimated maximal error on [nxmin,nxmax], taking into account
   mean and standard deviation,
   and update nxbest if it improves distance(get_type(*nxbest)) */
static double
max_heuristic3 (UTYPE nxmin, UTYPE nxmax, UTYPE *nxbest, int tid)
{
  TYPE x;
  UTYPE i, nx;
  double dbest, d, s = 0, v = 0, n = 0;
  x = get_type (*nxbest);
  dbest = distance (x);
  for (i = 0; i < threshold; i++)
    {
      nx = nxmin + my_random (nxmax - nxmin, tid);
      x = get_type (nx);
      d = distance (x);
      if (d != 0)
        {
          s += d;
          v += d * d;
          n ++;
        }
      if (d > dbest)
        {
          dbest = d;
          *nxbest = nx;
        }
    }
  /* compute mean and standard deviation */
  if (n != 0)
    {
      s = s / n;
      v = v / n - s * s;
      if (v < 0)
        v = 0;
      double sigma = mysqrt (v);
      /* we got n non-zero values out of threshold, thus we should get
         n/threshold*(nxmax-nxmin) */
      n = n * (double) (nxmax - nxmin) / (double) threshold;
      double logn = mylog (n);
      double t = mysqrt (2.0 * logn);
      /* Reference: A note on the first moment of extreme order statistics
         from the normal distribution, Max Petzold,
         https://gupea.ub.gu.se/handle/2077/3092 */
      s = s + sigma * (t - (mylog (logn) + 1.3766) / (2.0 * t));
    }
  return s;
}

#ifdef NO_OPENMP
static int
omp_get_num_threads (void)
{
  return 1;
}

static int
omp_get_thread_num (void)
{
  return 0;
}

static void
omp_set_num_threads (int i)
{
}
#endif

static int
mode (void)
{
  if (use_mode != -1)
    return use_mode;
  int i = omp_get_thread_num ();
  /* Modes 1 and 2 seem to be less efficient (for example for acosh with
     threshold=10000 they give only 0.983603 whereas mode 0 gives 2.18-2.19),
     thus we use only one thread on each. */
  if (i == 1 || i == 2)
    return i;
  return 0;
}

#ifdef STAT
unsigned long eval_heuristic = 0;
unsigned long eval_exhaustive = 0;
#endif

static double
max_heuristic (UTYPE nxmin, UTYPE nxmax, UTYPE *nxbest, int tid)
{
  int k = mode ();

#ifdef STAT
#pragma omp atomic update
  eval_heuristic += threshold;
#endif

  if (k == 0)
    return max_heuristic1 (nxmin, nxmax, nxbest, tid);
  else if (k == 1)
    return max_heuristic2 (nxmin, nxmax, nxbest, tid);
  else /* k = 2 */
    return max_heuristic3 (nxmin, nxmax, nxbest, tid);
}

typedef struct {
  UTYPE nxmin, nxmax;
  double d;
#ifdef RANK
  int rank; /* worst rank */
#endif
} chunk_t;

static double
chunk_size (chunk_t c)
{
  return (double) (c.nxmax - c.nxmin);
}

static void
chunk_swap (chunk_t *a, chunk_t *b)
{
  UTYPE tmp;
  tmp = a->nxmin; a->nxmin = b->nxmin; b->nxmin = tmp;
  tmp = a->nxmax; a->nxmax = b->nxmax; b->nxmax = tmp;
  double ump;
  ump = a->d; a->d = b->d; b->d = ump;
#ifdef RANK
  int rmp;
  rmp = a->rank; a->rank = b->rank; b->rank = rmp;
#endif
}

typedef struct {
  chunk_t *l;
  int size;
} List_struct;
typedef List_struct List_t[1];

/* Idea from Eric Schneider: instead of sampling only one interval at each
   level of the binary splitting tree, we sample up to LIST_ALLOC intervals,
   and keep the most promising ones. */
#define LIST_ALLOC 30

static void
List_init (List_t L)
{
  L->l = (chunk_t*) malloc (LIST_ALLOC * sizeof (chunk_t));
  L->size = 0;
}

static void
List_init2 (List_t L, UTYPE nxmin, UTYPE nxmax)
{
  L->l = (chunk_t*) malloc (LIST_ALLOC * sizeof (chunk_t));
  L->l[0].nxmin = nxmin;
  L->l[0].nxmax = nxmax;
  L->size = 1;
}

#if 0
static void
List_print (List_t L)
{
  int i;
  for (i = 0; i < L->size; i++)
    printf ("%.3g ", L->l[i].d);
  printf ("\n");
}

static void
List_check (List_t L)
{
  int i;
  for (i = 1; i < L->size; i++)
    if (L->l[i-1].d < L->l[i].d)
      {
        fprintf (stderr, "Error, list not sorted:\n");
        List_print (L);
        exit (1);
      }
}
#endif

static void
List_insert (List_t L, UTYPE nxmin, UTYPE nxmax, double d)
{
  int i = L->size;
  if (i < LIST_ALLOC || d > L->l[LIST_ALLOC-1].d)
    {
      /* if list is not full, we insert at position i,
         otherwise we insert at position LIST_ALLOC-1 */
      if (i == LIST_ALLOC) /* replaces previous chunk */
        i--;
      else
        L->size++;
      L->l[i].nxmin = nxmin;
      L->l[i].nxmax = nxmax;
      L->l[i].d = d;
      /* now insertion sort */
      while (i > 0 && d > L->l[i-1].d)
        {
          chunk_swap (L->l + (i-1), L->l + i);
#ifdef RANK
          if (L->l[i].rank < i)
            L->l[i].rank = i; /* updates maximal rank */
#endif
          i--;
        }
#ifdef RANK
      L->l[i].rank = i; /* set initial rank */
#endif
    }
  // List_check (L);
}

static void
List_swap (List_t L1, List_t L2)
{
  chunk_t *tmp;
  tmp = L1->l; L1->l = L2->l; L2->l = tmp;
  int ump;
  ump = L1->size; L1->size = L2->size; L2->size = ump;
}

static void
List_clear (List_t L)
{
  free (L->l);
}

static void
exhaustive_search (chunk_t *c, UTYPE *nxbest, double *dbest, int *rbest)
{
  UTYPE nx;
  for (nx = c->nxmin; nx < c->nxmax; nx ++)
    {
      TYPE x = get_type (nx);
      double d = distance (x);
      if (d > *dbest)
        {
          *dbest = d;
          *nxbest = nx;
#ifdef RANK
          *rbest = c->rank;
#endif
        }
    }
#ifdef STAT
#pragma omp atomic update
  eval_exhaustive += c->nxmax - c->nxmin;
#endif
}

/* search for nxmin <= nx < nxmax
   where the worst found so far is (nxbest,dbest) */
static void
search (UTYPE nxmin, UTYPE nxmax, UTYPE *nxbest, double *dbest, int *rbest)
{
  int tid = omp_get_thread_num ();
  List_t L;
  
  List_init2 (L, nxmin, nxmax);
  while (1)
    {
      assert (1 <= L->size && L->size <= LIST_ALLOC);
      double width = chunk_size (L->l[0]);
      if (width <= Threshold) /* exhaustive search */
        {
          int i;
          for (i = 0; i < L->size; i++)
            exhaustive_search (L->l + i, nxbest, dbest, rbest);
          break;
        }
      else /* split each chunk in two */
        {
          List_t NewL;
          int i;
          List_init (NewL);
          for (i = 0; i < L->size; i++)
            {
              UTYPE nxmin = L->l[i].nxmin;
              UTYPE nxmax = L->l[i].nxmax;
              UTYPE nxmid = nxmin + (nxmax - nxmin) / 2;
              double d1 = max_heuristic (nxmin, nxmid, nxbest, tid);
              List_insert (NewL, nxmin, nxmid, d1);
              double d2 = max_heuristic (nxmid, nxmax, nxbest, tid);
              List_insert (NewL, nxmid, nxmax, d2);
            }
          List_swap (L, NewL);
#ifdef STAT
          printf ("L: "); List_print (L);
#endif
          List_clear (NewL);
        }
    }
  List_clear (L);
}

static void
setround (int rnd)
{
#ifndef CRLIBM
  fesetround (rnd);
#endif
}

static void
doit (unsigned int seed)
{
  UTYPE nxbest = 0;
  double dbest = 0;
  int rbest = -1;

  /* set the random seed of the current thread */
  srand (seed);

  /* set the rounding mode of the current thread */
  setround (rnd1[rnd]);

  /* for thread 0, get the "worst" value so far as initial point */
  if (omp_get_thread_num () == 0)
    {
      dbest = Dbest;
      nxbest = get_utype (Xmax);
    }

  /* assume -xxx_MAX has the largest encoding */
  UTYPE bound = get_utype (-TYPE_MAX) + 1;
  search (0, bound, &nxbest, &dbest, &rbest);
#pragma omp critical
  if (dbest > Dbest)
    {
      Dbest = dbest;
#ifdef RANK
      Rbest = rbest;
#endif
      mode_best = mode ();
      Xmax = get_type (nxbest);
    }
  mpfr_free_cache (); /* free cache of current thread */
}

static void
init_Threshold (void)
{
  UTYPE bound = get_utype (-TYPE_MAX) + 1;
  double w = (double) bound; /* width of current interval */
  double s = 0;              /* number of evaluations so far */
  while (s < w)
    {
      w = w / 2.0; // cut in 2
      s += 2 * (double) threshold; // use threshold evaluations in each half
    }
  Threshold = w;
}

int
main (int argc, char *argv[])
{
  int verbose = 0;
  unsigned int seed = 0;
  int nthreads = 0;
  double worst_input = 0;

#ifdef CRLIBM
  /* We should call the crlibm_init() function, which sets the rounding
     precision to double. */
  crlibm_init ();
#endif

  while (argc >= 2 && argv[1][0] == '-')
    {
      if (argc >= 3 && strcmp (argv[1], "-threshold") == 0)
        {
          threshold = strtoul (argv[2], NULL, 10);
          argv += 2;
          argc -= 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-seed") == 0)
        {
#ifdef ARM_USE_HARDWARE_RNG
  printf("Warning: Unable to set the seed when hardware RNG is utilized.\n");
#endif
          seed = strtoul (argv[2], NULL, 10);
          argv += 2;
          argc -= 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-mode") == 0)
        {
          use_mode = strtoul (argv[2], NULL, 10);
          assert (0 <= use_mode && use_mode <= 2);
          argv += 2;
          argc -= 2;
        }
      else if (strcmp (argv[1], "-v") == 0)
        {
          verbose = 1;
          argv ++;
          argc --;
        }
      else if (strcmp (argv[1], "-rndn") == 0)
        {
          rnd = (mpfr_rnd_t) 0;
          argc --;
          argv ++;
        }
      else if (strcmp (argv[1], "-rndz") == 0)
        {
          rnd = (mpfr_rnd_t) 1;
          argc --;
          argv ++;
        }
      else if (strcmp (argv[1], "-rndu") == 0)
        {
          rnd = (mpfr_rnd_t) 2;
          argc --;
          argv ++;
        }
      else if (strcmp (argv[1], "-rndd") == 0)
        {
          rnd = (mpfr_rnd_t) 3;
          argc --;
          argv ++;
        }
      else if (argc >= 3 && strcmp (argv[1], "-nthreads") == 0)
        {
          nthreads = atoi (argv[2]);
          argc -= 2;
          argv += 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-worst") == 0)
        {
          sscanf (argv[2], "%la", &worst_input);
          argc -= 2;
          argv += 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-extra") == 0)
        {
          extra_file = fopen (argv[2], "r");
          argv += 2;
          argc -= 2;
        }
      else
        {
          fprintf (stderr, "Unknown option %s\n", argv[1]);
          exit (1);
        }
    }
  assert (threshold > 0);
  /* divide threshold by LIST_ALLOC so that the total number of evalutions
     does not vary with LIST_ALLOC */
  threshold = 1 + (threshold - 1) / LIST_ALLOC;
  init_Threshold ();

#ifdef GLIBC
  if (verbose)
    {
      printf("GNU libc version: %s\n", gnu_get_libc_version ());
      printf("GNU libc release: %s\n", gnu_get_libc_release ());
    }
#endif
#ifdef APPLE
  if (verbose)
  {
    /* for a full list of available info and selectors, type sysctl -a
       (thanks Ian Ollmann) */
    size_t len;
    sysctlbyname ("kern.osproductversion", NULL, &len, NULL, 0);
    char *p = malloc (len);
    sysctlbyname("kern.osproductversion", p, &len, NULL, 0);
    sysctlbyname ("kern.osversion", NULL, &len, NULL, 0);
    char *q = malloc (len);
    sysctlbyname("kern.osversion", q, &len, NULL, 0);
    sysctlbyname("machdep.cpu.brand_string", NULL, &len, NULL, 0);
    char *s = malloc (len);
    sysctlbyname("machdep.cpu.brand_string", s, &len, NULL, 0);
    printf ("MacOS %s on %s (kern.osversion=%s)\n", p, s, q);
    sysctlbyname ("kern.version", NULL, &len, NULL, 0);
    char *r = malloc (len);
    sysctlbyname("kern.version", r, &len, NULL, 0);
    printf ("%s\n", r);
    free (p);
    free (q);
    free (r);
    free (s);
  }
#endif
#ifdef MSC_VER
  if (verbose)
    printf ("Using Microsoft math library %d\n", MSC_VER);
#endif

  if (seed == 0)
    seed = getpid ();
  if (verbose)
    printf ("Using seed %u\n", seed);

#if defined(USE_FLOAT) || defined(USE_DOUBLE) || defined(USE_LDOUBLE)
#define NLIBS 12
#else
#define	NLIBS 2 /* only glibc and icc do provide binary128 */
#endif
#define SIZE (41*NLIBS)

#ifdef WORST
/* since CRLIBM is correctly rounded, we should not find any error larger
   than 0.5 ulp for rounding to nearest, or 1 ulp for directed rounding */
#if defined(GLIBC) || defined(CRLIBM)
#define NUMBER 0
#endif
#ifdef ICC
#define NUMBER 1
#endif
#ifdef AMD
#define NUMBER 2
#endif
#ifdef NEWLIB
#define NUMBER 3
#endif
#ifdef OPENLIBM
#define NUMBER 4
#endif
#ifdef MUSL
#define NUMBER 5
#endif
#ifdef APPLE
#define NUMBER 6
#endif
#ifdef LLVM
#define NUMBER 7
#endif
#ifdef LIBMVEC
#define NUMBER 8
#endif
#ifdef MSVC
#define NUMBER 9
#endif
#ifdef FREEBSD
#define NUMBER 10
#endif
#ifdef ARM
#define NUMBER 11
#endif
#ifndef NUMBER
#error "NUMBER undefined"
#endif
#define SIZE_EXTRA 100
#ifdef USE_FLOAT
TYPE worst[SIZE] = {
  0,
  };
TYPE extra[SIZE_EXTRA] = {
  0,
  };
#endif

#ifdef USE_DOUBLE
  TYPE worst[SIZE] = {
    /* acos */
    0x1.dffffb3488a4p-1,    /* glibc */
    0x1.6c05eb219ec46p-1,   /* icc */
    0x1.35b03e336a82bp-1,   /* AMD LibM */
    -0x1.0068b067c6feep-1,  /* Newlib */
    -0x1.0068b067c6feep-1,  /* OpenLibm */
    -0x1.0068b067c6feep-1,  /* Musl */
    -0x1.8d313198a2e03p-53, /* Apple 1.05154 */
    0,                      /* llvm */
#if LIBMVEC==128 /* sse4.2 */
    0x1.35a0de2b038fep-1,   /* libmvec 2.30611 */
#elif LIBMVEC==256 /* avx2 */
    0x1.ffc00159839aep-1,   /* libmvec 2.05784 */
#else /* avx512 */
    0x1.35b9bac9f42c6p-1,   /* libmvec 1.82629 */
#endif
    -0x1.010fd0ad6aa41p-1,  /* msvc-x64-acos 0.933759 */
    -0x1.0068b067c6feep-1,  /* freebsd 0.929812 */
    0x1.251869c3f7881p-1,   /* ArmPL 1.51061 */

    /* acosh */
    0x1.0001ff6afc4bap+0,   /* GNU libc */
    0x1.01825ca7da7e5p+0,   /* icx 0.508764 */
    0x1.209fae707a0edp+0,   /* AMD LibM */
    0x1.0001ff6afc4bap+0,   /* Newlib */
    0x1.0001ff6afc4bap+0,   /* OpenLibm */
    0x1.0001ff6afc4bap+0,   /* Musl 1.2.1 */
    0x1.00007fb3703ddp+0,   /* Apple 2.24182 */
    0,                      /* llvm */
#if LIBMVEC==128 /* sse4.2 */
    0x1.001ffe4f6d239p+0,   /* libmvec 3.31361 */
#elif LIBMVEC==256 /* avx2 */
    0x1.007ff5e6aae25p+0,   /* libmvec 3.28513 */
#else /* avx512 */
    0x1.0007ffe4f42f8p+0,   /* libmvec 1.98412 */
#endif
    0x1.0007fd4307b75p+0,   /* msvc-x64-acosh 3.21636 */
    0x1.0001ff6afc4bap+0,   /* freebsd 2.24122 */
    0x1.071334daf83adp+0,   /* ArmPL 2.65022 */

    /* asin */
    -0x1.0000045b2c904p-3,  /* glibc */
    0x1.6c042a6378102p-1,   /* icc 19.1.3.304 */
    -0x1.00d44cccfa99p-1,   /* AMD LibM */
    -0x1.004d1c5a9400bp-1,  /* Newlib */
    -0x1.004d1c5a9400bp-1,  /* OpenLibm */
    -0x1.004d1c5a9400bp-1,  /* Musl 1.2.1 */
    0x1.eae75e3d82b6fp-2,   /* Apple 0.708864 */
    0,                      /* llvm */
#if LIBMVEC==128 /* sse4.2 */
    0x1.000c80481e7f9p-1,   /* libmvec 3.48486 */
#elif LIBMVEC==256 /* avx2 */
    0x1.000fb59dbb7ffp-1,   /* libmvec 2.95635 */
#else /* avx512 */
    -0x1.0312655c1d169p-1,  /* libmvec 2.69006 */
#endif
    -0x1.0239000439deep-1,  /* msvc-x64-asin 1.04134 */
    -0x1.004d1c5a9400bp-1,  /* freebsd 0.980823 */
    0x1.0479b37d95e5cp-1,   /* ArmPL 2.68343 */

    /* asinh */
    -0x1.02657ff36d5f3p-2,  /* GNU libc */
    -0x1.000276d9cf31ap-4,  /* icx 0.506163 */
    0x1.005ae8d126f7ep+0,   /* AMD LibM */
    -0x1.02657ff36d5f3p-2,  /* Newlib */
    -0x1.02657ff36d5f3p-2,  /* OpenLibm */
    -0x1.0240f2bdb3f25p-2,  /* Musl 1.2.1 */
    -0x1.fdefd03df4cd7p-3,  /* Apple 1.57940 */
    0,                      /* llvm */
#if LIBMVEC==128 /* sse4.2 */
    -0x1.fff13ade9918p-12,  /* libmvec 3.58616 */
#elif LIBMVEC==256 /* avx2 */
    0x1.fffdfee9d0656p-12,  /* libmvec 3.58591 */
#else /* avx512 */
    -0x1.fff14d29165f4p-8,  /* libmvec 1.52036 */
#endif
    -0x1.00212bb59c31ep-4,  /* msvc-x64-asinh 2.04895 */
    -0x1.02657ff36d5f3p-2,  /* freebsd 1.91356 */
    -0x1.000eeed78380ap+0,  /* ArmPL 2.03455 */

    /* atan */
    0x1.f9004c4fef9eap-4,   /* glibc */
    -0x1.ffff8020d3d1dp-7,  /* icc */
    -0x1.60370d15396b7p-1,  /* AMD LibM 0.862182 */
    0x1.62ff6a1682c25p-1,   /* Newlib */
    0x1.62ff6a1682c25p-1,   /* OpenLibm */
    0x1.62ff6a1682c25p-1,   /* Musl */
    0x1.01e0be37af68fp+1,   /* Apple 0.875241 */
    0,                      /* llvm */
#if LIBMVEC==128 /* sse4.2 */
    0x1.000a5e848c0dp-3,    /* libmvec 2.64340 */
#elif LIBMVEC==256 /* avx2 */
    0x1.0029e0e2db7dp-3,    /* libmvec 2.64176 */
#else /* avx512 */
    -0x1.0010aea41501p-3,   /* libmvec 2.64024 */
#endif
    -0x1.60370d15396b7p-1,  /* msvc-x64-atan 0.862182 */
    0x1.62ff6a1682c25p-1,   /* freebsd 0.860246 */
    0x1.032b4811f3dc5p+0,   /* ArmPL 2.23722 */

    /* atanh */
    -0x1.ebb5133a9d9a4p-4,  /* GNU libc */
    -0x1.e2cfb2667f17ep-9,  /* icc */
    -0x1.d8fb311a52173p-2,  /* AMD LibM */
    -0x1.f97fabc0650c4p-4,  /* Newlib */
    -0x1.f97fabc0650c4p-4,  /* OpenLibm */
    -0x1.f8a404597baf4p-4,  /* Musl 1.2.1 */
    0x1.ffd834a270fp-10,    /* Apple 2.00021 */
    0,                      /* llvm */
#if LIBMVEC==128 /* sse4.2 */
    -0x1.fff0caf4c48dp-13,  /* libmvec 3.58751 */
#elif LIBMVEC==256 /* avx2 */
    -0x1.ffe2abaa5690dp-13, /* libmvec 3.58538 */
#else /* avx512 */
    0x1.85cb7cc1e1318p-6,   /* libmvec 1.51047 */
#endif
    -0x1.ffbe8dd88527fp-9,  /* msvc-x64-atanh 2.49417 */
    -0x1.f97fabc0650c4p-4,  /* freebsd 1.80512 */
    -0x1.e7c1f36602014p-4,  /* ArmPL 2.9904 */

    /* cbrt */
    0x1.7a337e1ba1ec2p-257,  /* GNU libc */
    -0x1.f7af4893d1d51p-616, /* icc */
    0x0.7ffffffffffffp-1022, /* AMD LibM */
    -0x1.00ddafe7d9deep-885, /* Newlib */
    -0x1.13a5ccd87c9bbp+1008,/* OpenLibm */
    -0x1.13a5ccd87c9bbp+1008,/* Musl */
    0x1.fed1fe9f1122dp+11,   /* Apple 0.728127 */
    0x1.fffffffffffffp-1,    /* llvm 4.51e+15 */
#if LIBMVEC==128 /* sse4.2 */
    -0x0.bd54cbc41f0b9p-1022,/* libmvec 3.42039 */
#elif LIBMVEC==256 /* avx2 */
    0x0.bdf2e4b035cc5p-1022, /* libmvec 3.40348 */
#else /* avx512 */
    0x1.477fc84889eabp-511,  /* libmvec 1.83354 */
#endif
    -0x1.55cd285f321f6p-64,  /* msvc-x64-cbrt 1.85080 */
    -0x1.13a5ccd87c9bbp+1008,/* freebsd 0.66715 */
    0x1.fffad1cec59fep-332,  /* ArmPL 1.78544 */

    /* cos */
    -0x1.7120161c92674p+0,   /* GNU libc */
    -0x1.d19ebc5567dcdp+311, /* icc */
    0x1.91e60af551108p-1,    /* AMD LibM */
    -0x1.4ae182c1ab422p+21,  /* Newlib */
    -0x1.34e729fd08086p+21,  /* OpenLibm */
    -0x1.34e729fd08086p+21,  /* Musl */
    0x1.2f29eb4e99fa2p+7,    /* Apple 0.947350 */
    -0x1.6a09e667f3bccp-27,  /* llvm 0.5 */
#if LIBMVEC==128 /* sse4.2 */
    0x1.852e715836081p+18,   /* libmvec 3.84216 */
#elif LIBMVEC==256 /* avx2 */
    -0x1.f5ec1ef4d1fb8p+3,   /* libmvec 3.66518 */
#else /* avx512 */
    -0x1.9a4f79002782p-6,    /* libmvec 3.65239 */
#endif
    -0x1.9200634d4471fp-1,   /* msvc-x64-cos 0.896140 */
    -0x1.34e729fd08086p+21,  /* freebsd 0.833135 */
    -0x1.7120161c92674p+0,   /* ArmPL 0.515628 */

    /* cosh */
    -0x1.633c654fee2bap+9,   /* GNU libc */
    -0x1.5a364e6b98134p+9,   /* icc */
    0x1.ff76fb3f476d5p+0,    /* AMD LibM */
    0x1.633cc2ae1c934p+9,    /* Newlib */
    -0x1.6310ab92794a8p+9,   /* OpenLibm */
    -0x1.502bf5ad80729p+0,   /* Musl */
    -0x1.62dabd4848dc4p-2,   /* Apple 0.522287 */
    0,                       /* llvm */
#if LIBMVEC==128 /* sse4.2 */
    -0x1.633c654fee2bap+9,   /* libmvec 1.92222 */
#elif LIBMVEC==256 /* avx2 */
    -0x1.633c654fee2bap+9,   /* libmvec 1.92222 */
#else /* avx512 */
    -0x1.2b3088f4a6e98p+4,   /* libmvec 2.02425 */
#endif
    -0x1.1ff088806d82ep+3,   /* msvc-x64-cosh 1.90245 */
    -0x1.6310ab92794a8p+9,   /* freebsd 1.46619 */
    -0x1.628af341989dap+9,   /* ArmPL 1.92568 */

    /* erf */
    0x1.c332bde7ca515p-5,    /* GNU libc */
    -0x1.c5bcc48b2d463p-21,  /* icc */
    0x1.11642f2eab9edp+0,    /* AMD LibM 0.999489 */
    -0x1.c57541b55c8ebp-16,  /* Newlib */
    -0x1.c57541b55c8ebp-16,  /* OpenLibm */
    -0x1.c57541b55c8ebp-16,  /* Musl 1.2.1 */
    -0x1.e057e7a0e494cp-2,   /* Apple 6.40351 */
    0,                       /* llvm */
#if LIBMVEC==128 /* sse4.2 */
    0x1.0000b7af4dcdp-8,     /* libmvec 2.54483 */
#elif LIBMVEC==256 /* avx2 */
    0x1.00005abf94234p-8,    /* libmvec 2.54487 */
#else /* avx512 */
    0x1.00001d2920fb7p-8,    /* libmvec 2.54487 */
#endif
    0x1.755dca4d8b458p+0,    /* msvc-x64-erf 4.61892 */
    -0x1.c57541b55c8ebp-16,  /* freebsd 1.01967 */
    0x1.0000187085e56p-8,    /* ArmPL 2.2867 */

    /* erfc */
    0x1.3ff2d63705b29p+0,    /* GNU libc */
    0x1.a8cf81fa2a28fp+4,    /* icx */
    0,                       /* AMD LibM */
    0x1.5182d8799b84bp+0,    /* Newlib */
    0x1.5182d8799b84bp+0,    /* OpenLibm */
    0x1.527f4fb0d9331p+0,    /* Musl 1.2.1 */
    0x1.bba14dc3507ccp+1,    /* Apple 10.6637 */
    0,                       /* llvm */
    /* GLIBC LIBMVEC has the medium accuracy Intel SVML functions
       (high accuracy: < 1 ulp, low accuracy: many ulps) */
#if LIBMVEC==128 /* sse4.2 */
    0x1.78afff6f3044cp+4,    /* libmvec 2.21867 */
#elif LIBMVEC==256 /* avx2 */
    0x1.78affead86a26p+4,    /* libmvec 2.20423 */
#else /* avx512 */
    0x1.78afff9d452cp+4,     /* libmvec 2.20537 */
#endif
    0x1.f6094003e85d6p+1,    /* msvc-x64-erfc 8.45184 */
    0x1.5182d8799b84bp+0,    /* freebsd 4.07682 */
    0x1.46cffdf330b13p+4,    /* ArmPL 1.70623 */

    /* exp */
    -0x1.49f33ad2c1c58p+9,   /* GNU libc 0.510359 */
    // -0x1.9df9aecc00001p-29,  /* GNU libc RNDZ 1.24399 */
    // 0x1.50f5a68c91c18p-1,    /* GNU libc RNDU 1.01597 */
    // -0x1.9df9aecc00001p-29,  /* GNU libc RNDD 1.24399 */
    0x1.fce66609f7428p+5,    /* icc 19.1.3.304 */
    0x1.b97dc8345c55p+5,     /* AMD LibM */
    0x1.2e8f20cf3cbe7p+8,    /* Newlib */
    0x1.2e8f20cf3cbe7p+8,    /* OpenLibm */
    -0x1.18209ecd19a8cp+6,   /* Musl */
    -0x1.4133f4fd79c1cp-13,  /* Apple 0.520417 */
    0x1p-53,                 /* llvm 0.5 */
#if LIBMVEC==128 /* sse4.2 */
    -0x1.205968aae119fp-8,   /* libmvec 3.20379 */
#elif LIBMVEC==256 /* avx2 */
    -0x1.2059763f8882bp-8,   /* libmvec 3.20362 */
#else /* avx512 */
    -0x1.205968a73d4abp-8,   /* libmvec 3.20361 */
#endif
    -0x1.74046dfefd9d1p+9,   /* msvc-x64-exp 1.5 */
    0x1.2e8f20cf3cbe7p+8,    /* freebsd 0.948331 */
    -0x1.49f33ad2c1c58p+9,   /* ArmPL 0.510359 */

    /* exp10 */
    -0x1.57449153f316ep-7,   /* GNU libc */
    -0x1.5cd9d94d49a85p+1,   /* icc */
    -0x1.285d82b75258fp+2,   /* AMD LibM */
    0x1.ce7ef793d4b0ap-2,    /* Newlib */
    0,                       /* OpenLibm */
    -0x1.fe8c27141c94ap+3,   /* Musl */
    -0x1.c37443e446523p-16,  /* Apple 0.520238 */
    0x1.bcb7b1526e50dp-55,   /* llvm 0.5 */
#if LIBMVEC==128 /* sse4.2 */
    0x1.33f4082f47b74p+8,    /* libmvec 2.00015 */
#elif LIBMVEC==256 /* avx2 */
    0x1.33f4082f47b74p+8,    /* libmvec 2.00015 */
#else /* avx512 */
    0x1.33f4082f47b74p+8,    /* libmvec 2.00015 */
#endif
    0,                       /* msvc_x64-exp10 0.0 */
    0,                        /* freebsd: NA */
    -0x1.5acf96f42165bp-7,   /* ArmPL 0.509874 */

    /* exp2 */
    -0x1.1a4ce073ea908p-5,   /* GNU libc */
    -0x1.8002f29666b99p-6,   /* icx */
    0x1.fffbff4152bafp+9,    /* AMD LibM 1.04695 */
    -0x1.ff95ecb4e6331p-2,   /* Newlib */
    -0x1.ff1eb5acee46bp+9,   /* OpenLibm */
    -0x1.1a4ce073ea908p-5,   /* Musl */
    -0x1.b3d9b47ad1b2fp-13,  /* Apple 0.520087 */
    0x1.71547652b82fep-53,   /* llvm */
#if LIBMVEC==128 /* sse4.2 */
    -0x1.4c31866f6d3bbp-6,   /* libmvec 1.64293 */
#elif LIBMVEC==256 /* avx2 */
    -0x1.4c3c931a5de98p-6,   /* libmvec 1.64097 */
#else /* avx512 */
    -0x1.8000e569a5545p-3,   /* libmvec 1.05024 */
#endif
    -0x1.72286b6f94510p-2,   /* msvc-x64-exp2 2.22059 */
    -0x1.ff1eb5acee46bp+9,   /* freebsd 0.750695 */
    -0x1.f7087fb1cf9e8p+9,   /* ArmPL 0.508998 */

    /* expm1 */
    0x1.63be411e096ep-2,     /* GNU libc */
    -0x1.62fe464c64f65p-8,   /* icc */
    0x1.9a57c1ec06a74p-2,    /* AMD LibM 0.669188 */
    0x1.62ff47a01658fp-2,    /* Newlib */
    0x1.62ff47a01658fp-2,    /* OpenLibm */
    0x1.62ff47a01658fp-2,    /* Musl */
    0x1.e7f93188565ecp-5,    /* Apple 0.705059 */
    0x1p-52,                 /* llvm 0.5 */
#if LIBMVEC==128 /* sse4.2 */
    0x1.86f57e8de4a5p-9,     /* libmvec 2.96513 */
#elif LIBMVEC==256 /* avx2 */
    0x1.856b41d86994cp-9,    /* libmvec 2.96463 */
#else /* avx512 */
    0x1.c3b7c858f0575p-6,    /* libmvec 2.11697 */
#endif
    -0x1.62d7c116d2e32p-1,   /* msvc-x64-expm1 3.05701 */
    0x1.62ff47a01658fp-2,    /* freebsd 0.908146 */
    0x1.633f993a730c9p-2,    /* ArmPL 2.17324 */

    /* j0 */
    0x1.33d152e971b4p+1,     /* GNU libc */
    0x1.aff859518c846p+7,    /* icc */
    0,                       /* AMD LibM */
    0x1.45f3067a0f4b2p+847,  /* Newlib */
    0x1.33d152e971b4p+1,     /* OpenLibm */
    -0x1.33d152e971b4p+1,    /* Musl 1.2.1 */
    0x1.6148f5b2c2e45p+2,    /* Apple 3.82247e+14 */
    0,                       /* llvm */
    0,                       /* libmvec */
    0x1.bbbd6a9201265p+657,  /* msvc-x64-_j0 1.87558e+26 */
    0x1.33d152e971b4p+1,     /* freebsd 4.51e+14 */
    0x1.6148f5b2c2e45p+2,    /* ArmPL 3.82247e+14 */

    /* j1 */
    -0x1.ea75575af6f09p+1,   /* GNU libc */
    -0x1.67b5541c7d8b7p+7,   /* icc 19.1.3.304 */
    0,                       /* AMD LibM */
    0x1.45f3066f80258p+325,  /* Newlib */
    -0x1.ea75575af6f09p+1,   /* OpenLibm */
    0x1.ea75575af6f09p+1,    /* Musl 1.2.1 */
    -0x1.ea75575af6f09p+1,   /* Apple 1.10e+15 */
    0,                       /* llvm */
    0,                       /* libmvec */
    0x1.a635d8219ad13p+157,  /* msvc-x64-_j1 3.84561e+26 */
    -0x1.ea75575af6f09p+1,   /* freebsd 1.10e+15 */
    -0x1.ea75575af6f09p+1,   /* ArmPL 8.95204e+14 */

    /* lgamma */
    -0x1.f613ab0969f81p+1,   /* GNU libc */
    -0x1.3f62c60e23b31p+2,   /* icc 19.1.3.304 */
    0,                       /* AMD LibM */
    -0x1.3a7fc9600f86cp+1,   /* RedHat Newlib 4.0.0 */
    -0x1.3a7fc9600f86cp+1,   /* OpenLibm */
    -0x1.3a7fc9600f86cp+1,   /* Musl 1.2.1 */
    -0x1.bffcbf76b86fp+2,    /* Apple 2.32471e+16 */
    0,                       /* llvm */
    0,                       /* libmvec */
    -0x1.bffe071eea741p+2,   /* msvc-x64-lgamma 5.09415e+13 */
    -0x1.3a7fc9600f86cp+1,   /* freebsd 4.45e+15 */
    -0x1.f60bdba970b3cp+1,   /* ArmPL 9.52506 */

    /* log */
    0x1.1211bef8f68e9p+0,    /* GNU libc */
    0x1.008000db2e8bep+0,    /* icc */
    0x1.10803885617a6p+0,    /* AMD LibM 0.609824 */
    0x1.48ae5a67204f5p+0,    /* Newlib */
    0x1.48ae5a67204f5p+0,    /* OpenLibm */
    0x1.dc0b586f2b26p-1,     /* Musl */
    0x1.490af72a25a81p-1,    /* Apple 0.507847 */
    0x1.5b6e7e4e96f86p+2,    /* llvm */
#if LIBMVEC==128 /* sse4.2 */
    0x1.00e000c7fa1c3p+0,    /* libmvec 1.58569 */
#elif LIBMVEC==256 /* avx2 */
    0x1.002001ffaa4ap+0,     /* libmvec 1.58883 */
#else /* avx512 */
    0x1.001f01ac83b3p+0,     /* libmvec 1.59111 */
#endif
    0x1.0ffc349469a2fp+0,    /* msvc-x64-log 0.576919 */
    0x1.48ae5a67204f5p+0,    /* freebsd 0.945306 */
    0x1.1211bef8f68e9p+0,    /* ArmPL 0.519023 */

    /* log10 */
    0x1.de02157073b31p-1,    /* GNU libc */
    0x1.feda7b62c1033p-1,    /* icc 19.1.3.304 */
    0x1.10fdf4211fd45p+0,    /* AMD LibM */
    0x1.55535a0140a21p+0,    /* Newlib */
    0x1.553e1cb579ee9p+0,    /* OpenLibm */
    0x1.553e1cb579ee9p+0,    /* Musl */
    0x1.2501ee5628b08p-1,    /* Apple 0.513076 */
    0x1.e12d66744ff81p+429,  /* llvm 0.5 */
#if LIBMVEC==128 /* sse4.2 */
    0x1.00201204555c8p+0,    /* libmvec 2.09444 */
#elif LIBMVEC==256 /* avx2 */
    0x1.001fffbd3f495p+0,    /* libmvec 2.09480 */
#else /* avx512 */
    0x1.f03ebdaea826bp-1,    /* libmvec 1.95902 */
#endif
    0x1.e005e1e891807p-1,    /* msvc-x64-log10 0.632133 */
    0x1.553e1cb579ee9p+0,    /* freebsd 0.813227 */
    0x1.de02157073b31p-1,    /* ArmPL 1.61828 */

    /* log1p */
    -0x1.2bf183e0344b2p-2,   /* GNU libc */
    0x1.000aee2a2757fp-9,    /* icc */
    0x1.e0013fd35cbbp-4,     /* AMD LibM */
    -0x1.2bf1de6b04a8ap-2,   /* Newlib */
    -0x1.2bf1de6b04a8ap-2,   /* OpenLibm */
    -0x1.2bf32aaf122e2p-2,   /* Musl */
    -0x1.ffffff3fffffdp-28,  /* Apple 0.666667 */
    0x1p-53,                 /* llvm */
#if LIBMVEC==128 /* sse4.2 */
    0x1.000bcdec306p-11,     /* libmvec 2.58927 */
#elif LIBMVEC==256 /* avx2 */
    0x1.fff86f9b9acp-12,     /* libmvec 2.58923 */
#else /* avx512 */
    0x1.075745181aabp-6,     /* libmvec 1.94684 */
#endif
    -0x1.8000000000000p-53,  /* msvc-x64-log1p 1.4375 */
    -0x1.2bf1de6b04a8ap-2,   /* freebsd 0.89558 */
    -0x1.2e496d25897ecp-2,   /* ArmPL 1.73667 */

    /* log2 */
    0x1.1406d79e1b574p+0,    /* GNU libc */
    0x1.b4ebe40c95a01p+0,    /* icx 0.500 */
    0x1.0b541b6746bd1p+0,    /* AMD LibM */
    0x1.68d778f076021p+0,    /* Newlib */
    0x1.67eaf07ce24d1p+0,    /* OpenLibm */
    0x1.0b53197bd66c8p+0,    /* Musl */
    0x1.6b015f8d9a784p-1,    /* Apple 0.514480 */
    0x1.b4ebe40c95a01p+0,    /* llvm */
#if LIBMVEC==128 /* sse4.2 */
    0x1.002000d8e91c5p+0,    /* libmvec 2.08932 */
#elif LIBMVEC==256 /* avx2 */
    0x1.002003e5a80e3p+0,    /* libmvec 2.08921 */
#else /* avx512 */
    0x1.ede4ac763282bp-1,    /* libmvec 1.86313 */
#endif
    0x1.160732376ad7fp+0,    /* msvc-x64-log2 0.811625 */
    0x1.67eaf07ce24d1p+0,    /* freebsd 0.920898 */
    0x1.0b53f741fb8c4p+0,    /* ArmPL 0.553243 */

    /* sin */
    -0x1.f8b791cafcdefp+4,   /* GNU libc */
    -0x1.0e16eb809a35dp+944, /* icc 19.1.3.304 */
    -0x1.85e624577c23ep-1,   /* AMD LibM */
    -0x1.842d8ec8f752fp+21,  /* Newlib */
    0x1.4d84db080b9fdp+21,   /* OpenLibm */
    0x1.4d84db080b9fdp+21,   /* Musl */
    -0x1.07e4c92b5349dp+4,   /* Apple 0.943598 */
    -0x1.7137449123ef6p-26,  /* llvm 0.5 */
#if LIBMVEC==128 /* sse4.2 */
    0x1.a5a68e24971a3p+20,   /* libmvec 3.83437 */
#elif LIBMVEC==256 /* avx2 */
    0x1.9977bea4253f1p+0,    /* libmvec 3.48842 */
#else /* avx512 */
    -0x1.99631ed67b43fp+0,   /* libmvec 3.48873 */
#endif
    -0x1.11b624b546894p+9,   /* msvc-x64-sin 0.798290 */
    0x1.4d84db080b9fdp+21,   /* freebsd 0.83076 */
    -0x1.f8b791cafcdefp+4,   /* ArmPL 0.515623 */

    /* sinh */
    -0x1.633c654fee2bap+9,   /* GNU libc */
    -0x1.adc135eb544c1p-2,   /* icc */
    0x1.1feb2a79f307p+3,     /* AMD LibM */
    -0x1.633cae1335f26p+9,   /* Newlib */
    -0x1.63324af2fb5b7p-1,   /* OpenLibm */
    -0x1.63324af2fb5b7p-1,   /* Musl */
    0x1.d7131e11fc6b3p-2,    /* Apple 0.538334 */
    0,                       /* llvm */
#if LIBMVEC==128 /* sse4.2 */
    -0x1.c5c9440e9422dp-9,   /* libmvec 2.39492 */
#elif LIBMVEC==256 /* avx2 */
    -0x1.633c654fee2bap+9,   /* libmvec 1.92222 */
#else /* avx512 */
    -0x1.633c654fee2bap+9,   /* libmvec 1.92222 */
#endif
    -0x1.aff899f6fcad6p+4,   /* msvc-x64-sinh 1.50819 */
    -0x1.63324af2fb5b7p-1,   /* freebsd 1.87405 */
    0x1.9fa32b1149d35p-2,    /* ArmPL 2.58013 */

    /* sqrt */
    0x1.fffffffffffffp-1,    /* GNU libc */
    0x1.fffffffffffffp-1,    /* icc 19.1.3.304 */
    0x1.fffffffffffffp-1,    /* AMD LibM */
    0x1.fffffffffffffp-1,    /* Newlib */
    0x1.fffffffffffffp-1,    /* OpenLibm */
    0x1.fffffffffffffp-1,    /* Musl 1.2.1 */
    0x1.fffffffffffffp-1,    /* Apple */
    0x1.fffffffffffffp-1,    /* llvm */
    0x1.fffffffffffffp-1,    /* libmvec */
    0x1.fffffffffffffp-1,    /* msvc-x64-sqrt */
    0x1.fffffffffffffp-1,    /* freebsd */
    0x1.fffffffffffffp-1,    /* ArmPL 0.5 */

    /* tan */
    -0x1.317cd745dd37cp+9,   /* glibc */
    0x1.49adfd996a81dp+18,   /* icc */
    0x1.371a47b7e4eb2p+11,   /* AMD LibM */
    0x1.3f9605aaeb51bp+21,   /* Newlib */
    0x1.3f9605aaeb51bp+21,   /* OpenLibm */
    0x1.3f9605aaeb51bp+21,   /* Musl 1.2.2 */
    -0x1.a81d98fc58537p+6,   /* Apple 3.52535 */
    0x1.d12ed0af1a27fp-27,   /* llvm 0.5 */
#if LIBMVEC==128 /* sse4.2 */
    0x1.72e90b4651593p+15,   /* libmvec 3.96274 */
#elif LIBMVEC==256 /* avx2 */
    0x1.3fab696843fbfp+8,    /* libmvec 3.53263 */
#else /* avx512 */
    -0x1.780c9aeca907cp+17,  /* libmvec 3.98992 */
#endif
    -0x1.4d7c8b8320237p+11,  /* msvc-x64-tan 1.31972 */
    0x1.3f9605aaeb51bp+21,   /* freebsd 1.01166 */
    -0x1.317cd745dd37cp+9,   /* ArmPL 0.618367 */

    /* tanh */
    0x1.e126eee514cbcp-3,    /* GNU libc 2.20642 */
    0x1.002629fd74484p+0,    /* icc */
    -0x1.fde5bd2769a01p-1,   /* AMD LibM */
    -0x1.e134557098e37p-3,   /* Newlib */
    -0x1.e134557098e37p-3,   /* OpenLibm */
    -0x1.e134557098e37p-3,   /* Musl */
    0x1.00cf9f273d84p+1,     /* Apple 0.612229 */
    0,                       /* llvm */
#if LIBMVEC==128 /* sse4.2 */
    -0x1.000a02c5a8b47p-2,   /* libmvec 2.17016 */
#elif LIBMVEC==256 /* avx2 */
    -0x1.00010c3967f16p-2,   /* libmvec 2.13884 */
#else /* avx512 */
    -0x1.001bf41f56582p-1,   /* libmvec 1.19944 */
#endif
    -0x1.fb52ec8460d82p-1,   /* msvc-x64-tanh 1.43907 */
    -0x1.e134557098e37p-3,   /* freebsd 2.21499 */
    -0x1.c416448380debp-3,   /* ArmPL 2.75884 */

    /* tgamma */
    -0x1.c18caecc00f7bp+2,   /* GNU libc */
    -0x1.3e0001ad3bee3p+6,   /* icc */
    0,                       /* AMD LibM */
    -0x1.535175475cc8dp+7,   /* Newlib */
    -0x1.540b170c4e65ep+7,   /* OpenLibm */
    -0x1.fc4b534c8eccp+2,    /* Musl */
    -0x1.5456e56919a19p+7,   /* Apple 1026.83 */
    0,                       /* llvm */
    0,                       /* libmvec */
    -0x1.5c00000003c15p+7,   /* msvc-x64-tgamma 9.00718e+15 */
    -0x1.547cf3ddaaddap+7,   /* freebsd 1026.47 */
    -0x1.c18caecc00f7bp+2,   /* ArmPL 8.67183 */

    /* y0 */
    0x1.c982eb8d417eap-1,    /* GNU libc */
    0x1.4cdee58a47eddp-31,   /* icc */
    0,                       /* AMD LibM */
    0x1.c982eb8d417eap-1,    /* Newlib */
    0x1.c982eb8d417eap-1,    /* OpenLibm */
    0x1.c982eb8d417eap-1,    /* Musl 1.2.1 */
    0x1.c982eb8d417eap-1,    /* Apple 1.42e+15 */
    0,                       /* llvm */
    0,                       /* libmvec */
    0x1.c1d741dc52512p+744,  /* msvc-x64-_y0 3.29303e+25 */
    0x1.c982eb8d417eap-1,    /* freebsd 1.42e+15 */
    0x1.c982eb8d417eap-1,    /* ArmPL 3.48447e+15 */

    /* y1 */
    0x1.193bed4dff243p+1,    /* GNU libc */
    0x1.c513c569fe78ep+0,    /* icc */
    0,                       /* AMD LibM */
    0x1.193bed4dff243p+1,    /* Newlib */
    0x1.193bed4dff243p+1,    /* OpenLibm */
    0x1.193bed4dff243p+1,    /* Musl 1.2.1 */
    0x1.193bed4dff243p+1,    /* Apple 5.56e+15 */
    0,                       /* llvm */
    0,                       /* libmvec */
    0,                       /* msvc-x64-_y1 +Inf */
    0x1.193bed4dff243p+1,    /* freebsd 5.56e+15 */
    0x1.193bed4dff243p+1,    /* ArmPL 4.70515e+15 */

    /* acospi */
    0x1.da0ea62dd7231p-1,    /* GNU libc 1.50739 */
    0x1.6a18f7dda5343p-1,    /* icc 0.540156 */
    0,                       /* AMD LibM */
    0,                       /* Newlib */
    0,                       /* OpenLibm */
    0,                       /* Musl */
    0,                       /* Apple */
    0,                       /* llvm */
    0,                       /* libmvec */
    0,                       /* msvc-x64-acospi */
    0,                       /* freebsd */
    0,                       /* ArmPL */

    /* asinpi */
    -0x1.8805060cb885cp-3,   /* GNU libc 1.49879 */
    0x1.921fabd2a2b25p-750,  /* icc 0.520834 */
    0,                       /* AMD LibM */
    0,                       /* Newlib */
    0,                       /* OpenLibm */
    0,                       /* Musl */
    0,                       /* Apple */
    0,                       /* llvm */
    0,                       /* libmvec */
    0,                       /* msvc-x64-asinpi */
    0,                       /* freebsd */
    0,                       /* ArmPL */

    /* atanpi */
    0x1.9601b055fdf97p-3,    /* GNU libc 1.50033 */
    0x1.a7fb417ac774ap-2,    /* icx 0.941040 */
    0,                       /* AMD LibM */
    0,                       /* Newlib */
    0,                       /* OpenLibm */
    0,                       /* Musl */
    0,                       /* Apple */
    0,                       /* llvm */
    0,                       /* libmvec */
    0,                       /* msvc-x64-atanpi */
    0,                       /* freebsd */
    0,                       /* ArmPL */

    /* cospi */
    -0x1.1a0a2fa299b92p+6,   /* GNU libc 1.85020 */
    -0x1.f01d619f61e5bp-8,   /* icx 0.502370 */
    -0x1.fffffffffffffp-28,  /* AMD LibM 2.46741 */
    0,                       /* Newlib */
    0,                       /* OpenLibm */
    0,                       /* Musl */
    0,                       /* Apple */
    0,                       /* llvm */
    0,                       /* libmvec */
    0,                       /* msvc-x64-cospi */
    -0x1.fe3bb5207682dp-3,   /* freebsd 0.811621 */
    0x1.5a33cc258p-22,       /* ArmPL 3.17244 */

    /* exp10m1 */
    0x1.e880c5bafbd41p-2,    /* GNU libc 3.53871 */
    0,                       /* icx */
    0,                       /* AMD LibM */
    0,                       /* Newlib */
    0,                       /* OpenLibm */
    0,                       /* Musl */
    0,                       /* Apple */
    0,                       /* llvm */
    0,                       /* libmvec */
    0,                       /* msvc-x64-exp10m1 */
    0,                       /* freebsd */
    0,                       /* ArmPL */

    /* exp2m1 */
    0x1.d047583a6c6dp-1,     /* GNU libc 1.92510 */
    0,                       /* icx */
    0,                       /* AMD LibM */
    0,                       /* Newlib */
    0,                       /* OpenLibm */
    0,                       /* Musl */
    0,                       /* Apple */
    0,                       /* llvm */
    0,                       /* libmvec */
    0,                       /* msvc-x64-exp2m1 */
    0,                       /* freebsd */
    0,                       /* ArmPL */

    /* log10p1 */
    -0x1.4c2971893052fp-1,   /* GNU libc 1.94190 */
    0,                       /* icx */
    0,                       /* AMD LibM */
    0,                       /* Newlib */
    0,                       /* OpenLibm */
    0,                       /* Musl */
    0,                       /* Apple */
    0,                       /* llvm */
    0,                       /* libmvec */
    0,                       /* msvc-x64-log10p1 */
    0,                       /* freebsd */
    0,                       /* ArmPL */

    /* log2p1 */
    0x1.a7b725780ff2cp-2,    /* GNU libc 1.87575 */
    0,                       /* icx */
    0,                       /* AMD LibM */
    0,                       /* Newlib */
    0,                       /* OpenLibm */
    0,                       /* Musl */
    0,                       /* Apple */
    0,                       /* llvm */
    0,                       /* libmvec */
    0,                       /* msvc-x64-rsqrt */
    0,                       /* freebsd */
    0,                       /* ArmPL */

    /* sinpi */
    -0x1.45f3e53e1d707p-7,   /* GNU libc 1.85066 */
    -0x0.07e17f27e5086p-1022,/* icx 0.531251 */
    -0x1.ffffed20ca4dep-14,  /* AMD LibM 2.55084 */
    0,                       /* Newlib */
    0,                       /* OpenLibm */
    0,                       /* Musl */
    0,                       /* Apple */
    0,                       /* llvm */
    0,                       /* libmvec */
    0,                       /* msvc-x64-sinpi */
    0x1.0806840ac80ap+3,     /* freebsd 0.810928 */
    0x1.ffffe4b85e77ap-2,    /* ArmPL 3.1593 */

    /* tanpi */
    -0x1.fae7d0ef22d4ep-2,   /* GNU libc 2.84904 */
    0x1.49b79692667bp+46,    /* icc 1.00 */
    0x1.ffffce32e4a7cp-15,   /* AMD LibM 2.55083 */
    0,                       /* Newlib */
    0,                       /* OpenLibm */
    0,                       /* Musl */
    0,                       /* Apple */
    0,                       /* llvm */
    0,                       /* libmvec */
    0,                       /* msvc-x64-tanpi */
    0x1.c9542a6e8f18fp+0,    /* freebsd 0.968891 */
    0,                       /* ArmPL */

    /* rsqrt */
    0,                       /* GNU libc */
    0x1.00018f5913816p-458,  /* icc 0.500733 */
    0,                       /* AMD LibM */
    0,                       /* Newlib */
    0,                       /* OpenLibm */
    0,                       /* Musl */
    0,                       /* Apple */
    0,                       /* llvm */
    0,                       /* libmvec */
    0,                       /* msvc-x64-rsqrt */
    0,                       /* freebsd */
    0,                       /* ArmPL */
  };
TYPE extra[SIZE_EXTRA] = {
  /* don't remove the following values: they should give an error > 0.5 for
     glibc asin after commit f67f9c9 */
  0x1.fcd5742999ab8p-1,
  -0x1.ee2b43286db75p-1,
  -0x1.f692ba202abcp-4,
  -0x1.9915e876fc062p-1,
  -0x1.fd7d13f1663afp-1, /* 5.0000005122065272e-01 for asin after f67f9c9 */
  0x1.16c08b622e36p-1, /* 4.9999999999919309e-01 for asin with glibc-2.32 */
  /* same for glibc acos */
  0x1.f63845056f35ep-1,
  0x1.fffff3634acd6p-1, /* glibc-2.32 acos 0.5000000044534775 */
  -0x1.cb3b399d747f3p-55,  /* crlibm bug acos_rn */
  0x1.1a62633145c07p-54,   /* crlibm bug acos_ru */
  0x1.800010834fed5p-1,    /* crlibm bug acospi_rn */
  -0x1.80052ddfdd07cp-1,   /* crlibm bug asin_rn */
  0x1p+0,                  /* crlibm bug atanpi_ru */
  0x1.499c0c2050962p-997,  /* crlibm bug sinpi_rn */
  -0x1.010e23e83c2a7p-997, /* crlibm bug sinpi_rz, sinpi_ru and tanpi_ru */
  -0x1.08b9e4381190dp-999, /* crlibm bug sinpi_rd */
  0x1.dd113d1bb494bp-998,  /* crlibm bug tanpi_rn */
  -0x1.ffffab1933322p-32,  /* crlibm bug tanpi_rz */
  0x1.fffbbb0468123p-32,   /* crlibm bug tanpi_rd */
  +0.0,                    /* msvc _y1 yields NaN */
  -0.0,
  0x1p+0, /* 10^0 */
  0x1.4p+3, /* 10^1 */
  0x1.9p+6, /* 10^2 */
  0x1.f4p+9, /* 10^3 */
  0x1.388p+13, /* 10^4 */
  0x1.86ap+16, /* 10^5 */
  0x1.e848p+19, /* 10^6 */
  0x1.312dp+23, /* 10^7 */
  0x1.7d784p+26, /* 10^8 */
  0x1.dcd65p+29, /* 10^9 */
  0x1.2a05f2p+33, /* 10^10 */
  0x1.74876e8p+36, /* 10^11 */
  0x1.d1a94a2p+39, /* 10^12 */
  0x1.2309ce54p+43, /* 10^13 */
  0x1.6bcc41e9p+46, /* 10^14 */
  0x1.c6bf52634p+49, /* 10^15 */
  0x1.1c37937e08p+53, /* 10^16 */
  0x1.6345785d8ap+56, /* 10^17 */
  0x1.bc16d674ec8p+59, /* 10^18 */
  0x1.158e460913dp+63, /* 10^19 */
  0x1.5af1d78b58c4p+66, /* 10^20 */
  0x1.b1ae4d6e2ef5p+69, /* 10^21 */
  0x1.0f0cf064dd592p+73, /* 10^22 */
  /* the following are inputs near zeros of Bessel functions j0,j1,y0,y1
     from Table 3 of https://www.cl.cam.ac.uk/~jrh13/papers/bessel.pdf */
  0x1.782b7a20df6d4p+66, /* j0 and y1 */
  0x1.66bd5424e5655p+89, /* j0 and y1 */
  0x1.07f285bb70bfp+38,  /* j0 */
  0x1.99caa5236feeap+75, /* j1 and y0 */
  0x1.e0664dbedfec5p+80, /* j0 and y1 */
  0x1.dbabcc5913d2fp+26, /* y0 */
  0x1.1a209b98a791fp+68, /* j1 and y0 */
  0x1.f19e5d71b26bap+84, /* j0 and y1 */
  0x1.b8fd7a4646112p+34, /* j1 and y0 */
  0x1.51eae0a757998p+10, /* j1 */
  0x1.4997d3ab50ac4p+30, /* j0 and y1 */
  0x1.097db7f70d377p+22, /* j1 and y0 */
  0x1.d63658a917489p+68, /* j0 and y1 */
  0x1.56a4aa740a5a7p+52, /* j0 and y1 */
  0x1.2ace52fd26fc2p+42, /* j0 and y1 */
  0x1.737d16f5eb092p+36, /* j1 and y0 */
  0x1.6f25c6bc40e4bp+17, /* j0 and y1 */
  0x1.2a32976bff5p+26,   /* j1 and y0 */
  0x1.ad01ea3bcbd83p+32, /* j0 and y1 */
  0x1.dfd42a8864ad7p+26, /* j1 and y0 */
  0x1.908de75d3884fp+7,  /* j0 */
  0x1.fc015e6663696p+12, /* j1 and y0 */
  0x1.cd4732748519dp+23, /* j1 and y0 */
  0x1.c982eb8d417eap-1,  /* y0 */
  0x1.0192a33615d8cp+0,  /* acosh icx */
  };
#endif
#ifdef USE_LDOUBLE
/* AMD Libm does not provide long double functions */
  TYPE worst[SIZE] = {
    /* acos */
    0xf.fe002cabd608585p-4l,   /* glibc 1.74297 */
    0x8.af256cd27462348p-4l,   /* icc 0.504696 */
    0,                         /* AMD */
    0,                         /* Newlib */
    -0x8.040541d0054d89p-4l,   /* OpenLibm 0.937062 */
    0xf.fe002cabd608585p-4l,   /* Musl 1.74297 */
    0,                         /* Apple */
    0,                         /* llvm */
    0,                         /* libmvec */
    0,                         /* msvc */
    -0x8.040541d0054d89p-4l,   /* freebsd 0.937062 */
    0,                         /* ArmPL */
    /* acosh */
    0x1.1ecdb5b8f0c5d79p+0l,   /* glibc 2.98085 */
    0x1.1f9c4feedfe4f2cp+0l,   /* icc 0.501721 */
    0,                         /* AMD */
    0,                         /* Newlib */
    0x1.10384b24aec007fcp+0l,  /* OpenLibm 3.13548 */
    0x1.1ecdb5b8f0c5d79p+0l,   /* Musl 2.98085 */
    0,                         /* Apple */
    0,                         /* llvm */
    0,                         /* libmvec */
    0,                         /* msvc */
    0x1.0001fe8811e16ee2p+0l,  /* freebsd 2.23545 */
    0,                         /* ArmPL */
    /* asin */
    0x8.171fd358c4cb27bp-4l,   /* glibc 1.1424 */
    -0x8.018aef8787e5a6bp-4l,  /* icc 0.505461 */
    0,                         /* AMD */
    0,                         /* Newlib */
    0x8.0519515d1e15a6bp-4l,   /* OpenLibm 1.02829 */
    -0x3.fff0a397b8dea17cp-8l, /* Musl 1.99565 */
    0,                         /* Apple */
    0,                         /* llvm */
    0,                         /* libmvec */
    0,                         /* msvc */
    0x8.0519515d1e15a6bp-4l,   /* freebsd 1.02829 */
    0,                         /* ArmPL */
    /* asinh */
    -0x8.0bb656992eac437p-4l,  /* glibc 2.95329 */
    0x7.ff15da44c3651abp-4l,   /* icc 0.505737 */
    0,                         /* AMD */
    0,                         /* Newlib */
    -0x5.c9866cb231f2c7c8p-4l, /* OpenLibm 3.18771 */
    -0x8.0bb656992eac437p-4l,  /* Musl 2.95329 */
    0,                         /* Apple */
    0,                         /* llvm */
    0,                         /* libmvec */
    0,                         /* msvc */
    -0x8.4d30725ad98215ap-4l,  /* freebsd 1.62386 */
    0,                         /* ArmPL */
    /* atan */
    -0x1.0411ae010d4c5b1ep+0l, /* glibc 0.639296 */
    -0x8.00f60592e42d79p+8l,   /* icc 0.500347 */
    0,                         /* AMD */
    0,                         /* Newlib */
    0x6.fffde214a06fb5f8p-4l,  /* OpenLibm 1.0979 */
    -0x1.0411ae010d4c5b1ep+0l, /* Musl 0.639295 */
    0,                         /* Apple */
    0,                         /* llvm */
    0,                         /* libmvec */
    0,                         /* msvc */
    0x6.fffde214a06fb5f8p-4l,  /* freebsd 1.0979 */
    0,                         /* ArmPL */
    /* atanh */
    -0x3.337ceaccc9025258p-4l, /* glibc 2.87376 */
    0x3.e7be418257523408p-4l,  /* icc 0.500536 */
    0,                         /* AMD */
    0,                         /* Newlib */
    -0xf.ffffffffffffe78p-32l, /* OpenLibm 85.3333 */
    0x3.344a915e34e5e6b8p-4l,  /* Musl 3.18856 */
    0,                         /* Apple */
    0,                         /* llvm */
    0,                         /* libmvec */
    0,                         /* msvc */
    -0x7.ff5cbca0e5646c78p-12l,/* freebsd 1.51456 */
    0,                         /* ArmPL */
    /* cbrt */
    -0xc.f4fd71a450e6a0bp-14732l, /* glibc 0.823366 */
    -0x2.320375fd33ed311cp-13376l,/* icc 0.502427 */
    0,                           /* AMD */
    0,                           /* Newlib */
    -0x3.fffffffa5623708p+4588l, /* OpenLibm 0.889414 */
    -0x3.fffffffa5623708p+4588l, /* Musl 0.889414 */
    0,                         /* Apple */
    0,                         /* llvm */
    0,                         /* libmvec */
    0,                         /* msvc */
    -0x3.fffffffa5623708p+4588l, /* freebsd 0.889414 */
    0,                         /* ArmPL */
    /* cos */
    -0x3.d067a048093bdf94p+9160l,/* glibc 1.50065 */
    -0x4.b0df0d7d55044918p+8l,   /* icc 0.501748 */
    0,                           /* AMD */
    0,                           /* Newlib */
    0x3.e0db6fa4b23541ap+4l,     /* OpenLibm 0.798423 */
    0x3.e0db6fa4b23541ap+4l,     /* Musl 0.798423 */
    0,                           /* Apple */
    0,                           /* llvm */
    0,                           /* libmvec */
    0,                           /* msvc */
    0x3.e0db6fa4b23541ap+4l,     /* freebsd 0.798423 */
    0,                           /* ArmPL */
    /* cosh */
    0x2.c5d375f827733ac4p+12l, /* glibc 3.39131 */
    -0x7.f6a09874512cf768p-4l, /* icc 0.501048 */
    0,                         /* AMD */
    0,                         /* Newlib */
    0x2.c5d374f9436efd1p+12l,  /* OpenLibm 4.85758 */
    0x2.c5d37484e4c162bp+12l,  /* Musl 3.72979 */
    0,                         /* Apple */
    0,                         /* llvm */
    0,                         /* libmvec */
    0,                         /* msvc */
    -0xf.c52a6a14a334e77p-4l,  /* freebsd 0.935725 */
    0,                         /* ArmPL */
    /* erf */
    0xd.7fe64ab05cf75e8p-4l,   /* glibc 1.16134 */
    -0x1.c55160e785ee1cbap-4l, /* icc 0.517002 */
    0,                         /* AMD */
    0,                         /* Newlib */
    0xd.7fe64ab05cf75e8p-4l,   /* OpenLibm 1.16134 */
    0xd.7fe64ab05cf75e8p-4l,   /* Musl 1.16134 */
    0,                         /* Apple */
    0,                         /* llvm */
    0,                         /* libmvec */
    0,                         /* msvc */
    0x1.c5a9ec676d7e551ep-24l, /* freebsd 0.994548 */
    0,                         /* ArmPL */
    /* erfc */
    0x1.59723d7ee47e3034p+0l,  /* glibc 4.72700 */
    0x3.03c7b9f943690558p-4l,  /* icc 0.526992 */
    0,                         /* AMD */
    0,                         /* Newlib */
    0x1.5cc0e1cc32a3dc98p+0l,  /* OpenLibm 5.76683 */
    0x1.5c9262fa4210902p+0l,   /* Musl 5.11383 */
    0,                         /* Apple */
    0,                         /* llvm */
    0,                         /* libmvec */
    0,                         /* msvc */
    0x3.fffffff7fffffc34p-36l, /* freebsd 1.37847e+08 */
    0,                         /* ArmPL */
    /* exp */
    0x5.8b9111182b4467ep-4l,   /* glibc 1.26732 */
    0x2.c590e6ab0d71c77p+12l,  /* icc 0.500644 */
    0,                         /* AMD */
    0,                         /* Newlib */
    0x8.aa2253c0d601dedp+0l,   /* OpenLibm 1.99636 */
    -0x2.c5a1073a0f38b61cp+12l,/* Musl 1.53612 */
    0,                         /* Apple */
    0,                         /* llvm */
    0,                         /* libmvec */
    0,                         /* msvc */
    -0x2.c5b2c28ca01620dcp+12l,/* freebsd 0.751719 */
    0,                         /* ArmPL */
    /* exp10 */
    0x1.2da9675e95849c3ep+12l, /* glibc 1.49666 */
    -0x1.2ab76ac25255a1aap+12l,/* icc 0.500307 */
    0,                         /* AMD */
    0,                         /* Newlib */
    0,                         /* OpenLibm */
    0xd.41cfea690e121b5p+8l,   /* Musl 40.0833 */
    0,                         /* Apple */
    0,                         /* llvm */
    0,                         /* libmvec */
    0,                         /* msvc */
    0,                         /* freebsd NA */
    -0x1.5acf96f42165bp-7,     /* ArmPL 0.509874 */
    /* exp2 */
    -0x7.3f819acf048f1678p-4l, /* glibc 0.787045 */
    -0x3.fe9a346527a75d98p-16l,/* icc 0.500109 */
    0,                         /* AMD */
    0,                         /* Newlib */
    -0xf.ffffd9f32ee1e06p-12l, /* OpenLibm 2.17787 */
    -0x7.3f819acf048f1678p-4l, /* Musl 0.787045 */
    0,                         /* Apple */
    0,                         /* llvm */
    0,                         /* libmvec */
    0,                         /* msvc */
    -0x3.ffe0d002661900a8p+12l,/* freebsd 0.752343 */
    0,                         /* ArmPL */
    /* expm1 */
    0x5.8b910bbe3c26818p-4l,   /* glibc 3.07539 */
    -0x1.0040016b56008656p-8l, /* icc 0.501961 */
    0,                         /* AMD */
    0,                         /* Newlib */
    0x2.c5c85fdf170c604cp+12l, /* OpenLibm Inf */
    0x2.c5c85fdf170c604cp+12l, /* Musl 9704.96 */
    0,                         /* Apple */
    0,                         /* llvm */
    0,                         /* libmvec */
    0,                         /* msvc */
    0x3.80e4f0677c553158p-4l,  /* freebsd 0.516189 */
    0,                         /* ArmPL */
    /* j0 */
    -0x2.67a2a5d2e367f784p+0l, /* glibc 9.78435e+17 */
    -0x1.6a09e667f3bd238cp-32l, /* icc 0.500000000000001 */
    0,                         /* AMD */
    0,                         /* Newlib */
    0,                         /* OpenLibm */
    0,                         /* Musl */
    0,                         /* Apple */
    0,                         /* llvm */
    0,                         /* libmvec */
    0,                         /* msvc */
    0,                         /* freebsd NA */
    0,                         /* ArmPL */
    /* j1 */
    0x3.d4eaaeb5ede115p+0l,    /* glibc 3.37624e+18 */
    0x8.001819d5fc886dap-4l,   /* icc 0.501 */
    0,                         /* AMD */
    0,                         /* Newlib */
    0,                         /* OpenLibm */
    0,                         /* Musl */
    0,                         /* Apple */
    0,                         /* llvm */
    0,                         /* libmvec */
    0,                         /* msvc */
    0,                         /* freebsd NA */
    0,                         /* ArmPL */
    /* lgamma */
    -0x3.ec9403f23a1f21cp+0l,  /* glibc 12.1499 */
    -0x4.07fe15510b6a28p+0l,   /* icc 0.548146 */
    0,                         /* AMD */
    0,                         /* Newlib */
    -0x2.74ff92c01f0d82acp+0l, /* OpenLibm 9.07174e+19 */
    -0x2.74ff92c01f0d82acp+0l, /* Musl 9.07174e+19 */
    0,                         /* Apple */
    0,                         /* llvm */
    0,                         /* libmvec */
    0,                         /* msvc */
    -0x2.74ff92c01f0d82acp+0l, /* freebsd 1.65e+20 */
    0,                         /* ArmPL */
    /* log */
    0x1.20dad075f537ae56p+0l,  /* glibc 0.997874 */
    0x1.1001246349edf00cp+0l,  /* icc 0.500594 */
    0,                         /* AMD */
    0,                         /* Newlib */
    0xb.504a14384e9b137p-4l,   /* OpenLibm 1.21863 */
    0x1.20dad075f537ae56p+0l,  /* Musl 0.997874 */
    0,                         /* Apple */
    0,                         /* llvm */
    0,                         /* libmvec */
    0,                         /* msvc */
    0x1.01007581714f057ap+0l,  /* freebsd 0.511884 */
    0,                         /* ArmPL */
    /* log10 */
    0x1.272b7c3bbb08ae12p+0l,  /* glibc 1.35874 */
    0x1.010141e1049fce68p+0l,  /* icc 0.501608 */
    0,                         /* AMD */
    0,                         /* Newlib */
    0xb.ffac4b4c47e00c3p-4l,   /* OpenLibm 1.21324 */
    0x1.272b7c3bbb08ae12p+0l,  /* Musl 1.35874 */
    0,                         /* Apple */
    0,                         /* llvm */
    0,                         /* libmvec */
    0,                         /* msvc */
    0x1.0101ff27167c589ap+0l,  /* freebsd 0.510482 */
    0,                         /* ArmPL */
    /* log1p */
    -0x6.451f6c3fd0d4a218p-4l, /* glibc 2.48222 */
    -0xe.fefa23913fa3eb7p-8l,  /* icc 0.500606 */
    0,                         /* AMD */
    0,                         /* Newlib */
    -0x4.c669bd1813ec8bd8p-4l, /* OpenLibm 2.59783 */
    -0x6.451f6c3fd0d4a218p-4l, /* Musl 2.48221 */
    0,                         /* Apple */
    0,                         /* llvm */
    0,                         /* libmvec */
    0,                         /* msvc */
    0x1.0071fc209b87c88p-8l,   /* freebsd 0.515820 */
    0,                         /* ArmPL */
    /* log2 */
    0x1.058f12b8b3ac44bep+0l,  /* glibc 0.994683 */
    0x1.01004bfffe4316bep+0l,  /* icc 0.501508 */
    0,                         /* AMD */
    0,                         /* Newlib */
    0x1.6646b082fd1065cep+0l,  /* OpenLibm 1.63300 */
    0x1.058f12b8b3ac44bep+0l,  /* Musl 0.994683 */
    0,                         /* Apple */
    0,                         /* llvm */
    0,                         /* libmvec */
    0,                         /* msvc */
    0x1.0101e3c5cde465ccp+0l,  /* freebsd 0.508654 */
    0,                         /* ArmPL */
    /* sin */
    -0x6.e2368c0ed74e5698p+16l,/* glibc 1.50131 */
    -0xc.141cf155623856bp+8l,  /* icc 0.501792 */
    0,                         /* AMD */
    0,                         /* Newlib */
    -0x2.a2a4a117ff34b034p+8l, /* OpenLibm 0.798636 */
    -0x2.a2a4a117ff34b034p+8l, /* Musl 0.798636 */
    0,                         /* Apple */
    0,                         /* llvm */
    0,                         /* libmvec */
    0,                         /* msvc */
    -0x2.a2a4a117ff34b034p+8l, /* freebsd 0.798636 */
    0,                         /* ArmPL */
    /* sinh */
    0x2.c5d375f827733ac4p+12l, /* glibc 3.39131 */
    0x7.b0af44fc25df3efp-4l,   /* icc 0.502360 */
    0,                         /* AMD */
    0,                         /* Newlib */
    -0x2.c5d375cbe7e4a81cp+12l,/* OpenLibm 4.84066 */
    0x2.c5c85fdbc1ccc354p+12l, /* Musl 9704.92 */
    0,                         /* Apple */
    0,                         /* llvm */
    0,                         /* libmvec */
    0,                         /* msvc */
    0xd.ae04a309c0411f3p-4l,   /* freebsd 0.802010 */
    0,                         /* ArmPL */
    /* sqrt */
    0xf.fffffffffffffffp-4l,   /* glibc 0.5 */
    0xf.fffffffffffffffp-4l,   /* icc 0.5 */
    0,                         /* AMD */
    0xf.fffffffffffffffp-4l,   /* Newlib */
    0xf.fffffffffffffffp-4l,   /* OpenLibm 0.5 */
    0xf.fffffffffffffffp-4l,   /* Musl 0.5 */
    0,                         /* Apple */
    0xf.fffffffffffffffp-4l,   /* llvm */
    0,                         /* libmvec */
    0,                         /* msvc */
    0xf.fffffffffffffffp-4l,   /* freebsd 0.5 */
    0,                         /* ArmPL */
    /* tan */
    0x1.974cd2181086913p+8l,   /* glibc 1.75334 */
    0xc.845cb771b06f4c5p+0l,   /* icc 0.503940 */
    0,                         /* AMD */
    0,                         /* Newlib */
    -0x6.fae45244b0bc104p+8l,  /* OpenLibm 1.01919 */
    -0x6.fae45244b0bc104p+8l,  /* Musl     1.01919 */
    0,                         /* Apple */
    0,                         /* llvm */
    0,                         /* libmvec */
    0,                         /* msvc */
    -0x6.fae45244b0bc104p+8l,  /* freebsd 1.01919 */
    0,                         /* ArmPL */
    /* tanh */
    0x3.b9979a543d0fbfa8p-4l,  /* glibc 3.21217 */
    0x7.fb808a1ef99076ep-4l,   /* icc 0.505790 */
    0,                         /* AMD */
    0,                         /* Newlib */
    0x3.8b2602d43bdf4c28p-4l,  /* OpenLibm 2.55094 */
    0x4.024182351388d15p-4l,   /* Musl 2.94887 */
    0,                         /* Apple */
    0,                         /* llvm */
    0,                         /* libmvec */
    0,                         /* msvc */
    0x1.80371de2d031a66ep+0l,  /* freebsd 0.639064 */
    0,                         /* ArmPL */
    /* tgamma */
    -0x1.70a55b2628a7cb68p+4l, /* glibc 9.76802 */
    -0x6.9c7abe03c485dbbp+8l,  /* icx 0.549329 */
    0,                         /* AMD */
    0,                         /* Newlib */
    -0x6.db747ae147ae148p+8l,  /* OpenLibm inf */
    -0x2.8d19fd20f3aa62cp+4l,  /* Musl 3.68935e+19 */
    0,                         /* Apple */
    0,                         /* llvm */
    0,                         /* libmvec */
    0,                         /* msvc */
    -0x6.e000000000000008p+8l, /* freebsd 4.23638e+16 */
    0,                         /* ArmPL */
    /* y0 */
    0xe.4c175c6a0bf51e8p-4l,   /* glibc 1.3775e+18 */
    0x1.000213a50d97fd8ep+0l,  /* icc 0.500001 */
    0,                         /* AMD */
    0,                         /* Newlib */
    0,                         /* OpenLibm */
    0,                         /* Musl */
    0,                         /* Apple */
    0,                         /* llvm */
    0,                         /* libmvec */
    0,                         /* msvc */
    0,                         /* freebsd NA */
    0,                         /* ArmPL */
    /* y1 */
    0xb.bfc89c6a1903022p+0l,   /* glibc 4.60036e+18 */
    0x4.002362c1b67ad6cp+0l,   /* icc 0.500001 */
    0,                         /* AMD */
    0,                         /* Newlib */
    0,                         /* OpenLibm */
    0,                         /* Musl */
    0,                         /* Apple */
    0,                         /* llvm */
    0,                         /* libmvec */
    0,                         /* msvc */
    0,                         /* freebsd NA */
    0,                         /* ArmPL */

    /* acospi */
    0xf.fec85670a4b0761p-4l, /* GNU libc 2.85433 */
    0,                       /* icc */
    0,                       /* AMD LibM */
    0,                       /* Newlib */
    0,                       /* OpenLibm */
    0,                       /* Musl */
    0,                       /* Apple */
    0,                       /* llvm */
    0,                       /* libmvec */
    0,                       /* msvc */
    0,                       /* freebsd */
    0,                       /* ArmPL */

    /* asinpi */
    0x8.14d7e32b5c44642p-4l, /* GNU libc 2.08296 */
    0,                       /* icc */
    0,                       /* AMD LibM */
    0,                       /* Newlib */
    0,                       /* OpenLibm */
    0,                       /* Musl */
    0,                       /* Apple */
    0,                       /* llvm */
    0,                       /* libmvec */
    0,                       /* msvc */
    0,                       /* freebsd */
    0,                       /* ArmPL */

    /* atanpi */
    0xc.916d8196a7d1bbfp-8l, /* GNU libc 1.55199 */
    0,                       /* icc */
    0,                       /* AMD LibM */
    0,                       /* Newlib */
    0,                       /* OpenLibm */
    0,                       /* Musl */
    0,                       /* Apple */
    0,                       /* llvm */
    0,                       /* libmvec */
    0,                       /* msvc */
    0,                       /* freebsd */
    0,                       /* ArmPL */

    /* cospi */
    0x7.ae7df669ffba0068p-4l,/* GNU libc 1.79322 */
    0,                       /* icc */
    0,                       /* AMD LibM */
    0,                       /* Newlib */
    0,                       /* OpenLibm */
    0,                       /* Musl */
    0,                       /* Apple */
    0,                       /* llvm */
    0,                       /* libmvec */
    0,                       /* msvc */
    -0x3.f25066fd5ea7265p-4l,/* freebsd 0.796643 */
    0,                       /* ArmPL */

    /* exp10m1 */
    0x2.c7fd02fd98797bf4p-4l,/* GNU libc 3.56736 */
    0,                       /* icx */
    0,                       /* AMD LibM */
    0,                       /* Newlib */
    0,                       /* OpenLibm */
    0,                       /* Musl */
    0,                       /* Apple */
    0,                       /* llvm */
    0,                       /* libmvec */
    0,                       /* msvc */
    0,                       /* freebsd */
    0,                       /* ArmPL */

    /* exp2m1 */
    0x9.3c1d13fc7c58944p-4l, /* GNU libc 3.19204 */
    0,                       /* icx */
    0,                       /* AMD LibM */
    0,                       /* Newlib */
    0,                       /* OpenLibm */
    0,                       /* Musl */
    0,                       /* Apple */
    0,                       /* llvm */
    0,                       /* libmvec */
    0,                       /* msvc */
    0,                       /* freebsd */
    0,                       /* ArmPL */

    /* log10p1 */
    0x4.a486e7fa771f839p-4l, /* GNU libc 4.16799 */
    0,                       /* icx */
    0,                       /* AMD LibM */
    0,                       /* Newlib */
    0,                       /* OpenLibm */
    0,                       /* Musl */
    0,                       /* Apple */
    0,                       /* llvm */
    0,                       /* libmvec */
    0,                       /* msvc */
    0,                       /* freebsd */
    0,                       /* ArmPL */

    /* log2p1 */
    0x5.843d01be597f38fp-4l, /* GNU libc 3.94667 */
    0,                       /* icx */
    0,                       /* AMD LibM */
    0,                       /* Newlib */
    0,                       /* OpenLibm */
    0,                       /* Musl */
    0,                       /* Apple */
    0,                       /* llvm */
    0,                       /* libmvec */
    0,                       /* msvc */
    0,                       /* freebsd */
    0,                       /* ArmPL */

    /* sinpi */
    0x2.8bea50886e293398p-8l,/* GNU libc 1.79410 */
    0,                       /* icc */
    0,                       /* AMD LibM */
    0,                       /* Newlib */
    0,                       /* OpenLibm */
    0,                       /* Musl */
    0,                       /* Apple */
    0,                       /* llvm */
    0,                       /* libmvec */
    0,                       /* msvc */
    -0x4.019c9d05624aa028p-4l, /* freebsd 0.796688 */
    0,                       /* ArmPL */

    /* tanpi */
    -0x3.a8c5dc49265fd13p+0l,/* GNU libc 3.01615 */
    0,                       /* icc */
    0,                       /* AMD LibM */
    0,                       /* Newlib */
    0,                       /* OpenLibm */
    0,                       /* Musl */
    0,                       /* Apple */
    0,                       /* llvm */
    0,                       /* libmvec */
    0,                       /* msvc */
    -0x1.7fffffffd731f108p+0l,/* freebsd 1.49749 */
    0,                       /* ArmPL */

    /* rsqrt */
    0,                       /* GNU libc */
    0x1.61e30a1ac16221eap+12600l, /* icc 0.500002 */
    0,                       /* AMD LibM */
    0,                       /* Newlib */
    0,                       /* OpenLibm */
    0,                       /* Musl */
    0,                       /* Apple */
    0,                       /* llvm */
    0,                       /* libmvec */
    0,                       /* msvc */
    0,                       /* freebsd */
    0,                       /* ArmPL */
  };
TYPE extra[SIZE_EXTRA] = {
  0xf.fffffffffffffffp-4l,
  -0xe.13d27cea72c3398p-20l, /* erf freebsd */
  0x2.6882b807049f54ap-4l,   /* exp10m1 glibc amd 3.84602 */
  0x9.58009dd25960a7bp-4l,   /* exp2m1 glibc amd 3.22750 */
  0x5.dda3a1f4c4020ebp-4l,   /* log2p1 glibc amd 4.04684 */
  };
#endif
#ifdef USE_FLOAT128
  TYPE worst[SIZE] = {
    /* acos */
    Q(0x9.fdbe71e81d65064f0f24b2602998p-4),    /* GNU libc  1.27701 */
    Q(0xf.f80616c2416bf63c33a739ae3a08p-4),    /* icc       0.501414 */
    /* acosh */
    Q(0x1.0f97586eba090200118df0902f99p+0),    /* GNU libc  3.99173 */
    Q(0x1.004ae7a1e9d7b621b12baeda616dp+0),    /* icc 19.1.3.304 0.500643 */
    /* asin */
    Q(0x7.79659a0b568bad280c8ec7eb8278p-4),    /* GNU libc  1.19481 */
    Q(0x7.ff86cc20db4e6f7fd33ce212282cp-8),    /* icc       0.501448 */
    /* asinh */
    Q(0x5.a924236647ffb723576b172b52fcp-4),    /* GNU libc 3.94074 */
    Q(0x1.0000f6bea05a0cafd1e775e627d3p-4),    /* icc 0.500541 */
    /* atan */
    Q(0x3.7ff864717fc99760d470d1a994cp-4),     /* GNU libc  1.40606 */
    Q(-0x1.15eb4e54ee6ca35bf8b1764f30d4p+0),   /* icc       0.500303 */
    /* atanh */
    Q(0x2.c02a24f3472c7840afbd8cfb68bap-4),    /* GNU libc  3.88862 */
    Q(-0xd.9fe29c463116c87fa567e436489p-8),    /* icc 19.1.3.304 0.500568 */
    /* cbrt */
    Q(-0x5.a837d1198a72e5a89695db79896cp-13792),/* GNU libc 0.735698 */
    Q(-0x2.10d29fbb2036d1d7ffdd8bf63184p+10912),/* icc 0.500125 */
    /* cos */
    Q(-0x3.08db9df46e0cd142071fdec7eb6p+64),   /* GNU libc  1.51382 */
    Q(-0x6.081f6e15f81d27ac2a6038eed3bp+2232), /* icc       0.500769 */
    /* cosh */
    Q(-0x2.c5d376fd225ce5739bef59cb0e16p+12),  /* GNU libc  1.91427 */
    Q(-0x2.ba5adc2ddaf3f5466db2cd018394p+4),   /* icc 0.500563 */
    /* erf */
    Q(0xd.f3a140b19b0e7d0fafae7eec5ebp-4),     /* GNU libc  1.41046 */
    Q(0x5.a5182e2e3fce6963a492839ebb3cp-8),    /* icc       0.500649 */
    /* erfc */
    Q(0x1.517e84504890cba9f9f65ff93206p+0),    /* GNU libc  4.37642 */
    Q(0x6.0a5ca72c4efcd78f90acc0aefbbp+0),     /* icc 0.503913 */
    /* exp */
    Q(-0x2.c5b323ac8f24d66ed41ee61ab6bap+12),  /* glibc 0.750011 */
    Q(-0x5.6622c128e27c6a8c991743947adcp-8),   /* icc 0.500448 */
    /* exp10 */
    Q(0x3.e9d3cc7e0cbdc5bc7fdfc1932fd6p+0),    /* GNU libc 1.99994 */
    Q(0x1.1e2a2ef09a4f66e4d3648a85045bp+12),   /* icc 0.500481 */
    /* exp2 */
    Q(0x1.fffe69758fd951b5213a6d47be1ap+0),    /* GNU libc 1.07279 */
    Q(-0x7.cab667376a3dd98217d7b028adccp-8),   /* icc 0.500374 */
    /* expm1 */
    Q(0x5.a1195b05aec378d0b236943f4a18p-4),    /* GNU libc 1.63777 */
    Q(0x8.ca3ec068eee81b45c0adcae049ap+4),     /* icc 0.500436 */
    /* j0 */
    Q(-0x8.a75ab6666f64eae68f8eb383dad8p+0),   /* GNU libc 4.09655e+32 */
    Q(0x3.7c3f883498c0d5e0dab7e54a98b2p+4),    /* icc 19.1.3.304 2.89264e+28 */
    /* j1 */
    Q(-0x1.7059c8d303730c6b82b12d9941b9p+8),   /* GNU libc  3.56308e+33 */
    Q(-0x1.7059c8d303730c6b82b12d9941b9p+8),   /* icc       3.32705e+31 */
    /* lgamma */
    Q(-0x3.ec2152452b5eaf0f070d215b3418p+0),   /* GNU 12.9968 */
    Q(-0x3.24c1b793cb35efb8be699ad3d9bap+0),   /* icc 19.1.3.304 2.78519e+30 */
    /* log */
    Q(0xf.d016f49074a9c4fe793af2394278p-4),    /* glibc 1.04404 */
    Q(0xc.4806c5e4877bbeb4b44ed03d9f18p-5364), /* icc 0.500099 */
    /* log10 */
    Q(0x1.6a291ea0aa11fb374f1df8b3ac6bp+0),    /* GNU libc 2.00831 */
    Q(0x1.9b621e77f399e4a8c1a85a964e94p-12364),/* icc 0.500174 */
    /* log1p */
    Q(0x6.a0aed5f6dad05d6ff33ecd883dc8p-4),    /* GNU libc 3.50265 */
    Q(-0x6.2611e37be5cf4388865319f859b4p-12),  /* icc 0.500299 */
    /* log2 */
    Q(0xb.54170d5cfa8fd72a47d6bda19068p-4),    /* GNU libc 3.30084 */
    Q(0xf.f63cee8e97ac6783532625273eap-4),     /* icc 0.5003182973124203 */
    /* sin */
    Q(0x5.6a5005df151cc2274e119666a9c8p+64),   /* GNU libc  1.51675 */
    Q(0x4.246e3c1f1094e4159999f13cff24p+5604), /* icc       0.500790 */
    /* sinh */
    Q(0x6.7e79f3aada38698b910c300b19b8p-4),    /* GNU libc  2.06202 */
    Q(-0x1.6606d9c89bc66d481844a8589dcbp+0),   /* icc 0.500741 */
    /* sqrt */
    Q(0xf.fffffffffffffffffffffffffff8p-4),    /* GNU libc  0.500 */
    Q(0xf.fffffffffffffffffffffffffff8p-4),    /* icc 19.1.3.304 0.500 */
    /* tan */
    Q(-0x3.832b771f9462df46117b6a863fa2p+8),   /* GNU libc  1.05232 */
    Q(0xb.eb95e948d6f2a74a1d3a7694bd88p+3816), /* icc       0.501580 */
    /* tanh */
    Q(-0x3.c26abeca541298cca288adbd1e12p-4),   /* GNU libc 2.38003 */
    Q(-0x2.01d7bf6773e2b04acd388c84cd4ep-4),   /* icc      0.500440 */
    /* tgamma */
    Q(-0x1.62ab0823decc5cf957d9a218cf27p+4),   /* GNU libc  10.6904 */
    Q(0x2.00003274fc8659f8ed68e96e0378p-16224),/* icc       8193.46 */
    /* y0 */
    Q(0x6.b99c822052e965e1754eb5ffeb08p+4),    /* GNU libc  1.68281e+33 */
    Q(0x3.9561432d16442ec543c74876d1c8p+4),    /* icc 4.7897e+27 */
    /* y1 */
    Q(0x2.3277da9bfe485c85c35e5bcc806p+0),     /* GNU libc  3.46847e+33 */
    Q(0x2.80bc307275f6a6a3feb2ab211838p+4),    /* icc 1.44943e+30 */
    /* acospi */
    Q(0xb.50b04f0a7917332097372387f018p-4),    /* GNU libc 1.69468 */
    Q(0x0p0),                                  /* icc */
    /* asinpi */
    Q(-0xa.7ca6c96caefe80b9d757de58a578p-4),   /* GNU libc 1.85708 */
    Q(0x0p0),                                  /* icc */
    /* atanpi */
    Q(0x3.2d7177f38d6ee175de059ee6394p-4),     /* GNU libc 2.31307 */
    Q(0x0p0),                                  /* icc */
    /* cospi */
    Q(-0x2.5845a017ae769ea1c20e04c7e47cp+28),  /* glibc 1.78487 */
    Q(0x0p0),                                  /* icx */
    /* exp10m1 */
    Q(0xb.2ee9062818e91afe8e80fae1818p-4),     /* GNU libc 3.24981 */
    Q(0x0p0),                                  /* icc */
    /* exp2m1 */
    Q(0x9.1217d12087d76b2ca347683e4b5p-4),     /* GNU libc 2.224991 */
    Q(0x0p0),                                  /* icc */
    /* log10p1 */
    Q(-0x6.eec527c6a8d6e31ca9f0dcdd747cp-4),   /* GNU libc 3.46319 */
    Q(0x0p0),                                  /* icc */
    /* log2p1 */
    Q(-0x7.fff3a57fdd2666bcecba33bd89f4p-4),   /* GNU libc 3.34125 */
    Q(0x0p0),                                  /* icc */
    /* rsqrt */
    Q(0x0p0),
    Q(0x1.00db76159f986d3a3614199fd36fp-188),  /* icc 0.500091 */
    /* sinpi */
    Q(-0x1.45f52c8be3999ba924f1f3e5a7b7p-8),   /* glibc 1.78628 */
    Q(0x0p0),                                  /* icx */
    /* tanpi */
    Q(-0x1.a8c117800f4a648c213cd0b6db1fp+0),   /* glibc 3.02592 */
    Q(0x0p0),                                  /* icx */
  };
TYPE extra[SIZE_EXTRA] = {
  0,
  Q(-0x4.179563a9af206c1p+60),                 /* huge error for tgamma with icx 2024 */
  };
#endif
  
  TYPE x;
  double d, Dbest0;
  int i;
  setround (rnd1[rnd]);
  /* first check the given -worst value, if any */
  if (worst_input != 0)
  {
    x = worst_input;
    d = distance (x);
      if (d > Dbest)
        {
          Dbest = d;
          Xmax = x;
        }
  }
  /* first check the 'worst' values for the given library, with the given
     rounding mode */
  for (i = NUMBER; i < SIZE; i+=NLIBS)
    {
      x = worst[i];
      d = distance (x);
      if (d > Dbest)
        {
          Dbest = d;
          Xmax = x;
        }
    }
  Dbest0 = Dbest;
  /* then check the 'worst' values for the other libraries */
  for (i = 0; i < SIZE; i++)
    {
      if ((i % NLIBS) == NUMBER)
        continue;
      x = worst[i];
      d = distance (x);
      if (d > Dbest)
        {
          Dbest = d;
          Xmax = x;
        }
    }
  for (i = 0; i < SIZE_EXTRA; i++)
    {
      x = extra[i];
      d = distance (x);
      if (d > Dbest)
      {
        Dbest = d;
        Xmax = x;
      }
    }

  /* then read extra file if given */
  if (extra_file != NULL)
  {
    char *buf = NULL;
    size_t buflength = 0;
    ssize_t nn;
    while ((nn = getline(&buf, &buflength, extra_file)) >= 0) {
      if (nn > 0 && buf[0] == '#')
        continue;
#ifdef USE_FLOAT
      if (sscanf (buf, "%a", &x) == 1)
#elif defined(USE_DOUBLE)
      if (sscanf (buf, "%la", &x) == 1)
#else
      fprintf (stderr, "extra_file not implemented for this type\n");
      exit (1);
#endif
      {
        d = distance (x);
        if (d > Dbest)
        {
          Dbest = d;
          Xmax = x;
        }
      }
    }
  }
#endif /* WORST */

  int n;
  /* Apparently Visual Studio does not properly set the number of threads. */
#pragma omp parallel
  if (nthreads <= 0)
    nthreads = omp_get_num_threads ();
  if (verbose)
    printf ("Using %d threads\n", nthreads);
  omp_set_num_threads (nthreads);

  assert (nthreads <= MAX_THREADS);

#pragma omp parallel for
  for (n = 0; n < nthreads; n++)
    {
      Seed[n] = seed + n;
      doit (Seed[n]);
    }
#ifdef WORST
  if (Dbest > Dbest0)
    printf ("NEW ");
#endif
  /* the ulps() function assumes we are in the target rounding mode */
  setround (rnd1[rnd]);
  double Dbestu = ulps (Xmax);
  setround (FE_TONEAREST);
  printf ("%s %d %d ", NAME, mode_best, Rbest);
  print_type_hex (Xmax);
  printf (" [%.0f]", Dbestu);
  printf (" [");
  print_error (Dbest);
  setround (FE_UPWARD);
  printf ("] %.6g %.16g", Dbest, Dbest);
  /* reset to the current rounding mode */
  setround (rnd1[rnd]);
  printf ("\n");
#ifdef CRLIBM /* force verification for CRLIBM */
  if (1)
#else
  if (verbose)
#endif
    {
      mpfr_t xx, yy;
      TYPE y, z = WRAPPER (Xmax);
      printf ("libm gives ");
      print_type_hex (z);
      printf ("\n");
      mpfr_set_emin (EMIN + 1);
      mpfr_set_emax (EMAX);
      mpfr_init2 (xx, PREC);
      mpfr_init2 (yy, PREC);
      mpfr_set_type (xx, Xmax, MPFR_RNDN);
      int ret = MPFR_FOO (yy, xx, rnd2[rnd]);
      mpfr_subnormalize (yy, ret, rnd2[rnd]);
      y = mpfr_get_type (yy, MPFR_RNDN);
      printf ("mpfr gives ");
      print_type_hex (y);
      printf ("\n");
#ifdef CRLIBM
      if (y != z)
        printf ("BUG in MPFR or CRLIBM\n");
#endif
      fflush (stdout);
      mpfr_clear (xx);
      mpfr_clear (yy);
    }
  fflush (stdout);
  mpfr_free_cache ();
#ifdef STAT
  printf ("eval_heuristic=%lu eval_exhaustive=%lu\n",
          eval_heuristic, eval_exhaustive);
#endif
  return 0;
}
