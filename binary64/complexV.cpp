/* Search worst cases of a univariate complex function.

   This program is open-source software distributed under the terms 
   of the GNU General Public License <http://www.fsf.org/copyleft/gpl.html>.

   Compile with:

   gcc -DFOO=exp -DUSE_xxx -O3 complex.c -lmpc -lmpfr -lgmp -lm -fopenmp
   icx -DFOO=exp -Qoption,cpp,--extended_float_types -no-ftz -DUSE_xxx -O3 complex.c -lmpc -lmpfr -lgmp -fopenmp
   c++ -Ofast -march=native -DFOO=sqrt -DGLIBC -DUSE_FLOAT complexV.cpp -L/afs/cern.ch/user/i/innocent/w5/lib -lmpc -lmpfr -lgmp -lm -fopenmp -I/afs/cern.ch/user/i/innocent/w5/mpinc/ -DVECTORIZE
   nvcc -DFOO=sqrt -DGLIBC -DUSE_FLOAT  -DVECTORIZE -O3 -std=c++17 --compiler-options="-O3 -march=native -L/afs/cern.ch/user/i/innocent/w5/lib -lmpc -lmpfr -lgmp -lm -fopenmp -I/afs/cern.ch/user/i/innocent/w5/mpinc/" --expt-relaxed-constexpr

   where xxx is FLOAT, DOUBLE, LDOUBLE, or FLOAT128.

   You can add -DWORST to use some precomputed values to guide the search.

   You can add -DGLIBC to print the GNU libc release (with -v).

   Command-line options:
   -threshold nnn : set the effort threshold to nnn, the runtime is roughly
                    proportional to nnn
   -seed nnn   sets the random seed to nnn
   -mode k     sets the mode of the heuristic search to k (0 <= k <= 2).
               k=0: considers the maximal error in the range
               k=1: considers the average error in the range
               k=2: considers the estimated maximal error
   -v          verbose
   -rndn       rounding to nearest (default)
   -rndz       rounding towards zero
   -rndu       rounding towards +Inf
   -rndd       rounding towards -Inf
   -nthreads n uses n threads
   -worst xxx,yyy uses xxx,yyy (hexadecimal values) as worst values
   -xmin  xxx  use xxx a minimal value for x
   -xmax  xxx  use xxx a maximal value for x
   -ymin  yyy  use yyy a minimal value for y
   -ymax  yyy  use yyy a maximal value for y

   References and credit:
   * the idea to sample several intervals instead of only one is due to
     Eric Schneider
*/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE /* to define f128 functions */
#endif

#define RANK /* print the maximal list-rank of best values */

#if defined(__clang__) && !defined(__INTEL_CLANG_COMPILER)
#define _Float128 _BitInt(128)
#endif

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <limits.h>
#ifndef NO_FLOAT128
#define MPFR_WANT_FLOAT128
#endif
/* icx does not have _Float128 */
#ifdef __INTEL_CLANG_COMPILER
#define _Float128 __float128
#endif
#include <mpc.h>
#ifndef ICX
#include <math.h>
#else
#include <mathimf.h>
#endif
#ifdef __cplusplus
#include<complex>
#include<limits>
#else
#include <complex.h>
#endif
#include <assert.h>
#include <sys/types.h>
#include <unistd.h>
#ifndef NO_OPENMP
#include <omp.h>
#endif
#include <float.h> /* for DBL_MAX */
#include <fenv.h>
#ifdef _MSC_VER
#include <BaseTsd.h>
typedef SSIZE_T ssize_t;
#endif

/* define GLIBC to print the GNU libc version */
#ifdef GLIBC
#include <gnu/libc-version.h>
#endif

#ifdef NEWLIB
/* without this, newlib says: undefined reference to `__errno' */
int errno;
int* __errno () { return &errno; }
#endif

#if defined(__aarch64__) && defined(__ARM_FEATURE_RNG)
  #define ARM_USE_HARDWARE_RNG
  #include <arm_acle.h>
#endif

#define MAX_THREADS 192

#ifndef atan2pif
extern float atan2pif (float, float);
#endif

/* rounding modes */
int rnd1[] = { FE_TONEAREST, FE_TOWARDZERO, FE_UPWARD, FE_DOWNWARD };
mpfr_rnd_t rnd2[] = { MPFR_RNDN, MPFR_RNDZ, MPFR_RNDU, MPFR_RNDD };
mpc_rnd_t crnd[] = { MPC_RNDNN, MPC_RNDZZ, MPC_RNDUU, MPC_RNDDD };
int rnd = 0; /* default rounding mode is MPFR_RNDN */
int verbose = 0;

/* mode (0,1,2), if -1 set according to omp_get_thread_num() */
int use_mode = -1;

FILE *extra_file = NULL;

#define CAT1(X,Y) X ## Y
#define CAT2(X,Y) CAT1(X,Y)
#ifndef MPC_FOO
#define MPC_FOO CAT2(mpc_,FOO)
#endif
#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)
#define NAME TOSTRING(FOO)

#ifdef USE_FLOAT
#if defined(USE_DOUBLE) || defined(USE_LDOUBLE) || defined(USE_FLOAT128)
#error "only one of USE_FLOAT, USE_DOUBLE, USE_LDOUBLE or USE_FLOAT128 can be defined"
#endif
#define TYPE float
#define UTYPE uint32_t
#ifdef __cplusplus
#define CTYPE std::complex<float>
#define CONSTRUCT  CTYPE
#define FOO1 FOO
#define FOO2 FOO1
#else
#define CTYPE float complex
#define CONSTRUCT CMPLXF
#define FOO1 CAT2(c,FOO)
#define FOO2 CAT2(FOO1,f)
#endif
#define EMAX 128
#define EMIN -149
#define PREC 24
#define mpfr_set_type mpfr_set_flt
#define mpfr_get_type mpfr_get_flt
#define TYPE_MAX FLT_MAX
#endif

#ifdef USE_DOUBLE
#if defined(USE_FLOAT) || defined(USE_LDOUBLE) || defined(USE_FLOAT128)
#error "only one of USE_FLOAT, USE_DOUBLE, USE_LDOUBLE or USE_FLOAT128 can be defined"
#endif
#define TYPE double
#define UTYPE uint64_t
#ifdef __cplusplus
#define CTYPE std::complex<double>
#define CONSTRUCT  CTYPE
#define FOO1 FOO
#define FOO2 FOO1
#else
#define CTYPE double complex
#define CONSTRUCT CMPLX
#define FOO1 CAT2(c,FOO)
#define FOO2 FOO1
#endif
#define EMAX 1024
#define EMIN -1074
#define PREC 53
#define mpfr_set_type mpfr_set_d
#define mpfr_get_type mpfr_get_d
#define TYPE_MAX DBL_MAX
#endif

#ifdef USE_LDOUBLE
#if defined(USE_FLOAT) || defined(USE_DOUBLE) || defined(USE_FLOAT128)
#error "only one of USE_FLOAT, USE_DOUBLE, USE_LDOUBLE or USE_FLOAT128 can be defined"
#endif
#define TYPE long double
#define UTYPE __uint128_t
#ifdef __cplusplus
#define CTYPE std::complex<long double>
#define CONSTRUCT  CTYPE
#define FOO1 FOO
#define FOO2 FOO1
#else
#define CTYPE long double complex
#define FOO1 CAT2(c,FOO)
#define FOO2 CAT2(FOO1,l)
#endif
#define EMAX 16384
#define EMIN -16445
#define PREC 64
#define mpfr_set_type mpfr_set_ld
#define mpfr_get_type mpfr_get_ld
#define TYPE_MAX LDBL_MAX
#endif

#ifdef USE_FLOAT128
#if defined(USE_FLOAT) || defined(USE_DOUBLE)
#error "only one of USE_FLOAT, USE_DOUBLE or USE_FLOAT128 can be defined"
#endif
#if defined(__INTEL_COMPILER) || defined(__INTEL_CLANG_COMPILER)
#define TYPE _Quad
#define CTYPE _Quad complex
#define FOO3 CAT2(FOO,q)
#define FOO2 CAT2(__c,FOO3)
extern _Quad FOO2 (_Quad);
#define Q(x) (x ## q)
#else
#define TYPE _Float128
#ifdef __cplusplus
#define CTYPE std::complex<_Float128>
#define CONSTRUCT  CTYPE
#define FOO1 FOO
#define FOO2 FOO1
#else
#define CTYPE _Float128 complex
#define FOO1 CAT2(c,FOO)
#define FOO2 CAT2(FOO1,f128)
#endif
#define Q(x) (x ## f128)
#endif
#define TYPE_MAX Q(0xf.fffffffffffffffffffffffffff8p+16380)
#define UTYPE __uint128_t
#define EMAX 16384
#define EMIN -16494
#define PREC 113
#define mpfr_set_type mpfr_set_float128
#define mpfr_get_type mpfr_get_float128
#endif

static void
print_type_hex (TYPE x)
{
#ifdef USE_FLOAT
  printf ("%a", x);
#endif
#ifdef USE_DOUBLE
  printf ("%a", x);
#endif
#ifdef USE_LDOUBLE
  mpfr_t y;
  mpfr_init2 (y, PREC);
  mpfr_set_type (y, x, MPFR_RNDN);
  mpfr_printf ("%Ral", y);
  mpfr_clear (y);
#endif
#ifdef USE_FLOAT128
  mpfr_t y;
  mpfr_init2 (y, PREC);
  mpfr_set_type (y, x, MPFR_RNDN);
  mpfr_printf ("%Ra", y);
  mpfr_clear (y);
#endif
}

#define CUDAFOO FOO

#ifdef __HIPCC__
#include "hipTypes.h"
#else
#include "cudaTypes.h"
#endif
#ifdef __CUDACC__
  using CMData = cudaMath::Data<cuda::std::complex<TYPE>>;
#else
 using CMData = cudaMath::Data<std::complex<TYPE>>;
#endif


typedef union { UTYPE n; TYPE x; } union_t;

TYPE
get_type (UTYPE n)
{
  union_t u;
  u.n = n;
  return u.x;
}

UTYPE
get_utype (TYPE x)
{
  union_t u;
  u.x = x;
  return u.n;
}

#ifdef __cplusplus
  template <typename T>
  constexpr bool isFinite(T x);

  template <typename T>
  constexpr bool isNotFinite(T x) {
    return !isFinite(x);
  }

  template <>
  constexpr bool isFinite(float x) {
    const unsigned int mask = 0x7f800000;
    union {
      unsigned int l;
      float d;
    } v = {.d = x};
    return (v.l & mask) != mask;
  }

  template <>
  constexpr bool isFinite(double x) {
    const unsigned long long mask = 0x7FF0000000000000LL;
    union {
      unsigned long long l;
      double d;
    } v = {.d = x};
    return (v.l & mask) != mask;
  }

  template <>
  constexpr bool isFinite(long double x) {
    double xx = x;
    return isFinite(xx);
  }
#endif

#ifdef USE_LDOUBLE
static int
is_valid (TYPE x)
{
  UTYPE n = get_utype (x);
  int e = (n >> 64) & 0x7fff; /* exponent */
  uint64_t s = (uint64_t) n;  /* significand */
  if (e == 0) return (s >> 63) == 0;
  else return (n >> 63) & 1;
}
#endif

#ifdef VECTORIZE
enum class What {one, fill, compute};
What what[CMData::maxNumOfThreads];
int icurr[CMData::maxNumOfThreads];
int irand[CMData::maxNumOfThreads];
CMData * gdata = nullptr;
inline CMData & cmdata() {  return *gdata;}
#endif

/* return the normwise distance between FOO2(x+i*y) and the value z computed by
   MPC with precision PREC+64, measured in terms of ulp(|z|) */
static double
distance (TYPE x, TYPE y, int tid)
{
#ifdef VECTORIZE
   if (What::fill==what[tid]) {
     assert(icurr[tid]<cmdata().bunchSize);
     cmdata().xpH[tid][icurr[tid]++] = {x,y};
     // std::cout << "fill  " << icurr[tid]  << ' ' << cmdata().xpH[tid][icurr[tid]] << std::endl;
     return 0;
   }
#endif
  mpc_t xx, tt;
  mpfr_t yyx, yyy, zzx, zzy;
  mpfr_exp_t ey; 
  TYPE zx, zy;
  double ret = 0;

#ifdef __cplusplus
//  auto r = std::hypot(x,y);
  if (isNotFinite (x) || isNotFinite (y) 
//      || r>=std::numeric_limits<TYPE>::max()
//      || (TYPE(1)/r)>=std::numeric_limits<TYPE>::max()
  )
#else
  if (isnan (x) || isnan (y))
#endif
  {
#ifdef VECTORIZE
   // std::cout << "nan  " << icurr[tid]  << ' ' << x<<','<<y << ' ' << cmdata().xpH[tid][icurr[tid]] << std::endl;;
   if (What::compute==what[tid]) icurr[tid]++;
#endif
    return 0;
  }

#ifdef USE_LDOUBLE
  if (!is_valid (x) || !is_valid (y))
    return 0;
#endif

  CTYPE z_in = CONSTRUCT (x, y);

#ifdef VECTORIZE
   CTYPE z_out;
   if (What::compute==what[tid]) {
     assert(icurr[tid]<cmdata().bunchSize);
     // std::cout << "comp  " << icurr[tid]  << ' ' << z_in << ' ' << cmdata().xpH[tid][icurr[tid]] << std::endl;;
     assert(z_in == cmdata().xpH[tid][icurr[tid]]);
     z_out = cmdata().zpH[tid][icurr[tid]++];
   } else {
    assert(What::one==what[tid]);
    assert(0==tid);
    cmdata().xpH[tid][0] = z_in;
    auto zp = wrap_foo(cmdata(),1,tid);
    z_out = zp[0];
  }
#else
  CTYPE z_out = FOO2 (z_in);
#endif  

#ifdef __cplusplus  
  zx = z_out.real();
  zy = z_out.imag();
#else
  zx = creal (z_out);
  zy = cimag (z_out);
#endif

#ifdef IGNORE_INF
  if ( isNotFinite(zx) || isNotFinite(zy) ) return 0;
#endif

  mpfr_exp_t emax = mpfr_get_emax ();
  mpfr_set_emax (EMAX);
  mpc_init2 (xx, PREC);
  mpfr_init2 (yyx, PREC);
  mpfr_init2 (yyy, PREC);
  mpc_init2 (tt, PREC+64);
  mpfr_init2 (zzx, PREC+64);
  mpfr_init2 (zzy, PREC+64);
  mpfr_set_type (mpc_realref(xx), x, MPFR_RNDN);
  mpfr_set_type (mpc_imagref(xx), y, MPFR_RNDN);
  MPC_FOO (tt, xx, crnd[rnd]);
  mpc_real (zzx, tt, MPFR_RNDN);
  mpc_imag (zzy, tt, MPFR_RNDN);
  int isnan_z = isnan (zx) || isnan (zy);
  int isnan_zz = mpfr_nan_p (zzx) || mpfr_nan_p (zzy);
  if (isnan_z && isnan_zz)
    goto end; // both results are NaN
  if (isnan_z || isnan_zz) // only one result is NaN
  {
    ret = 0x1p1023 + 0x1p1023;
    goto end;
  }
  mpfr_set_emax (mpfr_get_emax_max ());

  if (isinf (zx) && mpfr_inf_p (zzx))
  {
    /* both return Inf: if same sign, consider them as equal */
    if ((zx > 0 && mpfr_sgn (zzx) > 0) || (zx < 0 && mpfr_sgn (zzx) < 0))
    {
      mpfr_set_ui (yyx, 0, MPFR_RNDN);
      mpfr_set_ui (zzx, 0, MPFR_RNDN);
    }
    // otherwise they have different sign, and the difference will be Inf
  }
  else if (isinf (zx) && mpfr_inf_p (zzx) == 0)
    {
      /* libm returns Inf, MPFR not, we assume the libm rounded to the next
         number after the largest finite number in the format */
      mpfr_set_ui_2exp (yyx, 1, EMAX, MPFR_RNDN);
      if (zx < 0)
        mpfr_neg (yyx, yyx, MPFR_RNDN);
    }
  else if (!isinf (zx) && mpfr_inf_p (zzx))
  {
    /* MPFR yields Inf, but libm no: we recompute the MPFR result with
       unbounded exponent */
    MPC_FOO (tt, xx, crnd[rnd]);
    mpc_real (zzx, tt, MPFR_RNDN);
    mpfr_set_type (yyx, zx, MPFR_RNDN);
  }
  else
    mpfr_set_type (yyx, zx, MPFR_RNDN);

  if (isinf (zy) && mpfr_inf_p (zzy))
  {
    /* both return Inf: if same sign, consider them as equal */
    if ((zy > 0 && mpfr_sgn (zzy) > 0) || (zy < 0 && mpfr_sgn (zzy) < 0))
    {
      mpfr_set_ui (yyy, 0, MPFR_RNDN);
      mpfr_set_ui (zzy, 0, MPFR_RNDN);
    }
    // otherwise they have different sign, and the difference will be Inf
  }
  else if (isinf (zy) && mpfr_inf_p (zzy) == 0)
    {
      /* libm returns Inf, MPFR not, we assume the libm rounded to the next
         number after the largest finite number in the format */
      mpfr_set_ui_2exp (yyy, 1, EMAX, MPFR_RNDN);
      if (zy < 0)
        mpfr_neg (yyy, yyy, MPFR_RNDN);
    }
  else if (!isinf (zy) && mpfr_inf_p (zzy))
  {
    /* MPFR yields Inf, but libm no: we recompute the MPFR result with
       unbounded exponent */
    MPC_FOO (tt, xx, crnd[rnd]);
    mpc_real (zzy, tt, MPFR_RNDN);
    mpfr_set_type (yyy, zy, MPFR_RNDN);
  }
  else
    mpfr_set_type (yyy, zy, MPFR_RNDN);

  mpfr_sub (zzx, zzx, yyx, MPFR_RNDN);
  mpfr_sub (zzy, zzy, yyy, MPFR_RNDN);
  mpfr_hypot (zzx, zzx, zzy, MPFR_RNDN); // zzx is the difference norm
  mpfr_hypot (zzy, mpc_realref(tt), mpc_imagref(tt), MPFR_RNDN);
  if (mpfr_zero_p (zzy))
  {
    if (!mpfr_zero_p (zzx))
      ret = 0x1p1023 + 0x1p1023;
    goto end;
  }
  // the smallest normal number is 2^(EMIN+PREC-1)
  ey = mpfr_get_exp (zzy);
  if (mpfr_inf_p (zzy) || ey - PREC >= EMIN) // |tt| is normal
  {
    // divide the difference by ulp(|tt|)
    mpfr_mul_2si (zzx, zzx, PREC - ey, MPFR_RNDN);
  }
  else // divide the difference by 2^EMIN
    mpfr_div_2si (zzx, zzx, EMIN, MPFR_RNDN);
  ret = mpfr_get_d (zzx, MPFR_RNDU);
 end:
  mpc_clear (xx);
  mpfr_clear (yyx);
  mpfr_clear (yyy);
  mpfr_clear (zzx);
  mpfr_clear (zzy);
  mpc_clear (tt);
  mpfr_set_emax (emax);
  return ret;
}

uint64_t threshold = 1000000;
double Threshold;

static unsigned int Seed[128];

uint32_t my_rand_wrapper (int tid)
{
  uint32_t val = 0;
#ifdef VECTORIZE
  assert(irand[tid]<cmdata().bunchSize);
  if (What::compute==what[tid]) {
    val = cmdata().rndm[tid][irand[tid]++];
    // std::cout << "comp " << irand[tid] << ' ' << val << std::endl;
    return val;
  } 
#endif

#ifdef ARM_USE_HARDWARE_RNG
  /* Utilize hardware RNG instructions on the ARM processors that support them.
   *
   * During profiling, it was discovered that almost all of the processing time
   * was spent servicing the rand calls due to continual exhaustion of the
   * available system entropy. Using hardware RNG resolved this issue.
   *
   * TODO: The rndr instruction returns a 64 bit unsigned integer, of which we
   *       throw out 32 bits in order to be compatible with the existing code
   *       as it was written with rand() in mind, which returns a 32 bit
   *       integer. Ideally, we would refactor the code so that we don't throw
   *       out the bits just to request another 32 bits to build a full 64 bit
   *       value when USE_DOUBLE is defined. */
  uint64_t temp = 0;
  int res = __rndr(&temp);
  assert(res == 0);
  val = (uint32_t)temp;
#else
  val = rand_r (Seed + tid);
#endif
#ifdef VECTORIZE
  if (What::fill==what[tid]) {
    cmdata().rndm[tid][irand[tid]++]=val;
    // std::cout << "fill " << irand[tid] << ' ' << val << std::endl;
  }
#endif
  return val;
}

#if RAND_MAX == 2147483647
#ifdef USE_FLOAT
static uint32_t
my_random (uint32_t n, int tid)
{
  uint32_t ret = my_rand_wrapper (tid);
  if (n > RAND_MAX)
    ret = (ret << 31) | my_rand_wrapper (tid);
  return ret % n;
}
#else /* USE_FLOAT */
#ifdef USE_DOUBLE
static uint64_t
my_random (uint64_t n, int tid)
{
  uint64_t ret = my_rand_wrapper (tid);
  if (n > RAND_MAX)
    {
      ret = (ret << 31) | my_rand_wrapper (tid);
      /* now ret <= 2^62-1 */
      if (n >> 62)
        ret = (ret << 31) | my_rand_wrapper (tid);
    }
  return ret % n;
}
#else /* USE_LDOUBLE or USE_FLOAT128 */
static __uint128_t
my_random (__uint128_t n, int tid)
{
  __uint128_t ret = my_rand_wrapper (tid);
  if (n > RAND_MAX)
    {
      ret = (ret << 31) | my_rand_wrapper (tid);
      /* now ret <= 2^62-1 */
      if (n >> 62)
        {
          ret = (ret << 31) | my_rand_wrapper (tid);
          /* now ret <= 2^93-1 */
          if (n >> 93)
            {
              ret = (ret << 31) | my_rand_wrapper (tid);
              /* now ret <= 2^93-1 */
              if (n >> 124)
                ret = (ret << 31) | my_rand_wrapper (tid);
            }
        }
    }
  return ret % n;
}
#endif /* USE_DOUBLE */
#endif /* USE_FLOAT */

#elif RAND_MAX == 0x7fff /* Microsoft Visual C/C++ */

#ifdef USE_FLOAT
static uint32_t
my_random (uint32_t n, int tid)
{
  uint32_t ret = my_rand_wrapper (tid);
  if (n > RAND_MAX)
    {
      ret = (ret << 15) | my_rand_wrapper (tid);
      /* now ret <= 2^30-1 */
      if (n >> 30)
        ret = (ret << 15) | my_rand_wrapper (tid);
    }
  return ret % n;
}
#endif
#ifdef USE_DOUBLE
static uint64_t
my_random (uint64_t n, int tid)
{
  uint64_t ret = my_rand_wrapper (tid);
  if (n > RAND_MAX)
    {
      ret = (ret << 15) | my_rand_wrapper (tid);
      /* now ret <= 2^30-1 */
      if (n >> 30)
        {
          ret = (ret << 15) | my_rand_wrapper (tid);
          /* now ret <= 2^45-1 */
          if (n >> 45)
            {
              ret = (ret << 15) | my_rand_wrapper (tid);
              /* now ret <= 2^60-1 */
              if (n >> 60)
                ret = (ret << 15) | my_rand_wrapper (tid);
            }
        }
    }
  return ret % n;
}
#endif
#if defined(USE_LDOUBLE) || defined(USE_FLOAT128)
#error "my_random() not yet implemented for 128-bit type"
#endif

#else
#error "Unexpected value of RAND_MAX"
#endif

TYPE Xbest, Ybest; // best values found so far
TYPE Xmin, Xmax, Ymin, Ymax;
double Dbest = 0.0;
int Rbest = -1;
int mode_best = 0;

/* return the maximal error on [nxmin,nxmax] x [nymin,nymax],
   and update nxbest,nybest if it improves distance(x,y) */
static double
max_heuristic1 (UTYPE nxmin, UTYPE nxmax, UTYPE nymin, UTYPE nymax,
                UTYPE *nxbest, UTYPE *nybest, int tid)
{
  TYPE x, y;
  UTYPE i, nx, ny;
  double dbest, dmax, d;
  x = get_type (*nxbest);
  y = get_type (*nybest);
  dbest = distance (x, y, tid);
  dmax = 0;
  for (i = 0; i < threshold; i++)
    {
      nx = nxmin + my_random (nxmax - nxmin, tid);
      ny = nymin + my_random (nymax - nymin, tid);
#ifdef USE_LDOUBLE
      /* for long double, bit 63 must be set */
      nx |= (UTYPE) 1 << 63;
      ny |= (UTYPE) 1 << 63;
#endif
      x = get_type (nx);
      y = get_type (ny);
      d = distance (x, y, tid);
      if (d > dmax)
        {
          dmax = d;
          if (d > dbest)
            {
              dbest = d;
              *nxbest = nx;
              *nybest = ny;
            }
        }
    }
  return dmax;
}

/* return the average error on [nxmin,nxmax] x [nymin,nymax],
   given (nxbest,nybest) */
static double
max_heuristic2 (UTYPE nxmin, UTYPE nxmax, UTYPE nymin, UTYPE nymax,
               UTYPE *nxbest, UTYPE *nybest, int tid)
{
  TYPE x, y;
  UTYPE i, nx, ny;
  double dbest, d, s = 0, n = 0;
  x = get_type (*nxbest);
  y = get_type (*nybest);
  dbest = distance (x, y, tid);
  for (i = 0; i < threshold; i++)
    {
      nx = nxmin + my_random (nxmax - nxmin, tid);
      ny = nymin + my_random (nymax - nymin, tid);
#ifdef USE_LDOUBLE
      /* for long double, bit 63 must be set */
      nx |= (UTYPE) 1 << 63;
      ny |= (UTYPE) 1 << 63;
#endif
      x = get_type (nx);
      y = get_type (ny);
      d = distance (x, y, tid);
      if (d != 0)
        {
          s += d;
          n ++;
        }
      if (d > dbest)
        {
          dbest = d;
          *nxbest = nx;
          *nybest = ny;
        }
    }
  if (n != 0)
    s = s / n;
  return s;
}

/* some libraries do not have log(), for example llvm */
static double mylog (double n)
{
  mpfr_t x;
  mpfr_init2 (x, 53);
  mpfr_set_d (x, n, MPFR_RNDN);
  mpfr_log (x, x, MPFR_RNDN);
  double ret = mpfr_get_d (x, MPFR_RNDN);
  mpfr_clear (x);
  return ret;
}

/* return the estimated maximal error on [nxmin,nxmax] x [nymin,nymax],
   taking into account mean and standard deviation,
   and update (nxbest,nybest) */
static double
max_heuristic3 (UTYPE nxmin, UTYPE nxmax, UTYPE nymin, UTYPE nymax,
               UTYPE *nxbest, UTYPE *nybest, int tid)
{
  TYPE x, y;
  UTYPE i, nx, ny;
  double dbest, d, s = 0, v = 0, n = 0;
  x = get_type (*nxbest);
  y = get_type (*nybest);
  dbest = distance (x, y, tid);
  for (i = 0; i < threshold; i++)
    {
      nx = nxmin + my_random (nxmax - nxmin, tid);
      ny = nymin + my_random (nymax - nymin, tid);
#ifdef USE_LDOUBLE
      /* for long double, bit 63 must be set */
      nx |= (UTYPE) 1 << 63;
      ny |= (UTYPE) 1 << 63;
#endif
      x = get_type (nx);
      y = get_type (ny);
      d = distance (x, y, tid);
      if (d != 0)
        {
          s += d;
          v += d * d;
          n ++;
        }
      if (d > dbest)
        {
          dbest = d;
          *nxbest = nx;
          *nybest = ny;
        }
    }
  /* compute mean and standard deviation */
  if (n != 0)
    {
      s = s / n;
      v = v / n - s * s;
      if (v < 0)
        v = 0;
      double sigma = sqrt (v);
      /* we got n non-zero values out of threshold, thus we should get
         n/threshold*(nxmax-nxmin)*(nymin-nymax) */
      n = n * (double) (nxmax - nxmin) * (double) (nymax - nymin)
        / (double) threshold;
      double logn = mylog (n);
      double t = sqrt (2.0 * logn);
      /* Reference: A note on the first moment of extreme order statistics
         from the normal distribution, Max Petzold,
         https://gupea.ub.gu.se/handle/2077/3092 */
      s = s + sigma * (t - (mylog (logn) + 1.3766) / (2.0 * t));
    }
  return s;
}

#ifdef NO_OPENMP
static int omp_get_thread_num (void) { return 0; }
static int omp_get_num_threads (void) { return 1; }
static void omp_set_num_threads (int i) { }
#endif

static int
mode (void)
{
  if (use_mode != -1)
    return use_mode;
  int i = omp_get_thread_num ();
  /* Modes 1,2 seem to be less efficient, thus we use only 1 thread on each. */
  if (i == 1 || i == 2)
    return i;
  return 0;
}

#ifdef STAT
unsigned long eval_heuristic = 0;
unsigned long eval_exhaustive = 0;
#endif

static double
max_heuristic (UTYPE nxmin, UTYPE nxmax, UTYPE nymin, UTYPE nymax,
               UTYPE *nxbest, UTYPE *nybest, int tid)
{
  if (nxmin == nxmax || nymin == nymax)
    return 0;

  int k = mode ();

#ifdef STAT
#pragma omp atomic update
  eval_heuristic += threshold;
#endif

  if (k == 0)
    return max_heuristic1 (nxmin, nxmax, nymin, nymax, nxbest, nybest, tid);
  else if (k == 1)
    return max_heuristic2 (nxmin, nxmax, nymin, nymax, nxbest, nybest, tid);
  else
    return max_heuristic3 (nxmin, nxmax, nymin, nymax, nxbest, nybest, tid);
}

typedef struct {
  UTYPE nxmin, nxmax;
  UTYPE nymin, nymax;
  double d;
#ifdef RANK
  int rank; /* worst rank */
#endif
} chunk_t;

static double
chunk_size (chunk_t c)
{
  return (double) (c.nxmax - c.nxmin) * (double) (c.nymax - c.nymin);
}

static void
chunk_swap (chunk_t *a, chunk_t *b)
{
  UTYPE tmp;
  tmp = a->nxmin; a->nxmin = b->nxmin; b->nxmin = tmp;
  tmp = a->nxmax; a->nxmax = b->nxmax; b->nxmax = tmp;
  tmp = a->nymin; a->nymin = b->nymin; b->nymin = tmp;
  tmp = a->nymax; a->nymax = b->nymax; b->nymax = tmp;
  double ump;
  ump = a->d; a->d = b->d; b->d = ump;
#ifdef RANK
  int rmp;
  rmp = a->rank; a->rank = b->rank; b->rank = rmp;
#endif
}

typedef struct {
  chunk_t *l;
  int size;
} List_struct;
typedef List_struct List_t[1];

/* Idea from Eric Schneider: instead of sampling only one interval at each
   level of the binary splitting tree, we sample up to LIST_ALLOC intervals,
   and keep the most promising ones. We use the default value suggested by
   Eric Schneider (20). */
#define LIST_ALLOC 20

static void
List_init (List_t L)
{
  L->l = (chunk_t*) malloc (LIST_ALLOC * sizeof (chunk_t));
  L->size = 0;
}

static void
List_init2 (List_t L, UTYPE nxmin, UTYPE nxmax, UTYPE nymin, UTYPE nymax)
{
  L->l = (chunk_t*) malloc (LIST_ALLOC * sizeof (chunk_t));
  L->l[0].nxmin = nxmin;
  L->l[0].nxmax = nxmax;
  L->l[0].nymin = nymin;
  L->l[0].nymax = nymax;
  L->size = 1;
}

#if 0
static void
List_print (List_t L)
{
  for (int i = 0; i < L->size; i++)
    printf ("%.3g ", L->l[i].d);
  printf ("\n");
}

static void
List_check (List_t L)
{
  for (int i = 1; i < L->size; i++)
    if (L->l[i-1].d < L->l[i].d)
      {
        fprintf (stderr, "Error, list not sorted:\n");
        List_print (L);
        exit (1);
      }
}
#endif

static void
List_insert (List_t L, UTYPE nxmin, UTYPE nxmax, UTYPE nymin, UTYPE nymax,
             double d)
{
  if (nxmin == nxmax || nymin == nymax)
    return;
  int i = L->size;
  if (i < LIST_ALLOC || d > L->l[LIST_ALLOC-1].d)
    {
      /* if list is not full, we insert at position i,
         otherwise we insert at position LIST_ALLOC-1 */
      if (i == LIST_ALLOC)
        i--;
      else
        L->size++;
      L->l[i].nxmin = nxmin;
      L->l[i].nxmax = nxmax;
      L->l[i].nymin = nymin;
      L->l[i].nymax = nymax;
      L->l[i].d = d;
      /* now insertion sort */
      while (i > 0 && d > L->l[i-1].d)
        {
          chunk_swap (L->l + (i-1), L->l + i);
#ifdef RANK
          if (L->l[i].rank < i)
            L->l[i].rank = i; /* updates maximal rank */
#endif
          i--;
        }
#ifdef RANK
      L->l[i].rank = i; /* set initial rank */
#endif
    }
  // List_check (L);
}

static void
List_swap (List_t L1, List_t L2)
{
  chunk_t *tmp;
  tmp = L1->l; L1->l = L2->l; L2->l = tmp;
  int ump;
  ump = L1->size; L1->size = L2->size; L2->size = ump;
}

static void
List_clear (List_t L)
{
  free (L->l);
}

static void
exhaustive_search (chunk_t *c, UTYPE *nxbest, UTYPE *nybest, double *dbest,
                   int *rbest, int tid)
{
  for (UTYPE nx = c->nxmin; nx < c->nxmax; nx ++)
    {
      TYPE x = get_type (nx);
      for (UTYPE ny = c->nymin; ny < c->nymax; ny ++)
        {
          TYPE y = get_type (ny);
          double d = distance (x, y, tid);
          if (d > *dbest)
            {
              *dbest = d;
              *nxbest = nx;
              *nybest = ny;
#ifdef RANK
              *rbest = c->rank;
#endif
            }
        }
    }
#ifdef STAT
#pragma omp atomic update
  eval_exhaustive += (c->nxmax - c->nxmin) * (c->nymax - c->nymin);
#endif
}

static void
search_aux (UTYPE nxmin, UTYPE nxmax, UTYPE nymin, UTYPE nymax,
            UTYPE *nxbest, UTYPE *nybest, int tid, List_t L)
{
  double d;
#ifdef VECTORIZE
  what[tid] = What::fill;
  icurr[tid]=0;
  irand[tid]=0;
  assert(0 == max_heuristic (nxmin, nxmax, nymin, nymax, nxbest, nybest, tid));
  wrap_foo(cmdata(),icurr[tid],tid);
  what[tid] = What::compute;
  icurr[tid]=0;
  irand[tid]=0;
#endif
  d = max_heuristic (nxmin, nxmax, nymin, nymax, nxbest, nybest, tid);
  List_insert (L, nxmin, nxmax, nymin, nymax, d);
}

/* search for nxmin <= nx < nxmax and nymin <= ny < nymax,
   where the worst found so far is (nxbest,nybest,dbest) */
static void
search (UTYPE nxmin, UTYPE nxmax, UTYPE nymin, UTYPE nymax,
        UTYPE *nxbest, UTYPE *nybest, double *dbest, int *rbest)
{
  int tid = omp_get_thread_num ();
  List_t L;

  List_init2 (L, nxmin, nxmax, nymin, nymax);
  while (1)
    {
      assert (1 <= L->size && L->size <= LIST_ALLOC);
      double width = chunk_size (L->l[0]);
      if (width <= Threshold) /* exhaustive search */
        {
          for (int i = 0; i < L->size; i++) {
#ifdef VECTORIZE
            what[tid] = What::fill;
            icurr[tid]=0;
            exhaustive_search (L->l + i, nxbest, nybest, dbest, rbest, tid);
            what[tid] = What::compute;
            wrap_foo(cmdata(),icurr[tid],tid);
            icurr[tid]=0;
#endif
            exhaustive_search (L->l + i, nxbest, nybest, dbest, rbest, tid);
          }
          break;
        }
      else /* split each chunk in two */
        {
          List_t NewL;
          List_init (NewL);
          for (int i = 0; i < L->size; i++)
            {
              UTYPE nxmin = L->l[i].nxmin;
              UTYPE nxmax = L->l[i].nxmax;
              UTYPE width_x = nxmax - nxmin;
              UTYPE nymin = L->l[i].nymin;
              UTYPE nymax = L->l[i].nymax;
              UTYPE width_y = nymax - nymin;
              if (width_x >= width_y) // cut the x-side in two
              {
                UTYPE nxmid = nxmin + (nxmax - nxmin) / 2;
                search_aux (nxmin, nxmid, nymin, nymax, nxbest, nybest, tid, NewL);
                search_aux (nxmid, nxmax, nymin, nymax, nxbest, nybest, tid, NewL);
              }
              else // cut the y-side in two
              {
                UTYPE nymid = nymin + (nymax - nymin) / 2;
                search_aux (nxmin, nxmax, nymin, nymid, nxbest, nybest, tid, NewL);
                search_aux (nxmin, nxmax, nymid, nymax, nxbest, nybest, tid, NewL);
              }
            }
          List_swap (L, NewL);
#ifdef STAT
          printf ("L: "); List_print (L);
#endif
          List_clear (NewL);
        }
    }
  List_clear (L);
}

static void
setround (int rnd)
{
  fesetround (rnd1[rnd]);
}

static void
doit (unsigned int seed)
{
  UTYPE nxbest = 0, nybest = 0;
  double dbest = 0;
  int rbest = -1;

  /* set the random seed of the current thread */
  srand (seed);

  /* set the rounding mode of the current thread */
  setround (rnd);

  /* for thread 0, get the "worst" value so far as initial point */
  if (omp_get_thread_num () == 0)
    {
      dbest = Dbest;
      nxbest = get_utype (Xbest);
      nybest = get_utype (Ybest);
    }

  search (get_utype (Xmin), get_utype (Xmax) + 1,
          get_utype (Ymin), get_utype (Ymax) + 1, &nxbest, &nybest, &dbest, &rbest);
#pragma omp critical
  if (dbest > Dbest)
    {
      Dbest = dbest;
#ifdef RANK
      Rbest = rbest;
#endif
      mode_best = mode ();
      Xbest = get_type (nxbest);
      Ybest = get_type (nybest);
    }
  mpfr_free_cache (); /* free cache of current thread */
}

#ifdef WORST
#include "complex-worst.h"
#endif

static void
print_error (double d)
{
  mpfr_t x;
  mpfr_init2 (x, 53);
  mpfr_set_d (x, d, MPFR_RNDU);
  if (d < 0.999)
    mpfr_printf ("%.3RUf", x);
  else if (d < 9.99)
    mpfr_printf ("%.2RUf", x);
  else if (d < 99.9)
    mpfr_printf ("%.1RUf", x);
  else
    mpfr_printf ("%.2RUe", x);
  mpfr_clear (x);
}

static void
init_Threshold (void)
{
  UTYPE boundx = get_utype (Xmax) - get_utype (Xmin);
  UTYPE boundy = get_utype (Ymax) - get_utype (Ymin);
  double w = (double) boundx * (double) boundy;
  double s = 0;              /* number of evaluations so far */
  while (s < w)
    {
      w = w / 2.0; // cut in 2
      s += 2 * (double) threshold; // use threshold evaluations in each half
    }
  Threshold = w;
}

int
main (int argc, char *argv[])
{
  unsigned int seed = 0;
  int nthreads = 0;
  double worst_input_x = 0, worst_input_y = 0;

#ifdef GLIBC
      printf("GNU libc version: %s\n", gnu_get_libc_version ());
      printf("GNU libc release: %s\n", gnu_get_libc_release ());
#endif
#ifdef _MSC_VER
  printf ("Using Microsoft math library %d\n", _MSC_VER);
#endif

  Xmin = 0;
  Xmax = -TYPE_MAX;
  Ymin = 0;
  Ymax = -TYPE_MAX;

  while (argc >= 2 && argv[1][0] == '-')
    {
      if (strcmp (argv[1], "-v") == 0)
        {
          verbose ++;
          argc --;
          argv ++;
        }
      else if (argc >= 3 && strcmp (argv[1], "-threshold") == 0)
        {
          threshold = strtoul (argv[2], NULL, 10);
          argv += 2;
          argc -= 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-seed") == 0)
        {
#ifdef ARM_USE_HARDWARE_RNG
  printf("Warning: Unable to set the seed when hardware RNG is utilized.\n");
#endif
          seed = strtoul (argv[2], NULL, 10);
          argv += 2;
          argc -= 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-mode") == 0)
        {
          use_mode = strtoul (argv[2], NULL, 10);
          assert (0 <= use_mode && use_mode <= 2);
          argv += 2;
          argc -= 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-extra") == 0)
        {
          extra_file = fopen (argv[2], "r");
          argv += 2;
          argc -= 2;
        }
      else if (strcmp (argv[1], "-rndn") == 0)
        {
          rnd = 0;
          argc --;
          argv ++;
        }
      else if (strcmp (argv[1], "-rndz") == 0)
        {
          rnd = 1;
          argc --;
          argv ++;
        }
      else if (strcmp (argv[1], "-rndu") == 0)
        {
          rnd = 2;
          argc --;
          argv ++;
        }
      else if (strcmp (argv[1], "-rndd") == 0)
        {
          rnd = 3;
          argc --;
          argv ++;
        }
      else if (argc >= 3 && strcmp (argv[1], "-nthreads") == 0)
        {
          nthreads = atoi (argv[2]);
          argc -= 2;
          argv += 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-worst") == 0)
        {
          sscanf (argv[2], "%la,%la", &worst_input_x, &worst_input_y);
          argc -= 2;
          argv += 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-xmin") == 0)
        {
          long double t;
          sscanf (argv[2], "%La", &t);
          Xmin = t;
          argc -= 2;
          argv += 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-xmax") == 0)
        {
          long double t;
          sscanf (argv[2], "%La", &t);
          Xmax = t;
          argc -= 2;
          argv += 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-ymin") == 0)
        {
          long double t;
          sscanf (argv[2], "%La", &t);
          Ymin = t;
          argc -= 2;
          argv += 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-ymax") == 0)
        {
          long double t;
          sscanf (argv[2], "%La", &t);
          Ymax = t;
          argc -= 2;
          argv += 2;
        }
      else
        {
          fprintf (stderr, "Unknown option %s\n", argv[1]);
          exit (1);
        }
    }
  assert (threshold > 0);
  /* divide threshold by LIST_ALLOC so that the total number of evaluations
     does not vary with LIST_ALLOC */
  threshold = 1 + (threshold - 1) / LIST_ALLOC;
  init_Threshold ();
#ifdef VECTORIZE
  cudaInit();
  int bunchSize = (threshold >int(Threshold+1)) ? threshold : int(Threshold+1);
  CMData ldata(bunchSize ,omp_get_max_threads());
  gdata = &ldata;
  what[0] = What::one;
  icurr[0]=0;
#endif

  if (seed == 0)
    seed = getpid ();
  if (verbose)
    printf ("Using seed %lu\n", (unsigned long) seed);
  if (verbose)
  {
    printf ("Using xmin=");
    print_type_hex (Xmin);
    printf (" xmax=");
    print_type_hex (Xmax);
    printf (" ymin=");
    print_type_hex (Ymin);
    printf (" ymax=");
    print_type_hex (Ymax);
    printf ("\n");
  }

#ifdef WORST
  TYPE x, y;
  double d, Dbest0;
  setround (rnd);
  /* first check the given -worst value, if any */
  if (worst_input_x != 0 || worst_input_y != 0)
  {
    x = worst_input_x;
    y = worst_input_y;
    d = distance (x, y, 0);
      if (d > Dbest)
        {
          Dbest = d;
          Xbest = x;
          Ybest = y;
        }
  }
  /* first check the 'worst' values for the given library, with the given
     rounding mode */
  for (int i = NUMBER; i < SIZE; i+=NLIBS)
    {
      x = worst[i][0];
      y = worst[i][1];
      d = distance (x, y, 0);
      if (d > Dbest)
        {
          Dbest = d;
          Xbest = x;
          Ybest = y;
        }
    }
  Dbest0 = Dbest;

  /* Then check the 'worst' values for the other libraries.
     We also check rotations by pi/2, pi, 3pi/2, and symmetries
     around the axis x=y. */
  for (int i = 0; i < SIZE; i++)
    {
      if ((i % NLIBS) == NUMBER)
        continue;
      x = worst[i][0];
      y = worst[i][1];
      d = distance (x, y, 0);
      if (d > Dbest)
        {
          Dbest = d;
          Xbest = x;
          Ybest = y;
        }
      d = distance (-y, x, 0); // rotation by pi/2
      if (d > Dbest)
        {
          Dbest = d;
          Xbest = -y;
          Ybest = x;
        }
      d = distance (-x, -y, 0); // rotation by pi
      if (d > Dbest)
        {
          Dbest = d;
          Xbest = -x;
          Ybest = -y;
        }
      d = distance (y, -x, 0); // rotation by 3pi/2
      if (d > Dbest)
        {
          Dbest = d;
          Xbest = y;
          Ybest = -x;
        }
      d = distance (y, x, 0); // symmetry around x=y
      if (d > Dbest)
        {
          Dbest = d;
          Xbest = y;
          Ybest = x;
        }
      d = distance (x, -y, 0);
      if (d > Dbest)
        {
          Dbest = d;
          Xbest = x;
          Ybest = -y;
        }
      d = distance (-y, -x, 0);
      if (d > Dbest)
        {
          Dbest = d;
          Xbest = -y;
          Ybest = -x;
        }
      d = distance (-x, y, 0);
      if (d > Dbest)
        {
          Dbest = d;
          Xbest = -x;
          Ybest = y;
        }
    }

  /* then check the 'extra' values if any */
  for (int i = 0; i < SIZE_EXTRA; i++)
    {
      x = extra[i][0];
      y = extra[i][1];
      if (x == 0 && y == 0)
        break;
      d = distance (x, y, 0);
      if (d > Dbest)
        {
          Dbest = d;
          Xbest = x;
          Ybest = y;
        }
    }

  /* then read extra file if given */
  int ne=0;
  if (extra_file != NULL)
  {
    char *buf = NULL;
    size_t buflength = 0;
    ssize_t nn;
    while ((nn = getline(&buf, &buflength, extra_file)) >= 0) {
      if (nn > 0 && buf[0] == '#')
        continue;
#ifdef USE_FLOAT
      if (sscanf (buf, "%a,%a", &x, &y) == 2)
#elif defined(USE_DOUBLE)
      if (sscanf (buf, "%la,%la", &x, &y) == 2)
#else
      fprintf (stderr, "extra_file not implemented for this type\n");
      exit (1);
#endif
      ne++;
      d = distance (x, y, 0);
      if (d > Dbest)
        {
          Dbest = d;
          Xbest = x;
          Ybest = y;
        }
      d = distance (-y, x, 0); // rotation by pi/2
      if (d > Dbest)
        {
          Dbest = d;
          Xbest = -y;
          Ybest = x;
        }
      d = distance (-x, -y, 0); // rotation by pi
      if (d > Dbest)
        {
          Dbest = d;
          Xbest = -x;
          Ybest = -y;
        }
      d = distance (y, -x, 0); // rotation by 3pi/2
      if (d > Dbest)
        {
          Dbest = d;
          Xbest = y;
          Ybest = -x;
        }
      d = distance (y, x, 0); // symmetry around x=y
      if (d > Dbest)
        {
          Dbest = d;
          Xbest = y;
          Ybest = x;
        }
      d = distance (x, -y, 0);
      if (d > Dbest)
        {
          Dbest = d;
          Xbest = x;
          Ybest = -y;
           }
      d = distance (-y, -x, 0);
      if (d > Dbest)
        {
          Dbest = d;
          Xbest = -y;
          Ybest = -x;
        }
      d = distance (-x, y, 0);
      if (d > Dbest)
        {
          Dbest = d;
          Xbest = -x;
          Ybest = y;
        }
    }
  }
  printf("initial worse at %a,%a is %f %f %d\n",Xbest,Ybest,Dbest0,Dbest,ne);
#endif

  /* Apparently Visual Studio does not properly set the number of threads. */
#pragma omp parallel
  if (nthreads <= 0)
    nthreads = omp_get_num_threads ();
  if (verbose)
    printf ("Using %d thread(s)\n", nthreads);
  omp_set_num_threads (nthreads);

  assert (nthreads <= MAX_THREADS);

  int n;
#pragma omp parallel for
  for (n = 0; n < nthreads; n++)
    {
      Seed[n] = seed + n;
      doit (Seed[n]);
    }
#ifdef WORST
  if (Dbest > Dbest0)
    printf ("NEW ");
#endif
  printf ("c%s %d %d ", NAME, mode_best, Rbest);
  print_type_hex (Xbest);
  printf (",");
  print_type_hex (Ybest);
  printf (" [");
  print_error (Dbest);
  setround (2); // upward
  printf ("] %.6g %.16g\n", Dbest, Dbest);
  /* reset to the current rounding mode */
  setround (rnd);
  if (verbose)
    {
      mpc_t xx, yy;
      volatile TYPE y_re, y_im;
      CTYPE z_in = CONSTRUCT (Xbest, Ybest);
#ifdef VECTORIZE
      cmdata().xpH[0][0] = z_in;
      auto zp = wrap_foo(cmdata(),1,0);
      CTYPE z = *zp;
#else
      CTYPE z = FOO2 (z_in)
#endif
#ifdef __CUDACC__
      printf ("cuda gives (");
#elif defined __HIPCC__
      printf ("rocm gives (");
#else
      printf ("libm gives (");
#endif
#ifdef __cplusplus
      print_type_hex (z.real()); 
#else
      print_type_hex (creal (z));
#endif
      printf (",");
#ifdef __cplusplus
      print_type_hex (z.imag());
#else
      print_type_hex (cimag (z));
#endif
      printf (")\n");
      mpfr_set_emin (EMIN + 1);
      mpfr_set_emax (EMAX);
      mpc_init2 (xx, PREC);
      mpc_init2 (yy, PREC);
      mpfr_set_type (mpc_realref (xx), Xbest, MPFR_RNDN);
      mpfr_set_type (mpc_imagref (xx), Ybest, MPFR_RNDN);
      int ret = MPC_FOO (yy, xx, crnd[rnd]);
      mpfr_subnormalize (mpc_realref (yy), ret, rnd2[rnd]);
      mpfr_subnormalize (mpc_imagref (yy), ret, rnd2[rnd]);
      y_re = mpfr_get_type (mpc_realref (yy), MPFR_RNDN);
      y_im = mpfr_get_type (mpc_imagref (yy), MPFR_RNDN);
      printf ("mpc  gives (");
      print_type_hex (y_re);
      printf (",");
      print_type_hex (y_im);
      printf (")\n");
      fflush (stdout);
      mpc_clear (xx);
      mpc_clear (yy);
    }
  fflush (stdout);
  mpfr_free_cache ();
#ifdef STAT
  printf ("eval_heuristic=%lu eval_exhaustive=%lu\n",
          eval_heuristic, eval_exhaustive);
#endif

  if (extra_file != NULL)
    fclose (extra_file);

  return 0;
}
