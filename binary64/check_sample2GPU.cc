/* Search worst cases of a bivariate function, using a recursive algorithm.

   This program is open-source software distributed under the terms 
   of the GNU General Public License <http://www.fsf.org/copyleft/gpl.html>.

   Compile with:
   on NVidia
   nvcc -DFOO=$f -DUSE_xxx check_sample2GPU.cu --cudart shared -gencode arch=compute_$CCAP,code=sm_$CCAP -O3 -std=c++17 --compiler-options="-O3 -L$MPFR_DIR -lmpfr -lgmp -lm -fopenmp"
   on CPU
   c++ -DFOO=$f -DUSE_xxx -O3 check_sample2GPU.cc -lmpfr -lgmp -lm -fopenmp
   on radeon
   hipcc -O3 check_sample2GPU.hip.cpp -I$MPFR_DIR -L$MPFR_DIR -lmpfr -lgmp -lm -fopenmp -march=native -DFOO=$f -DUSE_xxx


   where xxx is FLOAT, DOUBLE, LDOUBLE, or FLOAT128.

   For NEWLIB: add -DNEWLIB (to avoid compilation error with __errno).

   You can add -DWORST to use some precomputed values to guide the search.

   You can add -DGLIBC to print the GNU libc release (with -v).

   References and credit:
   * https://www.vinc17.net/research/testlibm/: worst-cases computed by
     Vincent Lefèvre.
   * the idea to sample several intervals instead of only one is due to
     Eric Schneider
*/

#ifndef FOO
#error "please provide a value for FOO"
#endif


#ifndef _GNU_SOURCE
#define _GNU_SOURCE /* to define ...f128 functions */
#endif

#define RANK /* print the maximal list-rank of best values */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#ifndef NO_FLOAT128
#define MPFR_WANT_FLOAT128
#endif
#include <mpfr.h>
#include <math.h>
#include <assert.h>
#include <sys/types.h>
#include <unistd.h>
#ifndef NO_OPENMP
#include <omp.h>
#endif
#include <float.h> /* for DBL_MAX */

/* define GLIBC to print the GNU libc version */
#ifdef GLIBC
#include <gnu/libc-version.h>
#endif


#include<fstream>
#include<iomanip>
#include<algorithm>
#include<iostream>


#ifdef __CUDACC__
#include<cuda.h>
#include<cuda_runtime.h>
#include <cuda_runtime_api.h>
#include<iostream>

inline
bool cudaCheck_(const char* file, int line, const char* cmd, cudaError_t result)
{
    //std::cerr << file << ", line " << line << ": " << cmd << std::endl;
    if (result == cudaSuccess)
        return true;

    const char* error = cudaGetErrorName(result);
    const char* message = cudaGetErrorString(result);
    std::cerr << file << ", line " << line << ": " << error << ": " << message << std::endl;
    abort();
    return false;
}
#define cudaCheck(ARG) (cudaCheck_(__FILE__, __LINE__, #ARG, (ARG)))
#endif



mpfr_rnd_t rnd = MPFR_RNDN;
int verbose = 1;

/* mode (0,1,2), if -1 set according to omp_get_thread_num() */
int use_mode = -1;

#ifdef NEWLIB
/* without this, newlib says: undefined reference to `__errno' */
int errno;
int* __errno () { return &errno; }
#endif

#define CAT1(X,Y) X ## Y
#define CAT2(X,Y) CAT1(X,Y)
#ifndef MPFR_FOO
#define MPFR_FOO CAT2(mpfr_,FOO)
#endif
#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)
#define NAME TOSTRING(FOO)

#ifdef USE_FLOAT
#if defined(USE_DOUBLE) || defined(USE_LDOUBLE) || defined(USE_FLOAT128)
#error "only one of USE_FLOAT, USE_DOUBLE, USE_LDOUBLE or USE_FLOAT128 can be defined"
#endif
#define TYPE float
#define UTYPE uint32_t
#define FOO2 CAT2(FOO,f)
#define EMAX 128
#define EMIN -149
#define PREC 24
#define mpfr_set_type mpfr_set_flt
#define mpfr_get_type mpfr_get_flt
#define TYPE_MAX FLT_MAX
#endif

#ifdef USE_DOUBLE
#if defined(USE_FLOAT) || defined(USE_LDOUBLE) || defined(USE_FLOAT128)
#error "only one of USE_FLOAT, USE_DOUBLE, USE_LDOUBLE or USE_FLOAT128 can be defined"
#endif
#define TYPE double
#define UTYPE uint64_t
#define FOO2 FOO
#define EMAX 1024
#define EMIN -1074
#define PREC 53
#define mpfr_set_type mpfr_set_d
#define mpfr_get_type mpfr_get_d
#define TYPE_MAX DBL_MAX
#endif

#ifdef USE_LDOUBLE
#ifdef __CUDACC__
#error "long double not supported in CUDA"
#endif
#if defined(USE_FLOAT) || defined(USE_DOUBLE) || defined(USE_FLOAT128)
#error "only one of USE_FLOAT, USE_DOUBLE, USE_LDOUBLE or USE_FLOAT128 can be defined"
#endif
#define FOO2 CAT2(FOO,l)
#define TYPE long double
#define UTYPE __uint128_t
#define EMAX 16384
#define EMIN -16445
#define PREC 64
#define mpfr_set_type mpfr_set_ld
#define mpfr_get_type mpfr_get_ld
#define TYPE_MAX LDBL_MAX
#endif

#ifdef USE_FLOAT128
#ifdef __CUDACC__
#error "binary128 not supported in CUDA"
#endif
#if defined(USE_FLOAT) || defined(USE_DOUBLE)
#error "only one of USE_FLOAT, USE_DOUBLE or USE_FLOAT128 can be defined"
#endif
#ifdef __INTEL_COMPILER
#define TYPE _Quad
#define FOO3 CAT2(FOO,q)
#define FOO2 CAT2(__,FOO3)
extern _Quad FOO2 (_Quad, _Quad);
#define Q(x) (x ## q)
#else
#define TYPE _Float128
#define FOO2 CAT2(FOO,f128)
#define Q(x) (x ## f128)
#endif
#define TYPE_MAX Q(0xf.fffffffffffffffffffffffffff8p+16380)
#define UTYPE __uint128_t
#define EMAX 16384
#define EMIN -16494
#define PREC 113
#define mpfr_set_type mpfr_set_float128
#define mpfr_get_type mpfr_get_float128
#endif

static void
print_type_hex (TYPE x)
{
#ifdef USE_FLOAT
  printf ("%a", x);
#endif
#ifdef USE_DOUBLE
  printf ("%a", x);
#endif
#ifdef USE_LDOUBLE
  mpfr_t y;
  mpfr_init2 (y, PREC);
  mpfr_set_type (y, x, MPFR_RNDN);
  mpfr_printf ("%Ral", y);
  mpfr_clear (y);
#endif
#ifdef USE_FLOAT128
  mpfr_t y;
  mpfr_init2 (y, PREC);
  mpfr_set_type (y, x, MPFR_RNDN);
  mpfr_printf ("%Ra", y);
  mpfr_clear (y);
#endif
}

typedef union { UTYPE n; TYPE x; } union_t;

TYPE
get_type (UTYPE n)
{
  union_t u;
  u.n = n;
  return u.x;
}

UTYPE
get_utype (TYPE x)
{
  union_t u;
  u.x = x;
  return u.n;
}

/* return the distance (in ulps) between FOO(x,y) and the exact value */
#ifdef USE_LDOUBLE
static int
is_valid (TYPE x)
{
  UTYPE n = get_utype (x);
  int e = (n >> 64) & 0x7fff; /* exponent */
  uint64_t s = (uint64_t) n;  /* significand */
  if (e == 0) return (s >> 63) == 0;
  else return (n >> 63) & 1;
}
#endif


const int maxNumOfThreads = 256;
TYPE * ypD[maxNumOfThreads];
TYPE * ypH[maxNumOfThreads];
TYPE * xpD[maxNumOfThreads];
TYPE * xpH[maxNumOfThreads];
TYPE * zpD[maxNumOfThreads];
TYPE * zpH[maxNumOfThreads];


#ifdef __CUDACC__

__device__ __host__ inline TYPE div(TYPE x, TYPE y) {
  return x/y;
}

cudaStream_t streams[maxNumOfThreads];
__global__ void kernel_foo(TYPE const * px, TYPE const * py, TYPE * pz, int bunchSize) {
   int first = blockIdx.x * blockDim.x + threadIdx.x;
   for (int i=first; i<bunchSize; i+=gridDim.x*blockDim.x) {
     pz[i] = FOO2(px[i],py[i]);
   }
}
__global__ void kernel_foo(TYPE const x, TYPE const * py, TYPE * pz, int bunchSize) {
   int first = blockIdx.x * blockDim.x + threadIdx.x;
   for (int i=first; i<bunchSize; i+=gridDim.x*blockDim.x) {
     pz[i] = FOO2(x,py[i]);
   }

}
#else // CPU version
void  kernel_foo(TYPE const * px, TYPE const * py, TYPE * pz, int bunchSize) {
   int first = 0;
   for (int i=first; i<bunchSize; i++) {
     pz[i] = FOO2(px[i],py[i]);
   }
}
void  kernel_foo(TYPE const  x, TYPE const * py, TYPE * pz, int bunchSize) {
   int first = 0;
   for (int i=first; i<bunchSize; i++) {
     pz[i] = FOO2(x,py[i]);
   }
}
#endif

TYPE * wrap_foo(TYPE const * xH, TYPE const * yH, int bunchSize) {
  int nt = omp_get_thread_num();
#ifdef __CUDACC__
  cudaCheck(cudaMemcpyAsync(xpD[nt], xH, bunchSize*sizeof(TYPE), cudaMemcpyHostToDevice, streams[nt]));
  cudaCheck(cudaMemcpyAsync(ypD[nt], yH, bunchSize*sizeof(TYPE), cudaMemcpyHostToDevice, streams[nt]));
  kernel_foo<<<(bunchSize+128)/128,128,0,streams[nt]>>>(xpD[nt], ypD[nt],zpD[nt],bunchSize);
  cudaCheck(cudaMemcpyAsync(zpH[nt], zpD[nt], bunchSize*sizeof(TYPE), cudaMemcpyDeviceToHost, streams[nt]));
  cudaStreamSynchronize(streams[nt]);
#else
  kernel_foo(xH, yH, zpH[nt], bunchSize);
#endif
//  std::cout << nt << ' ' << n << ' ' << zpH[nt][0] << std::endl;
  return zpH[nt];
}

TYPE * wrap_foo(TYPE const x, TYPE const * yH, int bunchSize) {
  int nt = omp_get_thread_num();
#ifdef __CUDACC__
  cudaCheck(cudaMemcpyAsync(ypD[nt], yH, bunchSize*sizeof(TYPE), cudaMemcpyHostToDevice, streams[nt]));
  kernel_foo<<<(bunchSize+128)/128,128,0, streams[nt]>>>(x, ypD[nt],zpD[nt],bunchSize);
  cudaCheck(cudaMemcpyAsync(zpH[nt], zpD[nt], bunchSize*sizeof(TYPE), cudaMemcpyDeviceToHost, streams[nt]));
  cudaStreamSynchronize(streams[nt]);
#else
  kernel_foo(x, yH, zpH[nt], bunchSize);
#endif
//  std::cout << nt << ' ' << n << ' ' << zpH[nt][0] << std::endl;
  return zpH[nt];
}


static void
distance (TYPE const * x, TYPE const * y, double * ulp, int bunchSize, bool oneX)
{
  mpfr_t xx, yy, zz;
  double ret = 0;
  int underflow = 0;
  mpfr_exp_t expz;


  mpfr_set_emax (EMAX); 
  TYPE const * z = oneX ? wrap_foo(*x, y, bunchSize) : wrap_foo(x, y, bunchSize);
 
  for (int i=0; i<bunchSize; ++i) {
    int ix = oneX ? 0: i;
    int iy = i;
    ret = 0;

    if (isnan (x[ix]) || isnan (y[iy]))
      goto end2;

#ifdef USE_LDOUBLE
    if (!is_valid (x) || !is_valid (y[i]))
      goto end2;
#endif


    mpfr_init2 (xx, PREC);
    mpfr_init2 (yy, PREC+1);
    mpfr_init2 (zz, 2*PREC);
    mpfr_set_type (xx, x[ix], MPFR_RNDN);
    mpfr_set_type (yy, y[iy], MPFR_RNDN);
    MPFR_FOO (zz, xx, yy, rnd);
    if (isnan (z[i]) && mpfr_nan_p (zz)) {
       // printf("nan at %a %a",x[i], y[i]);
      goto end;
    }
    if (isinf (z[i]) && mpfr_inf_p (zz) == 0)
    {
      // printf("inf at %a %a",x[i], y[i]);
      /* libm returns Inf, MPFR not, we assume the libm rounded the midpoint
         between the largest non infinite number and the next power of 2 */
      mpfr_set_ui_2exp (yy, 1, EMAX, MPFR_RNDN);
      mpfr_nextbelow (yy);
      if (z[i] < 0)
        mpfr_neg (yy, yy, MPFR_RNDN);
    }
    else if (!isinf (z[i]) && mpfr_inf_p (zz))
    {
      /* MPFR yields Inf, but libm no: we recmmpute the MPFR result with
       unbounded exponent */
      mpfr_exp_t emax = mpfr_get_emax ();
      mpfr_set_emax (mpfr_get_emax_max ());
      MPFR_FOO (zz, xx, yy, rnd);
      mpfr_set_emax (emax);
      mpfr_set_type (yy, z[i], MPFR_RNDN);
      // printf("not inf at %a %a\n",x[i], y[i]);
   } else
    mpfr_set_type (yy, z[i], MPFR_RNDN);

    expz = mpfr_get_exp (zz);
    underflow = expz <= EMIN;
    // if (underflow) std::cout << "underflow at " << x[i] << ' ' << y[i] << ' ' << expz << std::endl;
    mpfr_sub (zz, zz, yy, MPFR_RNDN);
    mpfr_abs (zz, zz, MPFR_RNDN);

    /* we divide by the ulp of the correctly rounded value (zz)
    which is 2^(ez-PREC) if zz is normalized, and 2^EMIN otherwise */
    if (PREC - expz <= -EMIN)
        mpfr_mul_2si (zz, zz, PREC - expz, MPFR_RNDN);
    else
        mpfr_mul_2si (zz, zz, -EMIN, MPFR_RNDN);
    ret = (underflow) ? 0.0 : mpfr_get_d (zz, MPFR_RNDU);
    end:
    mpfr_clear (xx);
    mpfr_clear (yy);
    mpfr_clear (zz);
    end2:
    ulp[i] = ret;
  }
}


uint64_t threshold = 1000;
double Threshold;

static int
ndigits (UTYPE x)
{
  int n = 1; /* 0-9 have one digit */
  while (x >= 10)
    {
      x /= 10;
      n ++;
    }
  return n;
}

static void
print_utype (UTYPE x)
{
#ifndef USE_FLOAT128
  printf ("%lu", (unsigned long) x);
#else
  __uint128_t h, m, l;
  int first = 1;
  m = x / 10000000000UL;
  l = x % 10000000000UL;
  h = m / 10000000000UL;
  m = m % 10000000000UL;
  if (h != 0)
    {
      printf ("%lu", h);
      first = 0;
    }
  if (m != 0 || !first)
    {
      if (!first)
        {
          int n = ndigits (m);
          while (n++ < 10)
            printf ("0");
        }
      printf ("%lu", m);
    }
  if (!first)
    {
      int n = ndigits (l);
      while (n++ < 10)
        printf ("0");
    }
  printf ("%lu", l);
#endif
}

#if RAND_MAX == 2147483647
#ifdef USE_FLOAT
static uint32_t
my_random (uint32_t n, unsigned int *seed)
{
  uint32_t ret = rand_r (seed);
  if (n > RAND_MAX)
    ret = (ret << 31) | rand_r (seed);
  return ret % n;
}
#else /* USE_FLOAT */
#ifdef USE_DOUBLE
static uint64_t
my_random (uint64_t n, unsigned int *seed)
{
  uint64_t ret = rand_r (seed);
  if (n > RAND_MAX)
    {
      ret = (ret << 31) | rand_r (seed);
      /* now ret <= 2^62-1 */
      if (n > 0x3fffffffffffffffLU)
        ret = (ret << 31) | rand_r (seed);
    }
  return ret % n;
}
#else /* USE_LDOUBLE or USE_FLOAT128 */
static __uint128_t
my_random (__uint128_t n, unsigned int *seed)
{
  __uint128_t ret = rand_r (seed);
  if (n > RAND_MAX)
    {
      ret = (ret << 31) | rand_r (seed);
      /* now ret <= 2^62-1 */
      if (n >> 62)
        {
          ret = (ret << 31) | rand_r (seed);
          /* now ret <= 2^93-1 */
          if (n >> 93)
            {
              ret = (ret << 31) | rand_r (seed);
              /* now ret <= 2^93-1 */
              if (n >> 124)
                ret = (ret << 31) | rand_r (seed);
            }
        }
    }
  return ret % n;
}
#endif /* USE_DOUBLE */
#endif /* USE_FLOAT */
#else
#error "Unexpected value of RAND_MAX"
#endif

TYPE Xmax, Ymax;
double Dbest = -0.01;
int Rbest = -1;
int mode_best = 0;

static void
print_type (TYPE x)
{
#ifdef USE_FLOAT
  printf ("%.8e", x);
#endif
#ifdef USE_DOUBLE
  printf ("%.16e", x);
#endif
#ifdef USE_LDOUBLE
  mpfr_t y;
  mpfr_init2 (y, PREC);
  mpfr_set_type (y, x, MPFR_RNDN);
  mpfr_printf ("%.20Re", y);
  mpfr_clear (y);
#endif
#ifdef USE_FLOAT128
  mpfr_t y;
  mpfr_init2 (y, PREC);
  mpfr_set_type (y, x, MPFR_RNDN);
  mpfr_printf ("%.35Re", y);
  mpfr_clear (y);
#endif
}

/* return the maximal error on [nxmin,nxmax] x [nymin,nymax],
   and update nxbest,nybest if it improves distance(x,y) */
static double
max_heuristic1 (UTYPE nxmin, UTYPE nxmax, UTYPE nymin, UTYPE nymax,
                UTYPE *nxbest, UTYPE *nybest, unsigned int *seed)
{

  int nt = omp_get_thread_num();
  TYPE * x = xpH[nt];
  TYPE * y = ypH[nt];
  UTYPE nx[threshold];
  UTYPE ny[threshold];
  double d[threshold+1];


  TYPE xx = get_type (*nxbest);
  x[threshold] = xx;
  TYPE yy = get_type (*nybest);
  y[threshold] = yy;

  for (int i = 0; i < threshold; i++)
    {
      nx[i] = nxmin + my_random (nxmax - nxmin, seed);
      ny[i] = nymin + my_random (nymax - nymin, seed);
#ifdef USE_LDOUBLE
      /* for long double, bit 63 must be set */
      nx[i] |= (UTYPE) 1 << 63;
      ny[i] |= (UTYPE) 1 << 63;
#endif
      x[i] = get_type (nx[i]);
      y[i] = get_type (ny[i]);
    }
  distance (x, y,d,threshold+1,false);
  double dbest = d[threshold];
  double dmax = 0;
  for (int i = 0; i < threshold; i++)
    {
      if (d[i] > dmax)
        {
          dmax = d[i];
          if (d[i] > dbest)
            {
              dbest = d[i];
              *nxbest = nx[i];
              *nybest = ny[i];
            }
        }
    }
  return dmax;
}

/* return the average error on [nxmin,nxmax] x [nymin,nymax],
   given (nxbest,nybest) */
static double
max_heuristic2 (UTYPE nxmin, UTYPE nxmax, UTYPE nymin, UTYPE nymax,
               UTYPE *nxbest, UTYPE *nybest, unsigned int *seed)
{
  int nt = omp_get_thread_num();
  TYPE * x = xpH[nt];
  TYPE * y = ypH[nt];
  UTYPE nx[threshold];
  UTYPE ny[threshold];
  double d[threshold+1];

  TYPE xx = get_type (*nxbest);
  x[threshold] = xx;
  TYPE yy = get_type (*nybest);
  y[threshold] = yy;

  for (int i = 0; i < threshold; i++)
    {
      nx[i] = nxmin + my_random (nxmax - nxmin, seed);
      ny[i] = nymin + my_random (nymax - nymin, seed);
#ifdef USE_LDOUBLE
      /* for long double, bit 63 must be set */
      nx[i] |= (UTYPE) 1 << 63;
      ny[i] |= (UTYPE) 1 << 63;
#endif
      x[i] = get_type (nx[i]);
      y[i] = get_type (ny[i]);
    }
  distance (x, y,d,threshold+1,false);
  double dbest = d[threshold];

  double s = 0, n = 0;
  for (int i = 0; i < threshold; i++)
    {
      if (d[i] != 0)
        {
          s += d[i];
          n ++;
        }
      if (d[i] > dbest)
        {
          dbest = d[i];
          *nxbest = nx[i];
          *nybest = ny[i];
        }
    }
  if (n != 0)
    s = s / n;
  return s;
}

/* return the estimated maximal error on [nxmin,nxmax] x [nymin,nymax],
   taking into account mean and standard deviation,
   and update (nxbest,nybest) */
static double
max_heuristic3 (UTYPE nxmin, UTYPE nxmax, UTYPE nymin, UTYPE nymax,
               UTYPE *nxbest, UTYPE *nybest, unsigned int *seed)
{
  double s = 0, v = 0, n = 0;

 int nt = omp_get_thread_num();
  TYPE * x = xpH[nt];
  TYPE * y = ypH[nt];
  UTYPE nx[threshold];
  UTYPE ny[threshold];
  double d[threshold+1];

  TYPE xx = get_type (*nxbest);
  x[threshold] = xx;
  TYPE yy = get_type (*nybest);
  y[threshold] = yy;

  for (int i = 0; i < threshold; i++)
    {
      nx[i] = nxmin + my_random (nxmax - nxmin, seed);
      ny[i] = nymin + my_random (nymax - nymin, seed);
#ifdef USE_LDOUBLE
      /* for long double, bit 63 must be set */
      nx[i] |= (UTYPE) 1 << 63;
      ny[i] |= (UTYPE) 1 << 63;
#endif
      x[i] = get_type (nx[i]);
      y[i] = get_type (ny[i]);
    }
  distance (x, y,d,threshold+1,false);
  double dbest = d[threshold];

  for (int i = 0; i < threshold; i++)
    {
      if (d[i] != 0)
        {
          s += d[i];
          v += d[i] * d[i];
          n ++;
        }
      if (d[i] > dbest)
        {
          dbest = d[i];
          *nxbest = nx[i];
          *nybest = ny[i];
        }
    }
  /* compute mean and standard deviation */
  if (n != 0)
    {
      s = s / n;
      v = v / n - s * s;
      if (v < 0)
        v = 0;
      double sigma = sqrt (v);
      /* we got n non-zero values out of threshold, thus we should get
         n/threshold*(nxmax-nxmin)*(nymin-nymax) */
      n = n * (double) (nxmax - nxmin) * (double) (nymax - nymin)
        / (double) threshold;
      double logn = log (n);
      double t = sqrt (2.0 * logn);
      /* Reference: A note on the first moment of extreme order statistics
         from the normal distribution, Max Petzold,
         https://gupea.ub.gu.se/handle/2077/3092 */
      s = s + sigma * (t - (log (logn) + 1.3766) / (2.0 * t));
    }
  return s;
}

#ifdef NO_OPENMP
static int omp_get_thread_num (void) { return 0; }
static int omp_get_num_threads (void) { return 1; }
#endif

static int
mode (void)
{
  if (use_mode != -1)
    return use_mode;
  int i = omp_get_thread_num ();
  /* Modes 1,2 seem to be less efficient, thus we use only 1 thread on each. */
  if (i == 1 || i == 2)
    return i;
  return 0;
}

#ifdef STAT
unsigned long eval_heuristic = 0;
unsigned long eval_exhaustive = 0;
#endif

static double
max_heuristic (UTYPE nxmin, UTYPE nxmax, UTYPE nymin, UTYPE nymax,
               UTYPE *nxbest, UTYPE *nybest, unsigned int *seed)
{
  int k = mode ();

#ifdef STAT
#pragma omp atomic update
  eval_heuristic += threshold;
#endif

  if (k == 0)
    return max_heuristic1 (nxmin, nxmax, nymin, nymax, nxbest, nybest, seed);
  else if (k == 1)
    return max_heuristic2 (nxmin, nxmax, nymin, nymax, nxbest, nybest, seed);
  else
    return max_heuristic3 (nxmin, nxmax, nymin, nymax, nxbest, nybest, seed);
}

typedef struct {
  UTYPE nxmin, nxmax;
  UTYPE nymin, nymax;
  double d;
#ifdef RANK
  int rank; /* worst rank */
#endif
} chunk_t;

static double
chunk_size (chunk_t c)
{
  return (double) (c.nxmax - c.nxmin) * (double) (c.nymax - c.nymin);
}

static void
chunk_swap (chunk_t *a, chunk_t *b)
{
  UTYPE tmp;
  tmp = a->nxmin; a->nxmin = b->nxmin; b->nxmin = tmp;
  tmp = a->nxmax; a->nxmax = b->nxmax; b->nxmax = tmp;
  tmp = a->nymin; a->nymin = b->nymin; b->nymin = tmp;
  tmp = a->nymax; a->nymax = b->nymax; b->nymax = tmp;
  double ump;
  ump = a->d; a->d = b->d; b->d = ump;
#ifdef RANK
  int rmp;
  rmp = a->rank; a->rank = b->rank; b->rank = rmp;
#endif
}

typedef struct {
  chunk_t *l;
  int size;
} List_struct;
typedef List_struct List_t[1];

/* Idea from Eric Schneider: instead of sampling only one interval at each
   level of the binary splitting tree, we sample up to LIST_ALLOC intervals,
   and keep the most promising ones. We use the default value suggested by
   Eric Schneider (20). */
#define LIST_ALLOC 20

static void
List_init (List_t L)
{
  L->l = (chunk_t*)malloc (LIST_ALLOC * sizeof (chunk_t));
  L->size = 0;
}

static void
List_init2 (List_t L, UTYPE nxmin, UTYPE nxmax, UTYPE nymin, UTYPE nymax)
{
  L->l =(chunk_t*) malloc (LIST_ALLOC * sizeof (chunk_t));
  L->l[0].nxmin = nxmin;
  L->l[0].nxmax = nxmax;
  L->l[0].nymin = nymin;
  L->l[0].nymax = nymax;
  L->size = 1;
}

static void
List_print (List_t L)
{
  for (int i = 0; i < L->size; i++)
    printf ("%.3g ", L->l[i].d);
  printf ("\n");
}

static void
List_check (List_t L)
{
  for (int i = 1; i < L->size; i++)
    if (L->l[i-1].d < L->l[i].d)
      {
        fprintf (stderr, "Error, list not sorted:\n");
        List_print (L);
        exit (1);
      }
}

static void
List_insert (List_t L, UTYPE nxmin, UTYPE nxmax, UTYPE nymin, UTYPE nymax,
             double d)
{
  int i = L->size;
  if (i < LIST_ALLOC || d > L->l[LIST_ALLOC-1].d)
    {
      /* if list is not full, we insert at position i,
         otherwise we insert at position LIST_ALLOC-1 */
      if (i == LIST_ALLOC)
        i--;
      else
        L->size++;
      L->l[i].nxmin = nxmin;
      L->l[i].nxmax = nxmax;
      L->l[i].nymin = nymin;
      L->l[i].nymax = nymax;
      L->l[i].d = d;
      /* now insertion sort */
      while (i > 0 && d > L->l[i-1].d)
        {
          chunk_swap (L->l + (i-1), L->l + i);
#ifdef RANK
          if (L->l[i].rank < i)
            L->l[i].rank = i; /* updates maximal rank */
#endif
          i--;
        }
#ifdef RANK
      L->l[i].rank = i; /* set initial rank */
#endif
    }
  // List_check (L);
}

static void
List_swap (List_t L1, List_t L2)
{
  chunk_t *tmp;
  tmp = L1->l; L1->l = L2->l; L2->l = tmp;
  int ump;
  ump = L1->size; L1->size = L2->size; L2->size = ump;
}

static void
List_clear (List_t L)
{
  free (L->l);
}

static void
exhaustive_search (chunk_t *c, UTYPE *nxbest, UTYPE *nybest, double *dbest,
                   int *rbest)
{

  int bunchSize = c->nymax - c->nymin;
  auto nymin =  c->nymin;

  int nt = omp_get_thread_num();

  // printf("exha %d %d\n",nt,bunchSize);

  TYPE * y = ypH[nt];
  UTYPE ny[bunchSize];
  double d[bunchSize];

  for (int i = 0; i < bunchSize; i++)
     {
        ny[i] = nymin + i;
        y[i] = get_type (ny[i]);
     } // y


  for (UTYPE nx = c->nxmin; nx < c->nxmax; nx ++)
    {
      TYPE x = get_type (nx);
      distance (&x, y,d,bunchSize,true);
      for (int i = 0; i < bunchSize; i++)
        {

          if (d[i] > *dbest)
            {
              *dbest = d[i];
              *nxbest = nx;
              *nybest = ny[i];
#ifdef RANK
              *rbest = c->rank;
#endif
            }
        } // y
    }  // x
#ifdef STAT
#pragma omp atomic update
  eval_exhaustive += (c->nxmax - c->nxmin) * (c->nymax - c->nymin);
#endif
}

/* search for nxmin <= nx < nxmax and nymin <= ny < nymax,
   where the worst found so far is (nxbest,nybest,dbest) */
static void
search (UTYPE nxmin, UTYPE nxmax, UTYPE nymin, UTYPE nymax,
        UTYPE *nxbest, UTYPE *nybest, double *dbest, int *rbest,
        unsigned int *seed)
{
  List_t L;

  List_init2 (L, nxmin, nxmax, nymin, nymax);
  while (1)
    {
      assert (1 <= L->size && L->size <= LIST_ALLOC);
      double width = chunk_size (L->l[0]);
      if (width <= Threshold) /* exhaustive search */
        {
          for (int i = 0; i < L->size; i++)
            exhaustive_search (L->l + i, nxbest, nybest, dbest, rbest);
          break;
        }
      else /* split each chunk in four */
        {
          List_t NewL;
          List_init (NewL);
          for (int i = 0; i < L->size; i++)
            {
              UTYPE nxmin = L->l[i].nxmin;
              UTYPE nxmax = L->l[i].nxmax;
              UTYPE nxmid = nxmin + (nxmax - nxmin) / 2;
              UTYPE nymin = L->l[i].nymin;
              UTYPE nymax = L->l[i].nymax;
              UTYPE nymid = nymin + (nymax - nymin) / 2;
              double d1 = max_heuristic (nxmin, nxmid, nymin, nymid,
					 nxbest, nybest, seed);
              List_insert (NewL, nxmin, nxmid, nymin, nymid, d1);
              double d2 = max_heuristic (nxmin, nxmid, nymid, nymax,
					 nxbest, nybest, seed);
              List_insert (NewL, nxmin, nxmid, nymid, nymax, d2);
              double d3 = max_heuristic (nxmid, nxmax, nymin, nymid,
					 nxbest, nybest, seed);
              List_insert (NewL, nxmid, nxmax, nymin, nymid, d3);
              double d4 = max_heuristic (nxmid, nxmax, nymid, nymax,
					 nxbest, nybest, seed);
              List_insert (NewL, nxmid, nxmax, nymid, nymax, d4);
            }
          List_swap (L, NewL);
#ifdef STAT
          printf ("L: "); List_print (L);
#endif
          List_clear (NewL);
        }
    }
  List_clear (L);
}

#if defined(USE_FLOAT) || defined(USE_DOUBLE) || defined(USE_LDOUBLE)
#define NLIBS 8
#else
#define	NLIBS 2 /* only glibc and icc do provide binary128 */
#endif
#define SIZE (4*NLIBS) /* 4 functions (atan2, hypot, pow, div) */

#ifdef WORST
#ifdef GLIBC
#define NUMBER 0
#endif
#ifdef ICC
#define NUMBER 1
#endif
#ifdef AMD
#define NUMBER 2
#endif
#ifdef NEWLIB
#define NUMBER 3
#endif
#ifdef OPENLIBM
#define NUMBER 4
#endif
#ifdef MUSL
#define NUMBER 5
#endif
#ifdef APPLE
#define NUMBER 6
#endif
#ifdef  __CUDACC__
#define NUMBER 7
#endif
#ifndef NUMBER
#error "NUMBER undefined"
#endif
#endif

static void
doit (unsigned int seed)
{
  UTYPE nxbest = 0, nybest = 0;
  double dbest = 0;
  int rbest = -1;

  /* for thread 0, get the "worst" value so far as initial point */
  if (omp_get_thread_num () == 0)
    {
      dbest = Dbest;
      nxbest = get_utype (Xmax);
      nybest = get_utype (Ymax);
    }

  UTYPE bound = get_utype (-TYPE_MAX) + 1;
  search (0, bound, 0, bound, &nxbest, &nybest, &dbest, &rbest, &seed);
#pragma omp critical
  if (dbest > Dbest)
    {
      Dbest = dbest;
#ifdef RANK
      Rbest = rbest;
#endif
      mode_best = mode ();
      Xmax = get_type (nxbest);
      Ymax = get_type (nybest);
    }
  mpfr_free_cache (); /* free cache of current thread */
}

#ifdef WORST
#define SIZE_EXTRA 1
#ifdef USE_FLOAT
  TYPE worst[SIZE][2] = {
    /* atan2(x,y) */
    {-0x1.f9cf48p+49,0x1.f60598p+51},   /* GNU libc 2.32  1.51236 */
    {-0x1.ff67c2p+57,0x1.ff7f62p+60},   /* icc 19.1.3.304 0.576259 */
    {0x1.fffe24p+59,0x1.000adcp+73},    /* AMD LibM 3.5   0.583286 */
    {-0x1.f9cf48p+49,0x1.f60598p+51},   /* Newlib 4.1.0   1.51236 */
    {0x1.a10104p+123,0x1.99f182p+125},  /* OpenLibm 0.7.0 1.54065 */
    {0x1.a10104p+123,0x1.99f182p+125},  /* Musl 1.2.1     1.54065 */
    {-0x1.ce62cep-116,0x1.cbf9bp-113},  /* Darwin 20.4.0  0.721703 */
    {0x1.0e5beap+6,0x1.016188p+6},   /* CUDA 11.8 *   2.18 */
    /* hypot(x,y) */
    {0x1.3ac98p+67,-0x1.ba5ec2p+77},    /* GNU libc 2.32  d=5.00000000e-01 */
    {0x1.3ac98p+67,-0x1.ba5ec2p+77},    /* icc 19.1.3.304 d=5.00000000e-01 */
    {0x1.3ac98p+67,-0x1.ba5ec2p+77},    /* AMD LibM 3.5   0.5 */
    {-0x1.6b05c4p-127,0x1.6b3146p-126}, /* Newlib 4.1.0   1.20805 */
    {-0x1.6b05c4p-127,0x1.6b3146p-126}, /* OpenLibm 0.7.0 1.20805 */
    {0x1.26b188p-127,-0x1.a4f2fp-128},  /* Musl 1.2.1     0.926707 */
    {0x1.3ac98p+67,-0x1.ba5ec2p+77},    /* Darwin 20.4.0  0.5 */
    {0x1.007594p+1,-0x1.003512p+1},   /* CUDA 12.2    1.03 */
    /* pow(x,y) */
    {0x1.025736p+0,0x1.309f94p+13},     /* GNU libc 2.32  d=8.16736102e-01 */
    {0x1.fe759ep-1,-0x1.c1375ep+14},    /* icc 19.1.3.304 0.514356 */
    {0x1.e00c4cp-1,0x1.502b3cp+10},     /* AMD LibM 3.5   1.17574 */
#ifndef NEW
    {0x1.d55902p-1,-0x1.fe037ep+9},     /* Newlib 4.1.0   168.527 */
#else
    /* the following is with -D__OBSOLETE_MATH_DEFAULT=1 */
    {0x1.025736p+0,0x1.309f94p+13},     /* Newlib 4.1.0   0.816736 */
#endif
    {0x1.ffffeep-1,-0x1.000002p+27},    /* OpenLibm 0.7.0 2.81475e+14 */
    {0x1.025736p+0,0x1.309f94p+13},     /* Musl 1.2.1     8.16736102e-01 */
    {0x1.034016p+0,0x1.b782b4p+12},     /* Darwin 20.4.0  0.514506 */
    {0x1.6794b6p+0,-0x1.f9f1bap+7},     /* CUDA 12.2    2.60 */
    /* div(x,y) */
    {0x1.ffffffp-1,0x1.fffffffp-1},
    {0x1.ffffffp-1,0x1.fffffffp-1},
    {0x1.ffffffp-1,0x1.fffffffp-1},
    {0x1.ffffffp-1,0x1.fffffffp-1},
    {0x1.ffffffp-1,0x1.fffffffp-1},
    {0x1.ffffffp-1,0x1.fffffffp-1},
    {0x1.ffffffp-1,0x1.fffffffp-1},
    {0x1.ffffffp-1,0x1.fffffffp-1}
  };
TYPE extra[SIZE_EXTRA][2] = {
  {0x1.a10104p+123,0x1.99f182p+125}
  };
#endif

#ifdef USE_DOUBLE
  TYPE worst[SIZE][2] = {
    /* atan2(x,y) */
#ifndef GLIBC_NEW
    {-0x1.b48c630109d7ep+361,-0x1.878e5a0eb857dp+307}, /* glibc 2.32 0.5 */
#else
    {0x1.4aa3aada7c44fp-996,0x1.4f32ff56386e8p-993},   /* glibc 0.5231702973301076 */
#endif
    {0x1.e4b0731c7640dp-611,0x1.fc94bba330aedp-606},   /* icc 0.549503 */
    {0x1.ccbbbcfef3c02p-523,0x1.924bf639c1a94p+500},   /* AMD 0.7499999999996464 */
    {-0x1.358bb5eb25bdcp+813,0x1.2f86b82481a0ap+815},  /* Newlib 1.54908 */
    {-0x1.358bb5eb25bdcp+813,0x1.2f86b82481a0ap+815},  /* OpenLibm 1.54908 */
    {-0x1.358bb5eb25bdcp+813,0x1.2f86b82481a0ap+815},  /* Musl 1.54908 */
    {-0x1.69c48b993d3cp-412,0x1.d2c89fab27affp-411},   /* Apple 0.746152 */
    {0x1.9cde4ff190e45p+931,0x1.37d91467e558bp+931}, /* CUDA   1.76 */
    /* hypot(x,y) */
    {-0x0.5a934b7eac967p-1022,-0x0.b5265a7e06b82p-1022}, /* glibc 0.986776 */
    {0x0.19deaac345ffap-1022,0x0.92c8727c389b6p-1022},   /* icc 0.7500000255880763 */
    {0x1.95678c86e5d12p-189,-0x1.2bfba48a14a49p-188},    /* AMD 0.941725 */
    {0x0.938ccc3ea60ffp-1022,-0x1.6a09f20598aa9p-1004},  /* Newlib 1.20710 */
    {0x0.938ccc3ea60ffp-1022,-0x1.6a09f20598aa9p-1004},  /* OpenLibm 1.20710 */
    {0x1.00014d4b1c6b9p-1015,-0x1.000105ba9bf4p-1015},   /* Musl 1.03031 */
    {0x1.2719987d4c1fep-1023,-0x1.6a09f20598aa9p-1004},  /* Apple 1.20710 */
    {-0x1.41fcfeeb2e246p+420,-0x1.8d4d41eacdeccp+420}, /* CUDA 1.89 */
    /* pow(x,y) */
    {0x1.010e2e7ee71aep+0,0x1.44bf0047427f6p+17},    /* glibc 0.522908 */
    {0x1.fffff9c61ce4p-1,0x1.c4e304ed4c734p+31},     /* icc 1.72811 */
    {0x1.fffffca9dd1a7p-1,0x1.344c8c25b78dfp+32},    /* AMD 925.168 */
    {0x1.000002c5e2e99p+0,0x1.c9eee35374af6p+31},    /* Newlib 635.33 */
    {0x1.000002c5e2e99p+0,0x1.c9eee35374af6p+31},    /* OpenLibm 635.33 */
    {0x1.010e2e7ec0c83p+0,0x1.44bf00479249dp+17},    /* Musl 0.524292 */
    {0x1.6c6e5ab6b952cp+156,-0x1.a1eac96aac3e8p+2},   /* Apple 0.756322 */
    {0x1.6b2d4fdb85ba1p-1,-0x1.f0d1d713b0262p+10},    /* CUDA  1.84 */
    /* div(x,y) */
    {0x1.fffffffffffffp-1,0x1.fffffffffffffp-1},
    {0x1.fffffffffffffp-1,0x1.fffffffffffffp-1},
    {0x1.fffffffffffffp-1,0x1.fffffffffffffp-1},
    {0x1.fffffffffffffp-1,0x1.fffffffffffffp-1},
    {0x1.fffffffffffffp-1,0x1.fffffffffffffp-1},
    {0x1.fffffffffffffp-1,0x1.fffffffffffffp-1},
    {0x1.fffffffffffffp-1,0x1.fffffffffffffp-1},
    {0x1.fffffffffffffp-1,0x1.fffffffffffffp-1}
  };
TYPE extra[SIZE_EXTRA][2] = {
  {0,0}
  };
#endif

#ifdef USE_LDOUBLE
  TYPE worst[SIZE][2] = {
/* atan2(x,y) */
{-0x7.57df4ce5629d55bp+13236l,0x7.3a4c2996454fefcp+13236l}, /* glibc 0.639292 */
{-0x5.c0c9cc5a59632f88p+16340l,0x5.db7810fba1ce4908p+16348l},/* icc 0.500209 */
{0,0},                                                    /* AMD Libm */
{0,0},                                                    /* Newlib */
{0x3.d34c9d81dcd29354p+5568l,0xf.3afc4f6c9f5c4a2p+5568l}, /* OpenLibm 1.68651 */
{0x3.d0269f403274d5dcp-15224l,0x7.a034d028f81facep-15220l},/* Musl 0.749839 */
    {0,0},                                                /* Apple */
    /* hypot(x,y) */
{0x1.73f339f61eda21dp-16384l,0x2.e45f9f9500877e2p-16384l},/* glibc 0.980552 */
{-0x3.00bad8a56d87a0cp-16384l,-0xe.6d794db04791398p-16388l}, /* icc 0.75001 */
{0,0},                                                    /* AMD Libm */
{0x6.0d9f0aa73c9f1438p+11112l,0x1.82fdacf00c626b3ap+11116l}, /* Newlib inf */
{0x3.6526795f4176ep-16384l,0x3.ffffffffffffep-16352l},/* OpenLibm 1.84467e19 */
{0x2.00007da75fd5903cp-8960l,0x2.d42207352184bff4p-8960l},/* Musl 1.07731 */
    {0,0},                                                /* Apple */
    /* pow(x,y) */
{0x2.21dda4bcec55b158p-3616l,0x7.ef1ef5fbe3df50dp-16l},   /* glibc 0.913899 */
{0xc.b80572af668bb57p+152l,-0x6.8a6d3d7b442f3c18p+4l},    /* icc 0.500654 */
{0,0},                                                    /* AMD 0.913476 */
{0,0},                                                    /* Newlib */
/* the following OpenLibm value is with OMP_NUM_THREADS=1,
   with several threads OpenLibm's powl returns nonsense:
   https://github.com/JuliaMath/openlibm/issues/222 */
{0xc.f620c9ea4p+16380l,-0x4.0ffffcp-48l},                 /* OpenLibm 532.830 */
{0xc.f620c9ea4p+16380l,-0x4.0ffffcp-48l},                 /* Musl 532.830 */
    {0,0},                                                /* Apple */
  };
TYPE extra[SIZE_EXTRA][2] = {
  {-0x4p-16320l,0x4.0000000000000008p-16384l},
  {0x4p-16320l,0x4.0000000000000008p-16384l},
  {0x4p-16384l,0x4.0000000000000008p-16320l},
  {0x4p-16384l,-0x4.0000000000000008p-16320l},
  {0x4p-16320l,-0x4.0000000000000008p-16384l},
  {-0x4p-16384l,0x4.0000000000000008p-16320l},
  {-0x4p-16384l,-0x4.0000000000000008p-16320l},
  {-0x4p-16320l,-0x4.0000000000000008p-16384l},
  {-0x5.e7da736ff042237p-16320l,-0x5.1c6dd26cc70b3cc8p-16384l},
  {0x5.480b3781eecccadp-16384l,0x4.0687a05a152d8b18p-16320l},
  {0x6.c4e6069efd8b27b8p-16320l,-0x5.21e077ba3d981c7p-16384l},
  {-0x6.2d56393a2d535118p-16384l,-0x5.974a475e5cf1823p-16320l},
  /* the following is for musl hypot (non deterministic) */
  {0x1.868c729944440d8p-16388l,0x5.a9300a1cf661ee58p-16384l}, /* 1.20301 */
  {0x1.3c563460396af68p-16388l,0x5.ba04a504ccdc064p-16384l}, /* 1.19331 */
  {-0x9.4027deaa9f77748p-16388l,-0x5.a936e8226632238p-16384l}, /* 1.19401 */
  {-0x1.f8b424b1875c998p-16388l,-0x5.b1d67270d564d3p-16384l}, /* 1.19713 */
  };
#endif

#ifdef USE_FLOAT128
  TYPE worst[SIZE][2] = {
    /* atan2(x,y) */
    {Q(0x1.41df5aa214612c7e019fa6ade88p-13316),
     Q(0x5.e53b26a270a29eb9f77ef8ef7af8p-13316)},  /* glibc 1.88155 */
    {Q(-0x1.fb41ff205f5ade930a9fcbba8ea8p-16384),
     Q(0x2.23f098fd6b8799dbeb03219bfa08p-10520)},  /* icc 0.500500 */
    /* hypot(x,y) */
    {Q(-0x2.d8311789103b76133ea1d5bc38c4p-16384),
     Q(-0x1.6d85492006d7dcc6cc52938684p-16384)},   /* glibc 0.984073 */
    {Q(0x8.79ec30b61f9b839fe507bbdf414p-11908),
     Q(0xb.94f6832f64d0729ebd68035ed7a8p-11908)},  /* icc 0.500324 */
    /* pow(x,y) */
    {Q(0x1.36627b3005e120b290ed016d6087p+0),
     Q(-0xe.6019c4717345a8dec925ee6219fp+12)},     /* glibc 30.1981 */
    {Q(0x4p-16496),
     Q(0x3.ffffff39c102f0aa11bb2c8a91dp-128)},     /* icc 1.395601741238769 */
  };
TYPE extra[SIZE_EXTRA][2] = {
  {Q(0x0p0),Q(0x0p0)}
  };
#endif
#endif

static void
print_error (double d)
{
  mpfr_t x;
  mpfr_init2 (x, 53);
  mpfr_set_d (x, d, MPFR_RNDU);
  if (d < 0.999)
    mpfr_printf ("%.3RUf", x);
  else if (d < 9.99)
    mpfr_printf ("%.2RUf", x);
  else if (d < 99.9)
    mpfr_printf ("%.1RUf", x);
  else
    mpfr_printf ("%.2RUe", x);
  mpfr_clear (x);
}

static void
init_Threshold (void)
{
  UTYPE bound = get_utype (-TYPE_MAX) + 1;
  double w = (double) bound * (double) bound;
  double s = 0;              /* number of evaluations so far */
  while (s < w)
    {
      w = w / 2.0;
      s += 2 * (double) threshold;
    }
  Threshold = w;
}

int
main (int argc, char *argv[])
{
  unsigned int seed = 0;
  if (verbose)
    {
#ifdef GLIBC
      printf("GNU libc version: %s\n", gnu_get_libc_version ());
      printf("GNU libc release: %s\n", gnu_get_libc_release ());
#elif __CUDACC__
#ifndef CUDART_VERSION
 #warning "no " CUDART_VERSION
#else
    printf ("Using CUDA %d\n",CUDART_VERSION);
#endif
    int cuda_device = 0;
    cudaDeviceProp deviceProp;
    cudaGetDeviceProperties(&deviceProp, cuda_device);
    printf("CUDA Capable: SM %d.%d hardware\n", deviceProp.major, deviceProp.minor);
#endif
  }

  while (argc >= 2 && argv[1][0] == '-')
    {
      if (argc >= 3 && strcmp (argv[1], "-threshold") == 0)
        {
          threshold = strtoul (argv[2], NULL, 10);
          argv += 2;
          argc -= 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-seed") == 0)
        {
          seed = strtoul (argv[2], NULL, 10);
          argv += 2;
          argc -= 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-mode") == 0)
        {
          use_mode = strtoul (argv[2], NULL, 10);
          assert (0 <= use_mode && use_mode <= 2);
          argv += 2;
          argc -= 2;
        }
      else
        {
          fprintf (stderr, "Unknown option %s\n", argv[1]);
          exit (1);
        }
    }
  assert (threshold > 0);
  /* divide threshold by LIST_ALLOC so that the total number of evalutions
     does not vary with LIST_ALLOC */
  threshold = 1 + (threshold - 1) / LIST_ALLOC;
  init_Threshold ();


  int bunchSize = (threshold >int(Threshold+1)) ? threshold : int(Threshold+1);
  int nstreams = omp_get_max_threads();
  assert(maxNumOfThreads>=nstreams);

#ifdef __CUDACC__
  for (int i = 0; i < nstreams; i++)
    {
        cudaCheck(cudaStreamCreate(&(streams[i])));
        cudaCheck(cudaMalloc((void **)&zpD[i], bunchSize*sizeof(TYPE)));
        cudaCheck(cudaMallocHost((void **)&zpH[i], bunchSize*sizeof(TYPE)));
        cudaCheck(cudaMalloc((void **)&ypD[i], bunchSize*sizeof(TYPE)));
        cudaCheck(cudaMallocHost((void **)&ypH[i], bunchSize*sizeof(TYPE)));
        cudaCheck(cudaMalloc((void **)&xpD[i], bunchSize*sizeof(TYPE)));
        cudaCheck(cudaMallocHost((void **)&xpH[i], bunchSize*sizeof(TYPE)));

    }
#else
 for (int i = 0; i < nstreams; i++)
    {
      zpH[i] = (TYPE *)malloc(bunchSize*sizeof(TYPE));
      ypH[i] = (TYPE *)malloc(bunchSize*sizeof(TYPE));
      xpH[i] = (TYPE *)malloc(bunchSize*sizeof(TYPE));
    }
#endif





  if (seed == 0)
    seed = getpid ();
  if (verbose)
    printf ("Using seed %lu\n", (unsigned long) seed);

#ifdef WORST
 int nt = omp_get_thread_num();
  TYPE * x = xpH[nt];
  TYPE * y = ypH[nt];
  double d[2*SIZE];  // more than enough
  double Dbest0;
  int i;
  /* first check the 'worst' values for the given library, with the given
     rounding mode */
  // fesetround (rnd1[rnd]);
  int nval=0;
  for (i = NUMBER; i < SIZE; i+=NLIBS)
    {
      x[nval] = worst[i][0];
      y[nval++] = worst[i][1];
    }
    assert(nval<2*SIZE);
    distance (x,y,d,nval,false);
    for (i=0; i<nval; ++i) {
      if (d[i] > Dbest)
        {
          Dbest = d[i];
          Xmax = x[i];          
          Ymax = y[i];
        }
    }
    printf("first worse at %a %a is %f\n",Xmax, Ymax, Dbest);
  Dbest0 = Dbest;
  /* then check the 'worst' values for the other libraries */
  nval=0;
  for (i = 0; i < SIZE; i++)
    {
      if ((i % NLIBS) == NUMBER)
        continue;
      x[nval] = worst[i][0];
      x[nval++] = worst[i][1];
    }
   for (i = 0; i < SIZE_EXTRA; i++)
    {
      x[nval] = extra[i][0];
      y[nval++] = extra[i][1];
    }
    assert(nval<2*SIZE);
    distance (x,y,d,nval,false);
    for (i=0; i<nval; ++i) {
      if (d[i] > Dbest)
        {
          Dbest = d[i];
          Xmax = x[i];
          Ymax = y[i];
        }
    }
    printf("initial worse at %a %a is %f %f\n",Xmax, Ymax, Dbest0,Dbest);
#endif

  int nthreads;
#pragma omp parallel
  nthreads = omp_get_num_threads ();
#pragma omp parallel for
  for (int n = 0; n < nthreads; n++)
    doit (seed + n);
#ifdef WORST
  if (Dbest > Dbest0)
    printf ("NEW ");
#endif
  printf ("%s %d %d ", NAME, mode_best, Rbest);
  print_type_hex (Xmax);
  printf (",");
  print_type_hex (Ymax);
  printf (" [");
  print_error (Dbest);
  printf ("] %.6g %.16g\n", Dbest, Dbest);
  if (verbose)
    {
      mpfr_t xx, yy;
      TYPE z;
      z = *wrap_foo(&Xmax, &Ymax,1);
      printf ("libm gives ");
      print_type_hex (z);
      printf ("\n");
      mpfr_set_emax (EMAX);
      mpfr_init2 (xx, PREC);
      mpfr_init2 (yy, PREC);
      mpfr_set_type (xx, Xmax, MPFR_RNDN);
      mpfr_set_type (yy, Ymax, MPFR_RNDN);
      MPFR_FOO (yy, xx, yy, rnd);
      z = mpfr_get_type (yy, MPFR_RNDN);
      printf ("mpfr gives ");
      print_type_hex (z);
      printf ("\n");
      mpfr_clear (xx);
      mpfr_clear (yy);
    }
  fflush (stdout);
  mpfr_free_cache ();
#ifdef STAT
  printf ("eval_heuristic=%lu eval_exhaustive=%lu\n",
          eval_heuristic, eval_exhaustive);
#endif
  return 0;
}
