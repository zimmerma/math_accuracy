#include <iostream>
#include <cmath>
#include <cstdio>

#ifdef __CUDACC__
#include<cuda.h>
#include<cuda_runtime.h>
#include <cuda_runtime_api.h>

inline
bool cudaCheck_(const char* file, int line, const char* cmd, cudaError_t result)
{
    //std::cerr << file << ", line " << line << ": " << cmd << std::endl;
    if (result == cudaSuccess)
        return true;

    const char* error = cudaGetErrorName(result);
    const char* message = cudaGetErrorString(result);
    std::cerr << file << ", line " << line << ": " << error << ": " << message << std::endl;
    abort();
    return false;
}
#define cudaCheck(ARG) (cudaCheck_(__FILE__, __LINE__, #ARG, (ARG)))
#endif

__global__ void kernel_foo() {
 volatile double x = -0x1.1d7248a530504p-720;
 volatile double y = ldexp (x, -305);
 double z = -0x1.1d7248a5305p-1025;
 if (y != z)
   printf ("Error, ldexp(%la,-305) gives %la instead of %la\n", x, y, z);
 else
   printf ("OK, ldexp(%la,-305) gives %la  %la\n", x, y, z);
}


int
main (int argc, char *argv[])
{
#ifdef __CUDACC__
#ifndef CUDART_VERSION
 #warning "no " CUDART_VERSION
#else
    printf ("Using CUDA %d\n",CUDART_VERSION);
#endif
    int cuda_device = 0;
    cudaDeviceProp deviceProp;
    cudaGetDeviceProperties(&deviceProp, cuda_device);
    printf("CUDA Capable: SM %d.%d hardware\n", deviceProp.major, deviceProp.minor);
#endif

    kernel_foo<<<1,1>>>();
    cudaCheck(cudaDeviceSynchronize());


    std::cout << "on CPU" <<  std::endl;

 volatile double x = -0x1.1d7248a530504p-720;
 volatile double y = ldexp (x, -305);
 double z = -0x1.1d7248a5305p-1025;
 if (y != z)
   printf ("Error, ldexp(%la,-305) gives %la instead of %la\n", x, y, z);
 else
   printf ("OK, ldexp(%la,-305) gives %la  %la\n", x, y, z);

    return 0;

}
