// complie with nvcc oneComplex.cu -cudart shared -gencode arch=compute_$CCAP,code=sm_$CCAP -O3 -std=c++17 --compiler-options="-O3  -lmpfr -lgmp -lm -fopenmp"
// nvcc oneComplex.cu -cudart shared -gencode arch=compute_61,code=sm_61 -O3 -std=c++20 --compiler-options="-O3  -lmpfr -lgmp -lm -fopenmp -std=c++20 -march=native" -allow-unsupported-compiler --expt-relaxed-constexpr
#include <iostream>
#include <cmath>
#include <cstdio>

#ifdef __CUDACC__
#include<cuda.h>
#include<cuda_runtime.h>
#include <cuda_runtime_api.h>

#include <cuda/std/complex>
using CD = cuda::std::complex<double>;
using CF = cuda::std::complex<float>;
namespace cudastd = cuda::std;

/*
#include <complex>
using CD = std::complex<double>;
using CF = std::complex<float>;
namespace cudastd = std;
*/


inline
bool cudaCheck_(const char* file, int line, const char* cmd, cudaError_t result)
{
    //std::cerr << file << ", line " << line << ": " << cmd << std::endl;
    if (result == cudaSuccess)
        return true;

    const char* error = cudaGetErrorName(result);
    const char* message = cudaGetErrorString(result);
    std::cerr << file << ", line " << line << ": " << error << ": " << message << std::endl;
    abort();
    return false;
}
#define cudaCheck(ARG) (cudaCheck_(__FILE__, __LINE__, #ARG, (ARG)))
#endif

__global__ void kernel_foo(double r, double i) {
   CD  z(r,i);
   auto c = acosh(z);
   CD d = (2.0) * log(sqrt((0.5) * (z + (1.0)))
                 + sqrt((0.5) * (z - (1.0))));
   printf ("      acosh(%la,%la)  gives %la,%la\n", z.real(), z.imag(), c.real(), c.imag());
   printf ("naive acosh(%la,%la)  gives %la,%la\n", z.real(), z.imag(), d.real(), d.imag());
}

#include<complex>


int
main (int argc, char *argv[])
{
#ifdef __CUDACC__
#ifndef CUDART_VERSION
 #warning "no " CUDART_VERSION
#else
    printf ("Using CUDA %d\n",CUDART_VERSION);
#endif
    int cuda_device = 0;
    cudaDeviceProp deviceProp;
    cudaGetDeviceProperties(&deviceProp, cuda_device);
    printf("CUDA Capable: SM %d.%d hardware\n", deviceProp.major, deviceProp.minor);
#endif
//    double r = 0x1.481d607d542adp+94; //2.;
//    double i = -0x1.7ab11d26169b2p-1; //1.;
    double r = -0x1.019300384d993p+26;
    double i = -0x1.943eda1f20addp-1022;
    if (argc>2) {
      r = atof(argv[1]);
      i = atof(argv[2]);
    }

    kernel_foo<<<1,1>>>(r,i);
    cudaCheck(cudaDeviceSynchronize());

{
   std::cout << "on CPU std" <<  std::endl;
   std::complex<double>  z(r,i);
   auto c = std::acosh(z);
   std::complex<double> d = (2.0) * std::log(std::sqrt((0.5) * (z + (1.0)))
                 + std::sqrt((0.5) * (z - (1.0))));
   printf ("      acos(%la,%la)  gives %la,%la\n", z.real(), z.imag(), c.real(), c.imag());
   printf ("kahan acosh(%la,%la)  gives %la,%la\n", z.real(), z.imag(), d.real(), d.imag());
   auto e = std::log(z+ std::sqrt(z*z -1.));
   printf ("naive acosh(%la,%la)  gives %la,%la\n", z.real(), z.imag(), e.real(), e.imag());
}{
   std::cout << "on CPU cuda" <<  std::endl;
   CD  z(r,i);
   auto c = cudastd::acosh(z);
   CD d = (2.0) * cudastd::log(cudastd::sqrt((0.5) * (z + (1.0)))
                 + cudastd::sqrt((0.5) * (z - (1.0))));
   printf ("      acosh(%la,%la)  gives %la,%la\n", z.real(), z.imag(), c.real(), c.imag());
   printf ("kahan acosh(%la,%la)  gives %la,%la\n", z.real(), z.imag(), d.real(), d.imag());
   CD e = cudastd::log(z+ cudastd::sqrt(z*z -1.));
   printf ("naive acosh(%la,%la)  gives %la,%la\n", z.real(), z.imag(), e.real(), e.imag());

}

    return 0;

}
