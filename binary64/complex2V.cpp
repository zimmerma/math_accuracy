/* Search worst cases of a bivariate complex function.

   This program is open-source software distributed under the terms 
   of the GNU General Public License <http://www.fsf.org/copyleft/gpl.html>.

   Compile with:

   gcc -DFOO=add -DUSE_xxx -O3 complex2.c -lmpc -lmpfr -lgmp -lm -fopenmp
   icx -DFOO=add -Qoption,cpp,--extended_float_types -no-ftz -DUSE_xxx -O3 complex2.c -lmpc -lmpfr -lgmp -fopenmp

   where xxx is FLOAT, DOUBLE, LDOUBLE, or FLOAT128.

   You can add -DWORST to use some precomputed values to guide the search.

   You can add -DGLIBC to print the GNU libc release (with -v).

   Command-line options:
   -threshold nnn : set the effort threshold to nnn, the runtime is roughly
                    proportional to nnn
   -seed nnn   sets the random seed to nnn
   -mode k     sets the mode of the heuristic search to k (0 <= k <= 2).
               k=0: considers the maximal error in the range
               k=1: considers the average error in the range
               k=2: considers the estimated maximal error
   -v          verbose
   -rndn       rounding to nearest (default)
   -rndz       rounding towards zero
   -rndu       rounding towards +Inf
   -rndd       rounding towards -Inf
   -nthreads n uses n threads
   -worst xxx,yyy uses xxx,yyy (hexadecimal values) as worst values
   -x1min xxx  use xxx a minimal value for x1
   -x1max xxx  use xxx a maximal value for x1
   -y1min yyy  use yyy a minimal value for y1
   -y1max yyy  use yyy a maximal value for y1
   -x2min xxx  use xxx a minimal value for x2
   -x2max xxx  use xxx a maximal value for x2
   -y2min yyy  use yyy a minimal value for y2
   -y2max yyy  use yyy a maximal value for y2

   References and credit:
   * the idea to sample several intervals instead of only one is due to
     Eric Schneider
*/

// if IGNORE_NAN is defined, don't consider library results containing NaN
#define IGNORE_NAN

/* if IGNORE_INF is defined, don't consider cases when the library result
   is Inf and the MPC result is normal, or when the library result is normal
   and the MPC result if Inf (but we still consider cases when both results
   are Inf, if not of the same sign, this will give Inf as error) */
#define IGNORE_INF

#ifndef _GNU_SOURCE
#define _GNU_SOURCE /* to define f128 functions */
#endif

#define RANK /* print the maximal list-rank of best values */

#if defined(__clang__) && !defined(__INTEL_CLANG_COMPILER)
#define _Float128 _BitInt(128)
#endif

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <limits.h>
#ifndef NO_FLOAT128
#define MPFR_WANT_FLOAT128
#endif
/* icx does not have _Float128 */
#ifdef __INTEL_CLANG_COMPILER
#define _Float128 __float128
#endif
#include <mpc.h>
#ifndef ICX
#include <math.h>
#else
#include <mathimf.h>
#endif
#ifdef __cplusplus
#include<complex>
#include<limits>
#else
#include <complex.h>
#endif
#include <assert.h>
#include <sys/types.h>
#include <unistd.h>
#ifndef NO_OPENMP
#include <omp.h>
#endif
#include <float.h> /* for DBL_MAX */
#include <fenv.h>
#ifdef _MSC_VER
#include <BaseTsd.h>
typedef SSIZE_T ssize_t;
#endif

/* define GLIBC to print the GNU libc version */
#ifdef GLIBC
#include <gnu/libc-version.h>
#endif

#if defined(__aarch64__) && defined(__ARM_FEATURE_RNG)
  #define ARM_USE_HARDWARE_RNG
  #include <arm_acle.h>
#endif

#define MAX_THREADS 192

#ifndef atan2pif
extern float atan2pif (float, float);
#endif

/* rounding modes */
int rnd1[] = { FE_TONEAREST, FE_TOWARDZERO, FE_UPWARD, FE_DOWNWARD };
mpfr_rnd_t rnd2[] = { MPFR_RNDN, MPFR_RNDZ, MPFR_RNDU, MPFR_RNDD };
mpc_rnd_t crnd[] = { MPC_RNDNN, MPC_RNDZZ, MPC_RNDUU, MPC_RNDDD };
int rnd = 0; /* default rounding mode is MPFR_RNDN */
int verbose = 0;

/* mode (0,1,2), if -1 set according to omp_get_thread_num() */
int use_mode = -1;


#define CAT1(X,Y) X ## Y
#define CAT2(X,Y) CAT1(X,Y)
#ifndef MPC_FOO
#define MPC_FOO CAT2(mpc_,FOO)
#endif
#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)
#define NAME TOSTRING(FOO)

#ifdef USE_FLOAT
#if defined(USE_DOUBLE) || defined(USE_LDOUBLE) || defined(USE_FLOAT128)
#error "only one of USE_FLOAT, USE_DOUBLE, USE_LDOUBLE or USE_FLOAT128 can be defined"
#endif
#define TYPE float
#define UTYPE uint32_t
#ifdef __cplusplus
#define CTYPE std::complex<float>
#define CONSTRUCT  CTYPE
#define FOO1 FOO
#define FOO2 FOO1
#else
#define CTYPE float complex
#ifdef __clang__
#define CONSTRUCT(x,y) (x)+I*(y)
#else
#define CONSTRUCT(x,y) CMPLXF(x,y)
#endif
#define FOO1 CAT2(c,FOO)
#define FOO2 CAT2(FOO1,f)
#endif
#define EMAX 128
#define EMIN -149
#define PREC 24
#define mpfr_set_type mpfr_set_flt
#define mpfr_get_type mpfr_get_flt
#define TYPE_MAX FLT_MAX
#endif

#ifdef USE_DOUBLE
#if defined(USE_FLOAT) || defined(USE_LDOUBLE) || defined(USE_FLOAT128)
#error "only one of USE_FLOAT, USE_DOUBLE, USE_LDOUBLE or USE_FLOAT128 can be defined"
#endif
#define TYPE double
#define UTYPE uint64_t
#ifdef __cplusplus
#define CTYPE std::complex<double>
#define CONSTRUCT  CTYPE
#define FOO1 FOO
#define FOO2 FOO1
#else
#define CTYPE double complex
#ifdef __clang__
#define CONSTRUCT(x,y) (x)+I*(y)
#else
#define CONSTRUCT(x,y) CMPLX(x,y)
#endif
#define FOO1 CAT2(c,FOO)
#define FOO2 FOO1
#endif
#define EMAX 1024
#define EMIN -1074
#define PREC 53
#define mpfr_set_type mpfr_set_d
#define mpfr_get_type mpfr_get_d
#define TYPE_MAX DBL_MAX
#endif

#ifdef USE_LDOUBLE
#if defined(USE_FLOAT) || defined(USE_DOUBLE) || defined(USE_FLOAT128)
#error "only one of USE_FLOAT, USE_DOUBLE, USE_LDOUBLE or USE_FLOAT128 can be defined"
#endif
#define FOO1 CAT2(c,FOO)
#define FOO2 CAT2(FOO1,l)
#define TYPE long double
#define CTYPE long double complex
#define UTYPE __uint128_t
#define EMAX 16384
#define EMIN -16445
#define PREC 64
#define mpfr_set_type mpfr_set_ld
#define mpfr_get_type mpfr_get_ld
#define TYPE_MAX LDBL_MAX
#endif

#ifdef USE_FLOAT128
#if defined(USE_FLOAT) || defined(USE_DOUBLE)
#error "only one of USE_FLOAT, USE_DOUBLE or USE_FLOAT128 can be defined"
#endif
#if defined(__INTEL_COMPILER) || defined(__INTEL_CLANG_COMPILER)
#define TYPE _Quad
#define CTYPE _Quad complex
#define FOO3 CAT2(FOO,q)
#define FOO2 CAT2(__c,FOO3)
extern _Quad FOO2 (_Quad);
#define Q(x) (x ## q)
#else
#define TYPE _Float128
#define CTYPE _Float128 complex
#define FOO1 CAT2(c,FOO)
#define FOO2 CAT2(FOO1,f128)
#define Q(x) (x ## f128)
#endif
#define TYPE_MAX Q(0xf.fffffffffffffffffffffffffff8p+16380)
#define UTYPE __uint128_t
#define EMAX 16384
#define EMIN -16494
#define PREC 113
#define mpfr_set_type mpfr_set_float128
#define mpfr_get_type mpfr_get_float128
#endif

#define CUDAFOO2 FOO

#ifdef __cplusplus
#ifdef __HIPCC__
#include "hipTypes.h"
#else
#include "cudaTypes.h"
#endif
#ifdef __CUDACC__
  using CMData = cudaMath::Data<cuda::std::complex<TYPE>>;
#else
 using CMData = cudaMath::Data<std::complex<TYPE>>;
#endif
#else
static float complex
caddf (float complex z1, float complex z2)
{
  return z1 + z2;
}

static float complex
cmulf (float complex z1, float complex z2)
{
  return z1 * z2;
}

static float complex
cdivf (float complex z1, float complex z2)
{
  return z1 / z2;
}

static double complex
cadd (double complex z1, double complex z2)
{
  return z1 + z2;
}

static double complex
cmul (double complex z1, double complex z2)
{
  return z1 * z2;
}

static double complex
cdiv (double complex z1, double complex z2)
{
  return z1 / z2;
}
#endif

static void
print_type_hex (TYPE x)
{
#ifdef USE_FLOAT
  printf ("%a", x);
#endif
#ifdef USE_DOUBLE
  printf ("%a", x);
#endif
#ifdef USE_LDOUBLE
  mpfr_t y;
  mpfr_init2 (y, PREC);
  mpfr_set_type (y, x, MPFR_RNDN);
  mpfr_printf ("%Ral", y);
  mpfr_clear (y);
#endif
#ifdef USE_FLOAT128
  mpfr_t y;
  mpfr_init2 (y, PREC);
  mpfr_set_type (y, x, MPFR_RNDN);
  mpfr_printf ("%Ra", y);
  mpfr_clear (y);
#endif
}

typedef union { UTYPE n; TYPE x; } union_t;

TYPE
get_type (UTYPE n)
{
  union_t u;
  u.n = n;
  return u.x;
}

UTYPE
get_utype (TYPE x)
{
  union_t u;
  u.x = x;
  return u.n;
}


#ifdef __cplusplus
  template <typename T>
  constexpr bool isFinite(T x);

  template <typename T>
  constexpr bool isNotFinite(T x) {
    return !isFinite(x);
  }

  template <>
  constexpr bool isFinite(float x) {
    const unsigned int mask = 0x7f800000;
    union {
      unsigned int l;
      float d;
    } v = {.d = x};
    return (v.l & mask) != mask;
  }

  template <>
  constexpr bool isFinite(double x) {
    const unsigned long long mask = 0x7FF0000000000000LL;
    union {
      unsigned long long l;
      double d;
    } v = {.d = x};
    return (v.l & mask) != mask;
  }

  template <>
  constexpr bool isFinite(long double x) {
    double xx = x;
    return isFinite(xx);
  }
#endif




#ifdef USE_LDOUBLE
static int
is_valid (TYPE x)
{
  UTYPE n = get_utype (x);
  int e = (n >> 64) & 0x7fff; /* exponent */
  uint64_t s = (uint64_t) n;  /* significand */
  if (e == 0) return (s >> 63) == 0;
  else return (n >> 63) & 1;
}
#endif

#ifdef VECTORIZE
enum class What {one, fill, compute};
What what[CMData::maxNumOfThreads];
int icurr[CMData::maxNumOfThreads];
int irand[CMData::maxNumOfThreads];
CMData * gdata = nullptr;
inline CMData & cmdata() {  return *gdata;}
#endif


/* return the normwise distance between FOO2(x1+i*y1,x2+i*y2) and the value
   z computed by MPC with precision 3*PREC, measured in terms of ulp(|z|) */
static double
distance (TYPE x1, TYPE y1, TYPE x2, TYPE y2, int tid)
{
#ifdef VECTORIZE
   if (What::fill==what[tid]) {
     assert(icurr[tid]<cmdata().bunchSize);
     cmdata().xpH[tid][icurr[tid]] = {x1,y1};
     cmdata().ypH[tid][icurr[tid]++] = {x2,y2};
     // std::cout << "fill  " << icurr[tid]  << ' ' << cmdata().xpH[tid][icurr[tid]]  << ' ' << cmdata().ypH[tid][icurr[tid]] << std::endl;
     return 0;
   }
#endif


  mpc_t xx1, xx2, tt;
  mpfr_t yyx, yyy, zzx, zzy;
  mpfr_exp_t ey;
  TYPE zx, zy;
  double ret = 0;

  if (isnan (x1) || isnan (y1) || isnan (x2) || isnan (y2)) {
#ifdef VECTORIZE
   // std::cout << "nan  " << icurr[tid]  << ' ' << x1<<','<<y1 << ' ' << x2<<','<<y2 << cmdata().xpH[tid][icurr[tid]]  << ' ' << cmdata().ypH[tid][icurr[tid]] << std::endl;;
   if (What::compute==what[tid]) icurr[tid]++;
#endif
    return 0;
  }

#ifdef USE_LDOUBLE
  if (!is_valid (x) || !is_valid (y))
    return 0;
#endif

  CTYPE z1_in = CONSTRUCT (x1, y1);
  CTYPE z2_in = CONSTRUCT (x2, y2);


#ifdef VECTORIZE
   CTYPE z_out;
   if (What::compute==what[tid]) {
     assert(icurr[tid]<cmdata().bunchSize);
     // std::cout << "comp  " << icurr[tid]  << ' ' << z1_in << ' ' << z2_in << ' ' << cmdata().xpH[tid][icurr[tid]] << ' ' << cmdata().ypH[tid][icurr[tid]] << std::endl;;
     assert(z1_in == cmdata().xpH[tid][icurr[tid]]);
     assert(z2_in == cmdata().ypH[tid][icurr[tid]]);
     z_out = cmdata().zpH[tid][icurr[tid]++];
   } else {
    assert(What::one==what[tid]);
    assert(0==tid);
    cmdata().xpH[tid][0] = z1_in;
    cmdata().ypH[tid][0] = z2_in;
    auto zp = wrap_foo(cmdata(),1,tid);
    z_out = zp[0];
  }
#else
  CTYPE z_out = FOO2 (z1_in, z2_in);
#endif

#ifdef __cplusplus
  zx = z_out.real();
  zy = z_out.imag();
#else
  zx = creal (z_out);
  zy = cimag (z_out);
#endif

  mpfr_exp_t emax = mpfr_get_emax ();
  mpfr_set_emax (EMAX);
  mpc_init2 (xx1, PREC);
  mpc_init2 (xx2, PREC);
  mpfr_init2 (yyx, PREC+1);
  mpfr_init2 (yyy, PREC+1);
  mpc_init2 (tt, 3*PREC);
  mpfr_init2 (zzx, 3*PREC);
  mpfr_init2 (zzy, 3*PREC);
  mpfr_set_type (mpc_realref(xx1), x1, MPFR_RNDN);
  mpfr_set_type (mpc_imagref(xx1), y1, MPFR_RNDN);
  mpfr_set_type (mpc_realref(xx2), x2, MPFR_RNDN);
  mpfr_set_type (mpc_imagref(xx2), y2, MPFR_RNDN);
  MPC_FOO (tt, xx1, xx2, crnd[rnd]);
  mpc_real (zzx, tt, MPFR_RNDN);
  mpc_imag (zzy, tt, MPFR_RNDN);
  int isnan_z = isnan (zx) || isnan (zy);
  int isnan_zz = mpfr_nan_p (zzx) || mpfr_nan_p (zzy);
  if (isnan_z && isnan_zz)
    goto end; // both results are NaN
  if (isnan_z || isnan_zz) // only one result is NaN
  {
#ifndef IGNORE_NAN
    ret = 0x1p1023 + 0x1p1023;
#endif
    goto end;
  }
  mpfr_set_emax (mpfr_get_emax_max ());

  if (isinf (zx) && mpfr_inf_p (zzx))
  {
    /* both return Inf: if same sign, consider them as equal */
    if ((zx > 0 && mpfr_sgn (zzx) > 0) || (zx < 0 && mpfr_sgn (zzx) < 0))
    {
      mpfr_set_ui (yyx, 0, MPFR_RNDN);
      mpfr_set_ui (zzx, 0, MPFR_RNDN);
    }
    // otherwise they have different sign, and the difference will be Inf
  }
  else if (isinf (zx) && mpfr_inf_p (zzx) == 0)
    {
#ifdef IGNORE_INF
    goto end;
#endif
      /* libm returns Inf, MPFR not, we assume the libm rounded to the next
         number after the largest finite number in the format */
      mpfr_set_ui_2exp (yyx, 1, EMAX, MPFR_RNDN);
      if (zx < 0)
        mpfr_neg (yyx, yyx, MPFR_RNDN);
    }
  else if (!isinf (zx) && mpfr_inf_p (zzx))
  {
#ifdef IGNORE_INF
    goto end;
#endif
    /* MPFR yields Inf, but libm no: we recompute the MPFR result with
       unbounded exponent */
    MPC_FOO (tt, xx1, xx2, crnd[rnd]);
    mpc_real (zzx, tt, MPFR_RNDN);
    mpfr_set_type (yyx, zx, MPFR_RNDN);
  }
  else
    mpfr_set_type (yyx, zx, MPFR_RNDN);

  if (isinf (zy) && mpfr_inf_p (zzy))
  {
    /* both return Inf: if same sign, consider them as equal */
    if ((zy > 0 && mpfr_sgn (zzy) > 0) || (zy < 0 && mpfr_sgn (zzy) < 0))
    {
      mpfr_set_ui (yyy, 0, MPFR_RNDN);
      mpfr_set_ui (zzy, 0, MPFR_RNDN);
    }
    // otherwise they have different sign, and the difference will be Inf
  }
  else if (isinf (zy) && mpfr_inf_p (zzy) == 0)
    {
#ifdef IGNORE_INF
    goto end;
#endif
      /* libm returns Inf, MPFR not, we assume the libm rounded to the next
         number after the largest finite number in the format */
      mpfr_set_ui_2exp (yyy, 1, EMAX, MPFR_RNDN);
      if (zy < 0)
        mpfr_neg (yyy, yyy, MPFR_RNDN);
    }
  else if (!isinf (zy) && mpfr_inf_p (zzy))
  {
#ifdef IGNORE_INF
    goto end;
#endif
    /* MPFR yields Inf, but libm no: we recompute the MPFR result with
       unbounded exponent */
    MPC_FOO (tt, xx1, xx2, crnd[rnd]);
    mpc_real (zzy, tt, MPFR_RNDN);
    mpfr_set_type (yyy, zy, MPFR_RNDN);
  }
  else
    mpfr_set_type (yyy, zy, MPFR_RNDN);

  mpfr_sub (zzx, zzx, yyx, MPFR_RNDN);
  mpfr_sub (zzy, zzy, yyy, MPFR_RNDN);
  mpfr_hypot (zzx, zzx, zzy, MPFR_RNDN); // zzx is the difference norm
  mpfr_hypot (zzy, mpc_realref(tt), mpc_imagref(tt), MPFR_RNDN);
  if (mpfr_zero_p (zzy))
  {
    if (!mpfr_zero_p (zzx))
      ret = 0x1p1023 + 0x1p1023;
    goto end;
  }
  // the smallest normal number is 2^(EMIN+PREC-1)
  ey = mpfr_get_exp (zzy);
  if (mpfr_inf_p (zzy) || ey - PREC >= EMIN) // |tt| is normal
  {
    // divide the difference by ulp(|tt|)
    mpfr_mul_2si (zzx, zzx, PREC - ey, MPFR_RNDN);
  }
  else // divide the difference by 2^EMIN
    mpfr_div_2si (zzx, zzx, EMIN, MPFR_RNDN);
  ret = mpfr_get_d (zzx, MPFR_RNDU);
 end:
  mpc_clear (xx1);
  mpc_clear (xx2);
  mpfr_clear (yyx);
  mpfr_clear (yyy);
  mpfr_clear (zzx);
  mpfr_clear (zzy);
  mpc_clear (tt);
  mpfr_set_emax (emax);
  return ret;
}

uint64_t threshold = 1000000;
double Threshold;

static unsigned int Seed[128];

uint32_t my_rand_wrapper (int tid)
{
  uint32_t val = 0;
#ifdef VECTORIZE
  assert(irand[tid]<cmdata().bunchSize);
  if (What::compute==what[tid]) {
    val = cmdata().rndm[tid][irand[tid]++];
    // std::cout << "comp " << irand[tid] << ' ' << val << std::endl;
    return val;
  }
#endif



#ifdef ARM_USE_HARDWARE_RNG
  /* Utilize hardware RNG instructions on the ARM processors that support them.
   *
   * During profiling, it was discovered that almost all of the processing time
   * was spent servicing the rand calls due to continual exhaustion of the
   * available system entropy. Using hardware RNG resolved this issue.
   *
   * TODO: The rndr instruction returns a 64 bit unsigned integer, of which we
   *       throw out 32 bits in order to be compatible with the existing code
   *       as it was written with rand() in mind, which returns a 32 bit
   *       integer. Ideally, we would refactor the code so that we don't throw
   *       out the bits just to request another 32 bits to build a full 64 bit
   *       value when USE_DOUBLE is defined. */
  uint64_t temp = 0;
  int res = __rndr(&temp);
  assert(res == 0);
  val = (uint32_t)temp;
#else
  val = rand_r (Seed + tid);
#endif
#ifdef VECTORIZE
  if (What::fill==what[tid]) {
    cmdata().rndm[tid][irand[tid]++]=val;
    // std::cout << "fill " << irand[tid] << ' ' << val << std::endl;
  }
#endif
  return val;
}

#if RAND_MAX == 2147483647
#ifdef USE_FLOAT
static uint32_t
my_random (uint32_t n, int tid)
{
  uint32_t ret = my_rand_wrapper (tid);
  if (n > RAND_MAX)
    ret = (ret << 31) | my_rand_wrapper (tid);
  return ret % n;
}
#else /* USE_FLOAT */
#ifdef USE_DOUBLE
static uint64_t
my_random (uint64_t n, int tid)
{
  uint64_t ret = my_rand_wrapper (tid);
  if (n > RAND_MAX)
    {
      ret = (ret << 31) | my_rand_wrapper (tid);
      /* now ret <= 2^62-1 */
      if (n >> 62)
        ret = (ret << 31) | my_rand_wrapper (tid);
    }
  return ret % n;
}
#else /* USE_LDOUBLE or USE_FLOAT128 */
static __uint128_t
my_random (__uint128_t n, int tid)
{
  __uint128_t ret = my_rand_wrapper (tid);
  if (n > RAND_MAX)
    {
      ret = (ret << 31) | my_rand_wrapper (tid);
      /* now ret <= 2^62-1 */
      if (n >> 62)
        {
          ret = (ret << 31) | my_rand_wrapper (tid);
          /* now ret <= 2^93-1 */
          if (n >> 93)
            {
              ret = (ret << 31) | my_rand_wrapper (tid);
              /* now ret <= 2^93-1 */
              if (n >> 124)
                ret = (ret << 31) | my_rand_wrapper (tid);
            }
        }
    }
  return ret % n;
}
#endif /* USE_DOUBLE */
#endif /* USE_FLOAT */

#elif RAND_MAX == 0x7fff /* Microsoft Visual C/C++ */

#ifdef USE_FLOAT
static uint32_t
my_random (uint32_t n, int tid)
{
  uint32_t ret = my_rand_wrapper (tid);
  if (n > RAND_MAX)
    {
      ret = (ret << 15) | my_rand_wrapper (tid);
      /* now ret <= 2^30-1 */
      if (n >> 30)
        ret = (ret << 15) | my_rand_wrapper (tid);
    }
  return ret % n;
}
#endif
#ifdef USE_DOUBLE
static uint64_t
my_random (uint64_t n, int tid)
{
  uint64_t ret = my_rand_wrapper (tid);
  if (n > RAND_MAX)
    {
      ret = (ret << 15) | my_rand_wrapper (tid);
      /* now ret <= 2^30-1 */
      if (n >> 30)
        {
          ret = (ret << 15) | my_rand_wrapper (tid);
          /* now ret <= 2^45-1 */
          if (n >> 45)
            {
              ret = (ret << 15) | my_rand_wrapper (tid);
              /* now ret <= 2^60-1 */
              if (n >> 60)
                ret = (ret << 15) | my_rand_wrapper (tid);
            }
        }
    }
  return ret % n;
}
#endif
#if defined(USE_LDOUBLE) || defined(USE_FLOAT128)
#error "my_random() not yet implemented for 128-bit type"
#endif

#else
#error "Unexpected value of RAND_MAX"
#endif

TYPE X1best, Y1best, X2best, Y2best; // best values found so far
TYPE X1min, X1max, Y1min, Y1max, X2min, X2max, Y2min, Y2max;
double Dbest = 0.0;
int Rbest = -1;
int mode_best = 0;

/* return the maximal error on [nx1min,nx1max] x [ny1min,ny1max]
   x [nx2min,nx2max] x [ny2min,ny2max],
   and update nx1best,ny1best,nx2best,ny2best if it improves
   distance(x1,y1,x2,y2) */
static double
max_heuristic1 (UTYPE nx1min, UTYPE nx1max, UTYPE ny1min, UTYPE ny1max,
                UTYPE nx2min, UTYPE nx2max, UTYPE ny2min, UTYPE ny2max,
                UTYPE *nx1best, UTYPE *ny1best, UTYPE *nx2best, UTYPE *ny2best,
                int tid)
{
  TYPE x1, y1, x2, y2;
  UTYPE i, nx1, ny1, nx2, ny2;
  double dbest, dmax, d;
  x1 = get_type (*nx1best);
  y1 = get_type (*ny1best);
  x2 = get_type (*nx2best);
  y2 = get_type (*ny2best);
  dbest = distance (x1, y1, x2, y2, tid);
  dmax = 0;
  for (i = 0; i < threshold; i++)
    {
      nx1 = nx1min + my_random (nx1max - nx1min, tid);
      ny1 = ny1min + my_random (ny1max - ny1min, tid);
      nx2 = nx2min + my_random (nx2max - nx2min, tid);
      ny2 = ny2min + my_random (ny2max - ny2min, tid);
#ifdef USE_LDOUBLE
      /* for long double, bit 63 must be set */
      nx1 |= (UTYPE) 1 << 63;
      ny1 |= (UTYPE) 1 << 63;
      nx2 |= (UTYPE) 1 << 63;
      ny2 |= (UTYPE) 1 << 63;
#endif
      x1 = get_type (nx1);
      y1 = get_type (ny1);
      x2 = get_type (nx2);
      y2 = get_type (ny2);
      d = distance (x1, y1, x2, y2, tid);
      if (d > dmax)
        {
          dmax = d;
          if (d > dbest)
            {
              dbest = d;
              *nx1best = nx1;
              *ny1best = ny1;
              *nx2best = nx2;
              *ny2best = ny2;
            }
        }
    }
  return dmax;
}

/* return the average error on [nx1min,nx1max] x [ny1min,ny1max]
   x [nx2min,nx2max] x [ny2min,ny2max],
   given (nx1best,ny1best,nx2best,ny2best) */
static double
max_heuristic2 (UTYPE nx1min, UTYPE nx1max, UTYPE ny1min, UTYPE ny1max,
                UTYPE nx2min, UTYPE nx2max, UTYPE ny2min, UTYPE ny2max,
                UTYPE *nx1best, UTYPE *ny1best, UTYPE *nx2best, UTYPE *ny2best,
                int tid)
{
  TYPE x1, y1, x2, y2;
  UTYPE i, nx1, ny1, nx2, ny2;
  double dbest, d, s = 0, n = 0;
  x1 = get_type (*nx1best);
  y1 = get_type (*ny1best);
  x2 = get_type (*nx2best);
  y2 = get_type (*ny2best);
  dbest = distance (x1, y1, x2, y2, tid);
  for (i = 0; i < threshold; i++)
    {
      nx1 = nx1min + my_random (nx1max - nx1min, tid);
      ny1 = ny1min + my_random (ny1max - ny1min, tid);
      nx2 = nx2min + my_random (nx2max - nx2min, tid);
      ny2 = ny2min + my_random (ny2max - ny2min, tid);
#ifdef USE_LDOUBLE
      /* for long double, bit 63 must be set */
      nx1 |= (UTYPE) 1 << 63;
      ny1 |= (UTYPE) 1 << 63;
      nx2 |= (UTYPE) 1 << 63;
      ny2 |= (UTYPE) 1 << 63;
#endif
      x1 = get_type (nx1);
      y1 = get_type (ny1);
      x2 = get_type (nx2);
      y2 = get_type (ny2);
      d = distance (x1, y1, x2, y2, tid);
      if (d != 0)
        {
          s += d;
          n ++;
        }
      if (d > dbest)
        {
          dbest = d;
          *nx1best = nx1;
          *ny1best = ny1;
          *nx2best = nx2;
          *ny2best = ny2;
        }
    }
  if (n != 0)
    s = s / n;
  return s;
}

/* some libraries do not have log(), for example llvm */
static double mylog (double n)
{
  mpfr_t x;
  mpfr_init2 (x, 53);
  mpfr_set_d (x, n, MPFR_RNDN);
  mpfr_log (x, x, MPFR_RNDN);
  double ret = mpfr_get_d (x, MPFR_RNDN);
  mpfr_clear (x);
  return ret;
}

/* avoid depending from math.h */
static double mysqrt (double n)
{
  mpfr_t x;
  mpfr_init2 (x, 53);
  mpfr_set_d (x, n, MPFR_RNDN);
  mpfr_sqrt (x, x, MPFR_RNDN);
  double ret = mpfr_get_d (x, MPFR_RNDN);
  mpfr_clear (x);
  return ret;
}

/* return the estimated maximal error on [nx1min,nx1max] x [ny1min,ny1max]
   x [nx2min,nx2max] x [ny2min,ny2max],
   taking into account mean and standard deviation,
   and update (nx1best,ny1best,nx2best,ny2best) */
static double
max_heuristic3 (UTYPE nx1min, UTYPE nx1max, UTYPE ny1min, UTYPE ny1max,
                UTYPE nx2min, UTYPE nx2max, UTYPE ny2min, UTYPE ny2max,
                UTYPE *nx1best, UTYPE *ny1best, UTYPE *nx2best, UTYPE *ny2best,
                int tid)
{
  TYPE x1, y1, x2, y2;
  UTYPE i, nx1, ny1, nx2, ny2;
  double dbest, d, s = 0, v = 0, n = 0;
  x1 = get_type (*nx1best);
  y1 = get_type (*ny1best);
  x2 = get_type (*nx2best);
  y2 = get_type (*ny2best);
  dbest = distance (x1, y1, x2, y2, tid);
  for (i = 0; i < threshold; i++)
    {
      nx1 = nx1min + my_random (nx1max - nx1min, tid);
      ny1 = ny1min + my_random (ny1max - ny1min, tid);
      nx2 = nx2min + my_random (nx2max - nx2min, tid);
      ny2 = ny2min + my_random (ny2max - ny2min, tid);
#ifdef USE_LDOUBLE
      /* for long double, bit 63 must be set */
      nx1 |= (UTYPE) 1 << 63;
      ny1 |= (UTYPE) 1 << 63;
      nx2 |= (UTYPE) 1 << 63;
      ny2 |= (UTYPE) 1 << 63;
#endif
      x1 = get_type (nx1);
      y1 = get_type (ny1);
      x2 = get_type (nx2);
      y2 = get_type (ny2);
      d = distance (x1, y1, x2, y2, tid);
      if (d != 0)
        {
          s += d;
          v += d * d;
          n ++;
        }
      if (d > dbest)
        {
          dbest = d;
          *nx1best = nx1;
          *ny1best = ny1;
          *nx2best = nx2;
          *ny2best = ny2;
        }
    }
  /* compute mean and standard deviation */
  if (n != 0)
    {
      s = s / n;
      v = v / n - s * s;
      if (v < 0)
        v = 0;
      double sigma = mysqrt (v);
      /* we got n non-zero values out of threshold, thus we should get
         n/threshold*(nx1max-nx1min)*(ny1min-ny1max)*(nx2max-nx2min)*(ny2min-ny2max) */
      n = n * (double) (nx1max - nx1min) * (double) (ny1max - ny1min)
        * (double) (nx2max - nx2min) * (double) (ny2max - ny2min)
        / (double) threshold;
      double logn = mylog (n);
      double t = mysqrt (2.0 * logn);
      /* Reference: A note on the first moment of extreme order statistics
         from the normal distribution, Max Petzold,
         https://gupea.ub.gu.se/handle/2077/3092 */
      s = s + sigma * (t - (mylog (logn) + 1.3766) / (2.0 * t));
    }
  return s;
}

#ifdef NO_OPENMP
static int omp_get_thread_num (void) { return 0; }
static int omp_get_num_threads (void) { return 1; }
static void omp_set_num_threads (int i) { }
#endif

static int
mode (void)
{
  if (use_mode != -1)
    return use_mode;
  int i = omp_get_thread_num ();
  /* Modes 1,2 seem to be less efficient, thus we use only 1 thread on each. */
  if (i == 1 || i == 2)
    return i;
  return 0;
}

#ifdef STAT
unsigned long eval_heuristic = 0;
unsigned long eval_exhaustive = 0;
#endif

static double
max_heuristic (UTYPE nx1min, UTYPE nx1max, UTYPE ny1min, UTYPE ny1max,
               UTYPE nx2min, UTYPE nx2max, UTYPE ny2min, UTYPE ny2max,
               UTYPE *nx1best, UTYPE *ny1best, UTYPE *nx2best, UTYPE *ny2best,
               int tid)
{
  if (nx1min == nx1max || ny1min == ny1max || nx2min == nx2max || ny2min == ny2max)
    return 0;

  int k = mode ();

#ifdef STAT
#pragma omp atomic update
  eval_heuristic += threshold;
#endif

  if (k == 0)
    return max_heuristic1 (nx1min, nx1max, ny1min, ny1max, nx2min, nx2max, ny2min, ny2max, nx1best, ny1best, nx2best, ny2best, tid);
  else if (k == 1)
    return max_heuristic2 (nx1min, nx1max, ny1min, ny1max, nx2min, nx2max, ny2min, ny2max, nx1best, ny1best, nx2best, ny2best, tid);
  else
    return max_heuristic3 (nx1min, nx1max, ny1min, ny1max, nx2min, nx2max, ny2min, ny2max, nx1best, ny1best, nx2best, ny2best, tid);
}

typedef struct {
  UTYPE nx1min, nx1max;
  UTYPE ny1min, ny1max;
  UTYPE nx2min, nx2max;
  UTYPE ny2min, ny2max;
  double d;
#ifdef RANK
  int rank; /* worst rank */
#endif
} chunk_t;

static double
chunk_size (chunk_t c)
{
  return (double) (c.nx1max - c.nx1min) * (double) (c.ny1max - c.ny1min)
    * (double) (c.nx2max - c.nx2min) * (double) (c.ny2max - c.ny2min);
}

static void
chunk_swap (chunk_t *a, chunk_t *b)
{
  UTYPE tmp;
  tmp = a->nx1min; a->nx1min = b->nx1min; b->nx1min = tmp;
  tmp = a->nx1max; a->nx1max = b->nx1max; b->nx1max = tmp;
  tmp = a->ny1min; a->ny1min = b->ny1min; b->ny1min = tmp;
  tmp = a->ny1max; a->ny1max = b->ny1max; b->ny1max = tmp;
  tmp = a->nx2min; a->nx2min = b->nx2min; b->nx2min = tmp;
  tmp = a->nx2max; a->nx2max = b->nx2max; b->nx2max = tmp;
  tmp = a->ny2min; a->ny2min = b->ny2min; b->ny2min = tmp;
  tmp = a->ny2max; a->ny2max = b->ny2max; b->ny2max = tmp;
  double ump;
  ump = a->d; a->d = b->d; b->d = ump;
#ifdef RANK
  int rmp;
  rmp = a->rank; a->rank = b->rank; b->rank = rmp;
#endif
}

typedef struct {
  chunk_t *l;
  int size;
} List_struct;
typedef List_struct List_t[1];

/* Idea from Eric Schneider: instead of sampling only one interval at each
   level of the binary splitting tree, we sample up to LIST_ALLOC intervals,
   and keep the most promising ones. We use the default value suggested by
   Eric Schneider (20). */
#define LIST_ALLOC 20

static void
List_init (List_t L)
{
  L->l = (chunk_t*) malloc (LIST_ALLOC * sizeof (chunk_t));
  L->size = 0;
}

static void
List_init2 (List_t L, UTYPE nx1min, UTYPE nx1max, UTYPE ny1min, UTYPE ny1max,
            UTYPE nx2min, UTYPE nx2max, UTYPE ny2min, UTYPE ny2max)
{
  L->l = (chunk_t*) malloc (LIST_ALLOC * sizeof (chunk_t));
  L->l[0].nx1min = nx1min;
  L->l[0].nx1max = nx1max;
  L->l[0].ny1min = ny1min;
  L->l[0].ny1max = ny1max;
  L->l[0].nx2min = nx2min;
  L->l[0].nx2max = nx2max;
  L->l[0].ny2min = ny2min;
  L->l[0].ny2max = ny2max;
  L->size = 1;
}

#if 0
static void
List_check (List_t L)
{
  for (int i = 1; i < L->size; i++)
    if (L->l[i-1].d < L->l[i].d)
      {
        fprintf (stderr, "Error, list not sorted:\n");
        List_print (L);
        exit (1);
      }
}
#endif

static void
List_insert (List_t L, UTYPE nx1min, UTYPE nx1max, UTYPE ny1min, UTYPE ny1max,
             UTYPE nx2min, UTYPE nx2max, UTYPE ny2min, UTYPE ny2max, double d)
{
  if (nx1min == nx1max || ny1min == ny1max || nx2min == nx2max || ny2min == ny2max)
    return;
  int i = L->size;
  if (i < LIST_ALLOC || d > L->l[LIST_ALLOC-1].d)
    {
      /* if list is not full, we insert at position i,
         otherwise we insert at position LIST_ALLOC-1 */
      if (i == LIST_ALLOC)
        i--;
      else
        L->size++;
      L->l[i].nx1min = nx1min;
      L->l[i].nx1max = nx1max;
      L->l[i].ny1min = ny1min;
      L->l[i].ny1max = ny1max;
      L->l[i].nx2min = nx2min;
      L->l[i].nx2max = nx2max;
      L->l[i].ny2min = ny2min;
      L->l[i].ny2max = ny2max;
      L->l[i].d = d;
      /* now insertion sort */
      while (i > 0 && d > L->l[i-1].d)
        {
          chunk_swap (L->l + (i-1), L->l + i);
#ifdef RANK
          if (L->l[i].rank < i)
            L->l[i].rank = i; /* updates maximal rank */
#endif
          i--;
        }
#ifdef RANK
      L->l[i].rank = i; /* set initial rank */
#endif
    }
  // List_check (L);
}

static void
List_swap (List_t L1, List_t L2)
{
  chunk_t *tmp;
  tmp = L1->l; L1->l = L2->l; L2->l = tmp;
  int ump;
  ump = L1->size; L1->size = L2->size; L2->size = ump;
}

static void
List_clear (List_t L)
{
  free (L->l);
}

static void
exhaustive_search (chunk_t *c, UTYPE *nx1best, UTYPE *ny1best,
                   UTYPE *nx2best, UTYPE *ny2best, double *dbest,
                   int *rbest, int tid)
{
  for (UTYPE nx1 = c->nx1min; nx1 < c->nx1max; nx1 ++)
    {
      TYPE x1 = get_type (nx1);
      for (UTYPE ny1 = c->ny1min; ny1 < c->ny1max; ny1 ++)
        {
          TYPE y1 = get_type (ny1);
          for (UTYPE nx2 = c->nx2min; nx2 < c->nx2max; nx2 ++)
          {
            TYPE x2 = get_type (nx2);
            for (UTYPE ny2 = c->ny2min; ny2 < c->ny2max; ny2 ++)
            {
              TYPE y2 = get_type (ny2);
              double d = distance (x1, y1, x2, y2, tid);
              if (d > *dbest)
              {
                *dbest = d;
                *nx1best = nx1;
                *ny1best = ny1;
                *nx2best = nx2;
                *ny2best = ny2;
#ifdef RANK
                *rbest = c->rank;
#endif
              }
            }
          }
        }
    }
#ifdef STAT
#pragma omp atomic update
  eval_exhaustive += (c->nx1max - c->nx1min) * (c->ny1max - c->ny1min)
    * (c->nx2max - c->nx2min) * (c->ny2max - c->ny2min);
#endif
}

static void
search_aux (UTYPE nx1min, UTYPE nx1max, UTYPE ny1min, UTYPE ny1max,
            UTYPE nx2min, UTYPE nx2max, UTYPE ny2min, UTYPE ny2max,
            UTYPE *nx1best, UTYPE *ny1best, UTYPE *nx2best, UTYPE *ny2best,
            int tid, List_t L)
{
  double d;
#ifdef VECTORIZE
  what[tid] = What::fill;
  icurr[tid]=0;
  irand[tid]=0;
  assert(0  ==
      max_heuristic (nx1min, nx1max, ny1min, ny1max,
                     nx2min, nx2max, ny2min, ny2max,
                     nx1best, ny1best, nx2best, ny2best, tid)
  );
  wrap_foo(cmdata(),icurr[tid],tid);
  what[tid] = What::compute;
  icurr[tid]=0;
  irand[tid]=0;
#endif
  d = max_heuristic (nx1min, nx1max, ny1min, ny1max,
                     nx2min, nx2max, ny2min, ny2max,
                     nx1best, ny1best, nx2best, ny2best, tid);
  List_insert (L, nx1min, nx1max, ny1min, ny1max,
               nx2min, nx2max, ny2min, ny2max, d);
}

/* search for nx1min <= nx1 < nx1max, ny1min <= ny1 < ny1max,
   nx2min <= nx2 < nx2max, ny2min <= ny2 < ny2max,
   where the worst found so far is (nx1best,ny1best,nx2best,ny2best,dbest) */
static void
search (UTYPE nx1min, UTYPE nx1max, UTYPE ny1min, UTYPE ny1max,
        UTYPE nx2min, UTYPE nx2max, UTYPE ny2min, UTYPE ny2max,
        UTYPE *nx1best, UTYPE *ny1best, UTYPE *nx2best, UTYPE *ny2best,
        double *dbest, int *rbest)
{
  int tid = omp_get_thread_num ();
  List_t L;

  List_init2 (L, nx1min, nx1max, ny1min, ny1max, nx2min, nx2max, ny2min, ny2max);
  while (1)
    {
      assert (1 <= L->size && L->size <= LIST_ALLOC);
      double width = chunk_size (L->l[0]);
      if (width <= Threshold) /* exhaustive search */
        {
          for (int i = 0; i < L->size; i++) {
#ifdef VECTORIZE
            what[tid] = What::fill;
            icurr[tid]=0;
            exhaustive_search (L->l + i, nx1best, ny1best, nx2best, ny2best, dbest, rbest, tid);
            what[tid] = What::compute;
            wrap_foo(cmdata(),icurr[tid],tid);
            icurr[tid]=0;
#endif
            exhaustive_search (L->l + i, nx1best, ny1best, nx2best, ny2best, dbest, rbest, tid);
          }
          break;
        }
      else /* split each chunk in two */
        {
          List_t NewL;
          List_init (NewL);
          for (int i = 0; i < L->size; i++)
            {
              UTYPE nx1min = L->l[i].nx1min;
              UTYPE nx1max = L->l[i].nx1max;
              UTYPE width_x1 = nx1max - nx1min;
              UTYPE ny1min = L->l[i].ny1min;
              UTYPE ny1max = L->l[i].ny1max;
              UTYPE width_y1 = ny1max - ny1min;
              UTYPE nx2min = L->l[i].nx2min;
              UTYPE nx2max = L->l[i].nx2max;
              UTYPE width_x2 = nx2max - nx2min;
              UTYPE ny2min = L->l[i].ny2min;
              UTYPE ny2max = L->l[i].ny2max;
              UTYPE width_y2 = ny2max - ny2min;
              if (width_x1 >= width_y1 && width_x1 >= width_x2
                  && width_x1 >= width_y2) // cut the x1-side in two
              {
                UTYPE nx1mid = nx1min + (nx1max - nx1min) / 2;
                search_aux (nx1min, nx1mid, ny1min, ny1max, nx2min, nx2max, ny2min, ny2max, nx1best, ny1best, nx2best, ny2best, tid, NewL);
                search_aux (nx1mid, nx1max, ny1min, ny1max, nx2min, nx2max, ny2min, ny2max, nx1best, ny1best, nx2best, ny2best, tid, NewL);
              }
              else if (width_y1 >= width_x2 && width_y1 >= width_y2) // cut the y1-side in two
              {
                UTYPE ny1mid = ny1min + (ny1max - ny1min) / 2;
                search_aux (nx1min, nx1max, ny1min, ny1mid, nx2min, nx2max, ny2min, ny2max, nx1best, ny1best, nx2best, ny2best, tid, NewL);
                search_aux (nx1min, nx1max, ny1mid, ny1max, nx2min, nx2max, ny2min, ny2max, nx1best, ny1best, nx2best, ny2best, tid, NewL);
              }
              else if (width_x2 >= width_y2) // cut the x2-side in two
              {
                UTYPE nx2mid = nx2min + (nx2max - nx2min) / 2;
                search_aux (nx1min, nx1max, ny1min, ny1max, nx2min, nx2mid, ny2min, ny2max, nx1best, ny1best, nx2best, ny2best, tid, NewL);
                search_aux (nx1min, nx1max, ny1min, ny1max, nx2mid, nx2max, ny2min, ny2max, nx1best, ny1best, nx2best, ny2best, tid, NewL);
              }
              else { // cut the y2-side in two
                UTYPE ny2mid = ny2min + (ny2max - ny2min) / 2;
                search_aux (nx1min, nx1max, ny1min, ny1max, nx2min, nx2max, ny2min, ny2mid, nx1best, ny1best, nx2best, ny2best, tid, NewL);
                search_aux (nx1min, nx1max, ny1min, ny1max, nx2min, nx2max, ny2mid, ny2max, nx1best, ny1best, nx2best, ny2best, tid, NewL);
              }
            }
          List_swap (L, NewL);
#ifdef STAT
          printf ("L: "); List_print (L);
#endif
          List_clear (NewL);
        }
    }
  List_clear (L);
}

#ifdef WORST
#include "complex2-worst.h"
#endif

static void
setround (int rnd)
{
  fesetround (rnd1[rnd]);
}

static void
doit (unsigned int seed)
{
  UTYPE nx1best = 0, ny1best = 0, nx2best = 0, ny2best = 0;
  double dbest = 0;
  int rbest = -1;

  /* set the random seed of the current thread */
  srand (seed);

  /* set the rounding mode of the current thread */
  setround (rnd);

  /* for thread 0, get the "worst" value so far as initial point */
  if (omp_get_thread_num () == 0)
    {
      dbest = Dbest;
      nx1best = get_utype (X1best);
      ny1best = get_utype (Y1best);
      nx2best = get_utype (X2best);
      ny2best = get_utype (Y2best);
    }

  search (get_utype (X1min), get_utype (X1max) + 1,
          get_utype (Y1min), get_utype (Y1max) + 1,
          get_utype (X2min), get_utype (X2max) + 1,
          get_utype (Y2min), get_utype (Y2max) + 1,
          &nx1best, &ny1best, &nx2best, &ny2best, &dbest, &rbest);
#pragma omp critical
  if (dbest > Dbest)
    {
      Dbest = dbest;
#ifdef RANK
      Rbest = rbest;
#endif
      mode_best = mode ();
      X1best = get_type (nx1best);
      Y1best = get_type (ny1best);
      X2best = get_type (nx2best);
      Y2best = get_type (ny2best);
    }
  mpfr_free_cache (); /* free cache of current thread */
}

static void
print_error (double d)
{
  mpfr_t x;
  mpfr_init2 (x, 53);
  mpfr_set_d (x, d, MPFR_RNDU);
  if (d < 0.999)
    mpfr_printf ("%.3RUf", x);
  else if (d < 9.99)
    mpfr_printf ("%.2RUf", x);
  else if (d < 99.9)
    mpfr_printf ("%.1RUf", x);
  else
    mpfr_printf ("%.2RUe", x);
  mpfr_clear (x);
}

static void
init_Threshold (void)
{
  UTYPE boundx1 = get_utype (X1max) - get_utype (X1min);
  UTYPE boundy1 = get_utype (Y1max) - get_utype (Y1min);
  UTYPE boundx2 = get_utype (X2max) - get_utype (X2min);
  UTYPE boundy2 = get_utype (Y2max) - get_utype (Y2min);
  double w = (double) boundx1 * (double) boundy1
    * (double) boundx2 * (double) boundy2;
  double s = 0;              /* number of evaluations so far */
  while (s < w)
    {
      w = w / 2.0; // cut in 2
      s += 2 * (double) threshold; // use threshold evaluations in each half
    }
  Threshold = w;
}

// return non-zero if (x1,y1) (x2,y2) is in the given range
static int
in_range (TYPE x1, TYPE y1, TYPE x2, TYPE y2)
{
  return get_utype (X1min) <= get_utype (x1)
    && get_utype (x1) <= get_utype (X1max)
    && get_utype (Y1min) <= get_utype (y1)
    && get_utype (y1) <= get_utype (Y1max)
    && get_utype (X2min) <= get_utype (x2)
    && get_utype (x2) <= get_utype (X2max)
    && get_utype (Y2min) <= get_utype (y2)
    && get_utype (y2) <= get_utype (Y2max);
}

int
main (int argc, char *argv[])
{
  unsigned int seed = 0;
  int nthreads = 0;
  double worst_input_x1 = 0, worst_input_y1 = 0;
  double worst_input_x2 = 0, worst_input_y2 = 0;

#ifdef GLIBC
  if (verbose)
    {
      printf("GNU libc version: %s\n", gnu_get_libc_version ());
      printf("GNU libc release: %s\n", gnu_get_libc_release ());
    }
#endif
#ifdef _MSC_VER
  printf ("Using Microsoft math library %d\n", _MSC_VER);
#endif

  X1min = 0;
  X1max = -TYPE_MAX;
  Y1min = 0;
  Y1max = -TYPE_MAX;
  X2min = 0;
  X2max = -TYPE_MAX;
  Y2min = 0;
  Y2max = -TYPE_MAX;

  while (argc >= 2 && argv[1][0] == '-')
    {
      if (strcmp (argv[1], "-v") == 0)
        {
          verbose ++;
          argc --;
          argv ++;
        }
      else if (argc >= 3 && strcmp (argv[1], "-threshold") == 0)
        {
          threshold = strtoul (argv[2], NULL, 10);
          argv += 2;
          argc -= 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-seed") == 0)
        {
#ifdef ARM_USE_HARDWARE_RNG
  printf("Warning: Unable to set the seed when hardware RNG is utilized.\n");
#endif
          seed = strtoul (argv[2], NULL, 10);
          argv += 2;
          argc -= 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-mode") == 0)
        {
          use_mode = strtoul (argv[2], NULL, 10);
          assert (0 <= use_mode && use_mode <= 2);
          argv += 2;
          argc -= 2;
        }
      else if (strcmp (argv[1], "-rndn") == 0)
        {
          rnd = 0;
          argc --;
          argv ++;
        }
      else if (strcmp (argv[1], "-rndz") == 0)
        {
          rnd = 1;
          argc --;
          argv ++;
        }
      else if (strcmp (argv[1], "-rndu") == 0)
        {
          rnd = 2;
          argc --;
          argv ++;
        }
      else if (strcmp (argv[1], "-rndd") == 0)
        {
          rnd = 3;
          argc --;
          argv ++;
        }
      else if (argc >= 3 && strcmp (argv[1], "-nthreads") == 0)
        {
          nthreads = atoi (argv[2]);
          argc -= 2;
          argv += 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-worst") == 0)
        {
          sscanf (argv[2], "%la,%la,%la,%la", &worst_input_x1, &worst_input_y1,
                  &worst_input_x2, &worst_input_y2);
          argc -= 2;
          argv += 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-x1min") == 0)
        {
          long double t;
          sscanf (argv[2], "%La", &t);
          X1min = t;
          argc -= 2;
          argv += 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-x1max") == 0)
        {
          long double t;
          sscanf (argv[2], "%La", &t);
          X1max = t;
          argc -= 2;
          argv += 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-y1min") == 0)
        {
          long double t;
          sscanf (argv[2], "%La", &t);
          Y1min = t;
          argc -= 2;
          argv += 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-y1max") == 0)
        {
          long double t;
          sscanf (argv[2], "%La", &t);
          Y1max = t;
          argc -= 2;
          argv += 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-x2min") == 0)
        {
          long double t;
          sscanf (argv[2], "%La", &t);
          X2min = t;
          argc -= 2;
          argv += 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-x2max") == 0)
        {
          long double t;
          sscanf (argv[2], "%La", &t);
          X2max = t;
          argc -= 2;
          argv += 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-y2min") == 0)
        {
          long double t;
          sscanf (argv[2], "%La", &t);
          Y2min = t;
          argc -= 2;
          argv += 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-y2max") == 0)
        {
          long double t;
          sscanf (argv[2], "%La", &t);
          Y2max = t;
          argc -= 2;
          argv += 2;
        }
      else
        {
          fprintf (stderr, "Unknown option %s\n", argv[1]);
          exit (1);
        }
    }
  assert (threshold > 0);
  /* divide threshold by LIST_ALLOC so that the total number of evaluations
     does not vary with LIST_ALLOC */
  threshold = 1 + (threshold - 1) / LIST_ALLOC;
  init_Threshold ();
#ifdef VECTORIZE
  cudaInit();
  int bunchSize = (threshold >int(Threshold+1)) ? threshold : int(Threshold+1);
  CMData ldata(bunchSize ,omp_get_max_threads());
  gdata = &ldata;
  what[0] = What::one;
  icurr[0]=0;
#endif

  if (seed == 0)
    seed = getpid ();
  if (verbose)
    printf ("Using seed %lu\n", (unsigned long) seed);
  assert (get_utype (X1min) < get_utype (X1max));
  assert (get_utype (Y1min) < get_utype (Y1max));
  assert (get_utype (X2min) < get_utype (X2max));
  assert (get_utype (Y2min) < get_utype (Y2max));
  if (verbose)
  {
    printf ("Using x1min=");
    print_type_hex (X1min);
    printf (" x1max=");
    print_type_hex (X1max);
    printf (" y1min=");
    print_type_hex (Y1min);
    printf (" y1max=");
    print_type_hex (Y1max);
    printf (" x2min=");
    print_type_hex (X2min);
    printf (" x2max=");
    print_type_hex (X2max);
    printf (" y2min=");
    print_type_hex (Y2min);
    printf (" y2max=");
    print_type_hex (Y2max);
    printf ("\n");
  }

#ifdef WORST
  TYPE x1, y1, x2, y2;
  double d, Dbest0;
  setround (rnd);
  /* first check the given -worst value, if any */
  if ((worst_input_x1 != 0 || worst_input_y1 != 0
       || worst_input_x2 != 0 || worst_input_y2 != 0)
      && in_range (worst_input_x1, worst_input_y1, worst_input_x2, worst_input_y2))
  {
    x1 = worst_input_x1;
    y1 = worst_input_y1;
    x2 = worst_input_x2;
    y2 = worst_input_y2;
    d = distance (x1, y1, x2, y2,0);
    if (d > Dbest)
        {
          Dbest = d;
          X1best = x1;
          Y1best = y1;
          X2best = x2;
          Y2best = y2;
        }
  }
  /* first check the 'worst' values for the given library, with the given
     rounding mode */
  for (int i = NUMBER; i < SIZE; i+=NLIBS)
    {
      x1 = worst[i][0];
      y1 = worst[i][1];
      x2 = worst[i][2];
      y2 = worst[i][3];
      if (in_range (x1, y1, x2, y2)) {
        d = distance (x1, y1, x2, y2, 0);
        if (d > Dbest)
        {
          Dbest = d;
          X1best = x1;
          Y1best = y1;
          X2best = x2;
          Y2best = y2;
        }
      }
      // also try with operands swapped
      if (in_range (x2, y2, x1, y1)) {
        d = distance (x2, y2, x1, y1, 0);
        if (d > Dbest)
        {
          Dbest = d;
          X1best = x2;
          Y1best = y2;
          X2best = x1;
          Y2best = y1;
        }
      }
    }
  Dbest0 = Dbest;

  /* then check the 'worst' values for the other libraries */
  for (int i = 0; i < SIZE; i++)
    {
      if ((i % NLIBS) == NUMBER)
        continue;
      x1 = worst[i][0];
      y1 = worst[i][1];
      x2 = worst[i][2];
      y2 = worst[i][3];
      if (in_range (x1, y1, x2, y2)) {
        d = distance (x1, y1, x2, y2, 0);
        if (d > Dbest)
        {
          Dbest = d;
          X1best = x1;
          Y1best = y1;
          X2best = x2;
          Y2best = y2;
        }
      }
      // also try with operands swapped
      if (in_range (x2, y2, x1, y1)) {
        d = distance (x2, y2, x1, y1,0);
        if (d > Dbest)
        {
          Dbest = d;
          X1best = x2;
          Y1best = y2;
          X2best = x1;
          Y2best = y1;
        }
      }
    }

  /* then check the 'extra' values if any */
  for (int i = 0; i < SIZE_EXTRA; i++)
    {
      x1 = extra[i][0];
      y1 = extra[i][1];
      x2 = extra[i][2];
      y2 = extra[i][3];
      if (x1 == 0 && y1 == 0 && x2 == 0 && y2 == 0)
        break; // end of extra[] field
      if (in_range (x1, y1, x2, y2)) {
        d = distance (x1, y1, x2, y2,0);
        if (d > Dbest)
        {
          Dbest = d;
          X1best = x1;
          Y1best = y1;
          X2best = x2;
          Y2best = y2;
        }
      }
      // also try with operands swapped
      if (in_range (x2, y2, x1, y1)) {
        d = distance (x2, y2, x1, y1,0);
        if (d > Dbest)
        {
          Dbest = d;
          X1best = x2;
          Y1best = y2;
          X2best = x1;
          Y2best = y1;
        }
      }
    }
#endif
  printf("initial worse at %a,%a %a,%a is %f %f %d\n",X1best,Y1best,X2best,Y2best,Dbest0,Dbest,0);
  /* Apparently Visual Studio does not properly set the number of threads. */
#pragma omp parallel
  if (nthreads <= 0)
    nthreads = omp_get_num_threads ();
  if (verbose)
    printf ("Using %d thread(s)\n", nthreads);
  omp_set_num_threads (nthreads);

  assert (nthreads <= MAX_THREADS);

  int n;
#pragma omp parallel for
  for (n = 0; n < nthreads; n++)
    {
      Seed[n] = seed + n;
      doit (Seed[n]);
    }
#ifdef WORST
  if (Dbest > Dbest0)
    printf ("NEW ");
#endif
  printf ("c%s %d %d ", NAME, mode_best, Rbest);
  print_type_hex (X1best);
  printf (",");
  print_type_hex (Y1best);
  printf (",");
  print_type_hex (X2best);
  printf (",");
  print_type_hex (Y2best);
  printf (" [");
  print_error (Dbest);
  setround (2); // upward
  printf ("] %.6g %.16g\n", Dbest, Dbest);
  /* reset to the current rounding mode */
  setround (rnd);
  if (verbose)
    {
      mpc_t xx1, yy1, xx2, yy2;
      CTYPE z1 = CONSTRUCT (X1best, Y1best);
      CTYPE z2 = CONSTRUCT (X2best, Y2best);
#ifdef VECTORIZE
      cmdata().xpH[0][0] = z1;
      cmdata().ypH[0][0] = z2;
      auto zp = wrap_foo(cmdata(),1,0);
      CTYPE z = *zp;
#else
      CTYPE z = FOO2 (z1, z2);
#endif
#ifdef __CUDACC__
      printf ("cuda gives (");
#elif defined __HIPCC__
      printf ("rocm gives (");
#else
      printf ("libm gives (");
#endif
#ifdef __cplusplus
      print_type_hex (z.real());
#else
      print_type_hex (creal (z));
#endif
      printf (",");
#ifdef __cplusplus
      print_type_hex (z.imag());
#else
      print_type_hex (cimag (z));
#endif
      printf (")\n");
      mpfr_set_emin (EMIN + 1);
      mpfr_set_emax (EMAX);
      mpc_init2 (xx1, PREC);
      mpc_init2 (yy1, PREC);
      mpc_init2 (xx2, PREC);
      mpc_init2 (yy2, PREC);
      mpfr_set_type (mpc_realref (xx1), X1best, MPFR_RNDN);
      mpfr_set_type (mpc_imagref (xx1), Y1best, MPFR_RNDN);
      mpfr_set_type (mpc_realref (xx2), X2best, MPFR_RNDN);
      mpfr_set_type (mpc_imagref (xx2), Y2best, MPFR_RNDN);
      int ret = MPC_FOO (yy1, xx1, xx2, crnd[rnd]);
      mpfr_subnormalize (mpc_realref (yy1), ret, rnd2[rnd]);
      mpfr_subnormalize (mpc_imagref (yy1), ret, rnd2[rnd]);
      TYPE y_re = mpfr_get_type (mpc_realref (yy1), MPFR_RNDN);
      TYPE y_im = mpfr_get_type (mpc_imagref (yy1), MPFR_RNDN);
      printf ("mpc gives (");
      print_type_hex (y_re);
      printf (",");
      print_type_hex (y_im);
      printf (")\n");
      fflush (stdout);
      mpc_clear (xx1);
      mpc_clear (yy1);
      mpc_clear (xx2);
      mpc_clear (yy2);
    }
  fflush (stdout);
  mpfr_free_cache ();
#ifdef STAT
  printf ("eval_heuristic=%lu eval_exhaustive=%lu\n",
          eval_heuristic, eval_exhaustive);
#endif

  return 0;
}
