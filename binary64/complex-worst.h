#if defined(USE_FLOAT) || defined(USE_DOUBLE) || defined(USE_LDOUBLE)
#define NLIBS 6
#else
#define NLIBS 2  /* only glibc and icx provide binary128 */
#endif

#define SIZE (18*NLIBS)
#define SIZE_EXTRA 4

#if defined(GLIBC)
#define NUMBER 0
#endif
#ifdef ICX
#define NUMBER 1
#endif
#ifdef NEWLIB
#define NUMBER 2
#endif
#ifdef __CUDACC__
#define NUMBER 3
#endif
#ifdef __HIPCC__
#ifdef LIBCPP
#define NUMBER 4
#else
#define NUMBER 5
#endif
#endif
#ifndef NUMBER
#error "NUMBER undefined"
#endif

#ifdef USE_FLOAT
  TYPE worst[SIZE][2] = {
    /* cacosf */
    {0x1.01ff76p+0,0x1.09982p-12},      /* GNU libc 7.22850 */
    {-0x1.417984p+2,0x1.5003bp-23},     /* icx      0.707107 */
    {0,0},                              /* newlib */
    {-0x1.6a09e8p+127,0x0p+0}, /*cuda inf*/
    {-0x1.6a09e8p+127,0x0p+0}, /*hip libc++ inf*/
    {0x1.23fa3ap+119,-0x1.6f719ap-1}, /*hip libstdc++ inf*/
    /* cacoshf */
    {0x1.01ff66p+0,0x1.17fcfap-12},     /* GNU libc 7.25492 */
    {-0x1.358252p+2,-0x1.42fb28p-23},   /* icx      0.707107 */
    {0,0},                              /* newlib */
    {-0x1.6a09e8p+127,0x0p+0}, /*cuda inf*/
    {-0x1.6a09e8p+127,0x0p+0}, /*hip libc++ inf*/
    {-0x1.6a09e8p+127,0x0p+0}, /*hip libstdc++ inf*/
    /* casinf */
    {0x1.32e904p-9,-0x1.01167ep-1},     /* GNU libc 5.39359 */
    {0x1.4d245ap-10,-0x1.05aad6p-10},   /* icx      0.707107 */
    {0,0},                              /* newlib */
    {-0x0p+0,-0x1.6a09e8p+127}, /*hip libc++ inf*/
    {0x0p+0,0x1.6a09e8p+127}, /*cuda inf*/
    {0x1.23fa3ap+119,-0x1.6f719ap-1}, /*hip libstdc++ inf*/
    /* casinhf */
    {-0x1.01167ep-1,0x1.32dc2ep-9},     /* GNU libc 5.40436 */
    {-0x1.05aad6p-10,0x1.4d245ap-10},   /* icx      0.707107 */
    {0,0},                              /* newlib */
    {-0x1.6a09e8p+127,0x0p+0}, /*cuda inf*/
    {-0x1.6a09e8p+127,0x0p+0}, /*hip libc++ inf*/
    {0x1.23fa3ap+119,-0x1.6f719ap-1}, /*hip libstdc++ inf*/
    /* catanf */
    {0x1.ddb41ep-12,-0x1.f512fcp-3},    /* GNU libc 5.65767 */
    {-0x1.0a402ep-11,-0x1.0a4032p-11},  /* icx      0.707107 */
    {0,0},                              /* newlib */
    {-0x1.6a09e8p+127,0x0p+0}, /*hip libc++ 2.86e+45*/
    {0x0p+0,0x1.6a09e8p+127}, /*cuda 2.86e+45*/
    {0x1.23fa3ap+119,-0x1.6f719ap-1}, /*hip libstdc++ inf*/
    /* catanhf */
    {-0x1.edac5cp-3,0x1.7e748ap-7},     /* GNU libc 5.62454 */
    {0x1.250bfep-12,-0x1.250bfep-12},   /* icx      0.707107 */
    {0,0},                              /* newlib */
    {-0x1.6a09e8p+127,0x0p+0}, /*hip libc++ 2.86e+45*/
    {0x0p+0,0x1.6a09e8p+127}, /*cuda 2.86e+45*/
    {0x1.23fa3ap+119,-0x1.6f719ap-1}, /*hip libstdc++ inf*/
    /* ccosf */
    {0x1.577ca8p+57,0x1.738d0cp-1},     /* GNU libc 2.88043 */
    {0x1.419e3cp+122,0x1.59bc2ap+1},    /* icx      0.707088 */
    {0,0},                              /* newlib */
    {-0x0p+0,-0x1.6a09e8p+127}, /*hip libc++ inf*/
    {-0x0p+0,-0x1.6a09e8p+127}, /*hip libstdc++ inf*/
    {0x0p+0,0x1.6a09e8p+127}, /*cuda inf*/
    /* ccoshf */
    {0x1.7c238ep-1,0x1.1050bap+8},      /* GNU libc 2.85889 */
    {-0x1.5df04ap+1,0x1.b502e2p+110},   /* icx      0.707083 */
    {0,0},                              /* newlib */
    {-0x1.6a09e8p+127,0x0p+0}, /*cuda inf*/
    {-0x1.6a09e8p+127,0x0p+0}, /*hip libc++ inf*/
    {-0x1.6a09e8p+127,0x0p+0}, /*hip libstdc++ inf*/
    /* cexpf */
    {-0x1.c83604p-20,-0x1.c460c6p+2},   /* GNU libc 1.91736 */
    {-0x1.8d21b6p-16,0x1.62f926p+2},    /* icx      0.707093 */
    {0,0},                              /* newlib */
    {0x1.62e43p+6,0x1.95a766p+116}, /*cuda 8.39e+06*/
    {0x1.62e43p+6,0x1.be9774p+23}, /*hip libc++ 8.39e+06*/
    {0x1.6a09e8p+127,-0x0p+0}, /*hip libstdc++ inf*/
    /* cexp10f */
    {0,0},                              /* GNU libc n/a */
    {-0x1.815dc2p-21,0x1.a9b4a2p+29},   /* icx      4.65949 */
    {0,0},                              /* newlib */
    {0,0},                              /* cuda */
    {0,0},                              /* hip libc++ */
    {0,0},                              /* hip libstdc++ */
    /* cexp2f */
    {0,0},                              /* GNU libc n/a */
    {-0x1.56705cp-11,0x1.f37f96p+2},    /* icx      0.707080 */
    {0,0},                              /* newlib */
    {0,0},                              /* cuda */
    {0,0},                              /* hip libc++ */
    {0,0},                              /* hip libstdc++ */
    /* clogf */
    {0x1.37acbap-1,-0x1.9e3952p-58},    /* GNU libc 3.11758 */
    {0x1.ca040cp-25,0x1.ad6ce8p+1},     /* icx      0.707109 */
    {0,0},                              /* newlib */
    {-0x1.ffdb6ep+127,0x1.83p+122}, /*cuda 4.47e+43*/
    {-0x1.ffdb6ep+127,0x1.83p+122}, /*hip libc++ 4.47e+43*/
    {-0x1.ffdb6ep+127,0x1.83p+122}, /*hip libstdc++ 4.47e+43*/
    /* clog10f */
    {0x1.23c4cep-1,0x1.c7827ep-25},     /* GNU libc 4.36988 */
    {-0x1.88474p-29,-0x1.9ce5f6p-3},    /* icx      0.707107 */
    {0,0},                              /* newlib */
    {-0x1.ffdb6ep+127,0x1.83p+122}, /*cuda 8.93e+43*/
    {-0x1.ffdb6ep+127,0x1.83p+122}, /*hip libc++ 8.93e+43*/
    {-0x1.ffdb6ep+127,0x1.83p+122}, /*hip libstdc++ 8.93e+43*/
    /* csinf */
    {0x1.23fa3ap+119,-0x1.6f719ap-1},   /* GNU libc 2.75954 */
    {0x1.4f7474p-11,0x1.4f7474p-11},    /* icx      0.707107 */
    {0,0},                              /* newlib */
    {-0x0p+0,-0x1.6a09e8p+127}, /*hip libc++ inf*/
    {-0x0p+0,-0x1.6a09e8p+127}, /*hip libstdc++ inf*/
    {0x0p+0,0x1.6a09e8p+127}, /*cuda inf*/
    /* csinhf */
    {-0x1.80100ap-1,-0x1.224128p+95},   /* GNU libc 2.83848 */
    {0x1.4f7474p-11,0x1.4f7474p-11},    /* icx      0.707107 */
    {0,0},                              /* newlib */
    {-0x1.6a09e8p+127,0x0p+0}, /*cuda inf*/
    {-0x1.6a09e8p+127,0x0p+0}, /*hip libc++ inf*/
    {-0x1.6a09e8p+127,0x0p+0}, /*hip libstdc++ inf*/
    /* csqrtf */
    {0x1.c62c82p+45,-0x1.fd734p+51},    /* GNU libc 1.81971 */
    {-0x1.ed26c8p-54,-0x1.037f94p+11},  /* icx      0.722890 */
    {0,0},                              /* newlib */
    {-0x1.6a09e8p+127,-0x0p+0}, /*hip libstdc++ 3.10e+26*/
    {-0x1.ffdb6ep+127,0x1.83p+122}, /*cuda 4.38e+26*/
    {-0x1.ffdb6ep+127,0x1.83p+122}, /*hip libc++ 4.38e+26*/
    /* ctanf */
    {0x1.1ad534p+78,-0x1.0b2e16p+3},    /* GNU libc 7.06114 */
    {0x1.9a9fe2p-11,-0x1.01b7d2p-11},   /* icx      0.707107 */
    {0,0},                              /* newlib */
    {-0x1.6a09e8p+127,0x0p+0}, /*cuda inf*/
    {-0x1.6a09e8p+127,0x0p+0}, /*hip libc++ inf*/
    {0x1.7c238ep-1,0x1.1050bap+8}, /*hip libstdc++ inf*/
    /* ctanhf */
    {-0x1.0b2e16p+3,-0x1.0c1526p-1},    /* GNU libc 7.06127 */
    {0x1.250bfep-12,-0x1.250bfep-12},   /* icx      0.707107 */
    {0,0},                              /* newlib */
    {-0x0p+0,-0x1.6a09e8p+127}, /*hip libc++ inf*/
    {0x0p+0,0x1.6a09e8p+127}, /*cuda inf*/
    {0x1.577ca8p+57,0x1.738d0cp-1}, /*hip libstdc++ inf*/
  };
TYPE extra[SIZE_EXTRA][2] = {
  /* do not remove this example: sqrt(-x,-0) should be (-sqrt(x),+0),
     see https://gcc.gnu.org/bugzilla/show_bug.cgi?id=117216 */
  {-0x1p127,-0.0},
  {-4.0,-0.0}, // do not remove (sqrtf)
  };
#endif

#ifdef USE_DOUBLE
  TYPE worst[SIZE][2] = {
    /* cacos */
    {0x1.fc70889a6fc2bp+1023,0x1.7468cc70a712fp+1021}, /* GNU libc 1.46819 */
    {-0x1.baf961c959d41p-14,-0x1.5f8545d4c9c67p+0},    /* icx      0.707910 */
    {0,0},                                             /* newlib */
    {0x1.fc70889a6fc2bp+1023,0x1.7468cc70a712fp+1021}, /*cuda inf*/
    {0x1.fc70889a6fc2bp+1023,0x1.7468cc70a712fp+1021}, /*hip libc++ inf*/
    {0x1.fc70889a6fc2bp+1023,0x1.7468cc70a712fp+1021}, /*hip libstdc++ inf*/
    /* cacosh */
    {-0x1.b48dcc8757187p+1023,-0x1.1b22ac6962dd2p+1023},/* GNU libc 1.46820 */
    {-0x1.baf961c959d41p-14,-0x1.5f8545d4c9c67p+0},    /* icx      0.707910 */
    {0,0},                                             /* newlib */
    {0x1.fc70889a6fc2bp+1023,0x1.7468cc70a712fp+1021}, /*cuda inf*/
    {0x1.fc70889a6fc2bp+1023,0x1.7468cc70a712fp+1021}, /*hip libc++ inf*/
    {0x1.fc70889a6fc2bp+1023,0x1.7468cc70a712fp+1021}, /*hip libstdc++ inf*/
    /* casin */
    {0x1.cee4c5cb332cbp-20,-0x1.064032aadc6bbp-1},     /* GNU libc 5.37621 */
    {0x1.24f1950d2e001p-26,-0x1.24e1cc00012a1p-26},    /* icx      0.708077 */
    {0,0},                                             /* newlib */
    {0x1.fc70889a6fc2bp+1023,0x1.7468cc70a712fp+1021}, /*cuda inf*/
    {0x1.fc70889a6fc2bp+1023,0x1.7468cc70a712fp+1021}, /*hip libc++ inf*/
    {0x1.fc70889a6fc2bp+1023,0x1.7468cc70a712fp+1021}, /*hip libstdc++ inf*/
    /* casinh */
    {-0x1.00208b845a9adp-1,0x1.b93a0a0c236ap-10},      /* GNU libc 5.39905 */
    {-0x1.b2407ac7a19eap-32,0x1.c384a6a7f266dp+0},     /* icx      0.707733 */
    {0x0p+0,0x1.0000002d413cdp+26},                    /* newlib Inf */
    {0x1.fc70889a6fc2bp+1023,0x1.7468cc70a712fp+1021}, /*cuda inf*/
    {0x1.fc70889a6fc2bp+1023,0x1.7468cc70a712fp+1021}, /*hip libc++ inf*/
    {0x1.fc70889a6fc2bp+1023,0x1.7468cc70a712fp+1021}, /*hip libstdc++ inf*/
    /* catan */
    {0x1.bb415588c684ep-27,-0x1.f40fc1f113316p-3},     /* GNU libc 5.67991 */
    {0x1.47811ed9a687fp-7,-0x1.123ef9a16623fp-7},      /* icx      0.723924 */
    {0,0},                                             /* newlib */
    {0x1.fc70889a6fc2bp+1023,0x1.7468cc70a712fp+1021}, /*cuda inf*/
    {0x1.fc70889a6fc2bp+1023,0x1.7468cc70a712fp+1021}, /*hip libc++ inf*/
    {0x1.fc70889a6fc2bp+1023,0x1.7468cc70a712fp+1021}, /*hip libstdc++ inf*/
    /* catanh */
    {-0x1.fa7f4508dbef8p-4,-0x1.6a82caaa3005p-27},     /* GNU libc 5.67875 */
    {0x1.169efc6fd9f52p-7,0x1.79168def2f4e9p-7},       /* icx      0.723721 */
    {0,0},                                             /* newlib */
    {0x1.fc70889a6fc2bp+1023,0x1.7468cc70a712fp+1021}, /*cuda inf*/
    {0x1.fc70889a6fc2bp+1023,0x1.7468cc70a712fp+1021}, /*hip libc++ inf*/
    {0x1.fc70889a6fc2bp+1023,0x1.7468cc70a712fp+1021}, /*hip libstdc++ inf*/
    /* ccos */
    {0x1.481d607d542adp+94,-0x1.7ab11d26169b2p-1},     /* GNU libc 2.82107 */
    {-0x1.33910dd4d1a6p+275,0x1.5ba09a6992372p+0},     /* icx      0.720455 */
    {0,0},                                             /* newlib */
    {0x0p+0,0x1.0000002d413cdp+26}, /*cuda inf*/
    {0x0p+0,0x1.0000002d413cdp+26}, /*hip libc++ inf*/
    {0x0p+0,0x1.0000002d413cdp+26}, /*hip libstdc++ inf*/
    /* ccosh */
    {0x1.74fb1f3da65dfp-1,-0x1.07d32799a4ff6p+346},    /* GNU libc 2.79451 */
    {-0x1.5b368eb3b3d56p+0,0x1.37410c5e93febp+16},     /* icx      0.721476 */
    {0,0},                                             /* newlib */
    {-0x1.0000002d413cdp+26,0x0p+0}, /*cuda inf*/
    {-0x1.0000002d413cdp+26,0x0p+0}, /*hip libc++ inf*/
    {-0x1.0000002d413cdp+26,0x0p+0}, /*hip libstdc++ inf*/
    /* cexp */
    {-0x1.073d3e31b34cfp-26,-0x1.695e6803ff82cp+6},    /* GNU libc 1.89430 */
    {-0x1.a573dc283a42ep-8,-0x1.5c56ad805ae1ep+2},     /* icx 0.750486 */
    {0,0},                                             /* newlib */
    {-0x1.a1afb66a0477ep-42,0x1.f6864b1baffc7p+22}, /*cuda 2.28*/
    {-0x1.a8600c347750fp-43,0x1.ec33cc6cff2aep+16}, /*hip libc++ 2.20*/
    {0x1.0000002d413cdp+26,-0x0p+0}, /*hip libstdc++ inf*/
    /* cexp10 */
    {0,0},                                             /* GNU libc n/a */
    {-0x1.8dd58ed1692afp-74,0x1.93992e5e34b55p+29},    /* icx 4.60880e+09 */
    {0,0},                                             /* newlib */
    {0,0},                              /* cuda */
    {0,0},                              /* hip libc++ */
    {0,0},                              /* hip libstdc++ */
    /* cexp2 */
    {0,0},                                             /* GNU libc n/a */
    {-0x1.3212ac592a465p-9,-0x1.b14eea1818d19p+192},   /* icx      0.713847 */
    {0,0},                                             /* newlib */
    {0,0},                              /* cuda */
    {0,0},                              /* hip libc++ */
    {0,0},                              /* hip libstdc++ */
    /* clog */
    {0x1.369bdb4bf6e6bp-1,-0x1.968e9dc2d27p-676},      /* GNU libc 3.18401 */
    {-0x1.ff92be768bd5p-4,-0x1.89b21425ce4a7p-10},     /* icx 0.708304 */
    {0,0},                                             /* newlib */
    {0x1.fc70889a6fc2bp+1023,0x1.7468cc70a712fp+1021}, /*cuda inf*/
    {0x1.fc70889a6fc2bp+1023,0x1.7468cc70a712fp+1021}, /*hip libc++ inf*/
    {0x1.fc70889a6fc2bp+1023,0x1.7468cc70a712fp+1021}, /*hip libstdc++ inf*/
    /* clog10 */
    {0x1.22ab2c428a005p-1,-0x1.6963e3c580cbdp-286},    /* GNU libc 4.35452 */
    {-0x1.00048893442adp+4,0x1.fffc94333969ep-50},     /* icx      0.725725 */
    {0,0},                                             /* newlib */
    {0x1.fc70889a6fc2bp+1023,0x1.7468cc70a712fp+1021}, /*cuda inf*/
    {0x1.fc70889a6fc2bp+1023,0x1.7468cc70a712fp+1021}, /*hip libc++ inf*/
    {0x1.fc70889a6fc2bp+1023,0x1.7468cc70a712fp+1021}, /*hip libstdc++ inf*/
    /* csin */
    {-0x1.87fbb1cbcab1ep+705,-0x1.7eb488330b5dap-1},   /* GNU libc 2.83326 */
    {0x1.7445e52630819p+22,0x1.5fc99a01682d2p+1},      /* icx      0.721537 */
    {0,0},                                             /* newlib */
    {0x0p+0,0x1.0000002d413cdp+26}, /*cuda inf*/
    {0x0p+0,0x1.0000002d413cdp+26}, /*hip libc++ inf*/
    {0x0p+0,0x1.0000002d413cdp+26}, /*hip libstdc++ inf*/
    /* csinh */
    {-0x1.6fd84365b12c2p-1,0x1.17ba275c520d5p+86},     /* GNU libc 2.81873 */
    {0x1.d17ecce5f9cacp+5,-0x1.2b888e795d7dfp+643},    /* icx      0.720909 */
    {0,0},                                             /* newlib */
    {-0x1.0000002d413cdp+26,0x0p+0}, /*cuda inf*/
    {-0x1.0000002d413cdp+26,0x0p+0}, /*hip libc++ inf*/
    {-0x1.0000002d413cdp+26,0x0p+0}, /*hip libstdc++ inf*/
    /* csqrt */
    {0x1.889fbc6f8186p-848,0x1.f509b3dec4a7bp-843},    /* GNU libc 1.80886 */
    {-0x1.004p+1020,0x1.03f8p+961},                    /* icx Inf */
    {0,0},                                             /* newlib */
    {-0x1.0040000002bfdp+1020,0x1.feff3f8fa3a22p+1023}, /*hip libc++ 1.71e+170*/
    {-0x1.0040000002bfdp+1020,0x1.feff3f8fa3a22p+1023}, /*hip libstdc++ 1.21e+170*/
    {-0x1.0040000008424p+1020,-0x1.feff3f8fa397p+1023}, /*cuda 1.71e+170*/
    /* ctan */
    {0x1.f6019e40cc06bp-18,-0x1.38aa3ba3e97fap+2},     /* GNU libc 6.35433 */
    {0x1.6a2dae7e8b67dp+29,0x1.a46a81ff87536p-2},      /* icx      0.729059 */
    {0,0},                                             /* newlib */
    {0x1.fc70889a6fc2bp+1023,0x1.7468cc70a712fp+1021}, /*cuda inf*/
    {0x1.fc70889a6fc2bp+1023,0x1.7468cc70a712fp+1021}, /*hip libc++ inf*/
    {0x1.fc70889a6fc2bp+1023,0x1.7468cc70a712fp+1021}, /*hip libstdc++ inf*/
    /* ctanh */
    {0x1.64a2fb5b68aaep+2,-0x1.350c6886ce8c6p+194},    /* GNU libc 6.66385 */
    {0x1.40f1aa703532bp-2,-0x1.f094ba7ffcc71p+1},      /* icx      0.727831 */
    {0,0},                                             /* newlib */
    {-0x1.b48dcc8757187p+1023,-0x1.1b22ac6962dd2p+1023}, /*cuda inf*/
    {-0x1.b48dcc8757187p+1023,-0x1.1b22ac6962dd2p+1023}, /*hip libc++ inf*/
    {0x1.fc70889a6fc2bp+1023,0x1.7468cc70a712fp+1021}, /*hip libstdc++ inf*/
  };
TYPE extra[SIZE_EXTRA][2] = {
  /* do not remove the following input: its norm is near DBL_MAX,
     and it might yield intermediate overflow issues */
  {-0x1.0040000008424p+1020,-0x1.feff3f8fa397p+1023},
  /* do not remove this example: sqrt(-x,-0) should be (-sqrt(x),+0),
     see https://gcc.gnu.org/bugzilla/show_bug.cgi?id=117216 */
  {-0x1p1023,-0.0},
  };
#endif

#ifdef USE_LDOUBLE
  TYPE worst[SIZE][2] = {
    /* cexp */
    {0,0}, /* GNU libc */
    {0,0}, /* icx */
    /* clog */
    {0,0}, /* GNU libc */
    {0,0}, /* icx */
  };
TYPE extra[SIZE_EXTRA][2] = {
  };
#endif

#ifdef USE_FLOAT128
  TYPE worst[SIZE][2] = {
    /* cexp */
    {0,0}, /* GNU libc */
    {0,0}, /* icx */
    /* clog */
    {0,0}, /* GNU libc */
    {0,0}, /* icx */
  };
TYPE extra[SIZE_EXTRA][2] = {
  };
#endif
