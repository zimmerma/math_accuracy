/* Checks correct rounding of 64-bit division.

   This program is open-source software distributed under the terms 
   of the GNU General Public License <http://www.fsf.org/copyleft/gpl.html>.

   Usage:

   $ gcc -O3 check_div.c -o check_div -lm -lmpfr
   $ ./check_div [-seed nnn] [-rndz] [-rndu] [-rndd] [-subnormal] [-corner]

   The algorithm used with -corner is the following:
   * start from a random binary64 number x, 1/2 <= x < 1
   * multiply x by some power of 2 to get an integer, say
     2^104 <= 2^e*x < 2^105 or 2^105 <= 2^e*x < 2^106
   * let X = 2^e*x+/-k where k is a small odd integer
   * factor X into prime factors
   * for any divisor Y of X having exactly 53 bits, take Z = X/Y,
     if Z has exactly 54 bits, then x/y is a worst case, where y is Y scaled
     by any power of 2
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include <mpfr.h>
#include <fenv.h>
#include <assert.h>

mpfr_rnd_t rnd = MPFR_RNDN;

int verbose = 0;

/* With subnormal=1, check output in subnormal range. This is tricky due to
   double-rounding: https://hal-ens-lyon.archives-ouvertes.fr/ensl-00875366 */
int subnormal = 0;

/* With corner=1, check x/y near midpoints (RNDN) or 53-bit numbers (other
 rounding) */
int corner = 0;

static void
check (double x, double y)
{
  double z, t;
  mpfr_t X, Y, Z;

  if (verbose) { printf ("checking x=%la y=%la\n", x, y); fflush (stdout); }

  mpfr_init2 (X, 53);
  mpfr_init2 (Y, 53);
  mpfr_init2 (Z, 53);

  mpfr_set_d (X, x, MPFR_RNDN);
  mpfr_set_d (Y, y, MPFR_RNDN);
  int ret = mpfr_div (Z, X, Y, rnd);
  mpfr_subnormalize (Z, ret, rnd);
  z = mpfr_get_d (Z, MPFR_RNDN);
  t = x / y;
  if (t != z)
  {
    printf ("div and mpfr_div differ for x=%la,y=%la and rnd=%s\n", x, y,
            mpfr_print_rnd_mode (rnd));
    printf ("     div gives %la\n", t);
    printf ("mpfr_div gives %la\n", z);
    abort ();
  }

  mpfr_clear (X);
  mpfr_clear (Y);
  mpfr_clear (Z);
}

static void
checkall (double x, double y)
{
  mpfr_rnd_t rnd_old = rnd;
  int fenv_old = fegetround ();
  rnd = MPFR_RNDN;
  fesetround (FE_TONEAREST);
  check (x, y);
  rnd = MPFR_RNDZ;
  fesetround (FE_TOWARDZERO);
  check (x, y);
  rnd = MPFR_RNDU;
  fesetround (FE_UPWARD);
  check (x, y);
  rnd = MPFR_RNDD;
  fesetround (FE_DOWNWARD);
  check (x, y);
  rnd = rnd_old;
  fesetround (fenv_old);
}

#define MAXN 15
/* PMAX=50000 is close to optimal in terms of (x,y) pairs found per second
   with the naive integer factorization we use */
#define PMAX 50000

/* multiply y by all factor[i]^j for 0 <= i < nfactors,
   0 <= j <= exponent[i] */
static void
check_corner_aux2 (double x, __int128_t *factor, int *exponent, int nfactors,
                    __int128_t y, __int128_t N)
{
  if (nfactors == 0)
  {
    if (0x10000000000000 <= y && y < 0x20000000000000) /* y has 53 bits */
    {
      __int128_t z = N / y;
      if (!subnormal)
        {
          if ((rnd != MPFR_RNDN && 0x10000000000000 <= z && x < 0x20000000000000)
              || (rnd == MPFR_RNDN && 0x20000000000000 <= z && z < 0x40000000000000))
            check (x, ldexp (y, -52));
        }
      else /* subnormal case */
        {
          int nbits_z = 64 - __builtin_clzl (z);
          int k = (rnd == MPFR_RNDN);
          if (nbits_z <= 52 + k)
            {
              /* 1 <= x < 2 and 2^52 <= y < 2^53 */
              double yy = ldexp (y, 500);
              double zz = x / yy;
              int e;
              frexp (zz, &e);
              /* 2^(e-1) <= zz < 2^e and we want
                 2^(-1074-k+nbits_z-1) <= x/y < 2^(-1074-k+nbits_z) */
              int sh = -1074 - k + nbits_z - e;
              check (ldexp (x, sh), yy);
            }
        }
    }
    return;
  }
  for (int j = 0; j <= exponent[nfactors-1]; j++)
  {
    check_corner_aux2 (x, factor, exponent, nfactors-1, y, N);
    y *= factor[nfactors-1];
  }
}

unsigned long *Primes = NULL, nprimes = 0;

/* generates all odd primes < B */
static void
genPrimes (unsigned long B)
{
  Primes = malloc (B * sizeof (unsigned long));
  for (unsigned long p = 3; p < B; p++)
    {
      int ok = 1;
      for (int i = 0; i < nprimes && ok != 0; i++)
        {
          unsigned long q = Primes[i];
          if (p < q * q)
            break;
          if ((p % q) == 0)
            ok = 0;
        }
      if (ok)
        Primes[nprimes++] = p;
    }
  Primes = realloc (Primes, nprimes * sizeof (unsigned long));
}

#if 0
static void
print_int128 (__int128_t X)
{
  printf ("%lu %lu\n", (uint64_t) (X >> 64),
          (uint64_t) (X & (__int128_t) 0xffffffffffffffff));
}
#endif

static void
check_corner_aux (double x, int e, int s)
{
  assert (1 <= x && x < 2);
  __int128_t X = ldexp (x, 52);
  /* now 2^52 <= x < 2^53 */
  X = X << e;
  X += (__int128_t) s;
  /* we should have s odd so that X has only odd factors */
  __int128_t factor[MAXN], p, N = X;
  int exponent[MAXN], nfactors = 0;
  for (int i = 0; i < nprimes; i++)
  {
    p = Primes[i];
    if ((X % p) == 0)
    {
      factor[nfactors] = p;
      exponent[nfactors] = 1;
      X = X / p;
      while ((X % p) == 0)
      {
        exponent[nfactors] ++;
        X = X / p;
      }
      nfactors ++;
    }
    if (X < p * p) /* necessarily X is prime */
    {
      factor[nfactors] = X;
      exponent[nfactors] = 1;
      nfactors ++;
      X = 1;
      break;
    }
  }
  check_corner_aux2 (x, factor, exponent, nfactors, 1, N);

  if (X != 1 && X < 0x20000000000000)
    check_corner_aux2 (x, factor, exponent, nfactors, X, N);
}

static void
check_corner (double x)
{
  assert (0 <= x && x < 1);
  x = 1.0 + x;
  /* now 1 <= x < 2 */
  int e = 52 + (rnd == MPFR_RNDN);
  /* after scaling x to an integer, 2^52 <= x < 2^53, we multiply it by 2^e:
     for rounding to nearest we want 2^52 <= y < 2^53 and 2^53 <= z < 2^54
     thus 2^105 <= y*z < 2^107, thus the scaled x is multiplied by 2^53 or
     2^54.
     For directed rounding we want 2^52 <= y,z < 2^53 thus 2^104 <= y*z < 2^106
     and the scaled x is multiplied by 2^52 or 2^53.
     For z subnormal, we want 2^52 <= y < 2^53 and 2 <= z < 2^53 thus
     2^53 <= y*z < 2^105 thus the scaled x is multiplied by 2^1 to 2^52. */
    if (!subnormal)
      {
        check_corner_aux (x, e, 1);
        check_corner_aux (x, e, -1);
        check_corner_aux (x, e + 1, 1);
        check_corner_aux (x, e + 1, -1);
      }
    else /* subnormal case */
      for (int e = 1; e <= 52; e++)
        {
          check_corner_aux (x, e, 1);
          check_corner_aux (x, e, -1);
        }
}

int
main (int argc, char *argv[])
{
  long seed = 0;

  while (argc > 1 && argv[1][0] == '-')
  {
    if (argc > 2 && strcmp (argv[1], "-seed") == 0)
    {
      seed = atoi (argv[2]);
      argv += 2;
      argc -= 2;
    }
    else if (strcmp (argv[1], "-subnormal") == 0)
    {
      subnormal = 1;
      mpfr_set_emin (-1073);
      argv ++;
      argc --;
    }
    else if (strcmp (argv[1], "-corner") == 0)
    {
      corner = 1;
      genPrimes (PMAX);
      argv ++;
      argc --;
    }
    else if (strcmp (argv[1], "-verbose") == 0)
    {
      verbose = 1;
      argv ++;
      argc --;
    }
    else if (strcmp (argv[1], "-rndz") == 0)
    {
      rnd = MPFR_RNDZ;
      fesetround (FE_TOWARDZERO);
      argv ++;
      argc --;
    }
    else if (strcmp (argv[1], "-rndu") == 0)
    {
      rnd = MPFR_RNDU;
      fesetround (FE_UPWARD);
      argv ++;
      argc --;
    }
    else if (strcmp (argv[1], "-rndd") == 0)
    {
      rnd = MPFR_RNDD;
      fesetround (FE_DOWNWARD);
      argv ++;
      argc --;
    }
    else
    {
      printf ("Unknown option %s\n", argv[1]);
      exit (1);
    }
  }

  srand48 (seed);

  /* bug in CUDA 11.4: for rounding to nearest, gives 0x1.e36ec6f842663p-1
     instead of 0x1.e36ec6f842664p-1 */
  checkall (0x1.e2fd41b721c5p+0, 0x1.ff87c57114b09p+0);
  /* bug in CUDA 11.4: for rounding to nearest, gives 0x1.f008530726412p-1
     instead of 0x1.f008530726413p-1 */
  checkall (0x1.ef47f6673bfcp+0, 0x1.ff39722bdec53p+0);

  while (1)
  {
    double x, y;
    x = drand48 ();
    /* x and y are uniform over [0,1), thus for x,y in [1/2,1),
       we have x/y in (1/2,2) */
    if (corner)
      check_corner (x);
    else
    {
      y = drand48 ();
      if (subnormal)
      {
        x = x * 0x1p-512;
        y = y * 0x1p+511;
        /* for x,y originally in [1/2,1), we now have x/y in (2^-1024,2^-1022) */
      }
      check (x, y);
    }
  }
}
