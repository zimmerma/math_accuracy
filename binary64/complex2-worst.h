#if defined(USE_FLOAT) || defined(USE_DOUBLE) || defined(USE_LDOUBLE)
#define NLIBS 6 // glibc, icx, clang, cuda, hip libc++, hip libstdc++
#else
#define NLIBS 3  /* only glibc and icx provide binary128 */
#endif

#define SIZE (4*NLIBS)  /* 4 functions (cadd, cmul, cdiv, cpow) */
#define SIZE_EXTRA 7

#if defined(GLIBC)
#define NUMBER 0
#endif
#ifdef ICX
#define NUMBER 1
#endif
#ifdef CLANG
#define NUMBER 2
#endif
#ifdef __CUDACC__
#define NUMBER 3
#endif
#ifdef __HIPCC__
#ifdef LIBCPP
#define NUMBER 4
#else
#define NUMBER 5
#endif
#endif
#ifndef NUMBER
#error "NUMBER undefined"
#endif

#ifdef USE_FLOAT
  TYPE worst[SIZE][4] = {
    /* caddf */
    {-0x1p+74,0x1.5ecce4p+97,0x1.2ccad2p+98,0x1.4a5346p+97}, /* gcc 0.707107 */
    {-0x1p+74,0x1.5ecce4p+97,0x1.2ccad2p+98,0x1.4a5346p+97}, /* icx 0.707107 */
    {-0x1p+74,0x1.5ecce4p+97,0x1.2ccad2p+98,0x1.4a5346p+97}, /* clang 0.707107 */
    {-0x1p+74,0x1.5ecce4p+97,0x1.2ccad2p+98,0x1.4a5346p+97}, /* cuda 0.707107 */
    {-0x1p+74,0x1.5ecce4p+97,0x1.2ccad2p+98,0x1.4a5346p+97}, /* hip libc++ 0.707107 */
    {-0x1p+74,0x1.5ecce4p+97,0x1.2ccad2p+98,0x1.4a5346p+97}, /* hip libstdc++ 0.707107 */
    /* cmulf */
#if !defined(IGNORE_NAN) && !defined(IGNORE_INF)
    {0x1.6559aap-119,0x1.6559acp-119,-0x1.6ec9ep-32,-0x1.6ec9e2p-32}, /* gcc 1.41422 */
    {-0x1.80a55ep+0,-0x1.1bfep+80,0x1.ap+64,-0x1.54c296p+127}, /* icx Inf */
    {-0x1.80a55ep+0,-0x1.1bfep+80,0x1.ap+64,-0x1.54c296p+127}, /* clang Inf */
    {0,0},                              /* cuda */
    {0,0},                              /* hip libc++ */
    {0,0},                              /* hip libstdc++ */
#else
    {0x1.67ada2p-105,-0x1.67ada4p-105,-0x1.6c6a2p-46,0x1.6c6a22p-46},  /* gcc 1.41422 */
    {-0x1.5b9354p-17,-0x1.91b2fcp-17,0x1.8p-109,0x1.2eb6cep-125},      /* icx 1.76776 */
    {0x1.6912cap-41,-0x1.69020ap-41,0x1.d16ed4p-85,0x1.6b497ep-110},   /* clang 1.76727 */
    {0x1.6912cap-41,-0x1.69020ap-41,0x1.d16ed4p-85,0x1.6b497ep-110},   /* cuda 1.76727 */
    {0x1.67ada2p-105,-0x1.67ada4p-105,-0x1.6c6a2p-46,0x1.6c6a22p-46},  /* hip libc++ 1.41422 */
    {0x1.67ada2p-105,-0x1.67ada4p-105,-0x1.6c6a2p-46,0x1.6c6a22p-46},  /* hip libstdc++ 1.41422 */
#endif
    /* cdivf */
    {0x1.a0f77p-86,0x1.314772p-83,-0x1.0bdfecp+4,-0x1.dc888p+61}, /* gcc 0.707107 */
    {0x1.a0f77p-86,0x1.314772p-83,-0x1.0bdfecp+4,-0x1.dc888p+61}, /* icx 0.707107 */
    {0x1.a0f77p-86,0x1.314772p-83,-0x1.0bdfecp+4,-0x1.dc888p+61}, /* clang 0.707107 */
    {-0x0p+0,-0x1p-149,-0x1.d52016p-97,-0x1.28e43ep-97},          /* cuda 1.55e+07 */
    {-0x1p-149,0x1p-147,-0x1.d9b8cp-23,0x1.d6bc1ep-24},           /* hip libc++ 5.01e+06 */
    {-0x0p+0,-0x1p-149,-0x1.f2d196p-108,-0x1.c0baeep-108},        /* hip libstdc++ 1.29e+07 */
    /* cpowf */
#ifndef IGNORE_NAN
    {0x0p+0,-0x1p-149,-0x0p+0,0x1.838p+121},                   /* glibc Inf */
    {-0x1.80a55ep+0,-0x1.1bfep+80,0x1.ap+64,-0x1.54c296p+127}, /* icx Inf */
    {0x0p+0,-0x1p-149,-0x0p+0,0x1.838p+121},                   /* clang Inf */
    {0,0},                              /* cuda */
    {0,0},                              /* hip libc++ */
    {0,0},                              /* hip libstdc++ */
#else
    {0x1.6a3e4cp-127,0x1.b3f748p-127,0x1.b0aaf4p-13,0x1.f9038p+3}, /* glibc 4041.09 */
    {-0x1.a2ac18p-20,0x1.c834aep-92,0x1.fp+2,0x1.651f88p-127},       /* icx 0.707107 */
    {0x1.6a3e4cp-127,0x1.b3f748p-127,0x1.b0aaf4p-13,0x1.f9038p+3}, /* clang 4041.09 */
    {-0x1.74226p+4,0x1.65450ap+125,0x1.4a51eap-15,0x1.fc74ap+3},   /* cuda 3.11e+03  */
    {0x1.9a58f2p+117,0x1.303cbep+56,0x1.420282p-83,0x1.fe34b2p+3}, /* hip libc++  3.45e+03 */
    {-0x1.36e5d2p+121,0x1.527dd6p+46,0x1.3ee1a6p-16,0x1.fc6be8p+3}, /* hip libstdc++ 4.42e+03 */
#endif
  };
TYPE extra[SIZE_EXTRA][4] = {
  /* don't remove the following entry, it yields an error of 2.08597e+06
     for cmulf (gcc without -mfma, icx and clang) when IGNORE_INF is not
     defined */
  {-0x1.f9a182p+6,-0x1.fb7ea6p+5,-0x1.038p+121,-0x1.01fe26p+120},
  /* don't remove the following entry, it yields an error of 3.35545e+07
     for cpowf with gcc when the imaginary part of the exponent is not limited */
  {0x1.8422b8p+103,-0x1.4a5794p-31,-0x1.b2a91cp-110,-0x1.a25f44p+70},
  /* don't remove the following entry, it yields an error of 3.35545e+07
     for cpowf with icx when the imaginary part of the exponent is not limited */
  {0x1.6dfff4p+81,0x1.2a01bp-90,-0x1.704858p-66,0x1.386c1ep+72},
  /* don't remove the following entry: it is the example from Corollary 4 of
     "Error Bounds on Complex Floating-Point Multiplication" by
     Richard P. Brent, Colin Percival and Paul Zimmermann,
     Mathematics of Computation, 2007 */
  {0x1.8p-1,0x1.7ffffap-1,0x1.555564p-1,0x1.55555cp-1},
  /* don't remove the following entry: it is the example from Theorem 4.1 of
     "Error bounds on complex floating-point multiplication with an FMA" by
     Claude-Pierre Jeannerod, Peter Kornerup, Nicolas Louvet and Jean-Michel
     Muller, Mathematics of Computation, 2017 */
  {0x1p+11,0x1.001002p+23,0x1p+11,0x1.001002p+23},
  /* don't remove the following entry: it is the example from Theorem 4.2 of
     "Error bounds on complex floating-point multiplication with an FMA" by
     Claude-Pierre Jeannerod, Peter Kornerup, Nicolas Louvet and Jean-Michel
     Muller, Mathematics of Computation, 2017 */
  {0x1.fffffep+10,0x1.001002p+23,0x1.fffffep+10,0x1.001002p+23},
  /* don't remove the following entry: it is a test for cpowf */
  {-4.0f,-0.0f,0.5f,0.0f},
  };
#endif

#ifdef USE_DOUBLE
  TYPE worst[SIZE][4] = {
    /* cadd */
    {-0x1.4p+0,0x1.7ff1eee101ffap+0,-0x1.400000001ffe1p+0,0x1.001c223dfc00ep-1}, /* gcc 0.707107 */
    {0x1.a417000bff028p+767,-0x1.1fff0003ffcp+768,0x1.2df47ffa007edp+768,-0x1.100ffffff001fp+768}, /* icx 0.707107 */
    {0x1.9000000000001p+768,0x1.e01000400006p+767,-0x1.cfebff8p+769,0x1.0ff7ffdffffd1p+768}, /* clang 0.707107 */
    {-0x1.4p+0,0x1.7ff1eee101ffap+0,-0x1.400000001ffe1p+0,0x1.001c223dfc00ep-1}, /* cuda 0.707107 */
    {-0x1.4p+0,0x1.7ff1eee101ffap+0,-0x1.400000001ffe1p+0,0x1.001c223dfc00ep-1}, /* hip libc++ 0.707107 */
    {-0x1.4p+0,0x1.7ff1eee101ffap+0,-0x1.400000001ffe1p+0,0x1.001c223dfc00ep-1}, /* hip libstdc++ 0.707107 */
    /* cmul */
#if !defined(IGNORE_NAN) && !defined(IGNORE_INF)
    {-0x1.8ce95a9220b2cp-690,-0x1.e881aa8c77036p-694,-0x1.0c4fc9fd62002p-382,0x1.70edb5bc66c03p-379}, /* gcc 1.41422 */
    {-0x1.4747322505658p+512,0x1.6f6401002ffcp+266,0x1.8c080080f06ffp+832,0x1.907dbec4fbb01p+511}, /* icx Inf */
    {0x1.9000000000fffp+768,0x1.8fc804118862ep+776,-0x1.47dbf79ee5831p+247,0x1.af000000010fep+272}, /* clang Inf */
    {0,0},                              /* cuda */
    {0,0},                              /* hip libc++ */
    {0,0},                              /* hip libstdc++ */
#else
    {-0x1.31dbf4a984b7ap-213,0x1.31dbf4a984b79p-213,0x1.ac897c142e9f4p-863,-0x1.ac897c142e9f2p-863}, /* gcc 1.41422 */
    {0x1.9a680790d1c48p-251,0x1.7b75f7ecbc23p-251,0x1.9fc5f19d8021cp-771,0x1.48a0ed3684a9p-800}, /* icx 1.76715 */
    {0x1.9a680790d1c48p-251,0x1.7b75f7ecbc23p-251,0x1.9fc5f19d8021cp-771,0x1.48a0ed3684a9p-800}, /* clang 1.76715 */
    {-0x1.c9906bf537fffp-621,0x1.57cf212e9bdd5p-621,0x1.9d28b4b0cc366p+864,-0x1.3d2043742d3b4p+862}, /* cuda 1.47276 */
    {-0x1.31dbf4a984b7ap-213,0x1.31dbf4a984b79p-213,0x1.ac897c142e9f4p-863,-0x1.ac897c142e9f2p-863}, /* hip libc++ 1.41422 */
    {-0x1.31dbf4a984b7ap-213,0x1.31dbf4a984b79p-213,0x1.ac897c142e9f4p-863,-0x1.ac897c142e9f2p-863}, /* hip libstdc++ 1.41422 */
#endif
    /* cdiv */
#if !defined(IGNORE_NAN) && !defined(IGNORE_INF)
    {0x1.0348p+972,0x1.834p+972,0x1.ep-512,-0x0.0000000000001p-1022}, /* gcc Inf */
    {0x1.0348p+972,0x1.834p+972,0x1.ep-512,-0x0.0000000000001p-1022}, /* icx Inf */
    {0x1.0348p+972,0x1.834p+972,0x1.ep-512,-0x0.0000000000001p-1022}, /* clang Inf */
    {0,0},                              /* cuda */
    {0,0},                              /* hip libc++ */
    {0,0},                              /* hip libstdc++ */
#else
    {-0x1.0196b95d1df5cp-515,0x1.00ae2b8e6c9e7p-515,-0x1.01f56493ae34bp-788,-0x1.01482d2478bfap-788}, /* gcc 3.54437 */
    {0x1.9b92a5780a82p-946,0x1.dc228fbdcb165p-941,0x1.40020ebfb1166p+119,0x1.1fc6a86edd91p-487}, /* icx 0.707107 */
    {-0x1.0196b95d1df5cp-515,0x1.00ae2b8e6c9e7p-515,-0x1.01f56493ae34bp-788,-0x1.01482d2478bfap-788}, /* clang 3.54437 */
    {0x1.694641fed0682p-610,-0x1.477219ea9f84p-630,0x1.b47da6a1b5cb4p-691,-0x1.6bab82b49f2ap-681},    /* cuda 4.34114 */
    {-0x1.8c00df3c95fa2p-600,-0x1.6ac89d20340a7p-563,-0x1.f18e94736fbbfp-233,-0x1.6da4bc6c792bdp-218}, /* hip libc++ 4.2191 */
    {0x0.0000000000001p-1022,-0x0.0000000000001p-1022,-0x1.2258316d96f6dp-296,-0x1.f80acc5e0d4b5p-277}, /* hip libstdc++ 6.27e+15 */
#endif
    /* cpow */
#ifndef IGNORE_NAN
    {0x1.cfbcp+772,0x1.f3fep-320,-0x1.4dff8p-96,0x1.e95974p+1014}, /* glibc Inf */
    {0x1.cfbcp+772,0x1.f3fep-320,-0x1.4dff8p-96,0x1.e95974p+1014}, /* icx Inf */
    {0x1.cfbcp+772,0x1.f3fep-320,-0x1.4dff8p-96,0x1.e95974p+1014}, /* clang Inf */
    {0,0},                              /* cuda */
    {0,0},                              /* hip libc++ */
    {0,0},                              /* hip libstdc++ */
#else
    {0x1.9124bdbb8e3f1p+1023,0x1.d2f6686a31e6ap+1006,0x1.b781a20827e51p-24,0x1.fffe25018ad8fp+3}, /* glibc 32496.3 */
    {0x1.142921a4375aap+850,0x1.5c44dcd437b91p+796,0x1.665b62d80bfb2p-379,0x1.ff056cb17a49dp+3}, /* icx 8.68027 */
    {0x1.9124bdbb8e3f1p+1023,0x1.d2f6686a31e6ap+1006,0x1.b781a20827e51p-24,0x1.fffe25018ad8fp+3}, /* clang 32496.3 */
    {0x1.66c608dbc4d4dp-96,0x1.a2020e2090baap+1003,0x1.4718885abd008p-41,0x1.fc604c43a2b08p+3},  /* cuda  2.45e+04 */
    {0x1.66c608dbc4d4dp-96,0x1.a2020e2090baap+1003,0x1.4718885abd008p-41,0x1.fc604c43a2b08p+3}, /* hip libc++ 2.45e+04 */
    {0x1.66c608dbc4d4dp-96,0x1.a2020e2090baap+1003,0x1.4718885abd008p-41,0x1.fc604c43a2b08p+3}, /* hip libstdc++ 2.45e+04 */
#endif
  };
TYPE extra[SIZE_EXTRA][4] = {
  /* don't remove the following entry, it yields an error of 1.10574e+15
     for cmul (gcc, icx and clang) when IGNORE_INF is not defined */
  {0x1.bb8dcp+647,-0x1.c45a506276e91p+646,0x1.2098400377c86p+375,0x1.2888p+376},
  /* don't remove the following entry, it yields an error of 1.80144e+16
     for cpow with gcc when the imaginary part of the exponent is not limited */
  {0x1.27dfeed6ab2e3p+640,0x1.e5be225cbd5bfp-710,-0x1.cccf3a0b2f678p-102,0x1.d90cc1faea306p+393},
  /* don't remove the following entry, it yields an error of 1.80144e+16
     for cpow with icx when the imaginary part of the exponent is not limited */
    {0x1.789b9091ff688p+885,0x1.9157348032604p-769,-0x1.f5d2c6fa2893ap-95,0x1.008e8b52fc354p+690},
  /* don't remove the following entry: it is the example from Corollary 5 of
     "Error Bounds on Complex Floating-Point Multiplication" by
     Richard P. Brent, Colin Percival and Paul Zimmermann,
     Mathematics of Computation, 2007 */
    {0x1.8000000000003p-1,0x1.8p-1,0x1.555555555555ap-1,0x1.5555555555556p-1},
  /* don't remove the following entry: it is the example from Theorem 4.1 of
     "Error bounds on complex floating-point multiplication with an FMA" by
     Claude-Pierre Jeannerod, Peter Kornerup, Nicolas Louvet and Jean-Michel
     Muller, Mathematics of Computation, 2017 */
    {0x1.6a09e667f3bccp+25,0x1.0000002d413cdp+52,0x1.6a09e667f3bccp+25,0x1.0000002d413cdp+52},
  /* don't remove the following entry: it is the example from Theorem 4.2 of
     "Error bounds on complex floating-point multiplication with an FMA" by
     Claude-Pierre Jeannerod, Peter Kornerup, Nicolas Louvet and Jean-Michel
     Muller, Mathematics of Computation, 2017 */
    {0x1.6a09e667f3bcbp+25,0x1.0000002d413cdp+52,0x1.6a09e667f3bcbp+25,0x1.0000002d413cdp+52},
  /* don't remove the following entry: it is a test for cpow */
  {-4.0,-0.0,0.5,0.0},
  };
#endif

#ifdef USE_LDOUBLE
  TYPE worst[SIZE][4] = {
    /* cadd */
    {0,0,0,0},     /* GNU libc */
    {0,0,0,0},     /* icx */
    /* cmul */
    {0,0,0,0},     /* GNU libc */
    {0,0,0,0},     /* icx */
    /* cdiv */
    {0,0,0,0},     /* GNU libc */
    {0,0,0,0},     /* icx */
  };
TYPE extra[SIZE_EXTRA][4] = {
  };
#endif

#ifdef USE_FLOAT128
  TYPE worst[SIZE][4] = {
    /* cexp */
    {0,0}, /* GNU libc */
    {0,0}, /* icx */
    /* clog */
    {0,0}, /* GNU libc */
    {0,0}, /* icx */
  };
TYPE extra[SIZE_EXTRA][4] = {
  };
#endif
