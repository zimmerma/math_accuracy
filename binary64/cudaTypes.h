#pragma once

#ifdef __CUDACC__
#include<cuda.h>
#include<cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <cuda/std/complex>
#else
#include <complex>
#endif
#include<iostream>
#include<cstdio>
#include<cassert>
#include<cmath>

#ifdef __CUDACC__
inline
void cudaCheck_(const char* file, int line, const char* cmd, cudaError_t result)
{
    //std::cerr << file << ", line " << line << ": " << cmd << std::endl;
    if (result == cudaSuccess)
        return;

    const char* error = cudaGetErrorName(result);
    const char* message = cudaGetErrorString(result);
    std::cerr << file << ", line " << line << ": " << error << ": " << message << std::endl;
    abort();
}
#define cudaCheck(ARG) (cudaCheck_(__FILE__, __LINE__, #ARG, (ARG)))

inline
void cudaInit() {
#ifndef CUDART_VERSION
 #warning "no " CUDART_VERSION
#else
    printf ("Using CUDA %d\n",CUDART_VERSION);
#endif
    int cuda_device = 0;
    cudaDeviceProp deviceProp;
    cudaGetDeviceProperties(&deviceProp, cuda_device);
    printf("CUDA Capable: SM %d.%d hardware\n", deviceProp.major, deviceProp.minor);
}
#else
void cudaInit(){}
#endif


namespace cudaMath {

  template<typename T, int N=256>
  struct Data {
    using type = T;
    Data(){}
    Data(int bSize, int nstreams) {
      alloc(bSize, nstreams);
    }
    
    int bunchSize=0;
    static constexpr int maxNumOfThreads = N;
#ifdef __CUDACC__
    cudaStream_t streams[maxNumOfThreads];
#endif
    uint32_t * rndm[maxNumOfThreads];
    T * zpD[maxNumOfThreads];
    T * zpH[maxNumOfThreads];
    T * ypD[maxNumOfThreads];
    T * ypH[maxNumOfThreads];
    T * xpD[maxNumOfThreads];
    T * xpH[maxNumOfThreads];
    void alloc(int bSize, int nstreams) {
      assert(maxNumOfThreads>=nstreams);
      bunchSize = 20*bSize;
      for (int i = 0; i < nstreams; i++)
      {
#ifdef __CUDACC__
        cudaCheck(cudaStreamCreate(&(streams[i])));
        cudaCheck(cudaMalloc((void **)&zpD[i], bunchSize*sizeof(T)));
        cudaCheck(cudaMallocHost((void **)&zpH[i], bunchSize*sizeof(T)));
        cudaCheck(cudaMalloc((void **)&ypD[i], bunchSize*sizeof(T)));
        cudaCheck(cudaMallocHost((void **)&ypH[i], bunchSize*sizeof(T)));
        cudaCheck(cudaMalloc((void **)&xpD[i], bunchSize*sizeof(T)));
        cudaCheck(cudaMallocHost((void **)&xpH[i], bunchSize*sizeof(T)));
#else
        zpD[i] = (T*)malloc(bunchSize*sizeof(T));
        zpH[i] = zpD[i];
        ypD[i] = (T*)malloc(bunchSize*sizeof(T));
        ypH[i] = ypD[i];
        xpD[i] = (T*)malloc(bunchSize*sizeof(T));
        xpH[i] = xpD[i];
#endif
        rndm[i] = (uint32_t*)malloc(bunchSize*sizeof(uint32_t));
      }
    }
  };

 template<typename T>
  constexpr T add(T const & a, T const & b) { return a+b;}
  template<typename T>
  constexpr T mul(T const & a, T const & b) { return a*b;}
  template<typename T>
  constexpr T div(T const & a, T const & b) { return a/b;}
  template<typename T>
#ifdef __CUDACC__
  constexpr T pow(T const & a, T const & b) { return cuda::std::pow(a,b);}
#else
  constexpr T pow(T const & a, T const & b) { return std::pow(a,b);}
#endif
}

#ifdef __CUDACC__
namespace cudastd = cuda::std;
#else
namespace cudastd = std;
#endif



 template<typename T>
#ifdef __CUDACC__
__global__ 
#endif
void kernel_foo(T * const a, T * const b, T * c, int bunchSize) {
#ifdef __CUDACC__
   int first = blockIdx.x * blockDim.x + threadIdx.x;
   for (int i=first; i<bunchSize; i+=gridDim.x*blockDim.x) {
#else
   int first=0;
   for (int i=first; i<bunchSize; i++) {
#endif
#ifdef CUDAFOO2
     c[i] = cudaMath::CUDAFOO2(a[i],b[i]);
#else
     c[i] = cudastd::CUDAFOO(a[i]);
#endif
   }
//   printf("%f,%f %f,%f %f,%f\n",a[0].real(),a[0].imag(),b[0].real(),b[0].imag(),c[0].real(),c[0].imag());
}

template<typename T>
T * wrap_foo(cudaMath::Data<T> & data, int bunchSize, int nt) {
//  int nt = omp_get_thread_num();
#ifdef __CUDACC__
  cudaCheck(cudaMemcpyAsync(data.xpD[nt], data.xpH[nt], bunchSize*sizeof(T), cudaMemcpyHostToDevice, data.streams[nt]));
  cudaCheck(cudaMemcpyAsync(data.ypD[nt], data.ypH[nt], bunchSize*sizeof(T), cudaMemcpyHostToDevice, data.streams[nt]));
  kernel_foo<<<(bunchSize+128)/128,128,0,data.streams[nt]>>>(data.xpD[nt], data.ypD[nt], data.zpD[nt], bunchSize);
  cudaCheck(cudaMemcpyAsync(data.zpH[nt], data.zpD[nt], bunchSize*sizeof(T), cudaMemcpyDeviceToHost, data.streams[nt]));
  cudaCheck(cudaStreamSynchronize(data.streams[nt]));
//  cudaCheck(cudaDeviceSynchronize());
#else
  kernel_foo(data.xpD[nt], data.ypD[nt], data.zpD[nt],bunchSize);
#endif
  // std::cout << nt << ' ' << 0 << ' ' << data.xpH[nt][0] << ' ' << data.ypH[nt][0] << " = " << data.zpH[nt][0] << std::endl;
  return data.zpH[nt];
}
