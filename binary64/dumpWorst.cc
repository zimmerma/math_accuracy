#include<iostream>
#include<fstream>
#include<cstdlib>
#include<cassert>

int main(int argc, char ** argv) {

   std::string const names[] = {
     "acos", "acosh", "asin", "asinh", "atan", "atanh", "cbrt", "cos", "cospi", "cosh", "erf", "erfc", "exp", "exp10", "exp2", "expm1", "j0", "j1", 
     "lgamma", "log", "log10", "log1p", "log2", "rsqrt", "sin", "sinpi", "sinh", "sqrt", "tan", "tanh", "tgamma", "y0", "y1"};


   double worst[33*8];

   std::ifstream ifile; 
   ifile.open(argv[1],std::ios::binary);
   assert(ifile.good());
 
   ifile.read((char*)worst, sizeof(worst));
   assert(ifile.good());
   ifile.close();

   int nv=0;
   for (auto val : worst)  {
     auto nf = nv%8;
     if (0==nf)  std::cout << "/* " << names[nv/8] << " */" << std::endl;
     printf("%a,\n",val);
     nv++;
   }

  return 0;
}
