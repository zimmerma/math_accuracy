// compile wtrh hipcc -O3 oneComplex_hip.cpp -lm -fopenmp -march=native
#include <iostream>
#include <cmath>
#include <cstdio>

#ifdef __HIPCC__
#include<hip/hip_runtime.h>
#include<hip/hip_runtime.h>
#include <hip/hip_runtime_api.h>
#include <complex>

#ifndef  _GLIBCXX_USE_C99_COMPLEX
  #warning GLIBCXX_USE_C99_COMPLEX not defined
#else
#if  _GLIBCXX_USE_C99_COMPLEX
  #warning GLIBCXX_USE_C99_COMPLEX defined 1
#else
  #warning GLIBCXX_USE_C99_COMPLEX defined 0
#endif
#endif

inline
bool cudaCheck_(const char* file, int line, const char* cmd, hipError_t result)
{
    //std::cerr << file << ", line " << line << ": " << cmd << std::endl;
    if (result == hipSuccess)
        return true;

    const char* error = hipGetErrorName(result);
    const char* message = hipGetErrorString(result);
    std::cerr << file << ", line " << line << ": " << error << ": " << message << std::endl;
    abort();
    return false;
}
#define cudaCheck(ARG) (cudaCheck_(__FILE__, __LINE__, #ARG, (ARG)))
#endif

#ifdef __HIPCC__
__global__ void kernel_foo(double r, double i) {
   std::complex<double>  z(r,i);
   auto c = std::acosh(z);
   std::complex<double> d = (2.0) * std::log(std::sqrt((0.5) * (z + (1.0)))
                 + std::sqrt((0.5) * (z - (1.0))));
   printf ("      acosh(%la,%la)  gives %la,%la\n", z.real(), z.imag(), c.real(), c.imag());
   printf ("naive acosh(%la,%la)  gives %la,%la\n", z.real(), z.imag(), d.real(), d.imag());

}
#endif

#include<complex>

/*
#ifndef  _GLIBCXX_USE_C99_COMPLEX
  #warning GLIBCXX_USE_C99_COMPLEX not defined
#else
  #warning GLIBCXX_USE_C99_COMPLEX defined
#endif
*/

int
main (int argc, char *argv[])
{
#ifdef __HIPCC__
#ifndef CUDART_VERSION
 #warning "no " CUDART_VERSION
#else
    printf ("Using CUDA %d\n",CUDART_VERSION);
#endif
    int cuda_device = 0;
    hipDeviceProp_t deviceProp;
    hipGetDeviceProperties(&deviceProp, cuda_device);
    printf("HIP Capable: SM %d.%d hardware\n", deviceProp.major, deviceProp.minor);
#endif
    // volatile double r = 0x1.481d607d542adp+94; //2.;
    // volatile double i = -0x1.7ab11d26169b2p-1; //1.;
    double r = -0x1.019300384d993p+26;
    double i = -0x1.943eda1f20addp-1022;
#ifdef __HIPCC__
    kernel_foo<<<1,1>>>(r,i);
    cudaCheck(hipDeviceSynchronize());
#endif

    std::cout << "on CPU" <<  std::endl;

   std::complex<double>  z(r,i);
   auto c = std::acosh(z);
   std::complex<double> d = (2.0) * std::log(std::sqrt((0.5) * (z + (1.0)))
                 + std::sqrt((0.5) * (z - (1.0))));
    std::complex<double> e = std::log(z+ std::sqrt(z*z -1.));
   printf ("      acosh(%la,%la)  gives %la,%la\n", z.real(), z.imag(), c.real(), c.imag());
   printf ("kahan acosh(%la,%la)  gives %la,%la\n", z.real(), z.imag(), d.real(), d.imag());
   printf ("naive acosh(%la,%la)  gives %la,%la\n", z.real(), z.imag(), e.real(), e.imag());

    return 0;

}
