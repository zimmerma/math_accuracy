#include<cmath>
#include<complex>
#include<iostream>
#include <iomanip>

#include<cuda.h>
#include<cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <cuda/std/complex>


__global__ void kernel_foo(double r, double i) {
   cuda::std::complex<double>  b(r,i);
   auto c = cuda::std::sqrt(b);
   auto d = cuda::std::polar(std::sqrt(cuda::std::abs(b)),cuda::std::arg(b)/2);
   printf ("hypot/atan2(%la,%la)  gives %la,%la\n", b.real(), b.imag(), std::hypot(r,i),std::atan2(i,r));
   printf ("       sqrt(%la,%la)  gives %la,%la\n", b.real(), b.imag(), c.real(), c.imag());
   printf ("naive  sqrt(%la,%la)  gives %la,%la\n", b.real(), b.imag(), d.real(), d.imag());
}

int main() {


   std::cout << std::hexfloat;
   std::cout << hypot(-0x1.0040000008424p+1020,-0x1.feff3f8fa397p+1023) << std::endl;
   std::cout << nextafter(hypot(-0x1.0040000008424p+1020,-0x1.feff3f8fa397p+1023),INFINITY) << std::endl;

   std::cout << atan2(-0x1.0040000008424p+1020,-0x1.feff3f8fa397p+1023) << std::endl;
   std::complex<double> a{-0x1.0040000008424p+1020,-0x1.feff3f8fa397p+1023};
//   std::complex<double> a{-0x1.0040000008424p+1020,-0x1.0eff3f8fa397p+1023};
//   std::complex<double> a{0x0.19deaac345ffap+1022,0x0.92c8727c389b6p+1022};
   std::cout << abs(a) << ' ' << arg(a) << std::endl;
   std::cout << std::polar(sqrt(abs(a)),arg(a)/2) << std::endl;
   std::cout << std::sqrt(a) << std::endl;


   kernel_foo<<<1,1>>>(a.real(),a.imag());
   cudaDeviceSynchronize();

   return 0;
}
