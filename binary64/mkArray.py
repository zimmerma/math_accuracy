#!/usr/bin/env python3

# This script takes (as input) the output of one of the accuracy programs
# and generates (to stdout) a C-style array named "worst".

import sys

def emitArray(lines, label):  # lines is a list of strings, label is a string
    ncommas = lines[0].count(',')
    if ncommas > 0:
        print("  TYPE worst[SIZE][", ncommas + 1, "] = {", sep='');
    else:
        print("  TYPE worst[SIZE] = {");
    for line in lines:
        items = line.split()
        print("    /*", items[0], "*/")
        if ncommas > 0:
            print("    {", items[3], "},  ", sep='', end='')
        else:
            print("    ",  items[3],  ",  ", sep='', end='')
        print("/*", label, items[-2], "*/")
    print("  };");

def main():
    if len(sys.argv) < 2 or sys.argv[1].startswith(("-h", "--h")):
        print("Usage:", sys.argv[0], "-lxxx [filename]", file=sys.stderr)
        print(" e.g.:", sys.argv[0], "-lIML < data.txt", file=sys.stderr)
        print(" e.g.:", sys.argv[0], "-l\"GNU libc\" data.txt", file=sys.stderr)
        return
    if sys.argv[1].startswith("-l"):
        label = sys.argv[1][2:]
    else:
        print("Missing -l flag.  Try -h for help.", file=sys.stderr)
        return
    if len(sys.argv) == 2:
        lines = sys.stdin.readlines()
    else:
        with open(sys.argv[-1], "r") as f:
            lines = f.readlines()
    emitArray(lines, label)

if __name__ == '__main__':
    main()

