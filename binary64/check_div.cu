/* Checks correct rounding of 64-bit division.

   This program is open-source software distributed under the terms 
   of the GNU General Public License <http://www.fsf.org/copyleft/gpl.html>.

   Usage:

   $ gcc -O3 check_div.c -o check_div -lm -lmpfr
   $ ./check_div [-seed nnn] [-rndz] [-rndu] [-rndd] [-subnormal]
*/

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <mpfr.h>
#include <fenv.h>

#include <assert.h>
#include <sys/types.h>
#include <unistd.h>
#ifndef NO_OPENMP
#include <omp.h>
#endif

#include<fstream>
#include<iomanip>
#include<algorithm>
#include<iostream>
#include<atomic>


#ifdef __CUDACC__
#include<cuda.h>
#include<cuda_runtime.h>
#include <cuda_runtime_api.h>

inline
bool cudaCheck_(const char* file, int line, const char* cmd, cudaError_t result)
{
    //std::cerr << file << ", line " << line << ": " << cmd << std::endl;
    if (result == cudaSuccess)
        return true;

    const char* error = cudaGetErrorName(result);
    const char* message = cudaGetErrorString(result);
    std::cerr << file << ", line " << line << ": " << error << ": " << message << std::endl;
    abort();
    return false;
}
#define cudaCheck(ARG) (cudaCheck_(__FILE__, __LINE__, #ARG, (ARG)))
#endif


#define TYPE double
#define FOO2 div


const int maxNumOfThreads = 256;
TYPE * ypD[maxNumOfThreads];
TYPE * ypH[maxNumOfThreads];
TYPE * xpD[maxNumOfThreads];
TYPE * xpH[maxNumOfThreads];
TYPE * zpD[maxNumOfThreads];
TYPE * zpH[maxNumOfThreads];



#ifdef __CUDACC__

__device__ __host__ inline TYPE div(TYPE x, TYPE y) {
  return x/y;
}

#define DO_DIV
#ifdef DO_DIV
__global__ void kernel_div(TYPE const * px, TYPE const * py, TYPE * pz, int bunchSize,  int rd) {
  //__ddiv_[rn,rz,ru,rd](x)
  auto fn = __ddiv_rn;
  switch (rd) {
  case 0:
    fn = __ddiv_rn; break;
  case 1:
    fn = __ddiv_rz; break;
  case 2:
    fn = __ddiv_ru; break;
  case 3:
    fn = __ddiv_rd; break;
  default :
   fn = __ddiv_rn; break;
  }
   int first = blockIdx.x * blockDim.x + threadIdx.x;
   for (int i=first; i<bunchSize; i+=gridDim.x*blockDim.x) {
     pz[i] = fn(px[i],py[i]);
  }
}
#endif



cudaStream_t streams[maxNumOfThreads];
__global__ void kernel_foo(TYPE const * px, TYPE const * py, TYPE * pz, int bunchSize) {
   int first = blockIdx.x * blockDim.x + threadIdx.x;
   for (int i=first; i<bunchSize; i+=gridDim.x*blockDim.x) {
     pz[i] = FOO2(px[i],py[i]);
   }
}
__global__ void kernel_foo(TYPE const x, TYPE const * py, TYPE * pz, int bunchSize) {
   int first = blockIdx.x * blockDim.x + threadIdx.x;
   for (int i=first; i<bunchSize; i+=gridDim.x*blockDim.x) {
     pz[i] = FOO2(x,py[i]);
   }

}
#else // CPU version
void  kernel_foo(TYPE const * px, TYPE const * py, TYPE * pz, int bunchSize) {
   int first = 0;
   for (int i=first; i<bunchSize; i++) {
     pz[i] = FOO2(px[i],py[i]);
   }
}
void  kernel_foo(TYPE const  x, TYPE const * py, TYPE * pz, int bunchSize) {
   int first = 0;
   for (int i=first; i<bunchSize; i++) {
     pz[i] = FOO2(x,py[i]);
   }
}
#endif


TYPE * wrap_foo(TYPE const * xH, TYPE const * yH, int bunchSize, int rd) {
  int nt = omp_get_thread_num();
#ifdef __CUDACC__
  cudaCheck(cudaMemcpyAsync(xpD[nt], xH, bunchSize*sizeof(TYPE), cudaMemcpyHostToDevice, streams[nt]));
  cudaCheck(cudaMemcpyAsync(ypD[nt], yH, bunchSize*sizeof(TYPE), cudaMemcpyHostToDevice, streams[nt]));
#ifdef DO_DIV
  kernel_div<<<(bunchSize+128)/128,128,0,streams[nt]>>>(xpD[nt], ypD[nt],zpD[nt],bunchSize,rd);
#else
  kernel_foo<<<(bunchSize+128)/128,128,0,streams[nt]>>>(xpD[nt], ypD[nt],zpD[nt],bunchSize);
#endif
  cudaCheck(cudaMemcpyAsync(zpH[nt], zpD[nt], bunchSize*sizeof(TYPE), cudaMemcpyDeviceToHost, streams[nt]));
  cudaStreamSynchronize(streams[nt]);
#else
  kernel_foo(xH, yH, zpH[nt], bunchSize);
#endif
//  std::cout << nt << ' ' << n << ' ' << zpH[nt][0] << std::endl;
  return zpH[nt];
}

TYPE * wrap_foo(TYPE const x, TYPE const * yH, int bunchSize) {
  int nt = omp_get_thread_num();
#ifdef __CUDACC__
  cudaCheck(cudaMemcpyAsync(ypD[nt], yH, bunchSize*sizeof(TYPE), cudaMemcpyHostToDevice, streams[nt]));
  kernel_foo<<<(bunchSize+128)/128,128,0, streams[nt]>>>(x, ypD[nt],zpD[nt],bunchSize);
  cudaCheck(cudaMemcpyAsync(zpH[nt], zpD[nt], bunchSize*sizeof(TYPE), cudaMemcpyDeviceToHost, streams[nt]));
  cudaStreamSynchronize(streams[nt]);
#else
  kernel_foo(x, yH, zpH[nt], bunchSize);
#endif
//  std::cout << nt << ' ' << n << ' ' << zpH[nt][0] << std::endl;
  return zpH[nt];
}

mpfr_rnd_t rnd = MPFR_RNDN;


#define MAXN 15
/* PMAX=50000 is close to optimal in terms of (x,y) pairs found per second
   with the naive integer factorization we use */
#define PMAX 50000


struct Buff {
 double * x;
 double * y;
 int j;
 int n;

 bool full() const {return j==n;}
 void fill(double xi, double yi) { 
    // std::cout << "filling " << j << ' ' << xi << ' ' << yi << std::endl;
    assert(j<=n);
    if (j==n) return;
    x[j]=xi;
    y[j++] = yi;
 }

};


/* multiply y by all factor[i]^j for 0 <= i < nfactors,
   0 <= j <= exponent[i] */
static void
check_corner_aux2 (Buff & buff, double x, __int128_t *factor, int *exponent, int nfactors,
                    __int128_t y, __int128_t N)
{
  if (nfactors == 0)
  {
    if (0x10000000000000 <= y && y < 0x20000000000000) /* y has 53 bits */
    {
      __int128_t z = N / y;
      if ((rnd != MPFR_RNDN && 0x10000000000000 <= z && x < 0x20000000000000)
          || (rnd == MPFR_RNDN && 0x20000000000000 <= z && z < 0x40000000000000))
        buff.fill (x, ldexp (double(y), -52));
    }
    return;
  }
  for (int j = 0; j <= exponent[nfactors-1]; j++)
  {
    check_corner_aux2 (buff, x, factor, exponent, nfactors-1, y, N);
    y *= factor[nfactors-1];
  }
}

unsigned long *Primes = NULL, nprimes = 0;

/* generates all odd primes < B */
static void
genPrimes (unsigned long B)
{
  Primes = (unsigned long *)malloc (B * sizeof (unsigned long));
  for (unsigned long p = 3; p < B; p++)
    {
      int ok = 1;
      for (int i = 0; i < nprimes && ok != 0; i++)
        {
          unsigned long q = Primes[i];
          if (p < q * q)
            break;
          if ((p % q) == 0)
            ok = 0;
        }
      if (ok)
        Primes[nprimes++] = p;
    }
  Primes = (unsigned long *)realloc (Primes, nprimes * sizeof (unsigned long));
}

#if 0
static void
print_int128 (__int128_t X)
{
  printf ("%lu %lu\n", (uint64_t) (X >> 64),
          (uint64_t) (X & (__int128_t) 0xffffffffffffffff));
}
#endif

static void
check_corner_aux (Buff & buff, double x, int e, int s)
{
  assert (1 <= x && x < 2);
  __int128_t X = ldexp (x, 52);
  /* now 2^52 <= x < 2^53 */
  X = X << e;
  X += (__int128_t) s;
  /* we should have s odd so that X has only odd factors */
  __int128_t factor[MAXN], p, N = X;
  int exponent[MAXN], nfactors = 0;
  //  for (p = 3; p < PMAX; p += 2)
  for (int i = 0; i < nprimes; i++)
  {
    p = Primes[i];
    if ((X % p) == 0)
    {
      factor[nfactors] = p;
      exponent[nfactors] = 1;
      X = X / p;
      while ((X % p) == 0)
      {
        exponent[nfactors] ++;
        X = X / p;
      }
      nfactors ++;
    }
    if (X < p * p) /* necessarily X is prime */
    {
      factor[nfactors] = X;
      exponent[nfactors] = 1;
      nfactors ++;
      X = 1;
      break;
    }
  }
  check_corner_aux2 (buff, x, factor, exponent, nfactors, 1, N);

  if (X != 1 && X < 0x20000000000000)
    check_corner_aux2 (buff, x, factor, exponent, nfactors, X, N);
}

static void
check_corner (Buff & buff, double x)
{
  assert (0 <= x && x < 1);
  x = 1.0 + x;
  /* now 1 <= x < 2 */
  int i = 1;
  //for (int i = 1; i < 100; i+=2)
  {
    check_corner_aux (buff,x, 52 + (rnd == MPFR_RNDN), i);
    check_corner_aux (buff,x, 52 + (rnd == MPFR_RNDN), -i);
    check_corner_aux (buff,x, 53 + (rnd == MPFR_RNDN), i);
    check_corner_aux (buff,x, 53 + (rnd == MPFR_RNDN), -i);
  }
}



/* With corner=1, check x/y near midpoints */
int corner = 0;



/* With subnormal=1, check output in subnormal range. This is tricky due to
   double-rounding: https://hal-ens-lyon.archives-ouvertes.fr/ensl-00875366 */
int subnormal = 0;

int
main (int argc, char *argv[])
{
#ifdef __CUDACC__
#ifndef CUDART_VERSION
 #warning "no " CUDART_VERSION
#else
    printf ("Using CUDA %d\n",CUDART_VERSION);
#endif
    int cuda_device = 0;
    cudaDeviceProp deviceProp;
    cudaGetDeviceProperties(&deviceProp, cuda_device);
    printf("CUDA Capable: SM %d.%d hardware\n", deviceProp.major, deviceProp.minor);
#endif

  long seed = 0;

  while (argc > 1 && argv[1][0] == '-')
  {
    if (argc > 2 && strcmp (argv[1], "-seed") == 0)
    {
      seed = atoi (argv[2]);
      argv += 2;
      argc -= 2;
    }
    else if (strcmp (argv[1], "-subnormal") == 0)
    {
      subnormal = 1;
      mpfr_set_emin (-1073);
      std::cout << "subnormal selected" << std::endl;
      argv ++;
      argc --;
    }
    else if (strcmp (argv[1], "-corner") == 0)
    {
      corner = 1;
      genPrimes (PMAX);
      std::cout << "corner selected" << std::endl;
      argv ++;
      argc --;
    }
    else if (strcmp (argv[1], "-rndz") == 0)
    {
      rnd = MPFR_RNDZ;
      fesetround (FE_TOWARDZERO);
      argv ++;
      argc --;
    }
    else if (strcmp (argv[1], "-rndu") == 0)
    {
      rnd = MPFR_RNDU;
      fesetround (FE_UPWARD);
      argv ++;
      argc --;
    }
    else if (strcmp (argv[1], "-rndd") == 0)
    {
      rnd = MPFR_RNDD;
      fesetround (FE_DOWNWARD);
      argv ++;
      argc --;
    }
    else
    {
      printf ("Unknown option %s\n", argv[1]);
      exit (1);
    }
  }



  int nstreams = omp_get_max_threads();
  assert(maxNumOfThreads>=nstreams);
  int bunchSize = 100;

#ifdef __CUDACC__
  for (int i = 0; i < nstreams; i++)
    {
        cudaCheck(cudaStreamCreate(&(streams[i])));
        cudaCheck(cudaMalloc((void **)&zpD[i], bunchSize*sizeof(TYPE)));
        cudaCheck(cudaMallocHost((void **)&zpH[i], bunchSize*sizeof(TYPE)));
        cudaCheck(cudaMalloc((void **)&ypD[i], bunchSize*sizeof(TYPE)));
        cudaCheck(cudaMallocHost((void **)&ypH[i], bunchSize*sizeof(TYPE)));
        cudaCheck(cudaMalloc((void **)&xpD[i], bunchSize*sizeof(TYPE)));
        cudaCheck(cudaMallocHost((void **)&xpH[i], bunchSize*sizeof(TYPE)));

    }
#else
 for (int i = 0; i < nstreams; i++)
    {
      zpH[i] = (TYPE *)malloc(bunchSize*sizeof(TYPE));
      ypH[i] = (TYPE *)malloc(bunchSize*sizeof(TYPE));
      xpH[i] = (TYPE *)malloc(bunchSize*sizeof(TYPE));
    }
#endif

  if (seed == 0)
    seed = getpid ();
    printf ("Using seed %lu\n", (unsigned long) seed);

    int nthreads;
    std::atomic<unsigned long long> ccc=0;
#pragma omp parallel
  nthreads = omp_get_num_threads ();
#pragma omp parallel for
  for (int n = 0; n < nthreads; n++) {
    bool doprint=true;
    srand48 (seed+n);
    mpfr_t X, Y, Z;
    mpfr_init2 (X, 53);
    mpfr_init2 (Y, 53);
    mpfr_init2 (Z, 53);
    TYPE * x = xpH[n];
    TYPE * y = ypH[n];
    unsigned char nn = 0;
    while (1)
    {
     ccc++;
     if (ccc%(16*nthreads) == 0) std::cout << std::endl;  //  doprint = true;
     if ( (nn++) ==0 ) std::cout << '.'; 
     if (corner)   {
       Buff buff{x,y,0,bunchSize};
       while (!buff.full()) check_corner(buff,drand48());
     } else  {
       for (int i=0; i<bunchSize; ++i) {
          x[i] = 0.5*(1. + 0.5*drand48());
          y[i] = 0.5*(1. + 0.5*drand48());
          /* x and y are uniform over [0,1), thus for x,y in [1/2,1),
          we have x/y in (1/2,2) */
          if (subnormal) {
            x[i] *= 0x1p-512;
            y[1] *= 0x1p+511;
           /* for x,y originally in [1/2,1), we now have x/y in (2^-1024,2^-1022) */
          }
       } //  loop i 
     } // corner
      TYPE const * z = wrap_foo(x, y, bunchSize,rnd);
      for (int i=0; i<bunchSize; ++i) {
        mpfr_set_d (X, x[i], MPFR_RNDN);
        mpfr_set_d (Y, y[i], MPFR_RNDN);
        int ret = mpfr_div (Z, X, Y, rnd);
        if (subnormal)
          mpfr_subnormalize (Z, ret, rnd);
        TYPE t = mpfr_get_d (Z, MPFR_RNDN);
        if (t != z[i]) {
            std::cout << std::endl;
            printf ("div and mpfr_div differ for x=%la,y=%la\n", x[i], y[i]);
            printf ("     div gives %la\n", z[i]);
            printf ("mpfr_div gives %la\n", t);
        }
        if (doprint && i<10) {
            std::cout << "thread " << n << std::endl;
            printf ("x=%la,y=%la\n", x[i], y[i]);
            printf ("     div gives %la\n", z[i]);
            printf ("mpfr_div gives %la\n", t);
        }
      } // second i loop
     doprint = false;
    }  // while
  } // ttread loop

  return 0;

}
