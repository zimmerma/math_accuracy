#include<complex>
#include<iostream>



int main() {

{
   std::cout << "float " << std::endl;
   std::complex<float> z = {-4.0f,-0.0f};
   std::cout << z << ' ' << std::sqrt(z) << std::endl;
   std::cout << std::pow(z,0.5f) << std::endl;
   std::cout << std::hypot(z.real(),z.imag()) << ' ' << std::atan2(z.imag(),z.real()) << std::endl;
   std::cout << z.imag() << (z.imag() < 0 ? " negative" : " positive") << std::endl;
   std::cout << z.imag() << ( std::signbit(z.imag()) ? " negative" : " positive") << std::endl;
   std::cout << std::polar(std::sqrt(std::hypot(z.real(),z.imag())),0.5f*std::atan2(z.imag(),z.real())) << std::endl;
}

{
   std::cout << "double " << std::endl;
   std::complex<double> z = {-4.0,-0.0};
   std::cout << z << ' ' << std::sqrt(z) << std::endl;
   std::cout << std::pow(z,0.5f) << std::endl;
   std::cout << std::hypot(z.real(),z.imag()) << ' ' << std::atan2(z.imag(),z.real()) << std::endl;
   std::cout << z.imag() << (z.imag() < 0 ? " negative" : " positive") << std::endl;
   std::cout << z.imag() << ( std::signbit(z.imag()) ? " negative" : " positive") << std::endl;
   std::cout << std::polar(std::sqrt(std::hypot(z.real(),z.imag())),0.5*std::atan2(z.imag(),z.real())) << std::endl;
}


  std::complex<double> v = {-8.0,0.0};
  std::cout << v << ' ' << std::pow(v,1./3.) << std::endl;

  std::complex<double> w = {-1.0,-0.0};
  std::cout << w << ' ' << std::log(w) << std::endl;

  return 0;
}

