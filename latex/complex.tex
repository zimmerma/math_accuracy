% TODO:
% * mention C++ standard https://eel.is/c++draft/complex.numbers#general-4 ?
% * add FreeBSD, see https://bugs.freebsd.org/bugzilla/show_bug.cgi?id=216864
%   for example

\documentclass[11pt]{article}
\usepackage{fullpage}
\usepackage{booktabs}
\usepackage{url}
\usepackage{comment}
\usepackage{inconsolata}
\usepackage{newtxmath} % suggested by Andreas K. Huettel
% He also suggested newtxtext but this gives 0 with a bar inside

\usepackage[numbers,square]{natbib} % URLs now appear in the bibliography.
\usepackage[hidelinks]{hyperref}    % Gives info when hovering on a citation.

% Use these in math mode:
\newcommand*{\rnd}{{\circ}}  % represents rounding to target precision
\def\tinyspace{\mskip0.5\thinmuskip}
\def\ulp{\mathrm{ulp}}
\def\Inf{\mathrm{Inf}}

\title{Accuracy of Complex Mathematical Operations and Functions in Single and Double Precision}
\author{Paul Caprioli\footnote{High Performance Kernels LLC},\enspace
  Vincenzo Innocente\footnote{CERN},\enspace
% John Mather\footnote{Side Effects Software Inc.},\enspace
  Paul Zimmermann\footnote{Universit\'e de Lorraine, CNRS, Inria, LORIA}}
\date{September 2024}
\begin{document}
\maketitle

\section{Introduction}

The IEEE Standard for Floating-Point Arithmetic~\cite{IEEE754r2} requires
correct rounding for basic arithmetic operations (addition, subtraction,
multiplication, division, and square root) on real floating-point numbers,
but there is no such requirement for the corresponding operations on
complex floating-point numbers.
Furthermore, while the accuracy of mathematical functions has been studied in
various libraries for different IEEE real floating-point formats~\cite{acc24},
we are unaware of similar studies for complex-valued functions.

The C programming language~\cite{C17} specifies that complex floating point
types have the same representation and alignment requirements as an array type
containing exactly two elements corresponding, respectively, to the real and
imaginary parts of the complex number.
The C++ programming language~\cite{CPP20} does the same.
Their use of Cartesian coordinates facilitates addition and subtraction but
does lead to some non-trivial challenges for multiplication and division.
In fact, some implementations do not attempt to avoid intermediate overflows
nor NaN-values resulting from $\Inf - \Inf$.
Some compilers such as GCC clearly state that the accuracy of complex
floating-point operations is unknown \cite{GCC}.

Complex mathematical functions may involve branch cuts~\cite{KahanBranchCuts},
curves in the complex plane along which the function is discontinuous.
The best known example is probably the complex square root, for which
\begin{align*}
    \textrm{csqrt}(-1 + 0i) &= i \\
    \textrm{csqrt}(-1 - 0i) &= -i
\end{align*}
since the sign of the IEEE floating-point zero provides the extra bit of
information needed to resolve the discontinuity along the negative real axis.
Similarly, the same convention is adopted for the complex logarithm:
\begin{align*}
    \textrm{clog}(-1 + 0i) &= \pi i  \\
    \textrm{clog}(-1 - 0i) &= -\pi i. \\
\end{align*}

Consistency among complex functions motivates the following definition of
complex cube root:
\begin{equation*}
    \textrm{ccbrt}(z) = \textrm{cexp}\big(\textrm{clog}(z) / 3 \big).
\end{equation*}
However, this choice is discordant with the real-valued cube root function
for negative numbers (i.e., on the branch cut chosen for clog).
For example,
\begin{equation*}
    \textrm{ccbrt}(-8 + 0i) = 1 + \sqrt{3}\tinyspace i
\end{equation*}
whereas
\begin{equation*}
    \textrm{cbrt}(-8) = -2.
\end{equation*}

Despite the challenges, complex floating-point is widely used in high
performance computing.
For example, the Fast Fourier Transform is an essential algorithm to many
application areas~\cite{hpkfft}, and the accuracy both of the underlying
complex arithmetic as well as of the complex exponential function is
necessary for its overall accuracy~\cite{schatzman}.

\subsection{Theoretical background} \label{theory}

In this section, let $u = 2^{-p}$ for binary floating-point of precision $p$.
For example, $u=2^{-24}$ for single precision binary floating-point, and
$u=2^{-53}$ for double precision binary format.
Also, let $\rnd(\cdot)$ denote rounding to nearest, and assume that overflow,
underflow, and denormals do not occur.

Then, the complex sum as computed by
\begin{equation*}
    \begin{split}
      z_0 + z_1 &= (a + bi) + (c + di) \\
                &\approx \rnd(a + c) + \rnd(b + d)\tinyspace i \\
    \end{split}
\end{equation*}
is, by definition, correctly rounded in each of its components.
Therefore, letting $z = z_0 + z_1 = x + yi$ be the infinitely precise result
and $z'= x' + y'i$ be the rounded-to-nearest sum as computed above,
\begin{equation*}
    \begin{split}
      |z' - z| &= \sqrt{\left(x'-x\right)^2 + \left(y'-y\right)^2} \\[2pt]
               &< \sqrt{\left(u\tinyspace|x|\right)^2 +
                        \left(u\tinyspace|y|\right)^2}
               \,=\, u \sqrt{|x|^2 + |y|^2}
               \,=\, u \tinyspace|z|,
    \end{split}
\end{equation*}
so the relative normwise error, $|z' - z| / |z|$, of the computed sum is
less than $u$.
Furthermore,
\begin{equation*}
    \begin{split}
      |z' - z| &\le \sqrt{\left(\frac{1}{2} \ulp(x)\right)^2 + \:
                          \left(\frac{1}{2} \ulp(y)\right)^2}  \\[2pt]
               &\le \sqrt{\left(\frac{1}{2} \ulp(|z|)\right)^2 \! +
                          \left(\frac{1}{2} \ulp(|z|)\right)^2}
               =\, \frac{\sqrt{2}}{2}\,\ulp(|z|).
    \end{split}
\end{equation*}
Note that satisfying the inequalities above does not imply that $z'$ is
correctly rounded.
For example, suppose $z = 1/8 + i$ and $z' = (1/8 + u/2) + i$.
Then the real component of $z'$ is not correctly rounded.
Nevertheless, since $|z' - z| = u/2$ and $|z| > 1$, we have
$|z' - z| < u\tinyspace|z|$,
and also, since $\ulp(|z|) \ge 2u$, we have
$|z' - z| \le \bigl(\!\sqrt{2}/2\bigr)\tinyspace \ulp(|z|)$.

Consider the standard implementation of a complex product without using a
fused multiply-add (FMA) operation:
\begin{equation*}
    \begin{split}
      z_0 \cdot z_1 &= (a + bi) \cdot (c + di) \\
                    &\approx \rnd\big(\rnd(ac) - \rnd(bd)\big) +
                             \rnd\big(\rnd(ad) + \rnd(bc)\big)\tinyspace i. \\
    \end{split}
\end{equation*}
Brent, Percival, and Zimmermann~\cite{brent:inria-00120352} showed that,
for the algorithm above, the maximum relative normwise error is
$\sqrt{5}\tinyspace u$.

Using an FMA, the product can better be implemented as follows:
\begin{equation*}
    \begin{split}
      z_0 \cdot z_1 &= (a + bi) \cdot (c + di) \\
                    &\approx \rnd\big(ac - \rnd(bd)\big) +
                             \rnd\big(ad + \rnd(bc)\big)\tinyspace i. \\
    \end{split}
\end{equation*}
Jeannerod, Kornerup, Louvet, and Muller~\cite{jeannerod:hal-00867040} showed
that the maximum relative normwise error of this algorithm is $2u$.


\subsection{Alternatives} % state of the art

Fred Tydeman proposed in 2009 another way to do complex multiply and divide in
the C language~\cite{n1399}.
His FPCE test suite (\url{http://www.tybor.com})
has tests of accuracy of many complex mathematical functions, as
well as the elementary operations, but unfortunately, it is not free (only a
free sample is available, with no complex results).
The ISO/IEC standard 10967-3 (LIA) specifies complex integer and floating-point
arithmetic and complex elementary numerical functions; it introduces a very
strong zero, denoted 00, which yields, for example, $00 \cdot {\rm NaN} = 00$.
Strong zeros are not directly represented, but rather they arise implicitly
for real or pure imaginary values.
For example, $c\cdot(a+bi)$ can be evaluated as if it were $(c+00i)\cdot(a+bi)$
and optimized accordingly.

\section{Methodology}

In this research, we only consider the Cartesian representation of complex
numbers (as in the C standard); in particular, we don't consider the polar
representation.
We don't consider directed rounding modes from IEEE 754 (only the rounding to
nearest mode roundTiesToEven).
We don't check exception flags are correctly set
(underflow, overflow, invalid, division by zero, inexact).

Basic complex operations (addition, multiplication, division) might be dealt
with by the C compiler, or by a library call.
It is not always possible to know which case happens, which moreover
might depend on some compiler options.
We use the same compiler options for all operations, as this would typically
be the case when building an application program.

We use the algorithm described in Section~3.1 of \cite{acc24} to search for
large errors.
One difference is that for complex univariate functions, we have
two inputs (the real and imaginary parts), and for bivariate functions,
we have four inputs.
As a consequence, it is no longer possible to perform an exhaustive search
for single precision.

\subsection{How to measure the error?}

While for real values there is a consensus to measure the error in ulps
(units-in-last-place), for complex values there might be different ways
to measure the error.
Assume $z = (x,y)$ is the result of a computation
with infinite precision, and $z' = (x',y')$ is the compiler or library result.

\paragraph{Component-wise error.}
One way to measure the error is to compute the error $e_x$ in ulps between $x$
and $x'$ (see \cite{acc24} for a precise definition),
the error $e_y$ in ulps between $y$ and $y'$, and to take the
maximum of both values.
However, if say $y$ is very small compared to $x$, one might wonder if
a large ulp-error on $y$ really makes sense.
Consider for example the product in single precision of
$(\verb|0x1.994c36p-125|,\verb|-0x1.e588c4p-124|)$ by
$(\verb|-0x1.5a43d2p+49|,\verb|-0x1.9ac2c8p+50|)$,
where GCC yields $(\verb|-0x1.cabaeap-73|,\verb|-0x1p-97|)$ instead of
$(\verb|-0x1.cabaeap-73|,\verb|-0x1.7cb4p-106|)$.

\paragraph{Normwise error.}
Here we compute the complex difference $\delta = (x'-x,y'-y)$, and
compare $\delta$ to~$z$.
In the literature, one can find bounds of the
form $|\delta| < e \cdot u \cdot |z|$ for various constants $e$,
where $u = 2^{-p}$
for $p$ the binary precision,
and $|\cdot|$ denotes the Euclidean norm.
However, this kind of formula does not work well when $|z|$ is in the
subnormal range.
We thus prefer to bound $\delta$ in terms of $\ulp(|z|)$:
\begin{equation} \label{eq1}
  |\delta| \le e \cdot \ulp(|z|).
\end{equation}  
This formula nicely extends the one used in the real case \cite{acc24},
where the absolute value is replaced by the Euclidean norm.
In this article we use normwise error $e$ as defined in Eq.~(\ref{eq1}).
If some table entry contains say $e = 0.708$, this means the largest
normwise error we found is $|\delta| \le 0.708 \cdot \ulp(|z|)$,
and we found an example with $|\delta| > 0.707 \cdot \ulp(|z|)$.
Inputs attaining the corresponding bounds can be found in the source files
\verb|complex-worst.h| (for univariate functions) and \verb|complex2-worst.h|
(for bivariate functions), which are in the \verb|binary64| directory of the
\verb|math_accuracy| git project \cite{mathacc}
using revision \input{git_revision.tex}\unskip\ for this document.

\subsection {GPU devices}
We have performed experiments on both NVidia and AMD GPU devices.
Neither the cuda math library~\cite{cudaerrors} nor ROCm OCML~\cite{rocmlib}
implement device-specific C complex functions.
We have used C++ syntax based on the \verb|std::complex| class~\cite[\S29.4]{CPP20}.
The cuda compiler, nvcc, uses its own implementation in the \verb|cuda::std|
namespace~\cite{cudastd}.
On AMD we used either the hipcc compiler or clang++ with offload features.
Both use the implementation of either the gcc stdlibc++ (default) or the one in
the llvm libc++.\footnote{On CPU, stdlibc++ calls C functions on platforms
supporting C99 complex math.  However, libc++ and cuda::std instead use their
own implementation on CPU as well.}
All these implementations uses textbook formulas (see for instance table 1 in \cite{KahanBranchCuts})
that suffer from infinites and NaN in the intermediate steps, at least if used in the full range of the floating point under consideration.
Therefore almost all our experiments result in the largest error to be infinite.
We used cuda version 12.4 and ROCm version 5.7.
The libstdc++ library was from gcc 14.2, and libc++ from clang 19.1.

\subsection{Additional notes}

Our experiments were done on an Intel Xeon Silver 4214.

\paragraph{About GCC and GNU libc.}
We used GCC 14.2.0 and GNU libc 2.40
(which was configured with the default settings, i.e., without \verb|-mfma|).
The complex multiplication is handled directly by the compiler.
We used the compiler options \verb|-mfma| and \verb|-O3|;
without \verb|-mfma|, GCC does not emit FMA
(fused multiply-add) for the complex multiplication.

\paragraph{About the Intel compiler.}
We used the Intel compiler 2023.2.1, which calls the corresponding Intel math
library.
We used the following compiler options:
\verb|-ffp-model=precise|, \verb|-mfma|, \verb|-no-ftz|, and \verb|-O3|.
However, it appears that with \verb|-ffp-model=precise| the Intel compiler
is not using FMA for complex multiplication in single or double precision.

\paragraph{About clang.}
We use clang 16.0.6 with the GNU libc; thus results are identical to
GCC/GNU libc for mathematical functions implemented by library calls.
We used the following compiler options:
\verb|-mfma|, \verb|-ffp-model=precise|, \verb|-O3|.
The compiler option \verb|-fcomplex-arithmetic=promoted|,
available in clang 20.0.0git~\cite{clang-fcomplex}, is not compatible
with \verb|-ffp-model=precise|.
It does give better results for division; however multiplying
$(\verb|-0x1p+74|,\verb|0x1.5ecce4p+97|)$ by
$(\verb|0x1.2ccad2p+98|,\verb|0x1.4a5346p+97|)$ in single precision
yields NaN for the imaginary part instead of +Inf.

\paragraph{About NaN values.}
When intermediate overflows happens in a complex operation, the real or
imaginary part of the result might be NaN.
An example is when multiplying $(\verb|-0x1p+64|,\verb|0x1p+64|)$
by $(\verb|0x1p+64|,\verb|0x1p+64|)$ in single precision: the Intel compiler
and clang
yield (-Inf,NaN) instead of (-Inf,0).
Since this would hide more interesting results, we don't consider
library results containing NaN.
However, we hope that the compiler or
mathematical libraries will fix such issues, at least if the user
provides some compiler option.

\paragraph{About Inf values.}
Sometimes the library returns $\pm\Inf$ for the real or imaginary part,
whereas the exact value is a normal number.
One example is multiplying $(\verb|-0x1.f9a182p+6|,\verb|-0x1.fb7ea6p+5|)$
by $(\verb|-0x1.038p+121|,\verb|-0x1.01fe26p+120|)$ in single precision,
where the correctly rounded result is
$(\verb|0x1.80aebap+127|,\Inf)$, but the Intel compiler and clang yield
$+\Inf$ for the real part.
If the Intel compiler rounds to +Inf, it considers the real part would be
rounded to $2^{128}$ or larger with an extended exponent range;
we thus consider the Intel compiler result to be (at least) $2^{128}$, which
yields a huge error.
Since this would also hide more interesting results, we don't consider
library results when one of the real or imaginary parts is $\pm\Inf$
(resp.~a normal value),
and the corresponding part computed by MPC is a normal value (resp.~$\pm\Inf$).
(When both values are $\pm\Inf$, if they are of the same sign, we consider there
is no error, otherwise we consider the error is Inf.)
Like for NaN values, we hope that the compiler or
mathematical libraries will fix such issues, at least if the user
provides some compiler option.

\paragraph{About the power function.}
For the power function $x^y$, when the imaginary part of the exponent $y$ is large,
it requires an expensive argument reduction to get an accurate result, which does not
seem to be implemented in current libraries.
For example, consider in single precision $x=(\verb|0x1.6dfff4p+81|,\verb|0x1.2a01bp-90|)$
and $y = (\verb|-0x1.704858p-66|,\verb|0x1.386c1ep+72|)$, then the Intel math library
yields $(\verb|0x1.b8d186p-1|,\verb|0x1.046dcp-1|)$ instead of
$(\verb|-0x1.b8cf4ep-1|,\verb|-0x1.04718p-1|)$.
A similar issue exists for the GNU libc.
As a consequence, for the power function we limit the real and imaginary parts
of the exponent to $2^4$, for both single and double precision.

\paragraph{Tested mathematical functions.}
We test the mathematical functions from C99 against the implementation in GNU MPC \cite{MPC131},
which assumes there is an implementation both in the C library and
in GNU MPC.
For \verb|cbrt|, \verb|exp10|, \verb|exp2|, \verb|expm1|, \verb|log1p|,
\verb|log2|, \verb|atan2| and \verb|hypot|, there is no implementation in the latest
release MPC 1.3.1.
% glibc has no ccbrt, cexp10, cexp2, cexpm1, clog1p, clog2, catan2, chypot
% https://www.gnu.org/software/libc/manual/html_node/Errors-in-Math-Functions.html
GNU libc 2.40 does not provide \verb|exp10| nor \verb|exp2|.
The results with \verb|exp10|, \verb|exp2|,
\verb|tan| and \verb|tanh| were obtained with the development
version of MPC (for \verb|tan| and \verb|tanh|,
the MPC 1.3.1 implementation is too slow when the imaginary part is huge).

\clearpage
\section{Single Precision} \label{single}

The following table shows the normwise error, as described by Eq.~(\ref{eq1}),
for the complex bivariate operations in single precision.
Smaller values are better, and the best entries in each row are set in bold
type.
Note that ``gcc'' might mean either GCC or the GNU libc, depending on whether
the operation is computed by the compiler or the library.
The same applies to clang, which might call the GNU libc.
\begin{center}
\input{complex2_FLOAT_table.tex}
\vspace{\belowdisplayskip}
\end{center}

The large errors for \verb|cpowf| (and \verb|cpow|) for cuda and hip are for
the input arguments $(\verb|-4.0|,\verb|-0.0|)$, $(\verb|0.5|,\verb|0.0|)$
where the correct result is $(\verb|0.0|,\verb|2.0|)$ while those libraries
produce $({\approx}\verb|0.0|,\verb|-2.0|)$.

The following table shows the normwise error results for the complex univariate
functions in single precision:
\begin{center}
\input{complex_FLOAT_table.tex}
\end{center}

We see that except for \verb|cmulf|, \verb|cexp10f|, and \verb|csqrtf|,
all entries for the Intel math library are 0.708, which is
the rounding up of $\sqrt{2}/2$, the best possible bound for the normwise
error (see \textsection\ref{theory}).

\clearpage
\section{Double Precision} \label{double}

The following table shows the normwise error results for the complex bivariate
operations:
\begin{center}
\input{complex2_DOUBLE_table.tex}
\vspace{\belowdisplayskip}
\end{center}

The following table shows the normwise error results for the double precision
complex univariate functions:
\begin{center}
\input{complex_DOUBLE_table.tex}
\end{center}

% Large error for cexp10 and csqrt with icx confirmed by Intel.
The Intel library's ``Infinity'' error for \verb|csqrt| is obtained for
$(\verb|-0x1.004p+1020|,\verb|0x1.03f8p+961|)$ where
the Intel library yields NaN for the real part,
while the large error for \verb|cexp10| is obtained for the input point
$(\verb|-0x1.8dd58ed1692afp-74|,\verb|0x1.93992e5e34b55p+29|)$.

\section{Conclusion}

Some libraries publish largest known errors in complex mathematical
functions, for example the GNU libc \cite{glibcknownerrors}.
In that document we see the largest known error for \verb|casinf| on
\verb|x86_64| is $1 + 2i$, which we interpret as a maximal error of
1 ulp on the real part, and 2 ulps on the imaginary part.
However, we found an error of 5.34 ulps in section \ref{single} using
normwise error Eq.~(\ref{eq1}).
Investigating further, for the input point
$(\verb|0x1.332904p-9|,\verb|0x1.01167ep-1|)$ GNU libc yields
$(\verb|0x1.127fbp-9|,\verb|0x1.eeb51p-2|)$, with an error of
1.98 ulps on the real part and 5.34 ulps on the imaginary part,
which clearly shows the values from \cite{glibcknownerrors} are
underestimated.

\bibliographystyle{ACM-Reference-Format}
\bibliography{biblio}

\end{document}

\begin{comment}
Submit to ARITH ?
Vincenzo:         
to submit it to Arith (or other conference/journal) I think the paper should be restructured focussing on motivations, challanges and methodology and eventually moving results to an appendix.
It may be worth the effort, just to leave a "long-standing" trace of our work

Date: Tue, 13 Aug 2024 15:02:54 -0400
To: stds-754                             <stds-754@listserv.ieee.org>
From: Michel Hack <00002fdf62126191-dmarc-request@listserv.ieee.org>
Subject: Just curious -- lack of Complex arithmetic standardization in 754

I was wondering why there appear to have been so few discussions of
standardization requirements for Complex arithmetic primitives in this
standard -- especially for multiplication and division: avoidance of
intermediate overflow, rounding correctness, exception specification
and so on. Primitive operations could take advantage of operand re-use,
which would be especially important in interval arithmetic, for example.

Is it perhaps because compilers are so much better at scheduling the
individual scalar operations that forcing them into a single operation
could limit pipelining options? Or are numerical-computing requirements
too variable to come to agreement on a standard specification?

There is of course also the issue of representation: polar vs cartesian.
(Are there others?)

We have had 40 years to deal with this!

Michel.

P.S. I have found an interesting reference that addresses the issues:
  https://math.stackexchange.com/questions/4172817/complex-floating-point-types/4172954#4172954

Date: Tue, 13 Aug 2024 13:49:44 -0700
Subject: Re: [STDS-754 - 1936] Just curious -- lack of Complex arithmetic standardization in 754
To: Mike Cowlishaw <mfc@speleotrove.com>, "'Michel Hack'" <mhack@ACM.ORG>,
        STDS-754@LISTSERV.IEEE.ORG
Cc: James W DEMMEL <demmel@berkeley.edu>
From: James Demmel <000029deb1c2fa85-dmarc-request@listserv.ieee.org>

I will also mention this during my presentation at the next 754 meeting, 
but the C99 and C11
standards define a complex number to be "infinite" if either component 
is infinite, even if
one component is a NaN, and define multiplication so that 
infinite*(finite-nonzero or infinite)
is infinite, even if 754 rules would yield NaN +i*NaN. For example, 
straightforward complex
multiplication yields (Inf + i*0)*(Inf + i*Inf) = NaN + i*NaN, but a 
C-standard conforming
compiler yields Inf + i*Inf. The C standard includes a 30+ line 
procedure for
complex multiplication (see Annex G.5.1). There are similar rules for 
complex
division in the 2020 draft C standard, but no code. In contrast, the 
2008 and 2018
Fortran standards say nothing about handling exceptions in complex 
arithmetic.

Feel free to read section 2 of
    https://arxiv.org/abs/2207.09281
for more details before my talk, including more about C++.

Jim

Date: Tue, 27 Aug 2024 11:59:50 +0000 (UTC)
From: Joseph Myers <josmyers@redhat.com>
To: Paul Zimmermann <Paul.Zimmermann@inria.fr>
cc: paul@hpkfft.com
Subject: Re: accuracy of complex operations/functions

On Mon, 26 Aug 2024, Paul Zimmermann wrote:

> Would you be interested in working together on such a comparison?

I am interested in seeing the results.  I don't expect to work on it 
myself.

> * addition, multiplication, division, square root
>   (Joseph, maybe they only depend on the compiler ?)

Addition / subtraction / multiplication / division depend only on the 
compiler.  csqrt is a library function.

Addition and subtraction should be correctly rounded.  Two-argument 
operations (multiplication, division, cpow) are a lot harder to get good 
results for in all cases than one-argument functions; there are known 
large error cases for cpow in glibc (getting good results from cpowl 
(LDBL_MAX, I * LDBL_MAX) would need very high internal precision).

-- 
Joseph S. Myers
josmyers@redhat.com

From: "Anderson, Cristina S" <cristina.s.anderson@intel.com>
To: Paul Zimmermann <Paul.Zimmermann@inria.fr>, "Cornea, Marius"
	<marius.cornea@intel.com>
Subject: RE: accuracy of complex functions
Thread-Topic: accuracy of complex functions
Thread-Index: AQHa+HoPYojdHcqNLUK9HDCrjU8LSbI9wK0w
Date: Thu, 29 Aug 2024 06:07:19 +0000

                 Hello Paul,
         It appears that the icx compiler is now inlining "reasonably fast" sequences for complex multiply and division; library calls are no longer generated for these functions.

        This was a relatively recent decision.  They may reconsider with additional user feedback.

               Thank you very much,
               Cristina

From: Peter C B Henderson <petercbh91@gmail.com>
Date: Sat, 31 Aug 2024 16:39:01 +1000
Subject: Re: LAPACK and 0*NaN
To: STDS-754@LISTSERV.IEEE.ORG


[1:text/plain Show]


[2:text/html Hide Save:noname (4kB)]

Please find below an interesting reference, especially for the I hope
upcoming discussion on complex arithmetic, relevant to my comments on Jim
Demmel's presentation 

Peter C B Henderson

On 8/29/24 22:18, Peter C B Henderson wrote:

 On 8/27/24 00:50, James Demmel wrote:

 I attach updated slides for my talk this morning. 

 Jim Demmel 

 Thanks to Jim for his presentation.? There's a lot to consider there.

 We need to hear from the end-user community.? Also from the hardware
 community and especially the language standards community.

 A few comments and observations.

.

.

.

 0*NaN

.

.

.

 Crucial to whether NaNs should be propagated is to how the 0 was
 generated.

 First, I assume for "C=0*A*B+0*C", you were referring to the the idiom
 "C=alpha*A*B + beta*C".? In this case, if the zeros are explicitly
 used by to act as switches, then it is preferable not to propagate
 NaNs.? The reason being the NaNs were most likely the result of
 underflows or overflows and so, if a floating-point result had been
 produced instead, it too would have been ignored.? I.e. the NaNs can
 be ignored as they are irrelevant to the result for algorithmic reasons.

These issues are discussed in:

INTERNATIONAL STANDARD ISO/IEC 10967-3
First edition,
2006-05-01
Information technology Language independent arithmetic Part 3:
Complex integer and floating point arithmetic and complex elementary
numerical functions,

which can be accessed from
https://standards.iso.org/ittf/PubliclyAvailableStandards/.

In Clause B.5.2.4 Basic arithmetic for complex floating point of this
document the implicit zero of purely imaginary quantities is discussed,
referring to them as " a very strong zero, here written 00". It then
continues as follows:

Note that 00 is not represented in the datatype corresponding to F , nor in
i(F ) or c(F ), not even as a component.? 00 is strong in the sense that
00 overtrumps NaN and infinity values, which neither 0 nor 0 does. I.e. the
implicit zeroes (00)
work as if the rules of the following table applies (plus commutativity):
... mulF (00, +) = 00
... mulF (00, ) = 00
... mulF (00, qNaN) = 00
... mulF (00, sNaN) = 00

I've omitted the rules for +/-0 that also appear in this table.? They are
just the IEEE-754 rules.
\end{comment}

