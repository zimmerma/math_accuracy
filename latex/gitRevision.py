#!/usr/bin/env python3

# This script writes the file git_revision.tex if it is not already correct.

import re
import subprocess

def main():
    commandlist = ["git", "describe", "--tags", "--dirty", "--always"]
    result = subprocess.run(commandlist, check=True, text=True,
                            stdout=subprocess.PIPE)
    gitversion = result.stdout.strip()
    if "dirty" in gitversion:
        print("WARNING: Your git working tree has uncommitted changes.")
    try:
        with open("git_revision.tex", "r") as f:
            existing = f.read()
    except:
        existing = ""
    if m := re.fullmatch(r"\\verb.(.+).", existing):
        if m.group(1) == gitversion:
            return
    print("Updating git_revision.tex to revision:", gitversion)
    with open("git_revision.tex", "w") as f:
        print(r"\verb|", gitversion, "|", sep='', end='', file=f)

if __name__ == '__main__':
    main()

