#include <math.h>

#define STR tgamma
#define MPFR_FOO mpfr_gamma

#ifdef NEWLIB
#define EXCLUDE
int
exclude (float x)
{
#if 0
  /* newlib tgamma(-0) gives +inf instead of -inf */
  if (x == 0.0f && signbit (x) != 0)
    return 1;
  /* same for -2^-128 <= x < 0 with Newlib 3.3.0 */
  if (-0x1p-128f <= x && x < 0.0f)
    return 1;
#endif
  return 0;
}
#endif

#include "check_exhaustive.c"
