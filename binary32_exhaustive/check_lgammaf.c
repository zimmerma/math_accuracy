#ifndef NEWLIB
#define STR lgamma
#else
#define STR mylgamma
#endif
#define MPFR_FOO mpfr_mylgamma

#include <math.h>
#include <mpfr.h>

#ifdef NEWLIB
/* cf https://sourceware.org/pipermail/newlib/2020/018027.html */
float
mylgammaf (float x)
{
  int s;
  /* lgammaf_r avoids the use of _impure_ptr in newlib */
  return lgammaf_r (x, &s);
}
double
mylgamma (double x)
{
  int s;
  return lgamma_r (x, &s);
}
#endif

int
mpfr_mylgamma (mpfr_t y, mpfr_t x, mpfr_rnd_t r)
{
  int s;
  return mpfr_lgamma (y, &s, x, r);
}

#include "check_exhaustive.c"
